# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the GNU bison package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: GNU bison 3.5.2\n"
"Report-Msgid-Bugs-To: bug-bison@gnu.org\n"
"POT-Creation-Date: 2020-02-13 18:22+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/skeletons/glr.c:884 data/skeletons/yacc.c:669
msgid "syntax error: cannot back up"
msgstr ""

#: data/skeletons/glr.c:1793
msgid "syntax is ambiguous"
msgstr ""

#: data/skeletons/glr.c:2080 data/skeletons/glr.c:2165
#: data/skeletons/glr.c:2209 data/skeletons/glr.c:2447
#: data/skeletons/lalr1.cc:1386 data/skeletons/lalr1.cc:1407
#: data/skeletons/yacc.c:1250 data/skeletons/yacc.c:1762
#: data/skeletons/yacc.c:1768
msgid "syntax error"
msgstr ""

#: data/skeletons/glr.c:2166 data/skeletons/lalr1.cc:1387
#: data/skeletons/yacc.c:1251
#, c-format
msgid "syntax error, unexpected %s"
msgstr ""

#: data/skeletons/glr.c:2167 data/skeletons/lalr1.cc:1388
#: data/skeletons/yacc.c:1252
#, c-format
msgid "syntax error, unexpected %s, expecting %s"
msgstr ""

#: data/skeletons/glr.c:2168 data/skeletons/lalr1.cc:1389
#: data/skeletons/yacc.c:1253
#, c-format
msgid "syntax error, unexpected %s, expecting %s or %s"
msgstr ""

#: data/skeletons/glr.c:2169 data/skeletons/lalr1.cc:1390
#: data/skeletons/yacc.c:1254
#, c-format
msgid "syntax error, unexpected %s, expecting %s or %s or %s"
msgstr ""

#: data/skeletons/glr.c:2170 data/skeletons/lalr1.cc:1391
#: data/skeletons/yacc.c:1255
#, c-format
msgid "syntax error, unexpected %s, expecting %s or %s or %s or %s"
msgstr ""

#: data/skeletons/glr.c:2506 data/skeletons/yacc.c:1335
#: data/skeletons/yacc.c:1337 data/skeletons/yacc.c:1526
#: data/skeletons/yacc.c:1918
msgid "memory exhausted"
msgstr ""
