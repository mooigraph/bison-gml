
#ifndef lint
static char rcsid[]="$Header: machine.c,v 1.2 85/02/14 15:49:22 daveb Exp $";
#endif

# include	<stdio.h>
# include	<fcntl.h>

/*
**	machine.c
**
**	What machine are we on?
**
**	Usage:	machine [-v]
**
**	by default prints one of:
**
**		amdahl		Amdahl UTS
**		u370		IBM 370
**		u3b		ATT 3b/20
**		u3b2		3b/2
**		u3b5		3b/5
**		mc68k		some 68000's
**		mc68000		other 68000's
**		vax		DEC VAX
**		pdp11		pdp 11
**		pyr		Pyramid 90x
**		sun		Sun workstation.
**		tahoe		CCI Power 6
**		sel		Gould/SEL
**
**	-v 'verbose' output is for managers.
**
**	If nothing is printed, we don't know what we're on.
*/

static int	vflag;
static char	* myname;
static char	* indent = "";

main(argc, argv)
	int	argc;
	char	**argv;
{
	int	strcmp();
	int	i = 0;

	myname = argv[0];
	while(--argc)
	{
		if(!strcmp(*++argv, "-v"))
			vflag = 1;
		else
		{
			fprintf(stderr, "bad argument `%s'\n", *argv);
			fprintf(stderr, "Usage:  %s [-v]\n", myname);
			exit(1);
		}
	}

	if(vflag)
	{
		indent = "\t";
		printf("%s:\n", myname);
	}

#ifdef amdahl
	printf("%samdahl\n", indent);	/* could fold to u370 */
	if(vflag)
		printf("%sAmdahl 370 clone\n", indent);
	exit(0);
#endif
#ifdef u370
	printf("%su370\n", indent);
	if(vflag)
		printf("%sIBM 370 or clone\n", indent);
	exit(0);
#endif
#ifdef u3b
	printf("%su3b\n", indent);
	if(vflag)
		printf("%sATTIS 3b20\n", indent);
	exit(0);
#endif
#ifdef u3b2
	printf("%su3b2\n", indent);
	if(vflag)
		printf("%sATTIS 3b2\n", indent);
	exit(0);
#endif
#ifdef u3b5
	printf("%su3b5\n", indent);
	if(vflag)
		printf("%sATTIS 3b5\n", indent);
	exit(0);
#endif
#ifdef sun
	/* MUST BE BEFORE 68000 checks! */
	printf("%ssun\n", indent);
	if(vflag)
		printf("%sSUN Microsystems Workstation\n", indent);
	exit(0);
#endif
#ifdef mc68k
	printf("%smc68k\n", indent);	/* could fold to mc68000 */
	if(vflag)
		printf("%sSome (System V?) 68000\n", indent);
	exit(0);
#endif
#ifdef mc68000
	printf("%smc68000\n", indent);
	if(vflag)
		printf("%sSome (BSD?) 68000\n", indent);
	exit(0);
#endif
#ifdef vax
	printf("%svax\n", indent);
	if(vflag)
		printf("%sDEC VAX\n", indent);
	exit(0);
#endif
#ifdef pdp11
	printf("%spdp11\n", indent);
	if(vflag)
		printf("%sDEC PDP-11\n", indent);
	exit(0);
#endif
#ifdef pyr
	printf("%spyr\n", indent);
	if(vflag)
		printf("%sPyramid 90x\n", indent);
	exit(0);
#endif
#ifdef tahoe
	printf("%stahoe\n", indent);	/* CCI power 6 */
	if(vflag)
		printf("%sCCI Power 6\n", indent);
	exit(0);
#endif
#ifdef sel
	printf("%ssel\n", indent);	/* Gould/SEL */
	if(vflag)
		printf("%sGould/SEL\n", indent);
	exit(0);
#endif
}
