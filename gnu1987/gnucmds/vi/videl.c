/* Copyright (C) 1987 Free Software Foundation, Inc.

   This will be free software eventually,
   but not until it is finished.  */



/*
 * videl.c - pre-gnu-vi  rev. 0.3
 *
 * vi delete functions
 */




#include "vi.h"


int lastl1, lastl2, lastc1, lastc2;



       
delinline (linenum, first, last)

int linenum, first, last;

{
   auto int k;
   auto char *newline, *yline;
   if ( !(newline = malloc (strlen (texts [linenum]) + 5 + last - first) ) )
      nomem (13);
   for (k = 0; k < first; ++k)
      newline [k] = texts [linenum] [k];
   newline [k] = '\0';
   if (texts [linenum] [last + 1])
      strcat (newline, &texts [linenum] [last + 1]);
   yline = malloc (last - first + 9);
   for (k = first; k <= last; ++k)
      yline [k - first] = texts [linenum] [k];
   yline [k - first] = '\0';
   if (free (texts [linenum]) )
      nofree (9);
   k = 0;
   if (free (dbuf) )
      nofree (21);
   dbuf = yline;
   texts [linenum] = newline;
   dbufstat = CHAROP;
}


delinterline (oldl, oldc, newl, newc)

int oldl, oldc, newl, newc;

{
   auto char *newline, *tmpline, tmps [48], *ndbuf;
   auto int k, ndel, first, last;
   /* Free the delete buffer */
   /*k = 0;
   while (dbuf [k] != NULL) {
      if (free (dbuf [k]) )
         nofree (10);
      ++k;
   }*/
   if (free (dbuf) )
      nofree (21);
   /*
    * Newline is the head of the first line, with the
    * tail of the last line.
    */
   if ( !(newline = malloc (strlen (texts [oldl]) +
            strlen (texts [newl] + 5) ) ) )
      nomem (12);
   for (k = 0; k < oldc; ++k)
      newline [k] = texts [oldl][k];
   newline [k] = '\0';
   strcat (newline, &texts [newl] [newc]);
   /*
    * Tmpline is the head of the last line (saved
    * to put into dbuf later.
    */
   tmpline = malloc (newc + 4);
   for (k = 1; k < newc; ++k)
      tmpline [k - 1] = texts [newl] [k];
   tmpline [k - 1] = '\0';
   first = oldl + 1;
   last = newl + 1;
   ndel = last - first;
   /* Adjust all of the marks which may be affected */
   for (k = 0; k < 128; ++k) {
      if ( (first <= marks [k]) && (marks [k] < last) )
         marks [k] = 0;
      else if (marks [k] >= last)
         marks [k] = marks [k] - (ndel);
   }
   /* Build a new delete buffer */
   /*dbuf = (char **) malloc ( (ndel + 3) * sizeof (char *) );*/
   /*
    * Save the tail of the first line in the delete buffer.
    * Replace the first line with the head of the old
    * first line, and the tail of the last line
    */
   /*dbuf [0] = malloc (strlen (texts [oldl]) - curc + 3);
   strcpy (dbuf [0], &texts [oldl] [oldc]);*/
   dbuf = malloc (strlen (texts [oldl]) - curc + 3);
   strcpy (dbuf, &texts [oldl] [oldc]);
   if (free (texts [oldl]) )
      nofree (8);
   texts [oldl] = newline;
   k = 1;
   do {
      if (ndel > 0) {
         if (first != newl) {
            ndbuf = malloc (strlen (dbuf [0]) + strlen (texts [first]) + 4);
            if (!ndbuf)
               nomem (50);
            strcpy (ndbuf, dbuf);
            strcat (ndbuf, &texts [first] [1]);
            if (free (dbuf) )
               nofree (51);
            dbuf = ndbuf;
         }
         if (free (texts [first]) )
            nofree (50);
         --ndel;
      }
      texts [first] = texts [last];
      ++first;
      ++last;
   } while (last <= endl);
   /* Save the old head of the last line */
   ndbuf = malloc (strlen (dbuf) + strlen (tmpline) + 4);
   if (!ndbuf)
      nomem (51);
   strcpy (ndbuf, dbuf);
   strcat (ndbuf, tmpline);
   if (free (dbuf) )
      nofree (52);
   if (free (tmpline) )
      nofree (53);
   dbuf = ndbuf;
   endl = first - 1;
   if ( (newl - oldl) > showchange) {
      movtobot ();
      cleareol ();
      sprintf (tmps, "%d lines deleted", (newl - oldl) );
      writes (tmps);
      refscr ();
   }
   dbufstat = CHAROP;
}

   
dellines (first, last)

int first, last;

{
   auto int ndel, k;
   auto char *newdbuf;
   if (free (dbuf) )
      nofree (21);
   ++last;
   ndel = last - first;
   for (k = 0; k < 128; ++k) {
      if ( (first <= marks [k]) && (marks [k] < last) )
         marks [k] = 0;
      else if (marks [k] >= last)
         marks [k] = marks [k] - (ndel);
   }
   dbuf = malloc (1);
   if (!dbuf)
      nomem (501);
   strcpy (dbuf, "");
   k = 0;
   do {
      if (ndel > 0) {
         newdbuf = malloc (strlen (dbuf) + strlen (texts [first]) + 4);
         if (!newdbuf)
            nomem (505);
         strcpy (newdbuf, dbuf);
         strcat (newdbuf, &texts [first] [1]);
         if (free (dbuf) )
            nofree (502);
         dbuf = newdbuf;
         ++k;
         --ndel;
      }
      texts [first] = texts [last];
      ++first;
      ++last;
   } while (last <= endl);
   endl = first - 1;
   dbufstat = LINEOP;
}

int dodelete (count)

int count;

{
   auto char cmd, c, tmps [80];
   auto int oldl, oldc, newl, newc, tmp;
   lastuncmd = lastcmd = 'd';
   oldl = curl;
   oldc = curc;
   cmd = readch ();
   c = readch ();
   if ( (c == cmd) || (c == '\'') ) {
      /* dd or d'n , delete whole lines */
      if (c == cmd) {
	 if ( (curl + count - 1) >= endl) {
	    curl = oldl;
	    curc = oldc;
	    return (-1);
  	 }
	 newl = curl + count - 1;
      } else {
         unreadch (c);
         if (domove (&newl, &newc) ) {
            curl = oldl;
            curc = oldc;
            return (-1);
         }
         if (newl > curl) {
            oldl = curl;
         } else {
            oldl = newl;
            newl = curl;
         }
         count = newl - oldl + 1;
      }
      if (cmd == 'd') {
	 dellines (oldl, newl);
         if (count > showchange) {
            movtobot ();
            cleareol ();
            sprintf (tmps, "%d lines deleted", count);
            writes (tmps);
            refscr ();
         }
         curc = 1;
      } else {
         yanklines (oldl, newl);
         if (count > showchange) {
            movtobot ();
            cleareol ();
            sprintf (tmps, "%d lines yanked", count);
            writes (tmps);
            refscr ();
         }
      }
      laststring [0] = cmd;
      lastcount = count;
      lastdl = curl;
      lastdc = 1;
      return (0);
   } else {
	 if (texts [curl] [curc] == '\n') {
	    curl = oldl;
	    curc = oldc;
	    return (-1);
	 }
         unreadch (c);
	 if (docursor (count, &newl, &newc) ) {
	    curl = oldl;
	    curc = oldc;
	    return (-1);
	 }
         if ( (oldl > newl) || ( (oldl == newl) && (oldc > newc) ) ) {
             tmp = oldl;
             oldl = newl;
             newl = tmp;
             tmp = oldc;
             oldc = newc;
             newc = tmp;
             curl = oldl;
             curc = oldc;
         }
	 if (oldl == newl) {
            if ( (c != '$') && (c != 'F') && (c != 'f') && (c != 't') && 
                 (c != 'T') && (c != ';') && (c != ',') && (newl == oldl) )
	       --newc;
            if ( ( (c == ' ') || (c == 'l') ) && 
                ( (newc + 1 - oldc) < count) )
	    newc = strlen (texts [oldl]) - 2;
	    delinline (oldl, oldc, newc);
	 } else {
            delinterline (oldl, oldc, newl, newc);
            curc = oldc;
            curl = oldl;
	 }   
         if ( (curc != 1) && (texts [curl] [curc] == '\n') )
            --curc;
   }
   lastcount = count;
   if ( (c == 'F') || (c == 'f') || (c == 'T') || (c == 't') )
      laststring [0] = ';';
   else
      laststring [0] = c;
   if ( (curl >= endl) && (endl != 1) )
      curl = endl - 1;
   lastdl = curl;
   lastdc = curc;
   modflag = 1;
   return (0);
}

uninsert ()

{
   
   delinterline (lastl1, lastc1, lastl2, lastc2);
}

yanklines (first, last)

int first, last;

{
   auto int ndel, k;
   auto char *newdbuf;
   k = 0;
   /*while (dbuf [k] != NULL) {
      if (free (dbuf [k]) )
         nofree (10);
      ++k;
   }*/
   if (free (dbuf) )
      nofree (21);
   ++last;
   ndel = last - first;
   /*dbuf = (char **) malloc ( (ndel + 3) * sizeof (char *) );*/
   dbuf = malloc (1);
   if (!dbuf)
      nomem (510);
   strcpy (dbuf, "");
   k = 0;
   while (first < last) {
      newdbuf = malloc (strlen (dbuf) + strlen (texts [first]) + 3);
      strcpy (newdbuf, dbuf);
      strcat (newdbuf, &texts [first] [1]);
      if (free (dbuf) )
         nofree (511);
      dbuf = newdbuf;
      /*dbuf [k] = malloc (strlen (texts [first]) + 3);
      strcpy (dbuf [k], texts [first]);*/
      ++k;
      ++first;
   }
   /*dbuf [k] = NULL;*/
}



      
      
      
/* bottom */
