/* Copyright (C) 1987 Free Software Foundation, Inc.

   This will be free software eventually,
   but not until it is finished.  */



/*
 * viwin.c - pre-gnu-vi  rev. 0.3
 *
 * vi windowing functions
 */




#include "vi.h"


dowindow (line)

char *line;

{
   auto int h, n;
   for (n = 0; n < MAXWINS; ++n) {
      if (wins [n] == NULL)
         break;
   }
   if (n >= MAXWINS)
      return (-1);
   wins [n] = (struct winbuf *) malloc (sizeof (struct winbuf) );
   if (wins [n] == NULL) {
      nomem (701);
      return (-1);
   }
   delwin (curwin->mywin);
   h = curwin->height / 2;
   if ( (2 * h) < curwin->height)
      curwin->height = h + 1;
   else
      curwin->height = h;
   wins [n]->begx = 0;
   wins [n]->begy = curwin->begy + curwin->height;
   wins [n]->width = curwin->width;
   wins [n]->height = h;
   wins [n]->mytexts = (char **) malloc (512 * sizeof (char *) );
   wins [n]->mywin = newwin (wins [n]->height, wins [n]->width, wins [n]->begy, wins [n]->begx);
   wins [n]->wcurl = 1;
   wins [n]->wcurc = 1;
   wins [n]->wbscrl = 1;
   wins [n]->wendl = 1;
   wins [n]->mytexts [0] = malloc (4);
   wins [n]->mytexts [1] = malloc (4);
   strcpy (wins [n]->mytexts [0], "\n\n");
   strcpy (wins [n]->mytexts [1], "\n\n");
   strcpy (wins [n]->fname, "");
   wins [n]->wmod = 0;
   curwin->mywin = newwin (curwin->height, curwin->width, curwin->begy,
                               curwin->begx);
   swwindow (wins [n]);
   while (*line == ' ')
      ++line;
   newfile (line);
   updwins ();
}

updwins ()

{
   auto int n;
   struct winbuf *tmpwin;
   tmpwin = curwin;
   curwin->wcurl = curl;
   curwin->wcurc = curc;
   curwin->wbscrl = bscrl;
   curwin->wendl = endl;
   for (n = 0; n < MAXWINS; ++n) {
      if (wins [n] != NULL) {
         curwin = wins [n];
         texts = wins [n]->mytexts;
         curl = wins [n]->wcurl;
         curc = wins [n]->wcurc;
         bscrl = wins [n]->wbscrl;
         endl = wins [n]->wendl;
         unreadch (' ');
         doredraw ();
      }
   }
   curwin = tmpwin;
   texts = curwin->mytexts;
   curl = curwin->wcurl;
   curc = curwin->wcurc;
   bscrl = curwin->wbscrl;
   endl = curwin->wendl;
}

swwindow (new)

struct winbuf *new;

{
   curwin->wcurl = curl;
   curwin->wcurc = curc;
   curwin->wendl = endl;
   curwin->wbscrl = bscrl;
   strcpy (curwin->fname, curfname);
   curwin->wmod = modflag;
   texts = new->mytexts;
   curl = new->wcurl;
   curc = new->wcurc;
   endl = new->wendl;
   bscrl = new->wbscrl;
   strcpy (curfname, new->fname);
   modflag = new->wmod;
   curwin = new;
}
   
dowinn (winnum)

int winnum;

{
   if (wins [winnum] == NULL) 
      return (-1);
   swwindow (wins [winnum]);
   updwins ();
   return (0);
}

dowin2 ()

{
   swwindow (win2);
   updwins ();
}


