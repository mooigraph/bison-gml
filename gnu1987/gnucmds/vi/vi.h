/* Copyright (C) 1987 Free Software Foundation, Inc.

   This will be free software eventually,
   but not until it is finished.  */


/*
 * vi.h - pre-gnu-vi  rev. 0.3
 *
 * vi heading
 */

#include <curses.h>

struct winbuf {
   WINDOW *mywin;
   char **mytexts;
   int begy, begx, width, height, wcurl, wcurc, wbscrl, wendl, wmod;
   char fname [64];
};


#define AESC (27)

#define MAXWINS (10)

#define SCRHEIGHT (25)
#define SCRWIDTH 79

#define MAXSCRHEIGHT (26)
#define MAXSCRWIDTH (100)

#define STLFLBRACE "\n{"
#define ALBRACE '{'

#define LINEOP (0)
#define CHAROP (1)


#define skipblanks(line,l) while((line[l]==' ') || (line[l]=='\t')) ++l
#define isdigit(c) (('0'<=c) && (c<='9'))
#define tolower(c) ((('A'<=c) && (c<='Z'))?(c+'a'-'A'):c)


extern char *malloc ();


extern char **texts;

extern char oldscreen [] [MAXSCRWIDTH];

extern char *dbuf;

extern int dbufstat, lastdl, lastdc;

extern int lastl1, lastl2, lastc1, lastc2;

extern char *ibuf;

extern char lasts2, lastsearch [];

extern char lastfcmd, lastfchar;

extern char curfname [];

extern int marks [];

extern int incol;

extern char lastcmd, lastcount, laststring [];

extern char lastuncmd;

extern int curl, curc;

extern int endl;

extern int bscrl, escrl;

extern int mode;

extern int gj, gk;

extern int autoindent, ignorecase, magic, shiftwidth, wrapscan, showchange,
              tabstop;
extern int modflag;
extern int seekeol;

extern struct winbuf *mainwin, *curwin, *win2, *wins [MAXWINS];

