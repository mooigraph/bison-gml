/* `wc' word count utility.
   Copyright 1985 Copyright by Paul Rubin and Richard Stallman

   Permission is granted to anyone to make or distribute
   verbatim copies of this program
   provided that the copyright notice and this permission notice are preserved;
   and provided that the recipient is not asked to waive or limit his right to
   redistribute copies as permitted by this permission notice;
   and provided that anyone possessing a machine-executable copy
   is granted access to copy the source code, in machine-readable form,
   in some reasonable manner.

   Permission is granted to distribute derived works or enhanced versions of
   this program under the above conditions with the additional condition
   that the entire derivative or enhanced work
   must be covered by a permission notice identical to this one.

   Anything distributed as part of a package containing portions derived
   from this program, which cannot in current practice perform its function
   usefully in the absence of what was derived directly from this program,
   is to be considered as forming, together with the latter,
   a single work derived from this program,
   which must be entirely covered by a permission notice identical to this one
   in order for distribution of the package to be permitted.

 In other words, you are welcome to use, share and improve this program.
 You are forbidden to forbid anyone else to use, share and improve
 what you give them.   Help stamp out software-hoarding!  */

#define NULL 0
#define BUFSIZE 16384

char *flags = "lwc";

long lines, words, chars;
long tlines = 0, twords = 0, tchars = 0;

main(argc, argv)
     int argc;
     char **argv;
{
  int error = 0;
  int i, fd;
  
  if (argc > 1 && argv[1][0] == '-') {
    flags = &argv[1][1];
    for (i = 0; flags[i] != '\0'; i++) {
      if (index("lwc", flags[i]) == NULL) {
	printf("Usage: wc [-lwc] [files]\n");
	exit(1);
      }
    }
    argc--; argv++;
  }
  
  if (argc > 1) {
    for (i = 1; i < argc; i++) {
      if ((fd = open(argv[i], 0)) < 0) {
	perror(argv[i]);
	error++;
      } else {
	wc(fd, argv[i]);
	close(fd);
	tlines += lines;
	twords += words;
	tchars += chars;
      }
    }
    if (argc > 2)
      output(tlines, twords, tchars, "total");
  } else
    wc(0, "");
  
  exit (error);
}

wc(fd, fname)
     int fd;
     char *fname;
{
  char buf[BUFSIZE];
  register int n;
  register int state = 0;
  
  lines = words = chars = 0;
  
  while ((n = read(fd, buf, BUFSIZE)) > 0) {
    register char *p = buf;
    do {
      chars++;
      switch (*p++) {
      case '\n':
	lines++;
	/* fall through */
      case ' ':
      case '\t':
	if (state == 0) {
	  state++;
	  words++;
	}
	break;
      default:
	state = 0;
	break;
      }
    } while (--n);
  }
  output(lines, words, chars, fname);
}

output(l, w, c, fname)
     int l, w, c;
     char *fname;
{
  if (index(flags, 'l') != NULL)
    printf("%8ld", l);
  if (index(flags, 'w') != NULL)
    printf("%8ld", w);
  if (index(flags, 'c') != NULL)
    printf("%8ld", c);
  printf(" %s\n", fname);
}
