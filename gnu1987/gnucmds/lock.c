/*
 * lock.c - lock a tty until owner returns
 *
 * cc lock.c -O -s -o lock
 *
 * Toby Harness		3/18/84
 */

#include <stdio.h>
#include <signal.h>
#include <pwd.h>

#define MASTER "root"

extern	char *crypt(), *getpass();
extern	struct passwd *getpwuid(), *getpwnam();

main(argc, argv)
int argc;
char **argv;
{
        register int i;
	char salt[3], MasterSalt[3], *in_string, pwpasswd[128],
		Mpwpasswd[128];
	struct passwd *pw;

	if((pw = getpwuid(getuid())) == NULL) {
		fprintf(stderr, "%s:  Who are you?\n", *argv);
		exit(1);
	}
	strcpy(pwpasswd,pw->pw_passwd);
	strncpy(salt, pw->pw_passwd, 2);

	if((pw = getpwnam(MASTER)) == NULL) {
		fprintf(stderr, "%s:  can`t find passwd entry for %s\n",
			*argv, MASTER);
		exit(1);
	}
	strcpy(Mpwpasswd,pw->pw_passwd);
	strncpy(MasterSalt, pw->pw_passwd, 2);

	while(i < 19)
                signal(i++, SIG_IGN);

        in_string = getpass("\n\n\t\t\t\tTerminal LOCKED\n\nPassword: ");

        for (;;)
	  {
	   if(*in_string &&
		(!strcmp(pwpasswd, crypt(in_string, salt)) ||
		  !strcmp(Mpwpasswd, crypt(in_string, MasterSalt))))
                        break;
 	   in_string = getpass("Password:  ");
	  }
}
