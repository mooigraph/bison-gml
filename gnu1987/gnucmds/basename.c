/* -*- Mode:C -*- basename: strips directory and suffix off filenames
   mly 850320

   Copyright (C) 1985 Richard M. Stallman

   Permission is granted to anyone to make or distribute
   verbatim copies of this program
   provided that the copyright notice and permission notice are preserved;
   and provided that the recipient is not asked to waive or limit his right
   to redistribute copies as permitted by this permission notice;
   and provided that anyone possessing a machine-executable copy
   is granted access to copy the source code, in machine-readable form,
   in some reasonable manner.  Distribution of executable code without
   the source code must be accompanied prominently by this permission notice
   and a statement informing the recipient of how to obtain the source code.

   Permission is granted to distribute derived works or modified versions
   of this program or portions of it under the above conditions, with
   the additional requirement that the entire derivative or modified work
   must be covered by a permission notice identical to this one.

   Anything distributed with and usable only in conjunction with something
   derived from this program, whose useful purpose is conceptually
   to extend or adapt or add capabilities to this program,
   is to be considered as part of a modified version of this program
   under the requirements above.

   This software is distributed in the hope that it will be useful,
   but there is no warranty of any sort, and no contributor accepts
   responsibility for the consequences of using this program
   or for whether it serves any particular purpose.

 In other words, you are welcome to use, share and improve this program.
 You are forbidden to forbid anyone else to use, share and improve
 what you give them.  Help stamp out software-hoarding!  */


/* first arg is filename, optional second is suffix to strip.
   basename /usr/foo/lossage/functions.l
   => functions.l
   basename /usr/foo/lossage/functions.l .l
   => functions
   basename functions.lisp p
   => functions.lis
*/

#include <stdio.h>

char *progname;

main (argc,argv)
     char **argv;
{
  register char *p, *name, *suffix;

  progname = argv[0];
  if (argc < 2)
    fatal ("no arguments given\n Expected a filename, and optionally a suffix to strip", argc);
  if (argc > 3)
    fatal ("too many arguments (%d)", argc - 1);

  name = p = argv[1];
  /* find last "/" in pathname */
  while (*p)
    if (*p++ == '/') name = p;

  if (argc == 2)
    suffix = 0;
  else { for (suffix = argv[2]; *suffix; suffix++);
	 while (p > name && suffix > argv[2])
	   if (*--suffix != *--p)
	     /* mismatch */
	     { suffix = 0;
	       goto done; };
	 suffix = p; }
 done:
  for (p = name; p != suffix && *p; p++)
    putc (*p, stdout);
  putc ('\n', stdout);
  exit(0);
}

/* print error message and die with exit code 1 */
fatal (string, arg)
     char *string, *arg;
{
  fprintf (stderr, "%s: ", progname);
  fprintf (stderr, string, arg);
  fprintf (stderr, "\n");
  exit (1);
}
