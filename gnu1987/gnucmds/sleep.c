/* Return after fixed period of delay.   Copyright 1984 (C) Richard Stallman

   Permission is granted to anyone to make or distribute
   verbatim copies of this program
   provided that the copyright notice and this permission notice are preserved;
   and provided that the recipient is not asked to waive or limit his right to
   redistribute copies as permitted by this permission notice;
   and provided that anyone possessing a machine-executable copy
   is granted access to copy the source code, in machine-readable form,
   in some reasonable manner.

   Permission is granted to distribute derived works or enhanced versions of
   this program under the above conditions with the additional condition
   that the entire derivative or enhanced work
   must be covered by a permission notice identical to this one.

   Anything distributed as part of a package containing portions derived
   from this program, which cannot in current practice perform its function
   usefully in the absence of what was derived directly from this program,
   is to be considered as forming, together with the latter,
   a single work derived from this program,
   which must be entirely covered by a permission notice identical to this one
   in order for distribution of the package to be permitted.

 In other words, you are welcome to use, share and improve this program.
 You are forbidden to forbid anyone else to use, share and improve
 what you give them.   Help stamp out software-hoarding!  */

long argdecode ();

main (argc, argv)
     int argc;
     char **argv;
{
  int i;
  long time = 0;

  for (i = 1; i < argc; i++)
    {
      time += argdecode (argv[i]);
    }

  sleep (time);
}

long
argdecode (s)
     char *s;
{

  long value;
  int radix = 10;
  char *p = s;
  int c;

  if (*p != '0')
    radix = 10;
  else if (*++p == 'x')
    {
      radix = 16;
      p++;
    }
  else
    radix = 8;

  value = 0;
  while (((c = *p++) >= '0' && c <= '9')
	 || (radix == 16 && (c & ~40) >= 'A' && (c & ~40) <= 'Z'))
    {
      value *= radix;
      if (c >= '0' && c <= '9')
	value += c - '0';
      else
	value += (c & ~40) - 'A';
    }
  if (c == 's')
    ;
  else if (c == 'm')
    value *= 60;
  else if (c == 'h')
    value *= 60 * 60;
  else if (c == 'd')
    value *= 60 * 60 * 24;
  else
    p--;

  if (*p)
    fatal ("invalid time interval %s", s);
  return value;
}

/* Print error message and exit.  */

fatal (s1, s2)
     char *s1, *s2;
{
  error (s1, s2);
  exit (1);
}

/* Print error message.  `s1' is printf control string, `s2' is arg for it. */

error (s1, s2)
     char *s1, *s2;
{
  printf ("sleep: ");
  printf (s1, s2);
  printf ("\n");
}

