/* `ln' program to create links among files.
   Copyright (C) 1986 Free Software Foundation, Inc.

		       NO WARRANTY

  BECAUSE THIS PROGRAM IS LICENSED FREE OF CHARGE, WE PROVIDE ABSOLUTELY
NO WARRANTY, TO THE EXTENT PERMITTED BY APPLICABLE STATE LAW.  EXCEPT
WHEN OTHERWISE STATED IN WRITING, FREE SOFTWARE FOUNDATION, INC,
RICHARD M. STALLMAN AND/OR OTHER PARTIES PROVIDE THIS PROGRAM "AS IS"
WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY
AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE PROGRAM PROVE
DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR
CORRECTION.

 IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW WILL RICHARD M.
STALLMAN, THE FREE SOFTWARE FOUNDATION, INC., AND/OR ANY OTHER PARTY
WHO MAY MODIFY AND REDISTRIBUTE THIS PROGRAM AS PERMITTED BELOW, BE
LIABLE TO YOU FOR DAMAGES, INCLUDING ANY LOST PROFITS, LOST MONIES, OR
OTHER SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE
USE OR INABILITY TO USE (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR
DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY THIRD PARTIES OR
A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS) THIS
PROGRAM, EVEN IF YOU HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH
DAMAGES, OR FOR ANY CLAIM BY ANY OTHER PARTY.

		GENERAL PUBLIC LICENSE TO COPY

  1. You may copy and distribute verbatim copies of this source file
as you receive it, in any medium, provided that you conspicuously
and appropriately publish on each copy a valid copyright notice
"Copyright (C) 1986 Free Software Foundation, Inc"; and include following the
copyright notice a verbatim copy of the above disclaimer of warranty
and of this License.

  2. You may modify your copy or copies of this source file or
any portion of it, and copy and distribute such modifications under
the terms of Paragraph 1 above, provided that you also do the following:

    a) cause the modified files to carry prominent notices stating
    that you changed the files and the date of any change; and

    b) cause the whole of any work that you distribute or publish,
    that in whole or in part contains or is a derivative of this
    program or any part thereof, to be freely distributed
    and licensed to all third parties on terms identical to those
    contained in this License Agreement (except that you may choose
    to grant more extensive warranty protection to third parties,
    at your option).

  3. You may copy and distribute this program or any portion of it in
compiled, executable or object code form under the terms of Paragraphs
1 and 2 above provided that you do the following:

    a) cause each such copy to be accompanied by the
    corresponding machine-readable source code, which must
    be distributed under the terms of Paragraphs 1 and 2 above; or,

    b) cause each such copy to be accompanied by a
    written offer, with no time limit, to give any third party
    free (except for a nominal shipping charge) a machine readable
    copy of the corresponding source code, to be distributed
    under the terms of Paragraphs 1 and 2 above; or,

    c) in the case of a recipient of this program in compiled, executable
    or object code form (without the corresponding source code) you
    shall cause copies you distribute to be accompanied by a copy
    of the written offer of source code which you received along
    with the copy you received.

  4. You may not copy, sublicense, distribute or transfer this program
except as expressly provided under this License Agreement.  Any attempt
otherwise to copy, sublicense, distribute or transfer this program is void and
your rights to use the program under this License agreement shall be
automatically terminated.  However, parties who have received computer
software programs from you with this License Agreement will not have
their licenses terminated so long as such parties remain in full compliance.

*/

/*
 * Ln.  Usage is either `ln file1 file2' or `ln file1 file2 ... destdir'.  In
 *  either case, if the -s option is inserted just after ln then ln makes
 *  soft links instead of hard links.  In the first form, file1 is the
 *  existing file and file2 is the name under which the link is to be
 *  created.  If file2 is omitted, the link is made in the current directory
 *  with the same name as the last component of file1.  In the second form,
 *  the fileNs are existing files and destdir is a directory in which to
 *  create the links.  The links all have the same names as the last
 *  component of the linked-to files.
 *
 * This shares one problem with 4.2bsd ln which it would be very nearly
 *  impossible to remove in the general case, this problem being that if you
 *  make soft links to multiple files where the target is a directory you can
 *  lose, for example:
 *
 *	% ln -s foo bar baz subdir
 *	% cat subdir/foo
 *	cat: subdir/foo: Too many levels of symbolic links
 *	% ls -l subdir
 *	total 3
 *	-rw-r--r--  1 mouse           3 Apr 10 17:46 foo -> foo
 *	-rw-r--r--  1 mouse           3 Apr 16 05:08 bar -> bar
 *	-rw-r--r--  1 mouse           3 Apr 19 03:34 baz -> baz
 */

/*
 * This program assumes the following:
 *
 *	stat () as in 4.2bsd, at least for determining whether or not a file
 *	  is a directory.
 */

/* Author Mike Parker */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>

int dosoft;	/* true -> make soft links, false -> make hard links */
char *pgm;	/* name by which we were invoked (for error messages) */

main (ac, av)
     int ac;
     char **av;
{
  pgm = *av++;
  ac --;
  if (dosoft = (strcmp (*av, "-s") == 0))
    {
      av ++;
      ac --;
    }
  if (ac < 1)
    { 
      fprintf (stderr, "Usage: %s [ -s ] old\n\
or:    %s [ -s ] old new\n\
or:    %s [ -s ] old1 ... oldN dir\n", pgm, pgm, pgm);
      exit (1);
    }
  if (ac == 1)
    dolink (*av, ".");

  else if (ac == 2)
    dolink (av[0], av[1]);
  else
    {
      char *where = av[--ac];
      if (! isdir (where))
	{
	  fprintf (stderr, "%s: target must be a directory for multiple files\n",
		   pgm);
	  exit (1);
	}
      for (; ac; ac--, av++)
	dolink (*av, where);
    }
}

/* `old' should exist; link named `new' is made.  Handles directory issues. */
dolink (old, new)
     char *old;
     char *new;
{
  char *malloc ();
  char *rindex ();
  int link ();
  int symlink ();

  if (isdir (new))	/* target is directory, build full filename */
    {
      char *cp;
      char *new_new;
      int l;
      cp = rindex (old, '/');
      if (cp)
	cp++;
      else
	cp = old;
      l = strlen (cp) + 1 + strlen (new) + 1;
      new_new = (char *) alloca (l);
      sprintf (new_new, "%s/%s", new, cp);
      new = new_new;
    }
  if ((*(dosoft ? symlink : link)) (old, new) < 0)
    {
      fprintf (stderr, "%s: cannot %slink %s to %s: ",
	       pgm, dosoft ? "soft " : "",
	       new, old);
      perror (0);
    }
}

/* check whether the file named is a directory */
int
isdir (fn)
     char *fn;
{
 struct stat stb;

 if (stat (fn, &stb) < 0)
   return (0);
 return ((stb.st_mode & S_IFMT) == S_IFDIR);
}
