/* whoami: prints userid on standard output (followed by newline)
   mly 850320
   Copyright (C) 1985 Richard M. Stallman
   ... [generic gnu copyright clause]
*/

#include <stdio.h>
#include <pwd.h>

main (argc, argv, envp)
     int argc;
     char **argv, envp;
{
  register struct passwd *p;
  register int uid;

  if (argc > 1)
    {
      fprintf (stderr, "%s: No arguments accepted (%d supplied)\n",
	       argv[0], argc - 1);
      exit (1);
    }

  uid = getuid ();
  p = getpwuid (uid);
  if (!p)
    {
      fprintf (stderr,"%s: getuid() error for uid %d.\n", 
	       argv[0], uid);
      exit (1);
    }
  printf ("%s\n", p->pw_name);
  exit (0);
}
