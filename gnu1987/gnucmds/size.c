/* Size of rel file utility (`size') for GNU.
   Copyright (C) 1986 Richard M. Stallman

		       NO WARRANTY

  BECAUSE THIS PROGRAM IS LICENSED FREE OF CHARGE, WE PROVIDE ABSOLUTELY
NO WARRANTY, TO THE EXTENT PERMITTED BY APPLICABLE STATE LAW.  EXCEPT
WHEN OTHERWISE STATED IN WRITING, FREE SOFTWARE FOUNDATION, INC,
RICHARD M. STALLMAN AND/OR OTHER PARTIES PROVIDE THIS PROGRAM "AS IS"
WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY
AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE PROGRAM PROVE
DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR
CORRECTION.

 IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW WILL RICHARD M.
STALLMAN, THE FREE SOFTWARE FOUNDATION, INC., AND/OR ANY OTHER PARTY
WHO MAY MODIFY AND REDISTRIBUTE THIS PROGRAM AS PERMITTED BELOW, BE
LIABLE TO YOU FOR DAMAGES, INCLUDING ANY LOST PROFITS, LOST MONIES, OR
OTHER SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE
USE OR INABILITY TO USE (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR
DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY THIRD PARTIES OR
A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS) THIS
PROGRAM, EVEN IF YOU HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH
DAMAGES, OR FOR ANY CLAIM BY ANY OTHER PARTY.

		GENERAL PUBLIC LICENSE TO COPY

  1. You may copy and distribute verbatim copies of this source file
as you receive it, in any medium, provided that you conspicuously
and appropriately publish on each copy a valid copyright notice
"Copyright (C) 1986 Richard M. Stallman"; and include following the
copyright notice a verbatim copy of the above disclaimer of warranty
and of this License.

  2. You may modify your copy or copies of this source file or
any portion of it, and copy and distribute such modifications under
the terms of Paragraph 1 above, provided that you also do the following:

    a) cause the modified files to carry prominent notices stating
    that you changed the files and the date of any change; and

    b) cause the whole of any work that you distribute or publish,
    that in whole or in part contains or is a derivative of this
    program or any part thereof, to be freely distributed
    and licensed to all third parties on terms identical to those
    contained in this License Agreement (except that you may choose
    to grant more extensive warranty protection to third parties,
    at your option).

  3. You may copy and distribute this program or any portion of it in
compiled, executable or object code form under the terms of Paragraphs
1 and 2 above provided that you do the following:

    a) cause each such copy to be accompanied by the
    corresponding machine-readable source code, which must
    be distributed under the terms of Paragraphs 1 and 2 above; or,

    b) cause each such copy to be accompanied by a
    written offer, with no time limit, to give any third party
    free (except for a nominal shipping charge) a machine readable
    copy of the corresponding source code, to be distributed
    under the terms of Paragraphs 1 and 2 above; or,

    c) in the case of a recipient of this program in compiled, executable
    or object code form (without the corresponding source code) you
    shall cause copies you distribute to be accompanied by a copy
    of the written offer of source code which you received along
    with the copy you received.

  4. You may not copy, sublicense, distribute or transfer this program
except as expressly provided under this License Agreement.  Any attempt
otherwise to copy, sublicense, distribute or transfer this program is void and
your rights to use the program under this License agreement shall be
automatically terminated.  However, parties who have received computer
software programs from you with this License Agreement will not have
their licenses terminated so long as such parties remain in full compliance.

 In other words, you are welcome to use, share and improve this program.
 You are forbidden to forbid anyone else to use, share and improve
 what you give them.   Help stamp out software-hoarding!  */

/* define this for running on the TI NU machine.  */

/* #define numachine 1 */

#ifdef numachine
#define O_RDONLY 0
#endif

#include <a.out.h>
#include <stdio.h>
#include <strings.h>
#include <ar.h>

#ifndef numachine
#include <sys/file.h>
#endif

int number_of_files;

/* current file's name */

char *input_name;

/* current member's name, or 0 if processing a non-library file.  */

char *input_member;

/* offset within archive of the current member, if we are processing an archive.  */

int member_offset;

void do_one_file (), do_one_rel_file ();
char *concat ();

main(argc, argv)
     char **argv;
     int argc;
{
  int i;

  number_of_files = argc - 1;

  if (!number_of_files)
    fatal ("no files specified", 0);

  printf ("text\tdata\tbss\tdec\thex\n");

  /* Now scan and describe the files.  */

  for (i = 1; i < argc; i++)
    do_one_file (argv[i]);
}

/* open the file specified by `name', and return a descriptor.  */

/* Print the filename of the current file on 'outfile' (a stdio stream).  */

print_file_name (outfile)
     FILE *outfile;
{
  fprintf (outfile, "%s", input_name);
  if (input_member)
    fprintf (outfile, "(%s)", input_member);
}

/* process one input file */
void scan_library ();

void
do_one_file (name)
     char *name;
{
  int len, magicnum, desc;

  desc = open (name, O_RDONLY, 0);

  if (desc < 0)
    {
      perror_name (name);
      return;
    }

  input_name = name;
  input_member = 0;

  len = read (desc, &magicnum, sizeof magicnum);
  if (len != sizeof magicnum)
    error_with_file ("failure reading header of ");
  else if (!N_BADMAG(*((struct exec *)&magicnum)))
    do_one_rel_file (desc, 0);
  else
    {
      char armag[SARMAG];

      lseek (desc, 0, 0);
      if (SARMAG != read (desc, armag, SARMAG) || strncmp (armag, ARMAG, SARMAG))
	error_with_file ("malformed input file (not rel or archive) ");
      else
        scan_library (desc);
    }

  close (desc);
}

/* read in the archive data about one member.
  subfile_offset is the address within the archive of the start of that data.
  The value returned is the length of the member's contents, which does
  not include the archive data about the member.

  If there are no more valid members, zero is returned.  */

int
decode_library_subfile (desc, subfile_offset)
     int desc;
     int subfile_offset;
{
  int bytes_read;
  int namelen;
  int member_length;
  char *name;
  struct ar_hdr hdr1;

  lseek (desc, subfile_offset, 0);

  bytes_read = read (desc, &hdr1, sizeof hdr1);
  if (!bytes_read)
    ;		/* end of archive */

  else if (sizeof hdr1 != bytes_read)
    error_with_file ("malformed library archive ");

  else if (sscanf (hdr1.ar_size, "%d", &member_length) != 1)
    error_with_file ("malformatted header of archive member in ");

  else
    {
      namelen = index (hdr1.ar_name, ' ') - hdr1.ar_name;
      name = (char *) xmalloc (namelen+1);
      strncpy (name, hdr1.ar_name, namelen);
      *(name + namelen) = 0;

      input_member = name;

      return member_length;
    }
  return 0;   /* tell caller to exit loop */
}

/* Scan a library and describe each member.  */

void
scan_library (desc)
     int desc;
{
  int this_subfile_offset = SARMAG;
  int member_length;
  
  while (member_length = decode_library_subfile (desc, this_subfile_offset))
    {
      /* describe every member except the ranlib data if any */
      if (strcmp (input_member, "__.SYMDEF"))
	do_one_rel_file (desc, this_subfile_offset + sizeof (struct ar_hdr));

      this_subfile_offset += ((member_length + sizeof (struct ar_hdr)) + 1) & -2;
    }
}

void read_header ();

void
do_one_rel_file (desc, offset)
     int desc;
     int offset;
{
  struct exec header;   /* file header read in here */
  int total;

  header.a_magic = 0;

  read_header (desc, &header, offset);

  if (N_BADMAG(header))
    {
      error_with_file ("bad magic number in ");
      return;
    }

  total = header.a_text + header.a_data + header.a_bss;
  printf ("%d\t%d\t%d\t%d\t%x", header.a_text, header.a_data, header.a_bss, total, total);

  if (number_of_files > 1 || input_member)
    {
      printf ("\t");
      print_file_name (stdout);
    }
  printf ("\n");
}


/* read a file's header */

void
read_header (desc, loc, offset)
     int desc;
     struct exec *loc;
     int offset;
{
  int len;
  lseek (desc, offset, 0);
  len = read (desc, loc, sizeof (struct exec));
  if (len != sizeof (struct exec))
    error_with_file ("failure reading header of ");
}

/* Report a fatal error.  'string' is a printf format string and 'arg' is one arg for it.  */

fatal (string, arg)
     char *string, *arg;
{
  fprintf (stderr, "size: ");
  fprintf (stderr, string, arg);
  fprintf (stderr, "\n");
  exit (1);
}

/* Report a nonfatal error.  'string' is a printf format string and 'arg' is one arg for it.  */

error (string, arg)
     char *string, *arg;
{
  fprintf (stderr, "size: ");
  fprintf (stderr, string, arg);
  fprintf (stderr, "\n");
}

/* Report a nonfatal error.  'string' is printed, followed by the current file name.  */

error_with_file (string)
     char *string;
{
  fprintf (stderr, "size: ");
  fprintf (stderr, string);
  print_file_name (stderr);
  fprintf (stderr, "\n");
}

/* Report a fatal error using the message for the last failed system call,
 followed by the string 'name'.  */

perror_name (name)
     char *name;
{
  extern int errno, sys_nerr;
  extern char *sys_errlist[];
  char *s;

  if (errno < sys_nerr)
    s = concat ("", sys_errlist[errno], " for %s");
  else
    s = "cannot open %s";
  error (s, name);
}

/* Like malloc but get fatal error if memory is exhausted.  */

int
xmalloc (size)
     int size;
{
  int result = malloc (size);
  if (!result)
    fatal ("virtual memory exhausted", 0);
  return result;
}

/* Return a newly-allocated string whose contents concatenate those of s1, s2, s3.  */

char *
concat (s1, s2, s3)
     char *s1, *s2, *s3;
{
  int len1 = strlen (s1), len2 = strlen (s2), len3 = strlen (s3);
  char *result = (char *) xmalloc (len1 + len2 + len3 + 1);

  strcpy (result, s1);
  strcpy (result + len1, s2);
  strcpy (result + len1 + len2, s3);
  *(result + len1 + len2 + len3) = 0;

  return result;
}
