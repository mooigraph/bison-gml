/* print factors of n.  lose if > 2^32.  --phr 24 oct 86 */

#include <stdio.h>

main (argc, argv)
     int argc;
     char **argv;
{
  if (argc < 2)
    do_stdin ();
  else
    factor ((unsigned) atoi(argv[1]));
  exit (0);
}

factor (n0)
     unsigned long n0;
{
  register unsigned long n = n0, d;

  if (n < 1)
    return;

  while (n % 2 == 0) {
    printf ("\t2\n");
    n /= 2;
  }
  for (d = 3; d * d <= n; d += 2) {
    while (n % d == 0) {
      printf ("\t%d\n", d);
      n /= d;
    }
  }
  if (n != 1 || n0 == 1)
    printf ("\t%d\n", n);
}

do_stdin ()
{
  char buf[1000];

  for (;;) {
    if (read (0, buf, sizeof buf) <= 0)
      exit (1);
    factor ((unsigned long) atoi(buf));
  }
}
