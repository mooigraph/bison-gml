/*
 * what name1 name2 ...
 * Shows what versions of object modules were used to construct a binary
 * Copyright (C) 1986 Free Software Foundation
 * Written by Kayvan Aghaiepour
 *   
 * This program is distributed in the hope that it will be useful,
 * but without any warranty.  No author or distributor
 * accepts responsibility to anyone for the consequences of using it
 * or for whether it serves any particular purpose or works at all,
 * unless he says so in writing.
 * 
 * Everyone is granted permission to copy, modify and redistribute
 * this program, but only under the conditions described in the
 * document "GNU Emacs copying permission notice".   An exact copy
 * of the document is supposed to have been given to you along with
 * GNU Emacs so that you can know how you may redistribute it all.
 * It should be in a file named COPYING.  Among other things, the
 * copyright notice and this notice must be preserved on all copies.
 * 
 */

#include <stdio.h>

main(argc, argv)
     int argc;
     char **argv;
{
  FILE *fp;			/* file pointer to read */
  int i;

  i = 1;
  do {
    if (i < argc) {
      if ((fp = fopen(argv[i], "r")) == (FILE *) NULL) {
	perror(argv[i]);	/* There was a problem opening the file */
	exit(1);
      }
      else {
	printf("%s\n", argv[i]); /* print the filename */
	whatfile(fp);		/* do 'what' on the file */
	fclose(fp);		/* close the file */
      }
    }
    else {
      whatfile(stdin);		/* no arguments, so read from stdin */
    }
  } while (++i < argc);
}

/*
 * whatfile takes an open file pointer as input and searches for
 * the sccsid in the file, printing whatever is after the sccsid.
 *
 */

#define NOTDELIM(c) (((c) != '\n') && ((c) != '>') && ((c) != '\"') && ((c)))
#define MATCH(fp) (((char) getc(fp) == '(') && ((char) getc(fp) == '#') \
		   && ((char) getc(fp) == ')'))

whatfile(fp)
     FILE *fp;			/* open file pointer */
{
  register int ch;

  while ((ch = getc(fp)) != EOF) {
    if ((char) ch == '@') {	/* are we matching? */
      if (MATCH(fp)) {		/* we have a match */
	putchar('\t');
	while (((ch = getc(fp)) != EOF) && (NOTDELIM((char) ch))) {
	  putchar((char) ch);
	}
	putchar('\n');
      }
    }
  }
}
