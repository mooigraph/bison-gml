/* Configuration for GNU C-compiler.
   Motorola 68000 version.
   Copyright (C) 1985 Free Software Foundation, Inc.

This will be free software eventually, but not until it is finished.  */

/* #defines that need visibility everywhere.  */
#define FALSE 0
#define TRUE 1

/* target machine dependencies.
   tm.h is a symbolic link to the actual target specific file.   */
#include "tm.h"

/* This describes the machine the compiler is hosted on.  */
#define HOST_BITS_PER_CHAR 8
#define HOST_BITS_PER_SHORT 16
#define HOST_BITS_PER_INT 32
#define HOST_BITS_PER_LONG 32

/*
Local variables:
version-control: t
End:
*/
