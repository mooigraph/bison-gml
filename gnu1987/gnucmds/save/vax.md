;;%%
;;- Machine description for GNU compiler
;;- Vax Version
;;- Copyright (C) 1986 Free Software Foundation.
;;-
;;- This will be free software eventually, but not until
;;- it is finished.


;;- instruction definitions
;;-
;;- @@The original PO technology requires these to be ordered by speed,
;;- @@    so that assigner will pick the fastest.
;;-
;;- See file "rtl.def" for documentation on define_insn, match_*, et. al.
;;-
;;- When naming insn's (operand 0 of define_insn) be careful about using
;;- names from other targets machine descriptions.
;;-
;;- cpp macro #define NOTICE_UPDATE_CC in file tm.h handles condition code
;;- updates for most instructions.

;;- Operand classes for the register allocator:
;;- None needed.

;;-lines with the following format are in PO machine description format.
;;STK = SRCW1;NZ = SRCW1 ? 0;                     :=      pushl SRCW1
(define_insn "pushsi"
  [(set (match_operand:SI 0 "push_operand" "")
	(match_operand:SI 1 "general_operand" ""))]
  ""
  "pushl %1")

;;STK = WAD;NZ = WAD ? 0;                         :=      pushal WAD
(define_insn ""
  [(set (match_operand:SI 0 "push_operand" "")
	(address:SI (match_operand:DF 1 "address_operand" "")))]
  ""
  "pushad %1")

(define_insn ""
  [(set (match_operand:SI 0 "push_operand" "")
	(address:SI (match_operand:SF 1 "address_operand" "")))]
  ""
  "pushaf %1")

(define_insn ""
  [(set (match_operand:SI 0 "push_operand" "")
	(address:SI (match_operand:SI 1 "address_operand" "")))]
  ""
  "pushal %1")

(define_insn ""
  [(set (match_operand:SI 0 "push_operand" "")
	(address:SI (match_operand:HI 1 "address_operand" "")))]
  ""
  "pushaw %1")

(define_insn ""
  [(set (match_operand:SI 0 "push_operand" "")
	(address:SI (match_operand:QI 1 "address_operand" "")))]
  ""
  "pushab %1")

(define_insn "tstdf"
  [(set (cc0)
	(match_operand:DF 0 "general_operand" ""))]
  ""
  "tstd %0")

(define_insn "tstsf"
  [(set (cc0)
	(match_operand:SF 0 "general_operand" ""))]
  ""
  "tstf %0")

(define_insn "tstsi"
  [(set (cc0)
	(match_operand:SI 0 "general_operand" ""))]
  ""
  "tstl %0")

(define_insn "tsthi"
  [(set (cc0)
	(match_operand:HI 0 "general_operand" ""))]
  ""
  "tstw %0")

(define_insn "tstqi"
  [(set (cc0)
	(match_operand:QI 0 "general_operand" ""))]
  ""
  "tstb %0")

;;NZ = DSTW ? SRCW1;                              :=      cmpl DSTW,SRCW1
(define_insn "cmpdf"
  [(set (cc0)
	(minus (match_operand:DF 0 "general_operand" "")
	       (match_operand:DF 1 "general_operand" "")))]
  ""
  "cmpd %0,%1")

(define_insn "cmpsf"
  [(set (cc0)
	(minus (match_operand:SF 0 "general_operand" "")
	       (match_operand:SF 1 "general_operand" "")))]
  ""
  "cmpf %0,%1")

(define_insn "cmpsi"
  [(set (cc0)
	(minus (match_operand:SI 0 "general_operand" "")
	       (match_operand:SI 1 "general_operand" "")))]
  ""
  "cmpl %0,%1")

(define_insn "cmphi"
  [(set (cc0)
	(minus (match_operand:HI 0 "general_operand" "")
	       (match_operand:HI 1 "general_operand" "")))]
  ""
  "cmpw %0,%1")

(define_insn "cmpqi"
  [(set (cc0)
	(minus (match_operand:QI 0 "general_operand" "")
	       (match_operand:QI 1 "general_operand" "")))]
  ""
  "cmpb %0,%1")

;;NZ = DSTW & SRCW1 ? 0;                          :=      bitl DSTW,SRCW1
(define_insn "bitsi"
  [(set (cc0)
	(and:SI (match_operand:SI 0 "general_operand" "")
		(match_operand:SI 1 "general_operand" "")))]
  ""
  "bitl %0,%1")

(define_insn "bithi"
  [(set (cc0)
	(and:HI (match_operand:HI 0 "general_operand" "")
		(match_operand:HI 1 "general_operand" "")))]
  ""
  "bitw %0,%1")

(define_insn "bitqi"
  [(set (cc0)
	(and:QI (match_operand:QI 0 "general_operand" "")
		(match_operand:QI 1 "general_operand" "")))]
  ""
  "bitb %0,%1")

;;DSTW = 0;NZ = 0 ? 0;                            :=      clrl DSTW
(define_insn "clrdf"
  [(set (match_operand:DF 0 "general_operand" "")
	(const_int 0))]
  ""
  "clrd %0")

(define_insn "clrsf"
  [(set (match_operand:SF 0 "general_operand" "")
	(const_int 0))]
  ""
  "clrf %0")

(define_insn "clrsi"
  [(set (match_operand:SI 0 "general_operand" "")
	(const_int 0))]
  ""
  "clrl %0")

(define_insn "clrhi"
  [(set (match_operand:HI 0 "general_operand" "")
	(const_int 0))]
  ""
  "clrw %0")

(define_insn "clrqi"
  [(set (match_operand:QI 0 "general_operand" "")
	(const_int 0))]
  ""
  "clrb %0")

;;DSTW = SRCW1;NZ = SRCW1 ? 0;                    :=      movl SRCW1,DSTW
(define_insn "movdf"
  [(set (match_operand:DF 0 "general_operand" "")
	(match_operand:DF 1 "general_operand" ""))]
  ""
  "movd %1,%0")

(define_insn "movsf"
  [(set (match_operand:SF 0 "general_operand" "")
	(match_operand:SF 1 "general_operand" ""))]
  ""
  "movf %1,%0")

(define_insn "movti"
  [(set (match_operand:TI 0 "general_operand" "")
	(match_operand:TI 1 "general_operand" ""))]
  ""
  "movh %1,%0")

(define_insn "movdi"
  [(set (match_operand:DI 0 "general_operand" "")
	(match_operand:DI 1 "general_operand" ""))]
  ""
  "movd %1,%0")

(define_insn "movsi"
  [(set (match_operand:SI 0 "general_operand" "")
	(match_operand:SI 1 "general_operand" ""))]
  ""
  "movl %1,%0")

(define_insn "movhi"
  [(set (match_operand:HI 0 "general_operand" "")
	(match_operand:HI 1 "general_operand" ""))]
  ""
  "movw %1,%0")

(define_insn "movqi"
  [(set (match_operand:QI 0 "general_operand" "")
	(match_operand:QI 1 "general_operand" ""))]
  ""
  "movb %1,%0")

;;- load effective address 
;;DSTW = WAD;NZ = WAD ? 0;                         :=      moval WAD,DSTW
(define_insn ""
  [(set (match_operand:SI 0 "general_operand" "")
	(address:SI (match_operand:QI 1 "address_operand" "")))]
  ""
  "movab %1,%0")

(define_insn ""
  [(set (match_operand:SI 0 "general_operand" "")
	(address:SI (match_operand:HI 1 "address_operand" "")))]
  ""
  "movaw %1,%0")

(define_insn ""
  [(set (match_operand:SI 0 "general_operand" "")
	(address:SI (match_operand:SI 1 "address_operand" "")))]
  ""
  "moval %1,%0")

(define_insn ""
  [(set (match_operand:SI 0 "general_operand" "")
	(address:SI (match_operand:SF 1 "address_operand" "")))]
  ""
  "movaf %1,%0")

(define_insn ""
  [(set (match_operand:SI 0 "general_operand" "")
	(address:SI (match_operand:DF 1 "address_operand" "")))]
  ""
  "movad %1,%0")

;; The definition of this insn does not really explain what it does,
;; but it should suffice
;; that anything generated as this insn will be recognized as one
;; and that it won't successfully combine with anything.
;; @@?? Note we must not have more than 2 set operations
;; @@?? since tm-vax.h says no insn has more than that.
(define_insn "movstrhi"
  [(set (match_operand:BLK 0 "general_operand" "")
	(match_operand:BLK 1 "general_operand" ""))
   (use (match_operand:HI 2 "general_operand" ""))
   (clobber (nil))			;;- Clobber everything in memory
   (clobber (reg 0))
   (clobber (reg 1))
   (clobber (reg 2))
   (clobber (reg 3))
   (clobber (reg 4))
   (clobber (reg 5))]
  ""
  "movc3 %2,%1,%0")

;;DSTW = SRCB1;NZ = SRCB1 ? 0;                    :=      cvtbl SRCB1,DSTW
(define_insn "extendqihi2"
  [(set (match_operand:HI 0 "general_operand" "")
	(sign_extend:HI (match_operand:QI 1 "general_operand" "")))]
  ""
  "cvtbw %1,%0")

(define_insn "extendqisi2"
  [(set (match_operand:SI 0 "general_operand" "")
	(sign_extend:SI (match_operand:QI 1 "general_operand" "")))]
  ""
  "cvtbl %1,%0")

(define_insn "floatqisf2"
  [(set (match_operand:SF 0 "general_operand" "")
	(float:SF (match_operand:QI 1 "general_operand" "")))]
  ""
  "cvtbf %1,%0")

(define_insn "floatqidf2"
  [(set (match_operand:DF 0 "general_operand" "")
	(float:DF (match_operand:QI 1 "general_operand" "")))]
  ""
  "cvtbd %1,%0")

(define_insn "trunchiqi2"
  [(set (match_operand:QI 0 "general_operand" "")
	(truncate:QI (match_operand:HI 1 "general_operand" "")))]
  ""
  "cvtwb %1,%0")

(define_insn "extendhisi2"
  [(set (match_operand:SI 0 "general_operand" "")
	(sign_extend:SI (match_operand:HI 1 "general_operand" "")))]
  ""
  "cvtwl %1,%0")

(define_insn "floathisf2"
  [(set (match_operand:SF 0 "general_operand" "")
	(float:SF (match_operand:HI 1 "general_operand" "")))]
  ""
  "cvtwf %1,%0")

(define_insn "floathidf2"
  [(set (match_operand:DF 0 "general_operand" "")
	(float:DF (match_operand:HI 1 "general_operand" "")))]
  ""
  "cvtwd %1,%0")

(define_insn "truncsiqi2"
  [(set (match_operand:QI 0 "general_operand" "")
	(truncate:QI (match_operand:SI 1 "general_operand" "")))]
  ""
  "cvtlb %1,%0")

(define_insn "truncsihi2"
  [(set (match_operand:HI 0 "general_operand" "")
	(truncate:HI (match_operand:SI 1 "general_operand" "")))]
  ""
  "cvtlw %1,%0")

(define_insn "floatsisf2"
  [(set (match_operand:SF 0 "general_operand" "")
	(float:SF (match_operand:SI 1 "general_operand" "")))]
  ""
  "cvtlf %1,%0")

(define_insn "floatsidf2"
  [(set (match_operand:DF 0 "general_operand" "")
	(float:DF (match_operand:SI 1 "general_operand" "")))]
  ""
  "cvtld %1,%0")

(define_insn "fixsfqi2"
  [(set (match_operand:QI 0 "general_operand" "")
	(fix:QI (match_operand:SF 1 "general_operand" "")))]
  ""
  "cvtfb %1,%0")

(define_insn "fixsfhi2"
  [(set (match_operand:HI 0 "general_operand" "")
	(fix:HI (match_operand:SF 1 "general_operand" "")))]
  ""
  "cvtfw %1,%0")

(define_insn "fixsfsi2"
  [(set (match_operand:SI 0 "general_operand" "")
	(fix:SI (match_operand:SF 1 "general_operand" "")))]
  ""
  "cvtfl %1,%0")

(define_insn "extendsfdf2"
  [(set (match_operand:DF 0 "general_operand" "")
	(float_extend:DF (match_operand:SF 1 "general_operand" "")))]
  ""
  "cvtfd %1,%0")

(define_insn "fixdfqi2"
  [(set (match_operand:QI 0 "general_operand" "")
	(fix:QI (match_operand:DF 1 "general_operand" "")))]
  ""
  "cvtdb %1,%0")

(define_insn "fixdfhi2"
  [(set (match_operand:HI 0 "general_operand" "")
	(fix:HI (match_operand:DF 1 "general_operand" "")))]
  ""
  "cvtdw %1,%0")

(define_insn "fixdfsi2"
  [(set (match_operand:SI 0 "general_operand" "")
	(fix:SI (match_operand:DF 1 "general_operand" "")))]
  ""
  "cvtdl %1,%0")

(define_insn "truncdfsf2"
  [(set (match_operand:SF 0 "general_operand" "")
	(float_truncate:SF (match_operand:DF 1 "general_operand" "")))]
  ""
  "cvtdf %1,%0")

(define_insn "extendqwhi2"
  [(set (match_operand:HI 0 "general_operand" "")
	(zero_extend:HI (match_operand:QI 1 "general_operand" "")))]
  ""
  "movzbw %1,%0")

(define_insn "extendqwsi2"
  [(set (match_operand:SI 0 "general_operand" "")
	(zero_extend:SI (match_operand:QI 1 "general_operand" "")))]
  ""
  "movzbl %1,%0")

(define_insn "extendhwsi2"
  [(set (match_operand:SI 0 "general_operand" "")
	(zero_extend:SI (match_operand:HI 1 "general_operand" "")))]
  ""
  "movzwl %1,%0")

;;- All kinds of add instructions.

(define_insn "incsi"
  [(set (match_operand:SI 0 "general_operand" "")
	(plus:SI (match_dup 0)
		 (const_int 1)))]
  ""
  "incl %0")

(define_insn "inchi"
  [(set (match_operand:HI 0 "general_operand" "")
	(plus:HI (match_dup 0)
		 (const_int 1)))]
  ""
  "incw %0")

(define_insn "incqi"
  [(set (match_operand:QI 0 "general_operand" "")
	(plus:QI (match_dup 0)
		 (const_int 1)))]
  ""
  "incb %0")

(define_insn "adddf2"
  [(set (match_operand:DF 0 "general_operand" "")
	(plus:DF (match_dup 0)
		 (match_operand:DF 1 "general_operand" "")))]
  ""
  "addd2 %1,%0")

(define_insn "addsf2"
  [(set (match_operand:SF 0 "general_operand" "")
	(plus:SF (match_dup 0)
		 (match_operand:SF 1 "general_operand" "")))]
  ""
  "addf2 %1,%0")

(define_insn "addsi2"
  [(set (match_operand:SI 0 "general_operand" "")
	(plus:SI (match_dup 0)
		 (match_operand:SI 1 "general_operand" "")))]
  ""
  "addl2 %1,%0")

(define_insn "addhi2"
  [(set (match_operand:HI 0 "general_operand" "")
	(plus:HI (match_dup 0)
		 (match_operand:HI 1 "general_operand" "")))]
  ""
  "addw2 %1,%0")

(define_insn "addqi2"
  [(set (match_operand:QI 0 "general_operand" "")
	(plus:QI (match_dup 0)
		 (match_operand:QI 1 "general_operand" "")))]
  ""
  "addb2 %1,%0")

(define_insn "adddf3"
  [(set (match_operand:DF 0 "general_operand" "")
	(plus:DF (match_operand:DF 1 "general_operand" "")
		 (match_operand:DF 2 "general_operand" "")))]
  ""
  "addd3 %1,%2,%0")

(define_insn "addsf3"
  [(set (match_operand:SF 0 "general_operand" "")
	(plus:SF (match_operand:SF 1 "general_operand" "")
		 (match_operand:SF 2 "general_operand" "")))]
  ""
  "addf3 %1,%2,%0")

(define_insn "addsi3"
  [(set (match_operand:SI 0 "general_operand" "")
	(plus:SI (match_operand:SI 1 "general_operand" "")
		 (match_operand:SI 2 "general_operand" "")))]
  ""
  "*{ if (GET_CODE (operands[2]) == CONST_INT
          && FORMAT_i (operands[2], 0) < 64
	  && GET_CODE (operands[1]) == REG)
        return \"movab %c2(%1),%0\";
      return \"addl3 %1,%2,%0\";}")

(define_insn "addhi3"
  [(set (match_operand:HI 0 "general_operand" "")
	(plus:HI (match_operand:HI 1 "general_operand" "")
		 (match_operand:HI 2 "general_operand" "")))]
  ""
  "addw3 %1,%2,%0")

(define_insn "addqi3"
  [(set (match_operand:QI 0 "general_operand" "")
	(plus:QI (match_operand:QI 1 "general_operand" "")
		 (match_operand:QI 2 "general_operand" "")))]
  ""
  "addb3 %1,%2,%0")

;;- All kinds of subtract instructions.

(define_insn "decsi"
  [(set (match_operand:SI 0 "general_operand" "")
	(minus:SI (match_dup 0)
		  (const_int 1)))]
  ""
  "decl %0")

(define_insn "dechi"
  [(set (match_operand:HI 0 "general_operand" "")
	(minus:HI (match_dup 0)
		  (const_int 1)))]
  ""
  "decw %0")

(define_insn "decqi"
  [(set (match_operand:QI 0 "general_operand" "")
	(minus:QI (match_dup 0)
		  (const_int 1)))]
  ""
  "decb %0")

(define_insn "subdf2"
  [(set (match_operand:DF 0 "general_operand" "")
	(minus:DF (match_dup 0)
		  (match_operand:DF 1 "general_operand" "")))]
  ""
  "subd2 %1,%0")

(define_insn "subsf2"
  [(set (match_operand:SF 0 "general_operand" "")
	(minus:SF (match_dup 0)
		  (match_operand:SF 1 "general_operand" "")))]
  ""
  "subf2 %1,%0")

(define_insn "subsi2"
  [(set (match_operand:SI 0 "general_operand" "")
	(minus:SI (match_dup 0)
		  (match_operand:SI 1 "general_operand" "")))]
  ""
  "subl2 %1,%0")

(define_insn "subhi2"
  [(set (match_operand:HI 0 "general_operand" "")
	(minus:HI (match_dup 0)
		  (match_operand:HI 1 "general_operand" "")))]
  ""
  "subw2 %1,%0")

(define_insn "subqi2"
  [(set (match_operand:QI 0 "general_operand" "")
	(minus:QI (match_dup 0)
		  (match_operand:QI 1 "general_operand" "")))]
  ""
  "subb2 %1,%0")

(define_insn "subdf3"
  [(set (match_operand:DF 0 "general_operand" "")
	(minus:DF (match_operand:DF 1 "general_operand" "")
		  (match_operand:DF 2 "general_operand" "")))]
  ""
  "subd3 %2,%1,%0")

(define_insn "subsf3"
  [(set (match_operand:SF 0 "general_operand" "")
	(minus:SF (match_operand:SF 1 "general_operand" "")
		  (match_operand:SF 2 "general_operand" "")))]
  ""
  "subf3 %2,%1,%0")

(define_insn "subsi3"
  [(set (match_operand:SI 0 "general_operand" "")
	(minus:SI (match_operand:SI 1 "general_operand" "")
		  (match_operand:SI 2 "general_operand" "")))]
  ""
  "subl3 %2,%1,%0")

(define_insn "subhi3"
  [(set (match_operand:HI 0 "general_operand" "")
	(minus:HI (match_operand:HI 1 "general_operand" "")
		  (match_operand:HI 2 "general_operand" "")))]
  ""
  "subw3 %2,%1,%0")

(define_insn "subqi3"
  [(set (match_operand:QI 0 "general_operand" "")
	(minus:QI (match_operand:QI 1 "general_operand" "")
		  (match_operand:QI 2 "general_operand" "")))]
  ""
  "subb3 %2,%1,%0")

;;- Multiply instructions.

(define_insn "muldf2"
  [(set (match_operand:DF 0 "general_operand" "")
	(mult:DF (match_dup 0)
		 (match_operand:DF 1 "general_operand" "")))]
  ""
  "muld2 %1,%0")

(define_insn "mulsf2"
  [(set (match_operand:SF 0 "general_operand" "")
	(mult:SF (match_dup 0)
		 (match_operand:SF 1 "general_operand" "")))]
  ""
  "mulf2 %1,%0")

(define_insn "mulsi2"
  [(set (match_operand:SI 0 "general_operand" "")
	(mult:SI (match_dup 0)
		 (match_operand:SI 1 "general_operand" "")))]
  ""
  "*{
  if (GET_CODE (operands[1]) == CONST_INT)
    {
      register int log = int_log2 (FORMAT_i (operands[1], 0));
      if (FORMAT_i (operands[1], 0) == (1 << log))
	{
	  operands[1] = gen_rtx (CONST_INT, VOIDmode, log);
	  return \"ashl %1,%0,%0\";
	}
    }
  return \"mull2 %1,%0\"; }")

(define_insn "mulhi2"
  [(set (match_operand:HI 0 "general_operand" "")
	(mult:HI (match_dup 0)
		 (match_operand:HI 1 "general_operand" "")))]
  ""
  "mulw2 %1,%0")

(define_insn "mulqi2"
  [(set (match_operand:QI 0 "general_operand" "")
	(mult:QI (match_dup 0)
		 (match_operand:QI 1 "general_operand" "")))]
  ""
  "mulb2 %1,%0")

(define_insn "muldf3"
  [(set (match_operand:DF 0 "general_operand" "")
	(mult:DF (match_operand:DF 1 "general_operand" "")
		 (match_operand:DF 2 "general_operand" "")))]
  ""
  "muld3 %1,%2,%0")

(define_insn "mulsf3"
  [(set (match_operand:SF 0 "general_operand" "")
	(mult:SF (match_operand:SF 1 "general_operand" "")
		 (match_operand:SF 2 "general_operand" "")))]
  ""
  "mulf3 %1,%2,%0")

(define_insn "mulsi3"
  [(set (match_operand:SI 0 "general_operand" "")
	(mult:SI (match_operand:SI 1 "general_operand" "")
		 (match_operand:SI 2 "general_operand" "")))]
  ""
  ;; Note that a constant operand is always operand[2].
  ;; expand_binop arranges it that way, to increase chances of winning
  ;; with mull2.
  "*{
  if (GET_CODE (operands[2]) == CONST_INT)
    {
      register int log = int_log2 (FORMAT_i (operands[2], 0));
      if (FORMAT_i (operands[2], 0) == (1 << log))
	{
	  operands[2] = operands[1];
	  operands[1] = gen_rtx (CONST_INT, VOIDmode, log);
	  return \"ashl %1,%2,%0\";
	}
    }
  return \"mull3 %1,%2,%0\"; }")

(define_insn "mulhi3"
  [(set (match_operand:HI 0 "general_operand" "")
	(mult:HI (match_operand:HI 1 "general_operand" "")
		 (match_operand:HI 2 "general_operand" "")))]
  ""
  "mulw3 %1,%2,%0")

(define_insn "mulqi3"
  [(set (match_operand:QI 0 "general_operand" "")
	(mult:QI (match_operand:QI 1 "general_operand" "")
		 (match_operand:QI 2 "general_operand" "")))]
  ""
  "mulb3 %1,%2,%0")

;;- Divide instructions.

(define_insn "divdf2"
  [(set (match_operand:DF 0 "general_operand" "")
	(div:DF (match_dup 0)
		(match_operand:DF 1 "general_operand" "")))]
  ""
  "divd2 %1,%0")

(define_insn "divsf2"
  [(set (match_operand:SF 0 "general_operand" "")
	(div:SF (match_dup 0)
		(match_operand:SF 1 "general_operand" "")))]
  ""
  "divf2 %1,%0")

(define_insn "divsi2"
  [(set (match_operand:SI 0 "general_operand" "")
	(div:SI (match_dup 0)
		(match_operand:SI 1 "general_operand" "")))]
  ""
  "divl2 %1,%0")

(define_insn "divhi2"
  [(set (match_operand:HI 0 "general_operand" "")
	(div:HI (match_dup 0)
		(match_operand:HI 1 "general_operand" "")))]
  ""
  "divw2 %1,%0")

(define_insn "divqi2"
  [(set (match_operand:QI 0 "general_operand" "")
	(div:QI (match_dup 0)
		(match_operand:QI 1 "general_operand" "")))]
  ""
  "divb2 %1,%0")

(define_insn "divdf3"
  [(set (match_operand:DF 0 "general_operand" "")
	(div:DF (match_operand:DF 1 "general_operand" "")
		(match_operand:DF 2 "general_operand" "")))]
  ""
  "divd3 %2,%1,%0")

(define_insn "divsf3"
  [(set (match_operand:SF 0 "general_operand" "")
	(div:SF (match_operand:SF 1 "general_operand" "")
		(match_operand:SF 2 "general_operand" "")))]
  ""
  "divf3 %2,%1,%0")

(define_insn "divsi3"
  [(set (match_operand:SI 0 "general_operand" "")
	(div:SI (match_operand:SI 1 "general_operand" "")
		(match_operand:SI 2 "general_operand" "")))]
  ""
  "divl3 %2,%1,%0")

(define_insn "divhi3"
  [(set (match_operand:HI 0 "general_operand" "")
	(div:HI (match_operand:HI 1 "general_operand" "")
		(match_operand:HI 2 "general_operand" "")))]
  ""
  "divw3 %2,%1,%0")

(define_insn "divqi3"
  [(set (match_operand:QI 0 "general_operand" "")
	(div:QI (match_operand:QI 1 "general_operand" "")
		(match_operand:QI 2 "general_operand" "")))]
  ""
  "divb3 %2,%1,%0")

(define_insn "divmoddisi4"
  [(set (match_operand:SI 0 "general_operand" "")
	(div:SI (match_operand:DI 1 "general_operand" "")
		(match_operand:SI 2 "general_operand" "")))
   (set (match_operand:SI 3 "general_operand" "")
	(mod:SI (match_operand:DI 1 "general_operand" "")
		(match_operand:SI 2 "general_operand" "")))]
  ""
  "ediv %2,%1,%0,%3")

;;DSTW = SRCW1 & ~SRCW2;NZ = SRCW1 & ~SRCW2 ? 0;  :=      {
;;   !strcmp(DSTW, SRCW1): bicl2 SRCW2,SRCW1
;;   bicl3 SRCW2,SRCW1,DSTW


(define_insn "andcbsi2"
  [(set (match_operand:SI 0 "general_operand" "")
	(and:SI (match_dup 0)
		(not:SI (match_operand:SI 1 "general_operand" ""))))]
  ""
  "bicl2 %1,%0")

(define_insn "andcbhi2"
  [(set (match_operand:HI 0 "general_operand" "")
	(and:HI (match_dup 0)
		(not:HI (match_operand:HI 1 "general_operand" ""))))]
  ""
  "bicw2 %1,%0")

(define_insn "andcbqi2"
  [(set (match_operand:QI 0 "general_operand" "")
	(and:QI (match_dup 0)
		(not:QI (match_operand:QI 1 "general_operand" ""))))]
  ""
  "bicb2 %1,%0")

(define_insn "andcbsi3"
  [(set (match_operand:SI 0 "general_operand" "")
	(and:SI (match_operand:SI 1 "general_operand" "")
		(not:SI (match_operand:SI 2 "general_operand" ""))))]
  ""
  "bicl3 %2,%1,%0")

(define_insn "andcbhi3"
  [(set (match_operand:HI 0 "general_operand" "")
	(and:HI (match_operand:HI 1 "general_operand" "")
		(not:HI (match_operand:HI 2 "general_operand" ""))))]
  ""
  "bicw3 %2,%1,%0")

(define_insn "andcbqi3"
  [(set (match_operand:QI 0 "general_operand" "")
	(and:QI (match_operand:QI 1 "general_operand" "")
		(not:QI (match_operand:QI 2 "general_operand" ""))))]
  ""
  "bicb3 %2,%1,%0")

;;- Bit set instructions.

(define_insn "iorsi2"
  [(set (match_operand:SI 0 "general_operand" "")
	(ior:SI (match_dup 0)
		(match_operand:SI 1 "general_operand" "")))]
  ""
  "bisl2 %1,%0")

(define_insn "iorhi2"
  [(set (match_operand:HI 0 "general_operand" "")
	(ior:HI (match_dup 0)
		(match_operand:HI 1 "general_operand" "")))]
  ""
  "bisw2 %1,%0")

(define_insn "iorqi2"
  [(set (match_operand:QI 0 "general_operand" "")
	(ior:QI (match_dup 0)
		(match_operand:QI 1 "general_operand" "")))]
  ""
  "bisb2 %1,%0")

(define_insn "iorsi3"
  [(set (match_operand:SI 0 "general_operand" "")
	(ior:SI (match_operand:SI 1 "general_operand" "")
		(match_operand:SI 2 "general_operand" "")))]
  ""
  "bisl3 %2,%1,%0")

(define_insn "iorhi3"
  [(set (match_operand:HI 0 "general_operand" "")
	(ior:HI (match_operand:HI 1 "general_operand" "")
		(match_operand:HI 2 "general_operand" "")))]
  ""
  "bisw3 %2,%1,%0")

(define_insn "iorqi3"
  [(set (match_operand:QI 0 "general_operand" "")
	(ior:QI (match_operand:QI 1 "general_operand" "")
		(match_operand:QI 2 "general_operand" "")))]
  ""
  "bisb3 %2,%1,%0")

;;- xor instructions.

(define_insn "xorsi2"
  [(set (match_operand:SI 0 "general_operand" "")
	(xor:SI (match_dup 0)
		(match_operand:SI 1 "general_operand" "")))]
  ""
  "xorl2 %1,%0")

(define_insn "xorhi2"
  [(set (match_operand:HI 0 "general_operand" "")
	(xor:HI (match_dup 0)
		(match_operand:HI 1 "general_operand" "")))]
  ""
  "xorw2 %1,%0")

(define_insn "xorqi2"
  [(set (match_operand:QI 0 "general_operand" "")
	(xor:QI (match_dup 0)
		(match_operand:QI 1 "general_operand" "")))]
  ""
  "xorb2 %1,%0")

(define_insn "xorsi3"
  [(set (match_operand:SI 0 "general_operand" "")
	(xor:SI (match_operand:SI 1 "general_operand" "")
		(match_operand:SI 2 "general_operand" "")))]
  ""
  "xorl3 %2,%1,%0")

(define_insn "xorhi3"
  [(set (match_operand:HI 0 "general_operand" "")
	(xor:HI (match_operand:HI 1 "general_operand" "")
		(match_operand:HI 2 "general_operand" "")))]
  ""
  "xorw3 %2,%1,%0")

(define_insn "xorqi3"
  [(set (match_operand:QI 0 "general_operand" "")
	(xor:QI (match_operand:QI 1 "general_operand" "")
		(match_operand:QI 2 "general_operand" "")))]
  ""
  "xorb3 %2,%1,%0")

;;DSTW = -SRCW1;NZ = -SRCW ? 0;                   :=      mnegl SRCW1,DSTW

(define_insn "negdf2"
  [(set (match_operand:DF 0 "general_operand" "")
	(neg:DF (match_operand:DF 1 "general_operand" "")))]
  ""
  "mnegd %1,%0")

(define_insn "negsf2"
  [(set (match_operand:SF 0 "general_operand" "")
	(neg:SF (match_operand:SF 1 "general_operand" "")))]
  ""
  "mnegf %1,%0")

(define_insn "negsi2"
  [(set (match_operand:SI 0 "general_operand" "")
	(neg:SI (match_operand:SI 1 "general_operand" "")))]
  ""
  "mnegl %1,%0")

(define_insn "neghi2"
  [(set (match_operand:HI 0 "general_operand" "")
	(neg:HI (match_operand:HI 1 "general_operand" "")))]
  ""
  "mnegw %1,%0")

(define_insn "negqi2"
  [(set (match_operand:QI 0 "general_operand" "")
	(neg:QI (match_operand:QI 1 "general_operand" "")))]
  ""
  "mnegb %1,%0")

;;DSTW = ~SRCW1;NZ = ~SRCW ? 0;                   :=      mcoml SRCW1,DSTW

(define_insn "one_cmplsi2"
  [(set (match_operand:SI 0 "general_operand" "")
	(not:SI (match_operand:SI 1 "general_operand" "")))]
  ""
  "mcoml %1,%0")

(define_insn "one_cmplhi2"
  [(set (match_operand:HI 0 "general_operand" "")
	(not:HI (match_operand:HI 1 "general_operand" "")))]
  ""
  "mcomw %1,%0")

(define_insn "one_cmplqi2"
  [(set (match_operand:QI 0 "general_operand" "")
	(not:QI (match_operand:QI 1 "general_operand" "")))]
  ""
  "mcomb %1,%0")

;;DSTW = SRCW1 SO SRCW2;NZ = SRCW1 SO SRCW2 ? 0;  :=      ashl SRCW2,SRCW1,DSTW
(define_insn "ashlsi3"
  [(set (match_operand:SI 0 "general_operand" "")
	(ashift:SI (match_operand:SI 1 "general_operand" "")
		   (match_operand:SI 2 "general_operand" "")))]
  ""
  "ashl %2,%1,%0")

(define_insn "ashldi3"
  [(set (match_operand:DI 0 "general_operand" "")
	(ashift:DI (match_operand:DI 1 "general_operand" "")
		   (match_operand:DI 2 "general_operand" "")))]
  ""
  "ashq %2,%1,%0")

(define_insn "rotlsi3"
  [(set (match_operand:SI 0 "general_operand" "")
	(rotate:SI (match_operand:SI 1 "general_operand" "")
		   (match_operand:SI 2 "general_operand" "")))]
  ""
  "rotl %2,%1,%0")

(define_insn "rotldi3"
  [(set (match_operand:DI 0 "general_operand" "")
	(rotate:DI (match_operand:DI 1 "general_operand" "")
		   (match_operand:DI 2 "general_operand" "")))]
  ""
  "rotq %2,%1,%0")

(define_insn "index"
  [(set (match_operand:SI 0 "general_operand" "")
	(mult:SI (plus:SI (match_operand:SI 1 "general_operand" "")
			  (match_operand:SI 2 "general_operand" ""))
		 (match_operand:SI 3 "general_operand" "")))]
  ""
  "index %1,$??,$??,%3,%2,%0")

(define_insn "cmpv"
  [(set (cc0)
	(minus
	 (sign_extract:SI (match_operand:QI 0 "general_operand" "")
			  (match_operand:SI 1 "general_operand" "")
			  (match_operand:SI 2 "general_operand" ""))
	 (match_operand:SI 3 "general_operand" "")))]
  ""
  "cmpv %2,%1,%0,%3")

(define_insn "cmpzv"
  [(set (cc0)
	(minus
	 (zero_extract:SI (match_operand:QI 0 "general_operand" "")
			  (match_operand:SI 1 "general_operand" "")
			  (match_operand:SI 2 "general_operand" ""))
	 (match_operand:SI 3 "general_operand" "")))]
  ""
  "cmpzv %2,%1,%0,%3")

(define_insn "extv"
  [(set (match_operand:SI 0 "general_operand" "")
	(sign_extract:SI (match_operand:QI 1 "general_operand" "")
			 (match_operand:SI 2 "general_operand" "")
			 (match_operand:SI 3 "general_operand" "")))]
  ""
  "extv %3,%2,%1,%0")

(define_insn "extzv"
  [(set (match_operand:SI 0 "general_operand" "")
	(zero_extract:SI (match_operand:QI 1 "general_operand" "")
			 (match_operand:SI 2 "general_operand" "")
			 (match_operand:SI 3 "general_operand" "")))]
  ""
  "extzv %3,%2,%1,%0")

(define_insn "insv"
  [(set (zero_extract:SI (match_operand:QI 0 "general_operand" "")
			 (match_operand:SI 1 "general_operand" "")
			 (match_operand:SI 2 "general_operand" ""))
	(match_operand:SI 3 "general_operand" ""))]
  ""
  "insv %3,%2,%1,%0")


;;PC = LB;                                        :=      jbr LB
(define_insn "jump"
  [(set (pc)
	(label_ref (match_operand 0 "" "")))]
  ""
  "jbr %l0")

(define_insn "beq"
  [(set (pc)
	(if_then_else (eq (cc0)
			  (const_int 0))
		      (label_ref (match_operand 0 "" ""))
		      (pc)))]
  ""
  "jeql %l0")

(define_insn "bne"
  [(set (pc)
	(if_then_else (ne (cc0)
			  (const_int 0))
		      (label_ref (match_operand 0 "" ""))
		      (pc)))]
  ""
  "jneq %l0")

(define_insn "bgt"
  [(set (pc)
	(if_then_else (gt (cc0)
			  (const_int 0))
		      (label_ref (match_operand 0 "" ""))
		      (pc)))]
  ""
  "jgtr %l0")

(define_insn "bgtu"
  [(set (pc)
	(if_then_else (gtu (cc0)
			   (const_int 0))
		      (label_ref (match_operand 0 "" ""))
		      (pc)))]
  ""
  "jgtru %l0")

(define_insn "blt"
  [(set (pc)
	(if_then_else (lt (cc0)
			  (const_int 0))
		      (label_ref (match_operand 0 "" ""))
		      (pc)))]
  ""
  "jlss %l0")

(define_insn "bltu"
  [(set (pc)
	(if_then_else (ltu (cc0)
			   (const_int 0))
		      (label_ref (match_operand 0 "" ""))
		      (pc)))]
  ""
  "jlssu %l0")

(define_insn "bge"
  [(set (pc)
	(if_then_else (ge (cc0)
			  (const_int 0))
		      (label_ref (match_operand 0 "" ""))
		      (pc)))]
  ""
  "jgeq %l0")

(define_insn "bgeu"
  [(set (pc)
	(if_then_else (geu (cc0)
			   (const_int 0))
		      (label_ref (match_operand 0 "" ""))
		      (pc)))]
  ""
  "jgequ %l0")

(define_insn "ble"
  [(set (pc)
	(if_then_else (le (cc0)
			  (const_int 0))
		      (label_ref (match_operand 0 "" ""))
		      (pc)))]
  ""
  "jleq %l0")

(define_insn "bleu"
  [(set (pc)
	(if_then_else (leu (cc0)
			   (const_int 0))
		      (label_ref (match_operand 0 "" ""))
		      (pc)))]
  ""
  "jlequ %l0")

(define_insn ""
  [(set (pc)
	(if_then_else
	 (ne (sign_extract:SI (match_operand 0 "general_operand" "")
			      (const_int 1)
			      (match_operand:SI 1 "general_operand" ""))
	     (const_int 0))
	 (label_ref (match_operand 2 "" ""))
	 (pc)))]
  ""
  "jbs %1,%0,%l2")

(define_insn ""
  [(set (pc)
	(if_then_else
	 (eq (sign_extract:SI (match_operand 0 "general_operand" "")
			      (const_int 1)
			      (match_operand:SI 1 "general_operand" ""))
	     (const_int 0))
	 (label_ref (match_operand 2 "" ""))
	 (pc)))]
  ""
  "jbc %1,%0,%l2")

(define_insn ""
  [(set (pc)
	(if_then_else
	 (ne (and:SI (match_operand:SI 0 "general_operand" "")
		     (const_int 1))
	     (const_int 0))
	 (label_ref (match_operand 1 "" ""))
	 (pc)))]
  ""
  "jlbs %0,%l1")

(define_insn ""
  [(set (pc)
	(if_then_else
	 (eq (and:SI (match_operand:SI 0 "general_operand" "")
		     (const_int 1))
	     (const_int 0))
	 (label_ref (match_operand 1 "" ""))
	 (pc)))]
  ""
  "jlbc %0,%l1")

;; These two entries allow a jlbc or jlbs to be made
;; by combination with a bic.
(define_insn ""
  [(set (pc)
	(if_then_else
	 (ne (and:SI (match_operand:SI 0 "general_operand" "")
		     (not:SI (const_int -2)))
	     (const_int 0))
	 (label_ref (match_operand 1 "" ""))
	 (pc)))]
  ""
  "jlbs %0,%l1")

(define_insn ""
  [(set (pc)
	(if_then_else
	 (eq (and:SI (match_operand:SI 0 "general_operand" "")
		     (not:SI (const_int -2)))
	     (const_int 0))
	 (label_ref (match_operand 1 "" ""))
	 (pc)))]
  ""
  "jlbc %0,%l1")

(define_insn ""
  [(set (pc)
	(if_then_else
	 (gt (minus:SI (match_operand:SI 0 "general_operand" "")
		       (const_int 1))
	     (const_int 0))
	 (label_ref (match_operand 1 "" ""))
	 (pc)))
   (set (match_dup 0)
	(minus:SI (match_dup 0)
		  (const_int 1)))]
  ""
  "jsobgtr %0,%l1")

(define_insn ""
  [(set (pc)
	(if_then_else
	 (ge (minus:SI (match_operand:SI 0 "general_operand" "")
		       (const_int 1))
	     (const_int 0))
	 (label_ref (match_operand 1 "" ""))
	 (pc)))
   (set (match_dup 0)
	(minus:SI (match_dup 0)
		  (const_int 1)))]
  ""
  "jsobgeq %0,%l1")

(define_insn ""
  [(set (pc)
	(if_then_else
	 (lt (minus (plus:SI (match_operand:SI 0 "general_operand" "")
			     (const_int 1))
		    (match_operand:SI 1 "general_operand" ""))
	     (const_int 0))
	 (label_ref (match_operand 2 "" ""))
	 (pc)))
   (set (match_dup 0)
	(plus:SI (match_dup 0)
		 (const_int 1)))]
  ""
  "jaoblss %1,%0,%l2")

(define_insn ""
  [(set (pc)
	(if_then_else
	 (le (minus (plus:SI (match_operand:SI 0 "general_operand" "")
			     (const_int 1))
		    (match_operand:SI 1 "general_operand" ""))
	     (const_int 0))
	 (label_ref (match_operand 2 "" ""))
	 (pc)))
   (set (match_dup 0)
	(plus:SI (match_dup 0)
		 (const_int 1)))]
  ""
  "jaobleq %1,%0,%l2")

(define_insn "call"
  [(call (match_operand:QI 0 "general_operand" "")
	 (match_operand:QI 1 "general_operand" ""))]
  ""
  "calls %1,%0")

(define_insn "return"
  [(return)]
  ""
  "ret")

;;PC = DSTW - SRCW1 LSS SRCW2 IMP LB | PC;     :=      casel DSTW,SRCW1,SRCW2
(define_insn "casesi"
  [(set (pc)
	(if_then_else (le (minus:SI (match_operand:SI 0 "general_operand" "")
				    (match_operand:SI 1 "general_operand" ""))
			  (match_operand:SI 2 "general_operand" ""))
		      (plus:SI (label_ref:SI (match_operand 3 "" ""))
			       (sign_extend:SI
				(mem:HI (plus:SI (pc)
						 (minus:SI (match_dup 0)
							   (match_dup 1))))))
		      (pc)))]
  ""
  "casel %0,%1,%2")

;;- Local variables:
;;- mode:emacs-lisp
;;- comment-start: ";;- "
;;- eval: (set-syntax-table (copy-sequence (syntax-table)))
;;- eval: (modify-syntax-entry ?[ "(]")
;;- eval: (modify-syntax-entry ?] ")[")
;;- eval: (modify-syntax-entry ?{ "(}")
;;- eval: (modify-syntax-entry ?} "){")
;;- End:
