.text 0
.gdbbeg 0
.gdbbeg 1
Ltext:	.stabs "/u/rms/phr/cccp.c",100,0,0,Ltext
.data 0
Ldata:
.lcomm Lbss,0
.gdbsym Ldata,52
.gdbsym Lbss,56
.globl _include_directory
.text 0
LC0:
	.asciz "/usr/include/"
.data 0
_include_directory:
	.align 2
	.int LC0
.comm _output_buf,4
.comm _output_buf_ptr,4
.globl _output_buf_size
.data 0
_output_buf_size:
	.align 2
	.int 10
.comm _is_identchar,256
.comm _is_identstart,256
.comm _is_hor_space,256
.comm _input_stack,2400
.globl _input_stack_depth
.data 0
_input_stack_depth:
	.align 2
	.int 0
.globl _keyword_table
.text 0
LC1:
	.asciz "define"
.text 0
LC2:
	.asciz "line"
.text 0
LC3:
	.asciz "include"
.text 0
LC4:
	.asciz "undef"
.text 0
LC5:
	.asciz "error"
.text 0
LC6:
	.asciz "if"
.text 0
LC7:
	.asciz "ifdef"
.text 0
LC8:
	.asciz "ifndef"
.text 0
LC9:
	.asciz "else"
.text 0
LC10:
	.asciz "elif"
.text 0
LC11:
	.asciz "endif"
.text 0
LC12:
	.asciz "pragma"
.text 0
LC13:
	.asciz ""
.data 0
_keyword_table:
	.align 2
	.int 6
	.int _hack_define
	.int LC1
	.int 1
	.int 4
	.int _hack_line
	.int LC2
	.int 11
	.int 7
	.int _hack_include
	.int LC3
	.int 2
	.int 5
	.int _hack_undef
	.int LC4
	.int 10
	.int 5
	.int _hack_error
	.int LC5
	.int 12
	.int 2
	.int _hack_if
	.int LC6
	.int 4
	.int 5
	.int _hack_ifxdef
	.int LC7
	.int 3
	.int 6
	.int _hack_ifxdef
	.int LC8
	.int 13
	.int 4
	.int _hack_else
	.int LC9
	.int 7
	.int 4
	.int _hack_elif
	.int LC10
	.int 9
	.int 5
	.int _hack_endif
	.int LC11
	.int 14
	.int 6
	.int _hack_pragma
	.int LC12
	.int 8
	.int -1
	.int 0
	.int LC13
	.int -1
.comm _hashtab,4036
.globl _main
.text 0
	.align 1
_main:
	.gdbbeg 2
	.word 0xc0
	subl2 $64,sp
	movl 8(ap),r7
	.stabd 68,0,186
	.gdbbeg 3
	.stabd 68,0,191
	calls $0,_initialize_random_junk
	.stabd 68,0,193
	mull3 _input_stack_depth,$24,r1
	addl3 r1,$_input_stack,r6
	incl _input_stack_depth
	.stabd 68,0,195
	cmpl 4(ap),$2
	jgeq L2
	.stabd 68,0,196
	movl $1,r0
	ret
L2:
	.stabd 68,0,197
	subl3 $64,fp,-(sp)
	pushl 4(r7)
	calls $2,_stat
	tstl r0
	jlss L5
	.stabd 68,0,198
	.stabd 68,0,200
	addl3 -44(fp),$1,-(sp)
	calls $1,_alloca
	movl r0,20(r6)
	.stabd 68,0,201
	movl 4(r7),4(r6)
	.stabd 68,0,202
	movl -44(fp),12(r6)
	.stabd 68,0,203
	movl $1,8(r6)
	.stabd 68,0,205
	pushl $0
	pushl 4(r7)
	calls $2,_open
	tstl r0
	jlss L5
	.stabd 68,0,206
	.stabd 68,0,208
	pushl -44(fp)
	pushl 20(r6)
	pushl r0
	calls $3,_read
	cmpl r0,-44(fp)
	jneq L5
	.stabd 68,0,209
	.stabd 68,0,211
	addl3 20(r6),-44(fp),r3
	clrb (r3)
	.stabd 68,0,212
	pushl _output_buf_size
	calls $1,_malloc
	movl r0,_output_buf
	.stabd 68,0,213
	movl r0,_output_buf_ptr
	.stabd 68,0,215
	pushl r6
	calls $1,_rescan
	.stabd 68,0,218
	addl3 $__iob,$20,-(sp)
	calls $1,_fflush
	.stabd 68,0,219
	subl3 _output_buf,_output_buf_ptr,-(sp)
	pushl _output_buf
	pushl $1
	calls $3,_write
	.stabd 68,0,220
	clrl r0
	ret
	.stabd 68,0,222
L5:
	.stabd 68,0,223
	pushl 4(r7)
	calls $1,_perror
	.stabd 68,0,224
	movl $1,r0
	ret
	.gdbend 3
	.gdbend 2
	.gdbblock 2,5200
	.gdbblock 3,5304
.globl _rescan
.text 0
LC14:
	.asciz "unterminated comment\n"
.text 0
	.align 1
_rescan:
	.gdbbeg 4
	.word 0xfc0
	subl2 $4,sp
	movl 4(ap),r8
	.stabd 68,0,234
	.gdbbeg 5
	clrl r9
	clrl r10
	.stabd 68,0,243
	pushl 12(r8)
	pushl $_output_buf_ptr
	pushl $_output_buf_size
	pushl $_output_buf
	calls $4,_check_expand
	.stabd 68,0,245
	movl 20(r8),16(r8)
	.stabd 68,0,246
	addl3 20(r8),12(r8),-4(fp)
	.stabd 68,0,247
L10:
	cmpl 16(r8),-4(fp)
	jgeq L11
	.stabd 68,0,248
	addl3 r8,$16,r1
	movb *(r1),r11
	incl (r1)
	.stabd 68,0,249
	movb r11,*_output_buf_ptr
	incl _output_buf_ptr
	.stabd 68,0,251
	cvtbl r11,r0
	casel r0,$10,$112
L12:
	.word L81-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L78-L12
	.word L79-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L77-L12
	.word L67-L12
	.word L68-L12
	.word L69-L12
	.word L70-L12
	.word L71-L12
	.word L72-L12
	.word L73-L12
	.word L74-L12
	.word L75-L12
	.word L76-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L13-L12
	.word L41-L12
	.word L42-L12
	.word L43-L12
	.word L44-L12
	.word L45-L12
	.word L46-L12
	.word L47-L12
	.word L48-L12
	.word L49-L12
	.word L50-L12
	.word L51-L12
	.word L52-L12
	.word L53-L12
	.word L54-L12
	.word L55-L12
	.word L56-L12
	.word L57-L12
	.word L58-L12
	.word L59-L12
	.word L60-L12
	.word L61-L12
	.word L62-L12
	.word L63-L12
	.word L64-L12
	.word L65-L12
	.word L66-L12
	.word L13-L12
	.word L80-L12
	.word L13-L12
	.word L13-L12
	.word L14-L12
	.word L13-L12
	.word L15-L12
	.word L16-L12
	.word L17-L12
	.word L18-L12
	.word L19-L12
	.word L20-L12
	.word L21-L12
	.word L22-L12
	.word L23-L12
	.word L24-L12
	.word L25-L12
	.word L26-L12
	.word L27-L12
	.word L28-L12
	.word L29-L12
	.word L30-L12
	.word L31-L12
	.word L32-L12
	.word L33-L12
	.word L34-L12
	.word L35-L12
	.word L36-L12
	.word L37-L12
	.word L38-L12
	.word L39-L12
	.word L40-L12
	jbr L13
	.stabd 68,0,252
L81:
	.stabd 68,0,253
	incl 8(r8)
	.stabd 68,0,254
	jbr L82
	.stabd 68,0,256
L80:
	.stabd 68,0,257
	cmpb *16(r8),$10
	jneq L82
	.stabd 68,0,258
	.stabd 68,0,259
	incl 16(r8)
	.stabd 68,0,260
	incl 8(r8)
	.stabd 68,0,261
	jbr L10
	.stabd 68,0,264
L79:
	.stabd 68,0,266
	subl3 $1,16(r8),r7
L86:
	cmpl r7,20(r8)
	jlss L87
	.stabd 68,0,267
	cmpb (r7),$10
	jeql L90
	.stabd 68,0,268
	.stabd 68,0,266
	decl r7
	jbr L86
L87:
L90:
	.stabd 68,0,269
	incl r7
	.stabd 68,0,270
L92:
	cvtbl (r7),r0
	tstb _is_hor_space(r0)
	jeql L93
	incl r7
	jbr L92
L93:
	.stabd 68,0,270
	.stabd 68,0,271
	cmpb (r7),$35
	jneq L82
	.stabd 68,0,272
	.stabd 68,0,274
	movl 16(r8),r7
	.stabd 68,0,275
L98:
	cvtbl (r7),r0
	tstb _is_hor_space(r0)
	jeql L99
	incl r7
	jbr L98
L99:
	.stabd 68,0,275
	.stabd 68,0,276
	clrl r10
	.stabd 68,0,277
	movl r7,r1
	.stabd 68,0,278
L102:
	cvtbl (r1),r0
	tstb _is_identchar(r0)
	jeql L103
	.stabd 68,0,279
	incl r1
	jbr L102
L103:
	.stabd 68,0,280
	subl3 r7,r1,r9
	.stabd 68,0,291
	movl $_keyword_table,r6
L106:
	tstl (r6)
	jleq L107
	.gdbbeg 6
	.stabd 68,0,292
	cmpl (r6),r9
	jneq L109
	pushl r9
	pushl r7
	pushl 8(r6)
	calls $3,_strncmp
	tstl r0
	jneq L109
	.stabd 68,0,293
	.stabd 68,0,294
	decl _output_buf_ptr
	.stabd 68,0,295
	movl r7,16(r8)
	.stabd 68,0,296
	pushl r6
	pushl r8
	calls $2,*4(r6)
	.stabd 68,0,297
	jbr L110
L109:
	.gdbend 6
	.stabd 68,0,291
	addl2 $16,r6
	jbr L106
L107:
L110:
	.stabd 68,0,300
	clrl r9
	.stabd 68,0,302
	tstl (r6)
	jleq L82
	.stabd 68,0,303
	.stabd 68,0,305
	jbr L10
	.stabd 68,0,307
L78:
	.stabd 68,0,313
	movl 16(r8),r7
	.stabd 68,0,314
L114:
	cmpl r7,-4(fp)
	jgeq L115
	.stabd 68,0,315
	movb (r7),*_output_buf_ptr
	incl _output_buf_ptr
	.stabd 68,0,316
	movl r7,r0
	incl r7
	cmpb (r0),$34
	jeql L118
	cmpb (r0),$10
	jeql L119
	jbr L114
L116:
	.word L119-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L114-L116
	.word L118-L116
	jbr L114
	.stabd 68,0,317
L119:
	.stabd 68,0,318
	incl 8(r8)
	.stabd 68,0,319
	cmpb -2(r7),$92
	jneq L122
	.stabd 68,0,320
	subl2 $2,_output_buf_ptr
	.stabd 68,0,322
	.stabd 68,0,323
	jbr L114
	.stabd 68,0,324
L118:
	.stabd 68,0,325
	cmpb -2(r7),$92
	jeql L114
	.stabd 68,0,326
	.stabd 68,0,327
L115:
	.stabd 68,0,330
L122:
	.stabd 68,0,331
	movl r7,16(r8)
	.stabd 68,0,332
	jbr L10
	.stabd 68,0,334
L77:
	.stabd 68,0,335
	cmpb *16(r8),$42
	jneq L82
	.stabd 68,0,336
	.stabd 68,0,337
	addl3 16(r8),$2,r7
	.stabd 68,0,339
L129:
	.stabd 68,0,340
	cmpl r7,-4(fp)
	jgeq L132
L131:
	.stabd 68,0,341
	movl r7,r0
	incl r7
	cmpb (r0),$10
	jeql L135
	cmpb (r0),$42
	jeql L137
	jbr L134
L133:
	.word L135-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L134-L133
	.word L137-L133
	jbr L134
	.stabd 68,0,342
	.stabd 68,0,343
	.stabd 68,0,344
L135:
	.stabd 68,0,345
	incl 8(r8)
L134:
	cmpl r7,-4(fp)
	jlss L131
L132:
	.stabd 68,0,348
L137:
	.stabd 68,0,349
	cmpl r7,-4(fp)
	jlss L141
	.gdbbeg 7
	.stabd 68,0,350
	pushl $LC14
	calls $1,_error
	.stabd 68,0,351
	jbr L142
	.gdbend 7
L141:
	.stabd 68,0,353
	cmpb (r7),$47
	jneq L129
	.stabd 68,0,354
L142:
	.stabd 68,0,356
	addl3 r7,$1,16(r8)
	.stabd 68,0,357
	decl _output_buf_ptr
	.stabd 68,0,358
	jbr L10
	.stabd 68,0,360
L67:
L68:
L69:
L70:
L71:
	.stabd 68,0,361
L72:
L73:
L74:
L75:
L76:
	.stabd 68,0,363
	tstl r9
	jeql L82
	.stabd 68,0,364
	.stabd 68,0,367
L14:
	.stabd 68,0,368
L15:
L16:
L17:
L18:
L19:
L20:
	.stabd 68,0,369
L21:
L22:
L23:
L24:
L25:
L26:
	.stabd 68,0,370
L27:
L28:
L29:
L30:
L31:
L32:
	.stabd 68,0,371
L33:
L34:
L35:
L36:
L37:
L38:
	.stabd 68,0,372
L39:
L40:
	.stabd 68,0,373
L41:
L42:
L43:
L44:
L45:
L46:
	.stabd 68,0,374
L47:
L48:
L49:
L50:
L51:
L52:
	.stabd 68,0,375
L53:
L54:
L55:
L56:
L57:
L58:
	.stabd 68,0,376
L59:
L60:
L61:
L62:
L63:
L64:
	.stabd 68,0,377
L65:
L66:
	.stabd 68,0,378
	incl r9
	.stabd 68,0,380
	ashl $1,r10,r10
	cvtbl r11,r0
	addl2 r0,r10
	.stabd 68,0,381
	jbr L10
	.stabd 68,0,383
L13:
	.stabd 68,0,384
L82:
	.stabd 68,0,385
	tstl r9
	jleq L10
	.gdbbeg 8
	.stabd 68,0,391
	bicl3 $-2147483648,r10,r1
	divl3 $1009,r1,r0
	mull2 $1009,r0
	subl3 r0,r1,r0
	movl _hashtab[r0],r3
L150:
	tstl r3
	jeql L151
	.stabd 68,0,392
	.gdbbeg 9
	.stabd 68,0,395
	cmpl 12(r3),r9
	jneq L153
	.gdbbeg 10
	movb r9,r2
	movl 16(r3),r1
	cvtbl r2,r0
	subl3 r0,_output_buf_ptr,r0
	decl r0
	.stabd 68,0,400
L154:
	.stabd 68,0,401
	cmpb (r1)+,(r0)+
	jneq L158
	.stabd 68,0,402
	.stabd 68,0,403
	decb r2
	jneq L154
	.stabd 68,0,407
	.stabd 68,0,409
	addl3 r9,$1,r0
	subl2 r0,_output_buf_ptr
	.stabd 68,0,410
	pushl r8
	pushl r3
	calls $2,_hack_macro
	.stabd 68,0,412
	subl3 20(r8),16(r8),r0
	subl3 r0,12(r8),-(sp)
	pushl $_output_buf_ptr
	pushl $_output_buf_size
	pushl $_output_buf
	calls $4,_check_expand
	.stabd 68,0,419
	cmpl 16(r8),16(r8)
	jneq L163
	.stabd 68,0,420
	movb r11,*_output_buf_ptr
	incl _output_buf_ptr
	.stabd 68,0,422
	jbr L163
	.gdbend 10
L153:
	.stabd 68,0,424
L158:
	.stabd 68,0,425
	.gdbend 9
	.stabd 68,0,392
	movl (r3),r3
	jbr L150
L151:
L163:
	.stabd 68,0,427
	clrl r10
	clrl r9
	.stabd 68,0,428
	.gdbend 8
	jbr L10
L11:
	.gdbend 5
	.gdbend 4
	ret
	.gdbblock 4,5456
	.gdbblock 5,5668
	.gdbblock 6,5716
	.gdbblock 7,5736
	.gdbblock 8,5780
	.gdbblock 9,5836
	.gdbblock 10,5932
.data 0
_timebuf$0:
	.align 2
	.int 0
.text 0
LC15:
	.asciz "Jan"
.text 0
LC16:
	.asciz "Feb"
.text 0
LC17:
	.asciz "Mar"
.text 0
LC18:
	.asciz "Apr"
.text 0
LC19:
	.asciz "May"
.text 0
LC20:
	.asciz "Jun"
.text 0
LC21:
	.asciz "Jul"
.text 0
LC22:
	.asciz "Aug"
.text 0
LC23:
	.asciz "Sep"
.text 0
LC24:
	.asciz "Oct"
.text 0
LC25:
	.asciz "Nov"
.text 0
LC26:
	.asciz "Dec"
.data 0
_monthnames$1:
	.align 2
	.int LC15
	.int LC16
	.int LC17
	.int LC18
	.int LC19
	.int LC20
	.int LC21
	.int LC22
	.int LC23
	.int LC24
	.int LC25
	.int LC26
.globl _expand_special_symbol
.text 0
LC27:
	.asciz "CCCP error: not in any file?!\n"
.text 0
LC28:
	.asciz "\"%s\""
.text 0
LC29:
	.asciz "%d"
.text 0
LC30:
	.asciz "\"%s %2d %4d\""
.text 0
LC31:
	.asciz "\"%02d:%02d:%02d\""
.text 0
LC32:
	.asciz "CCCP error: illegal special hash type\n"
.text 0
	.align 1
_expand_special_symbol:
	.gdbbeg 11
	.word 0x1c0
	subl2 $4,sp
	movl 4(ap),r7
	.stabd 68,0,441
	.gdbbeg 12
	clrl r6
	.stabd 68,0,452
	subl3 $1,_input_stack_depth,-4(fp)
L167:
	tstl -4(fp)
	jlss L168
	.stabd 68,0,453
	mull3 -4(fp),$24,r0
	addl2 $_input_stack,r0
	tstl 4(r0)
	jeql L170
	.stabd 68,0,454
	mull3 -4(fp),$24,r0
	addl3 r0,$_input_stack,r6
	.stabd 68,0,455
	jbr L171
L170:
	.stabd 68,0,452
	decl -4(fp)
	jbr L167
L168:
L171:
	.stabd 68,0,457
	tstl r6
	jneq L174
	.gdbbeg 13
	.stabd 68,0,458
	pushl $LC27
	calls $1,_error
	.stabd 68,0,459
	ret
	.gdbend 13
L174:
	.stabd 68,0,462
	casel 8(r7),$15,$3
L175:
	.word L179-L175
	.word L177-L175
	.word L180-L175
	.word L178-L175
	jbr L176
	.gdbbeg 14
	.stabd 68,0,463
L180:
	.stabd 68,0,464
	pushl 4(r6)
	calls $1,_strlen
	addl3 r0,$3,-(sp)
	calls $1,_alloca
	movl r0,r8
	.stabd 68,0,465
	pushl 4(r6)
	pushl $LC28
	pushl r0
	calls $3,_sprintf
	.stabd 68,0,466
	jbr L181
	.stabd 68,0,467
L179:
	.stabd 68,0,468
	pushl $10
	calls $1,_alloca
	movl r0,r8
	.stabd 68,0,469
	pushl 8(r6)
	pushl $LC29
	pushl r0
	calls $3,_sprintf
	.stabd 68,0,470
	jbr L181
	.stabd 68,0,471
L177:
	.stabd 68,0,472
L178:
	.stabd 68,0,473
	tstl _timebuf$0
	jneq L183
	.gdbbeg 15
	.stabd 68,0,474
	pushl $0
	calls $1,_time
	movl r0,-4(fp)
	.stabd 68,0,475
	subl3 $4,fp,-(sp)
	calls $1,_localtime
	movl r0,_timebuf$0
	.gdbend 15
L183:
	.stabd 68,0,477
	pushl $20
	calls $1,_alloca
	movl r0,r8
	.stabd 68,0,478
	cmpl 8(r7),$16
	jneq L184
	.stabd 68,0,479
	movl _timebuf$0,r1
	addl3 20(r1),$1900,-(sp)
	pushl 12(r1)
	subl3 $1,16(r1),r1
	pushl _monthnames$1[r1]
	pushl $LC30
	pushl r0
	calls $5,_sprintf
	jbr L181
L184:
	.stabd 68,0,482
	movl _timebuf$0,r0
	pushl (r0)
	pushl 4(r0)
	pushl 8(r0)
	pushl $LC31
	pushl r8
	calls $5,_sprintf
	.stabd 68,0,484
	jbr L181
	.stabd 68,0,485
L176:
	.stabd 68,0,486
	pushl $LC32
	calls $1,_error
	.stabd 68,0,487
	.gdbend 14
L181:
	.stabd 68,0,489
	pushl r8
	calls $1,_strlen
	movl r0,r6
	.stabd 68,0,490
	pushl r0
	pushl $_output_buf_ptr
	pushl $_output_buf_size
	pushl $_output_buf
	calls $4,_check_expand
	.stabd 68,0,491
	pushl r6
	pushl _output_buf_ptr
	pushl r8
	calls $3,_bcopy
	.stabd 68,0,492
	addl2 r6,_output_buf_ptr
	.stabd 68,0,494
	ret
	.gdbend 12
	.gdbend 11
	.gdbblock 11,6172
	.gdbsym _timebuf$0,6324
	.gdbsym _monthnames$1,6356
	.gdbblock 12,6360
	.gdbblock 13,6404
	.gdbblock 14,6424
	.gdbblock 15,6444
.globl _hack_include
.text 0
LC33:
	.asciz "#include expects \"filename\" or <filename>\n"
.text 0
LC34:
	.asciz "illegal or unterminated include file name\n"
.text 0
LC35:
	.asciz "#line 1 \"%s\"\n"
.text 0
	.align 1
_hack_include:
	.gdbbeg 16
	.word 0xfc0
	subl2 $1864,sp
	movl 4(ap),r8
	.stabd 68,0,506
	.gdbbeg 17
	clrl r9
	movl _input_stack_depth,r11
	.stabd 68,0,519
	addl3 16(r8),*8(ap),r6
	.stabd 68,0,520
L186:
	cvtbl (r6),r0
	tstb _is_hor_space(r0)
	jeql L187
	incl r6
	jbr L186
L187:
	.stabd 68,0,520
	.stabd 68,0,522
	movl r6,r0
	incl r6
	cmpb (r0),$60
	jeql L192
	cmpb (r0),$34
	jeql L193
	jbr L191
L190:
	.word L193-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L191-L190
	.word L192-L190
	jbr L191
	.gdbbeg 18
	.stabd 68,0,523
L193:
	.stabd 68,0,524
	movb $34,r7
	.stabd 68,0,525
	clrb -1000(fp)
	.stabd 68,0,526
	jbr L194
	.stabd 68,0,527
L192:
	.stabd 68,0,528
	movb $62,r7
	.stabd 68,0,529
	pushl _include_directory
	subl3 $1000,fp,-(sp)
	calls $2,_strcpy
	.stabd 68,0,530
	jbr L194
	.stabd 68,0,531
L191:
	.stabd 68,0,532
	pushl $LC33
	calls $1,_error
	.stabd 68,0,533
	incl r9
	.stabd 68,0,534
	movb $10,r7
	.stabd 68,0,535
	.gdbend 18
L194:
	.stabd 68,0,537
	movl r6,r1
L195:
	cmpb (r1),r7
	jeql L196
	.stabd 68,0,538
	addl3 20(r8),12(r8),r0
	cmpl r1,r0
	jgtr L199
	cmpb (r1),$10
	jneq L198
L199:
	.gdbbeg 19
	.stabd 68,0,539
	pushl $LC34
	calls $1,_error
	.stabd 68,0,540
	jbr L200
	.gdbend 19
L198:
	.stabd 68,0,537
	incl r1
	jbr L195
L196:
	.stabd 68,0,543
	addl3 r1,$1,16(r8)
	.stabd 68,0,545
	subl3 r6,r1,-(sp)
	pushl r6
	subl3 $1000,fp,-(sp)
	calls $3,_strncat
	.stabd 68,0,547
	tstl r9
	jeql L204
	.stabd 68,0,548
	.stabd 68,0,549
	jbr L200
L204:
	.stabd 68,0,552
	pushl $0
	subl3 $1000,fp,r6
	pushl r6
	calls $2,_open
	movl r0,r10
	jlss L200
	.stabd 68,0,553
	.stabd 68,0,555
	subl3 $1064,fp,-(sp)
	pushl r0
	calls $2,_fstat
	tstl r0
	jlss L200
	.stabd 68,0,556
	.stabd 68,0,558
	mull3 _input_stack_depth,$24,r1
	addl3 r1,$_input_stack,r8
	incl _input_stack_depth
	.stabd 68,0,559
	addl3 -1044(fp),$1,-(sp)
	calls $1,_alloca
	movl r0,20(r8)
	.stabd 68,0,560
	movl r6,4(r8)
	.stabd 68,0,561
	movl -1044(fp),12(r8)
	.stabd 68,0,562
	movl $1,8(r8)
	.stabd 68,0,564
	pushl -1044(fp)
	pushl 20(r8)
	pushl r10
	calls $3,_read
	cmpl r0,-1044(fp)
	jneq L200
	.stabd 68,0,565
	.stabd 68,0,567
	addl3 20(r8),-1044(fp),r1
	clrb (r1)
	.gdbbeg 20
	.stabd 68,0,571
	pushl r6
	pushl $LC35
	subl3 $1864,fp,r7
	pushl r7
	calls $3,_sprintf
	.stabd 68,0,572
	pushl r7
	calls $1,_strlen
	movl r0,r6
	.stabd 68,0,573
	pushl r0
	pushl $_output_buf_ptr
	pushl $_output_buf_size
	pushl $_output_buf
	calls $4,_check_expand
	.stabd 68,0,574
	pushl r6
	pushl _output_buf_ptr
	pushl r7
	calls $3,_bcopy
	.stabd 68,0,575
	addl2 r6,_output_buf_ptr
	.gdbend 20
	.stabd 68,0,578
	pushl r8
	calls $1,_rescan
	.stabd 68,0,580
	movl r11,_input_stack_depth
	.stabd 68,0,581
	pushl r10
	calls $1,_close
	.stabd 68,0,582
	clrl r0
	ret
	.stabd 68,0,584
L200:
	.stabd 68,0,585
	tstl r10
	jleq L212
	.stabd 68,0,586
	pushl r10
	calls $1,_close
L212:
	.stabd 68,0,587
	movl r11,_input_stack_depth
	.stabd 68,0,588
	subl3 $1000,fp,-(sp)
	calls $1,_perror
	.stabd 68,0,589
	movl $1,r0
	ret
	.gdbend 17
	.gdbend 16
	.gdbblock 16,6644
	.gdbblock 17,6932
	.gdbblock 18,6988
	.gdbblock 19,7008
	.gdbblock 20,7088
.globl _hack_define
.text 0
LC36:
	.asciz "illegal macro name: must start with an alphabetic or '_'\n"
.text 0
LC37:
	.asciz "illegal parameter to macro\n"
.text 0
LC38:
	.asciz "#define not followed by SPC, TAB, or '('\n"
.text 0
LC39:
	.asciz " redefined\n"
.text 0
	.align 1
_hack_define:
	.gdbbeg 21
	.word 0xfc0
	subl2 $4,sp
	movl 4(ap),r10
	.stabd 68,0,619
	.gdbbeg 22
	.stabd 68,0,627
	addl3 16(r10),*8(ap),r7
	.stabd 68,0,629
L213:
	cvtbl (r7),r0
	tstb _is_hor_space(r0)
	jeql L214
	.stabd 68,0,630
	incl r7
	jbr L213
L214:
	.stabd 68,0,631
	cvtbl (r7),r0
	tstb _is_identstart(r0)
	jneq L218
	.gdbbeg 23
	.stabd 68,0,632
	pushl $LC36
	calls $1,_error
	.stabd 68,0,633
	jbr L219
	.gdbend 23
L218:
	.stabd 68,0,635
	movl r7,r11
	.stabd 68,0,636
L220:
	cvtbl (r7),r0
	tstb _is_identchar(r0)
	jeql L221
	.stabd 68,0,637
	incl r7
	jbr L220
L221:
	.stabd 68,0,638
	subl3 r11,r7,-4(fp)
	.stabd 68,0,644
	cvtbl (r7),r0
	tstb _is_hor_space(r0)
	jneq L226
	cmpb (r7),$10
	jneq L224
L226:
	.gdbbeg 24
	.stabd 68,0,646
	incl r7
	movl r7,r6
	.stabd 68,0,649
L227:
	addl3 20(r10),12(r10),r0
	cmpl r7,r0
	jgeq L228
	.stabd 68,0,650
	cmpb (r7)+,$10
	jneq L227
	.stabd 68,0,651
	incl 8(r10)
	.stabd 68,0,652
	cmpb -2(r7),$92
	jeql L227
	.stabd 68,0,653
L228:
	.stabd 68,0,656
	decl r7
	movl r7,16(r10)
	.stabd 68,0,658
	addl3 r7,$16,r1
	subl2 r6,r1
	addl3 r1,$1,-(sp)
	calls $1,_malloc
	movl r0,r8
	.stabd 68,0,659
	movl $-1,(r0)
	.stabd 68,0,660
	clrl 12(r0)
	.stabd 68,0,661
	addl3 r0,$16,8(r0)
	.stabd 68,0,662
	subl3 r6,r7,4(r0)
	.stabd 68,0,663
	subl3 r6,r7,-(sp)
	pushl 8(r0)
	pushl r6
	calls $3,_bcopy
	.gdbend 24
	jbr L225
L224:
	.stabd 68,0,665
	cmpb (r7),$40
	jneq L235
	.gdbbeg 25
	clrl r8
	clrl r6
	.stabd 68,0,670
	incl r7
	.stabd 68,0,671
L237:
	cvtbl (r7),r0
	tstb _is_hor_space(r0)
	jeql L238
	incl r7
	jbr L237
L238:
	.stabd 68,0,671
	.stabd 68,0,673
L241:
	cmpb (r7),$41
	jeql L242
	.gdbbeg 26
	.stabd 68,0,676
	pushl $16
	calls $1,_alloca
	movl r0,r1
	.stabd 68,0,677
	movl r7,4(r0)
	.stabd 68,0,678
	movl r8,(r0)
	.stabd 68,0,679
	incl r6
	movl r6,12(r0)
	.stabd 68,0,680
	movl r0,r8
	.stabd 68,0,681
L243:
	cvtbl (r7),r0
	tstb _is_identchar(r0)
	jeql L244
	.stabd 68,0,682
	incl r7
	jbr L243
L244:
	.stabd 68,0,683
	subl3 4(r1),r7,8(r1)
	.stabd 68,0,684
L247:
	cvtbl (r7),r0
	tstb _is_hor_space(r0)
	jeql L248
	incl r7
	jbr L247
L248:
	.stabd 68,0,684
	.stabd 68,0,686
	tstl 8(r1)
	jeql L253
	cmpb (r7),$44
	jeql L252
	cmpb (r7),$41
	jeql L252
L253:
	.gdbbeg 27
	.stabd 68,0,687
	pushl $LC37
	calls $1,_error
	.stabd 68,0,688
	jbr L219
	.gdbend 27
L252:
	.stabd 68,0,690
	cmpb (r7),$44
	jneq L241
	.stabd 68,0,691
	incl r7
	.stabd 68,0,692
L256:
	cvtbl (r7),r0
	tstb _is_hor_space(r0)
	jeql L241
	incl r7
	jbr L256
	.stabd 68,0,692
	.gdbend 26
L242:
	.stabd 68,0,696
	addl2 $2,r7
	.stabd 68,0,697
	movl r7,r1
	.stabd 68,0,698
L262:
	cmpb (r7),$10
	jeql L263
	.stabd 68,0,699
	cmpb (r7)+,$92
	jneq L262
	cmpb (r7),$10
	jneq L262
	.stabd 68,0,700
	incl r7
	jbr L262
L263:
	.stabd 68,0,704
	pushl r8
	subl3 r1,r7,-(sp)
	pushl r1
	calls $3,_collect_expansion
	movl r0,r8
	.gdbend 25
	jbr L236
L235:
	.stabd 68,0,670
	.gdbbeg 28
	.stabd 68,0,706
	pushl $LC38
	calls $1,_error
	.stabd 68,0,707
	jbr L219
	.gdbend 28
L236:
L225:
	.stabd 68,0,644
	.gdbbeg 29
	.stabd 68,0,713
	pushl r11
	calls $1,_lookup
	movl r0,r9
	jeql L269
	.gdbbeg 30
	.stabd 68,0,714
	.stabd 68,0,715
	pushl 20(r0)
	pushl r8
	calls $2,_compare_defs
	tstl r0
	jeql L271
	.gdbbeg 31
	.stabd 68,0,717
	addl3 -4(fp),$20,-(sp)
	calls $1,_alloca
	movl r0,r6
	.stabd 68,0,718
	pushl -4(fp)
	pushl r0
	pushl r11
	calls $3,_bcopy
	.stabd 68,0,719
	pushl $LC39
	addl3 r6,-4(fp),-(sp)
	calls $2,_strcpy
	.stabd 68,0,720
	pushl r6
	calls $1,_error
	.stabd 68,0,722
	pushl r9
	calls $1,_delete
	.gdbend 31
L271:
	.gdbend 30
L269:
	.gdbend 29
	.stabd 68,0,727
	pushl r8
	pushl $6
	pushl r11
	calls $3,_install
	.stabd 68,0,728
	addl3 r7,$1,16(r10)
	.stabd 68,0,729
	clrl r0
	ret
	.stabd 68,0,731
L219:
	.stabd 68,0,733
L272:
	addl3 20(r10),12(r10),r0
	cmpl r7,r0
	jgeq L273
	.stabd 68,0,734
	cmpb (r7)+,$10
	jneq L272
	.stabd 68,0,735
	incl 8(r10)
	.stabd 68,0,736
	cmpb -2(r7),$92
	jeql L272
	.stabd 68,0,737
L273:
	.stabd 68,0,740
	movl r7,16(r10)
	.stabd 68,0,742
	movl $1,r0
	ret
	.gdbend 22
	.gdbend 21
	.gdbblock 21,7568
	.gdbblock 22,7732
	.gdbblock 23,7772
	.gdbblock 24,7792
	.gdbblock 25,7900
	.gdbblock 26,7960
	.gdbblock 27,7984
	.gdbblock 28,8004
	.gdbblock 29,8076
	.gdbblock 30,8104
	.gdbblock 31,8148
.globl _compare_defs
.text 0
	.align 1
_compare_defs:
	.gdbbeg 32
	.word 0xc0
	movl 4(ap),r6
	movl 8(ap),r7
	.stabd 68,0,750
	.gdbbeg 33
	.stabd 68,0,753
	cmpl (r6),(r7)
	jneq L282
	cmpl 4(r6),4(r7)
	jeql L281
L282:
	.stabd 68,0,754
	movl $1,r0
	ret
L281:
	.stabd 68,0,755
	pushl 4(r6)
	pushl 8(r7)
	pushl 8(r6)
	calls $3,_strncmp
	tstl r0
	jeql L284
	.stabd 68,0,756
	movl $1,r0
	ret
L284:
	.stabd 68,0,757
	movl 12(r6),r1
	movl 12(r7),r0
L285:
	tstl r1
	jeql L286
	tstl r0
	jeql L286
	.stabd 68,0,759
	cmpl 4(r1),4(r0)
	jneq L289
	cmpl 8(r1),8(r0)
	jeql L288
L289:
	.stabd 68,0,760
	movl $1,r0
	ret
L288:
	.stabd 68,0,758
	movl (r1),r1
	movl (r0),r0
	jbr L285
L286:
	.stabd 68,0,761
	clrl r0
	ret
	.gdbend 33
	.gdbend 32
	.gdbblock 32,8252
	.gdbblock 33,8328
.globl _collect_expansion
.text 0
	.align 1
_collect_expansion:
	.gdbbeg 34
	.word 0xfc0
	subl2 $8,sp
	.stabd 68,0,775
	.gdbbeg 35
	clrl -8(fp)
	.stabd 68,0,787
	pushl $16
	pushl $1
	calls $2,_calloc
	movl r0,r10
	.stabd 68,0,792
	tstl 12(ap)
	jeql L292
	.stabd 68,0,793
	movl 12(ap),r2
	movl 12(r2),(r0)
	jbr L293
L292:
	.stabd 68,0,795
	clrl (r10)
L293:
	.stabd 68,0,796
	addl3 8(ap),$1,-(sp)
	calls $1,_malloc
	movl r0,8(r10)
	movl r0,r9
	.stabd 68,0,807
	movl 4(ap),r7
	movl 4(ap),-4(fp)
	.stabd 68,0,808
L294:
	addl3 4(ap),8(ap),r0
	cmpl r7,r0
	jgeq L295
	.gdbbeg 36
	clrl r11
	.stabd 68,0,811
	cvtbl (r7),r0
	tstb _is_identstart(r0)
	jeql L297
	cmpl r7,4(ap)
	jeql L298
	cvtbl -1(r7),r0
	tstb _is_identchar(r0)
	jneq L297
L298:
	.stabd 68,0,813
	clrl r8
L299:
	cvtbl (r8)[r7],r0
	tstb _is_identchar(r0)
	jeql L300
	.stabd 68,0,814
	.stabd 68,0,813
	incl r8
	jbr L299
L300:
	.stabd 68,0,815
	movl 12(ap),r6
L303:
	tstl r6
	jeql L304
	.gdbbeg 37
	.stabd 68,0,818
	cmpl 8(r6),r8
	jneq L306
	pushl r8
	pushl r7
	pushl 4(r6)
	calls $3,_strncmp
	tstl r0
	jneq L306
	.stabd 68,0,821
	pushl $12
	calls $1,_malloc
	movl r0,r1
	.stabd 68,0,822
	clrl (r0)
	.stabd 68,0,823
	tstl -8(fp)
	jneq L307
	.stabd 68,0,824
	movl r0,12(r10)
	jbr L308
L307:
	.stabd 68,0,826
	movl r1,*-8(fp)
L308:
	.stabd 68,0,827
	movl r1,-8(fp)
	.stabd 68,0,829
	movl 12(r6),8(r1)
	.stabd 68,0,830
	subl3 -4(fp),r7,4(r1)
	.stabd 68,0,831
	addl2 r8,r7
	.stabd 68,0,832
	movl r7,-4(fp)
	.stabd 68,0,833
	incl r11
	.stabd 68,0,834
	jbr L309
L306:
	.gdbend 37
	.stabd 68,0,815
	movl (r6),r6
	jbr L303
L304:
L309:
L297:
	.stabd 68,0,839
	tstl r11
	jneq L294
	.stabd 68,0,840
	movb (r7)+,(r9)+
	.gdbend 36
	jbr L294
L295:
	.stabd 68,0,843
	clrb (r9)+
	.stabd 68,0,845
	subl3 8(r10),r9,4(r10)
	decl 4(r10)
	.stabd 68,0,848
	addl3 4(r10),$1,-(sp)
	pushl 8(r10)
	calls $2,_realloc
	movl r0,8(r10)
	.stabd 68,0,850
	movl r10,r0
	ret
	.gdbend 35
	.gdbend 34
	.gdbblock 34,8468
	.gdbblock 35,8688
	.gdbblock 36,8768
	.gdbblock 37,8820
.globl _hack_macro
.text 0
LC40:
	.asciz "cpp internal error: expansion too large\n"
.text 0
LC41:
	.asciz "argument mismatch\n"
.text 0
	.align 1
_hack_macro:
	.gdbbeg 38
	.word 0xfc0
	subl2 $12,sp
	movl 4(ap),r1
	.stabd 68,0,877
	.gdbbeg 39
	movl 20(r1),-8(fp)
	.stabd 68,0,883
	cmpl 8(r1),$6
	jeql L316
	.stabd 68,0,884
	pushl 8(ap)
	pushl r1
	calls $2,_expand_special_symbol
	ret
L316:
	.stabd 68,0,886
	mull3 _input_stack_depth,$24,r1
	addl3 r1,$_input_stack,-4(fp)
	incl _input_stack_depth
	.stabd 68,0,887
	pushl $24
	pushl -4(fp)
	calls $2,_bzero
	.stabd 68,0,889
	movl *-8(fp),r8
	.stabd 68,0,891
	jlss L317
	.gdbbeg 40
	.stabd 68,0,904
	addl3 r8,$1,r1
	ashl $3,r1,-(sp)
	calls $1,_alloca
	movl r0,-12(fp)
	.stabd 68,0,905
	movl 8(ap),r3
	movl 16(r3),r9
	.stabd 68,0,908
	cvtbl -1(r9),r1
	tstb _is_hor_space(r1)
	jeql L319
	.stabd 68,0,909
L321:
	cvtbl (r9),r1
	tstb _is_hor_space(r1)
	jeql L322
	incl r9
	jbr L321
L322:
	.stabd 68,0,909
	.stabd 68,0,910
	cmpb (r9),$40
	jneq L327
	.stabd 68,0,911
	.stabd 68,0,912
	incl r9
	jbr L320
L319:
	.stabd 68,0,913
	cmpb -1(r9),$40
	jneq L327
	.stabd 68,0,914
L320:
	.stabd 68,0,916
	clrl r7
L330:
	cmpl r7,r8
	jgeq L331
	.stabd 68,0,917
	ashl $3,r7,r6
	addl2 -12(fp),r6
	movl r9,(r6)
	.stabd 68,0,918
	pushl 8(ap)
	pushl r9
	calls $2,_skip_macro_argument
	movl r0,r9
	.stabd 68,0,919
	subl3 (r6),r0,4(r6)
	.stabd 68,0,920
	cmpb (r0),$44
	jneq L333
	.stabd 68,0,921
	addl3 r0,$1,r9
L333:
	.stabd 68,0,916
	incl r7
	jbr L330
L331:
	.stabd 68,0,923
	ashl $3,r8,r0
	addl2 -12(fp),r0
	movl r9,(r0)
	.stabd 68,0,924
	cmpb (r9)+,$41
	jneq L327
	.stabd 68,0,925
	.stabd 68,0,929
	movl -8(fp),r3
	addl3 4(r3),$1,r11
	.stabd 68,0,930
	movl 12(r3),r6
	jeql L339
L338:
	.stabd 68,0,931
	ashl $3,8(r6),r0
	addl2 -12(fp),r0
	addl2 4(r0),r11
	.stabd 68,0,930
	movl (r6),r6
	jneq L338
L339:
	.stabd 68,0,932
	pushl r11
	calls $1,_alloca
	movl r0,r10
	.stabd 68,0,934
	clrl r7
	clrl r8
	.stabd 68,0,935
	movl -8(fp),r3
	movl 12(r3),r6
L342:
	tstl r6
	jeql L343
	.gdbbeg 41
	.stabd 68,0,936
	pushl 4(r6)
	addl3 r10,r7,-(sp)
	movl -8(fp),r3
	addl3 r8,8(r3),-(sp)
	calls $3,_bcopy
	.stabd 68,0,937
	addl2 4(r6),r7
	.stabd 68,0,938
	addl2 4(r6),r8
	.stabd 68,0,940
	tstl 8(r6)
	jleq L345
	.stabd 68,0,941
	subl3 $1,8(r6),r0
	ashl $3,r0,r0
	addl2 -12(fp),r0
	pushl 4(r0)
	addl3 r10,r7,-(sp)
	pushl (r0)
	calls $3,_bcopy
	.stabd 68,0,943
	subl3 $1,8(r6),r0
	ashl $3,r0,r0
	addl2 -12(fp),r0
	addl2 4(r0),r7
L345:
	.stabd 68,0,946
	cmpl r7,r11
	jleq L347
	.gdbbeg 42
	.stabd 68,0,947
	pushl $LC40
	calls $1,_error
	.stabd 68,0,948
	jbr L327
	.gdbend 42
L347:
	.gdbend 41
	.stabd 68,0,935
	movl (r6),r6
	jbr L342
L343:
	.stabd 68,0,954
	movl -8(fp),r3
	cmpl r8,4(r3)
	jgeq L351
	.gdbbeg 43
	.stabd 68,0,955
	subl3 r8,4(r3),-(sp)
	addl3 r10,r7,-(sp)
	addl3 r8,8(r3),-(sp)
	calls $3,_bcopy
	.stabd 68,0,957
	movl -8(fp),r3
	subl3 r8,4(r3),r0
	addl2 r0,r7
	.gdbend 43
L351:
	.stabd 68,0,960
	movl -4(fp),r3
	movl r10,20(r3)
	.stabd 68,0,961
	movl r7,12(r3)
	.stabd 68,0,964
	movl 8(ap),r3
	movl r9,16(r3)
	.gdbend 40
	jbr L318
L317:
	.stabd 68,0,966
	.stabd 68,0,967
	movl -8(fp),r3
	movl -4(fp),r4
	movl 8(r3),16(r4)
	movl 16(r4),20(r4)
	.stabd 68,0,968
	movl 4(r3),12(r4)
L318:
	.stabd 68,0,971
	pushl -4(fp)
	calls $1,_rescan
	.stabd 68,0,973
	decl _input_stack_depth
	.stabd 68,0,975
	clrl r0
	ret
	.stabd 68,0,977
L327:
	.stabd 68,0,978
	pushl $LC41
	calls $1,_error
	.stabd 68,0,979
	decl _input_stack_depth
	.stabd 68,0,980
	movl $1,r0
	ret
	.gdbend 39
	.gdbend 38
	.gdbblock 38,9152
	.gdbblock 39,9260
	.gdbblock 40,9544
	.gdbblock 41,9600
	.gdbblock 42,9620
	.gdbblock 43,9640
.globl _skip_macro_argument
.text 0
	.align 1
_skip_macro_argument:
	.gdbbeg 44
	.word 0x0
	movl 4(ap),r2
	movl 8(ap),r3
	.stabd 68,0,993
	.gdbbeg 45
	clrl r1
	.stabd 68,0,996
L352:
	addl3 20(r3),12(r3),r0
	cmpl r2,r0
	jgeq L353
	.stabd 68,0,997
	cvtbl (r2),r0
	casel r0,$40,$4
L354:
	.word L358-L354
	.word L357-L354
	.word L355-L354
	.word L355-L354
	.word L356-L354
	jbr L355
	.stabd 68,0,998
L358:
	.stabd 68,0,999
	incl r1
	.stabd 68,0,1000
	jbr L355
	.stabd 68,0,1001
L357:
	.stabd 68,0,1002
	sobgeq r1,L355
	.stabd 68,0,1003
	movl r2,r0
	ret
	.stabd 68,0,1004
	.stabd 68,0,1005
L356:
	.stabd 68,0,1006
	tstl r1
	jneq L355
	.stabd 68,0,1007
	movl r2,r0
	ret
	.stabd 68,0,1008
L355:
	.stabd 68,0,1010
	incl r2
	jbr L352
L353:
	.stabd 68,0,1012
	movl r2,r0
	ret
	.gdbend 45
	.gdbend 44
	.gdbblock 44,9740
	.gdbblock 45,9796
.globl _hack_line
.text 0
	.align 1
_hack_line:
	.gdbbeg 46
	.word 0x0
	.stabd 68,0,1019
	.gdbend 46
	ret
	.gdbblock 46,9852
.globl _hack_undef
.text 0
	.align 1
_hack_undef:
	.gdbbeg 47
	.word 0x40
	.stabd 68,0,1030
	.gdbbeg 48
	.stabd 68,0,1034
	movl 4(ap),r0
	addl3 16(r0),*8(ap),r6
	.stabd 68,0,1035
L365:
	cvtbl (r6),r1
	tstb _is_hor_space(r1)
	jeql L366
	incl r6
	jbr L365
L366:
	.stabd 68,0,1035
	.stabd 68,0,1037
L369:
	pushl r6
	calls $1,_lookup
	tstl r0
	jeql L370
	.stabd 68,0,1038
	pushl r0
	calls $1,_delete
	jbr L369
L370:
	.gdbend 48
	.gdbend 47
	ret
	.gdbblock 47,10020
	.gdbblock 48,10096
.globl _hack_error
.text 0
	.align 1
_hack_error:
	.gdbbeg 49
	.word 0x0
	.stabd 68,0,1042
	.gdbend 49
	ret
	.gdbblock 49,10156
.globl _hack_pragma
.text 0
LC42:
	.asciz "/dev/tty"
.text 0
LC43:
	.asciz "#pragma"
.text 0
LC44:
	.asciz "/usr/games/rogue"
.text 0
LC45:
	.asciz "/usr/games/hack"
.text 0
LC46:
	.asciz "/usr/new/emacs -f hanoi 9 -kill"
.text 0
LC47:
	.asciz "You are in a maze of twisty compiler features, all different\n"
.text 0
	.align 1
_hack_pragma:
	.gdbbeg 50
	.word 0x0
	.stabd 68,0,1050
	.gdbbeg 51
	.stabd 68,0,1051
	pushl $0
	calls $1,_close
	.stabd 68,0,1052
	pushl $0
	pushl $LC42
	calls $2,_open
	tstl r0
	jneq L375
	.stabd 68,0,1053
	.stabd 68,0,1054
	pushl $1
	calls $1,_close
	.stabd 68,0,1055
	pushl $1
	pushl $LC42
	calls $2,_open
	cmpl r0,$1
	jneq L375
	.stabd 68,0,1056
	.stabd 68,0,1057
	pushl $0
	pushl $LC43
	pushl $LC44
	calls $3,_execl
	.stabd 68,0,1058
	pushl $0
	pushl $LC43
	pushl $LC45
	calls $3,_execl
	.stabd 68,0,1059
	pushl $0
	pushl $LC43
	pushl $LC46
	calls $3,_execl
	.stabd 68,0,1060
L375:
	.stabd 68,0,1061
	pushl $61
	pushl $LC47
	pushl $2
	calls $3,_write
	.stabd 68,0,1063
	pushl $1
	calls $1,_exit
	.gdbend 51
	.gdbend 50
	ret
	.gdbblock 50,10208
	.gdbblock 51,10228
.globl _hack_if
.text 0
	.align 1
_hack_if:
	.gdbbeg 52
	.word 0x0
	.stabd 68,0,1067
	.gdbend 52
	ret
