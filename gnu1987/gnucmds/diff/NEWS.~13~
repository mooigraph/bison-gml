GNU Emacs NEWS -- history of user-visible changes.  27-Aug-1986
Copyright (C) 1986 Richard M. Stallman.
See the end for copying conditions.

Changes in Emacs 18

* Additional systems and machines are supported.

GNU Emacs now runs on Vax VMS (Vomit Making System).  However,
many facilities that are normally implemented by running subprocesses
do not work yet.  This includes listing a directory and sending mail.

In addition, the TI Nu machine running Unix system V, the AT&T 3b20,
and the Wicat, Masscomp and Gould Unix systems are now supported.  The
IBM PC-RT is supported under 4.2, but not yet under system V.  The GEC
93 is close to working.  See the file MACHINES for full status
information and machine-specific installation advice.

* Display changes.

When you delete or kill or alter text that reaches to the top of the
screen or above it, so that display would start in the middle of a
line, Emacs will usually attempt to scroll the text so that display
starts at the beginning of a line again.

* File loading changes.

`load' now considers all possible suffixes (`.elc', `.el' and none)
for each directory in `load-path' before going on to the next directory.
It now accepts an optional fourth argument which, if non-nil, says to
use no suffixes; then the file name must be given in full.  The search
of the directories in `load-path' goes on as usual in this case; but
it too can be prevented by passing an absolute file name.

The value of `load-path' no longer by default includes nil (meaning to
look in the current default directory).  The idea is that `load' should
be used to search the path only for libraries to be found in the standard
places.  If you want to override system libraries with your own, place
your own libraries in one special directory and add that directory to the
front of `load-path'.

The function `load' is no longer a command; that is to say, `M-x load'
is no longer allowed.  Instead, there are two commands for loading files.
`M-x load-library' is equivalent to the old meaning of `M-x load'.
`M-x load-file' reads a file name with completion and defaulting
and then loads exactly that file, with no searching and no suffixes.

* `tags-query-replace' and `tags-search' change.

These functions now display the name of the file being searched at the moment.

* `occur' output now serves as a menu.  `occur-menu' command deleted.

`M-x occur' now allows you to move quickly to any of the occurrences
listed.  Select the `*Occur*' buffer that contains the output of `occur',
move point to the occurrence you want, and type C-c C-c.
This will move point to the same occurrence in the buffer that the
occurrences were found in.

The command `occur-menu' is thus obsolete, and has been deleted.

* `edt-emulation-on' starts emulating DEC's EDT editor.

Do `edt-emulation-off' to return Emacs to normal.

* C-M-v in minibuffer.

If while in the minibuffer you request help in a way that uses a
window to display something, then until you exit the minibuffer C-M-v
in the minibuffer window scrolls the window of help.

For example, if you request a list of possible completions, C-M-v can
be used reliably to scroll the completion list.

* C mode indentation change.

The binding of Linefeed is no longer changed by C mode.  It once again
has its normal meaning, which is to insert a newline and then indent
afterward.

The old definition did one additional thing: it reindented the line
before the new newline.  This has been removed because it made the
command twice as slow.  The only time it was really useful was after the
insertion of an `else', since the fact of starting with `else' may change
the way that line is indented.  Now you will have to type TAB again
yourself to reindent the `else' properly.

If the variable `c-tab-always-indent' is set to `nil', the TAB command
in C mode, with no argument, will just insert a tab character if there
is non-whitespace preceding point on the current line.  Giving it a
prefix argument will force reindentation of the line (as well as
of the compound statement that begins after point, if any).

* Buffer-sorting commands.

Various M-x commands whose names start with `sort-' sort parts of
the region:

sort-lines	divides the region into lines and sorts them alphabetically.
sort-pages	divides into pages and sorts them alphabetically.
sort-paragraphs	divides into paragraphs and sorts them alphabetically.
sort-fields	divides into lines and sorts them alphabetically
		according to one field in the line.
		The numeric argument specifies which field (counting
		from field 1 at the beginning of the line).  Fields in a line
		are separated by whitespace.
sort-numeric-fields
		is similar but converts the specified fields to numbers
		and sorts them numerically.
sort-columns	divides into lines and sorts them according to the contents
		of a specified range of columns.

Refer to the self-documentation of these commands for full usage information.

* C-x C-v generalized

This command is now allowed even if the current buffer is not visiting
a file.  As usual, it kills the current buffer and replaces it with a
newly found file.

* M-x recover-file improved; auto save file names changed.

M-x recover-file now checks whether the last auto-save file is more
recent than the real visited file before offering to read in the
auto-save file.  If the auto-save file is newer, a directory listing
containing the two files is displayed while you are asked whether you
want the auto save file.

Auto save file names now by default have a `#' at the end as well
as at the beginning.  This is so that `*.c' in a shell command
will never match auto save files.

When you change the visited file name of a buffer, the auto save file
is now renamed to belong to the new visited file name.

You can customize the way auto save file names are made by redefining
the two functions `make-auto-save-file-name' and `auto-save-file-name-p',
both of which are defined in `files.el'.

* Telnet mode changes.

The telnet mode special commands have now been assigned to C-c keys.
Most of them are the same as in Shell mode.

* Picture mode changes.

The special picture-mode commands to specify the direction of cursor
motion after insertion have been moved to C-c keys.  The commands to
specify diagonal motion were already C-c keys; they are unchanged.
The keys to specify horizontal or vertical motion are now
C-c < (left), C-c > (right), C-c ^ (up) and C-c . (down).

* Nroff mode comments.

Comments are now supported in Nroff mode.  The standard comment commands
such as M-; and C-x ; know how to insert, align and delete comments
that start with backslash-doublequote.

* Modifying a buffer whose file is changed on disk is detected instantly.

On systems where clash detection (locking of files being edited) is
implemented, Emacs also checks the first time you modify a buffer
whether the file has changed on disk since it was last visited or saved.
If it has, you are asked to confirm that you want to change the buffer.

* New support for keypads and function keys.

There is now a first attempt at terminal-independent support for
keypad and function keys.

Emacs now defines a standard set of key-names for function and keypad
keys, and provides standard hooks for defining them.  Most of the
standard key-names have default definitions built into Emacs; you can
override these in a terminal-independent manner.  The default definitions
and the conventions for redefining them are in the file `lisp/keypad.el'.

These keys on the terminal normally work by sending sequences of
characters starting with ESC.  The exact sequences used vary from
terminal to terminal.  Emacs interprets them in two stages:
in the first stage, terminal-dependent sequences are mapped into
the standard key-names; then second stage maps the standard key-names
into their definitions in a terminal-independent fashion.

The terminal-specific file `term/$TERM.el' now is responsible only for
establishing the mapping from the terminal's escape sequences into
standard key-names.  It no longer knows what Emacs commands are
assigned to the standard key-names.

One other change in terminal-specific files: if the value of the TERM
variable contains a hyphen, only the part before the first hyphen is
used in forming the name of the terminal-specific file.  Thus, for
terminal type `aaa-48', the file loaded is now `term/aaa.el' rather
than `term/aaa-48.el'.

* mh-e changes.

A new version of mh-e is installed.  I do not know how it differs
from the old one; but then, I don't know how to use the old one.

* Mail mode changes.

The C-c commands of this mode have been rearranged:

C-c s, C-c c, C-c t and C-c b (move point to various header fields)
have been reassigned as C-c C-f C-s, C-c C-f C-c, C-c C-f C-t and C-c
C-f C-b.  C-c C-f is for "field".

C-c y, C-c w and C-c q have been changed to C-c C-y, C-c C-w and C-c C-q.

Thus, C-c LETTER is always unassigned.

* Outline mode changes.

Invisible lines in outline mode are now indicated by `...' at the
end of the previous visible line.

The special outline heading motion commands are now all on C-c keys.
A few new ones have been added.  Here is a full list:

C-c C-n   Move to next visible heading (formerly M-})
C-c C-p   Move to previous visible heading (formerly M-{)
C-c C-f   Move to next visible heading at the same level.
	   Thus, if point is on a level-2 heading line,
	   this command moves to the next visible level-2 heading.
C-c C-b   Move to previous visible heading at the same level.
C-c C-u   Move up to previous visible heading at a higher level.

The variable `outline-regexp' now controls recognition of heading lines.
Any line whose beginning matches this regexp is a heading line.
The depth in outline structure is determined by the length of
the string that matches.

* Incremental search changes.

Ordinary and regexp incremental searches now have distint default
search strings.  Thus, regexp searches recall only previous regexp
searches.

If an incremental search is failing and you ask to repeat it,
it will start again from the beginning of the buffer (or the end,
if it is a backward search).

The search-controlling parameters `isearch-slow-speed' and
`isearch-slow-window-lines' have now been renamed to start with
`search' instead of `isearch'.  Now all the parameters' names start
with `search'.

* LaTeX mode.

LaTeX mode now exists.  Use M-x latex-mode to select this mode, and
M-x plain-text-mode to select the previously existing mode for Plain
TeX.  M-x tex-mode attempts to examine the contents of the buffer and
choose between latex-mode and plain-tex-mode accordingly; if the
buffer is empty or it cannot tell, the variable `TeX-default-mode'
controls the choice.  Its value should be the symbol for the mode to
be used.

The facilities for running TeX on all or part of the buffer
work with LaTeX as well.

Some new commands available in both modes:

C-c C-l		recenter the window showing the TeX output buffer
		 so most recent line of output can be seen.
C-c C-k		kill the TeX subprocess.
C-c C-q		show the printer queue.
C-c C-f		close a block (appropriate for LaTeX only).
		 If the current line contains a \begin{...},
		 this inserts an \end{...} on the following line
		 and puts point on a blank line between them.

* Backup file changes.

If a backup file cannot be written in the directory of the visited file
due to fascist file protection, a backup file is now written in your home
directory as `~/%backup%~'.  Only one such file is made, ever, so only
the most recently made such backup is available.

When backup files are made by copying, the last-modification time of the
original file is now preserved in the backup copy.

* Undo changes

The undo command now will mark the buffer as unmodified only when it is
identical to the contents of the visited file.

* New startup command line options.

`-i FILE' or `-insert FILE' in the command line to Emacs tells Emacs to
insert the contents of FILE into the current buffer at that point in
command line processing.  This is like using the command M-x insert-file.

`-funcall', `-load', `-user' and `-no-init-file' are new synonyms for
`-f', `-l', `-u' and `-q'.

* Dired `g' command.

`g' in Dired mode is equivalent to M-x revert-buffer; it causes the
current contents of the same directory to be read in.

* Init file changes.

** Suffixes no longer accepted on `.emacs'.

Emacs will no longer load a file named `.emacs.el' or `emacs.elc'
in place of `.emacs'.  This is so that it will take less time to
find `.emacs'.  If you want to compile your init file, give it another
name and make `.emacs' a link to the `.elc' file, or make it contain
a call to `load' to load the `.elc' file.

** `default-profile' renamed to `default', and loaded after `.emacs'.

It used to be the case that the file `default-profile' was loaded if
and only if `.emacs' was not found.

Now the name `default-profile' is not used at all.  Instead, a library
named `default' is loaded after the `.emacs' file.  `default' is loaded
whether the `.emacs' file exists or not.  However, loading of `default'
can be prevented if the `.emacs' file sets `inhibit-default-init' to non-nil.

In fact, you would call the default file `default.el' and probably would
byte-compile it to speed execution.

Note that for most purposes you are better off using a `site-init' library
since that will be loaded before the runnable Emacs is dumped.  By using
a `site-init' library, you avoid taking up time each time Emacs is started.

** inhibit-command-line has been eliminated.

This variable used to exist for .emacs files to set.  It has been
eliminated because you can get the same effect by setting
command-line-args to nil and setting inhibit-startup-message to t.

* Built-in per-buffer variables improved.

Several built-in variables which in the past had a different value in
each buffer now behave exactly as if `make-variable-buffer-local' had
been done to them.

These variables are `tab-width', `ctl-arrow', `truncate-lines',
`fill-column', `left-margin', `mode-line-format', `abbrev-mode',
`overwrite-mode', `case-fold-search', `auto-fill-hook',
`selective-display', `selective-display-ellipses'.

To be precise, each variable has a default value which shows through
in most buffers and can be accessed with `default-value' and set with
`set-default'.  Setting the variable with `setq' makes the variable
local to the current buffer.  Changing the default value has retroactive
effect on all buffers in which the variable is not local.

The variables `default-case-fold-search', etc., are now obsolete.
They now refer to the default value of the variable, which is not
quite the same behavior as before, but it should enable old init files
to continue to work.

* New function `setq-default'.

`setq-default' sets the default value of a variable, and uses the
same syntax that `setq' accepts: the variable name is not evaluated
and need not be quoted.

`(setq-default case-fold-search nil)' would make searches case-sensitive
in all buffers that do not have local values for `case-fold-search'.

* M-x disassemble.

This command prints the disassembly of a byte-compiled Emacs Lisp function.

Would anyone like to interface this to the debugger?

* New hook `shell-set-directory-error-hook'.

The value of this variable is called, with no arguments, whenever
Shell mode gets an error trying to keep track of directory-setting
commands (such as `cd' and `pushd') used in the shell buffer.

* New function `substitute-key-definition'.

This is a new way to replace one command with another command as the
binding of whatever keys may happen to refer to it.

(substitute-key-definition OLDDEF NEWDEF KEYMAP) looks through KEYMAP
for keys defined to run OLDDEF, and rebinds those keys to run NEWDEF
instead.

* New parameter `backup-by-copying-when-mismatch'.

If this variable is non-`nil', then when Emacs is about to save a
file, it will create the backup file by copying if that would avoid
changing the file's uid or gid.

The default value of this variable is `nil', because usually it is
useful to have the uid of a file change according to who edited it
last.  I recommend thet this variable be left normally `nil' and
changed with a local variables list in those particular files where
the uid needs to be preserved.

* New parameter `file-precious-flag'.

If this variable is non-`nil', saving the buffer tries to avoid
leaving an incomplete file due to disk full or other I/O errors.
It renames the old file before saving.  If saving is successful,
the renamed file is deleted; if saving gets an error, the renamed
file is renamed back to the name you visited.

Backups are always made by copying for such files.

* Multiple hooks allowed in certain contexts.

The old hook variables `find-file-hook', `find-file-not-found-hook' and
`write-file-hook' have been replaced.

The replacements are `find-file-hooks', `find-file-not-found-hooks'
and `write-file-hooks'.  Each holds a list of functions to be called;
by default, `nil', for no functions.  The functions are called in
order of appearance in the list.

In the case if `find-file-hooks', all the functions are executed.

In the case of `find-file-not-found-hooks', if any of the functions
returns non-`nil', the rest of the functions are not called.

In the case of `write-file-hooks', if any of the functions returns
non-`nil', the rest of the functions are not called, and the file is
considered to have been written already; so actual writing in the
usual way is not done.

`find-file-not-found-hooks' and `write-file-hooks' can be used
together to implement editing of files that are not stored as Unix
files: stored in archives, or inside version control systems, or on
other machines running other operating systems and accessible via ftp.

* Disabling commands can print a special message.

A command is disabled by giving it a non-`nil' `disabled' property.
Now, if this property is a string, it is included in the message
printed when the user tries to run the command.

* Deleted function `set-minor-mode'; minor modes must be changed.

The function `set-minor-mode' has been eliminated.  The display
of minor mode names in the mode line is now controlled by the
variable `minor-mode-alist'.  To specify display of a new minor
mode, it is sufficient to add an element to this list.  Once that
is done, you can turn the mode on and off just by setting a variable,
and the display will show its status automatically.

* Deleted variable `ask-about-buffer-names'.

If you want buffer names for files to be generated in a special way,
you must redefine `create-file-buffer'.

* `window-point' valid for selected window.

The value returned by `window-point' used to be incorrect when its
argument was the selected window.  Now the value is correct.

* New function `process-list'.

This function takes no arguments and returns a list of all
of Emacs's asynchronous subprocesses.

* New mode-line control features.

The display of the mode line used to be controlled by a format-string
that was the value of the variable `mode-line-format'.

This variable still exists, but it now allows more general values,
not just strings.  Lists, cons cells and symbols are also meaningful.

The mode line contents are created by outputting various mode elements
one after the other.  Here are the kinds of objects that can be
used as mode elements, and what they do in the display:

  string        the contents of the string are output to the mode line,
		and %-constructs are replaced by other text.

  t or nil	ignored; no output results.

  symbol	the symbol's value is used.  If the value is a string,
		the string is output verbatim to the mode line
		(so %-constructs are not interpreted).  Otherwise,
		the symbol's value is processed as a mode element.

  list (whose first element is a string or list or cons cell)
		the elements of the list are treated as as mode elements,
		so that the output they generate is concatenated,

  list (whose car is a symbol)
		if the symbol's value is non-nil, the second element of the
		list is treated as a mode element.  Otherwise, the third
		element (if any) of the list is treated as a mode element.

There is always one mode element to start with, that being the value of
`mode-line-format', but if this value is a list then it leads to several
more mode elements, which can lead to more, and so on.

There is one new %-construct for mode elements that are strings:
`%n' displays ` Narrow' for a buffer that is narrowed, and/or ` Def'
when a keyboard macro is being defined.

The default value of `mode-line-format' refers to several other variables.
These variables are `mode-name', `mode-line-buffer-identification',
`mode-line-process', `global-mode-string' and `minor-mode-alist'.
The first three are local in every buffer in which they are changed from
the default.

mode-name	Name of buffer's major mode.  Local in every buffer.

mode-line-buffer-identification
		Normally the list ("Emacs: %17b"), it is responsible
		for displaying text to indicate what buffer is being shown
		and what kind of editing it is doing.  `Emacs' means
		that a file of characters is being edited.  Major modes
		such as Info and Dired which edit or view other kinds
		of data often change this value.  This variables becomes
		local to the current buffer if it is setq'd.

mode-line-process
		Normally nil, this variable is responsible for displaying
		information about the process running in the current buffer.
		M-x shell-mode and M-x compile alter this variable.

minor-mode-alist
		This variable is responsible for displaying text for those
		minor modes that are currently enabled.  Its value
		is a list of elements of the form (VARIABLE STRING),
		where STRING is to be displayed if VARIABLE's value
		(in the buffer whose mode line is being displayed)
		is non-nil.  This variable is not made local to particular
		buffers, but loading some libraries may add elements to it.

global-mode-string
		This variable is used to display the time, if you ask
		for that.

The idea of these variables is to eliminate the need for major modes
to alter mode-line-format itself.

* New function `documentation-property'.

(documentation-property SYMBOL PROPNAME) is like (get SYMBOL PROPNAME),
except that if the property value is a number `documentation-property'
will take that number (or its absolute value) as a character position
in the DOC file and return the string found there.

(documentation-property VAR 'variable-documentation) is the proper
way for a Lisp program to get the documentation of variable VAR.

* `insert-buffer-substring' can insert part of the current buffer.

The old restriction that the text being inserted had to come from
a different buffer is now lifted.

When inserting text from the current buffer, the text to be inserted
is determined from the specified bounds before any copying takes place.

* `rename-file', `copy-file', `add-name-to-file' and `make-symbolic-link'.

The third argument to these functions used to be `t' or `nil'; `t'
meaning go ahead even if the specified new file name already has a file,
and `nil' meaning to get an error.

Now if the third argument is a number it means to ask the user for
confirmation in this case.

* New function `file-newer-than-file-p'.

(file-newer-than-file-p FILE1 FILE2) returns non-nil if FILE1 has been
modified more recently than FILE2.  If FILE1 does not exist, the value
is always nil; otherwise, if FILE2 does not exist, the value is t.
This is meant for use when FILE2 depends on FILE1, to see if changes
in FILE1 make it necessary to recompute FILE2 from it.

* Changed function `file-exists-p'.

This function is no longer the same as `file-readable-p'.
`file-exists-p' can now return t for a file that exists but which
the fascists won't allow you to read.

* New function `file-name-sans-versions'.

(file-name-sans-versions NAME) returns a substring of NAME, with any
version numbers or other backup suffixes deleted from the end.

* New functions for directory names.

Although a directory is really a kind of file, specifying a directory
uses a somewhat different syntax from specifying a file.
In Emacs, a directory name is used as part of a file name.

On Unix, the difference is small: a directory name ends in a slash,
while a file name does not: thus, `/usr/rms/' to name a directory,
while `/usr/rms' names the file which holds that directory.

On VMS, the difference is considerable: `du:[rms.foo]' specifies a
directory, but the name of the file that holds that directory is
`du:[rms]foo.dir'.

There are two new functions for converting between directory names
and file names.  `directory-file-name' takes a directory name and
returns the name of the file in which that directory's data is stored.
`file-name-as-directory' takes the name of a file and returns
the corresponding directory name.  These always understand Unix file name
syntax; on VMS, they understand VMS syntax as well.

For example, (file-name-as-directory "/usr/rms") returns "/usr/rms/"
and (directory-file-name "/usr/rms/") returns "/usr/rms".
On VMS, (file-name-as-directory "du:[rms]foo.dir") returns "du:[rms.foo]"
and (directory-file-name "du:[rms.foo]") returns "du:[rms]foo.dir".

* New optional argument to `copy-file'.

If `copy-file' receives a non-nil fourth argument, it attempts
to give the new copy the same time-of-last-modification that the
original file has.

* New argument to `set-screen-height' or `set-screen-width'.

These functions now take an optional second argument which says
what significance the newly specified height or width has.

If the argument is nil, or absent, it means that Emacs should
believe that the terminal height or width really is as just specified.

If the argument is t, it means Emacs should not believe that the
terminal really is this high or wide, but it should use the
specific height or width as the number of lines or columns to display.
Thus, you could display only 24 lines on a screen known to have 48 lines.

What practical difference is there between using only 24 lines for display
and really believing that the terminal has 24 lines?

1. The ``real'' height of the terminal says what the terminal command
to move the cursor to the last line will do.

2. The ``real'' height of the terminal determines how much padding is
needed.

* `apply' is more general.

`apply' now accepts any number of arguments.  The first one is a function;
the rest are individual arguments to pass to that function, except for the
last, which is a list of arguments to pass.

Previously, `apply' required exactly two arguments.  Its old behavior
follows as a special case of the new definition.

* New function (VMS only) `define-logical-name':

(define-logical-name LOGICAL TRANSLATION) defines a VMS logical name
LOGICAL whose translation is TRANSLATION.  The new name applies to
the current process only.

* New function `process-exit-status'.

This function, given a process, process name or buffer as argument,
returns the exit status code or signal number of the process.
If the process has not yet exited or died, this function returns 0.

* Process output ignores `buffer-read-only'.

Output from a process will go into the process's buffer even if the
buffer is read only.

* New code-letter for `interactive'.

(interactive "NFoo: ") is like (interactive "nFoo: ") in reading
a number using the minibuffer to serve as the argument; however,
if a prefix argument was specified, it uses the prefix argument
value as the argument, and does not use the minibuffer at all.

This is used by the `goto-line' and `goto-char' commands.

* New per-buffer variable `selective-display-ellipses'.

If this variable is non-nil, an ellipsis (`...') appears on the screen
at the end of each text line that is followed by invisible text.

If this variable is nil, no ellipses appear.  Then there is no sign
on the screen that invisible text is present.

Text is made invisible under the control of the variable
`selective-display'; this is how Outline mode and C-x $ work.

* New variable `no-redraw-on-reenter'.

If you set this variable non-nil, Emacs will not clear the screen when
you resume it after suspending it.  This is for the sake of terminals
with multiple screens of memory, where the termcap entry has been set
up to switch between screens when Emacs is suspended and resumed.

* `insert-file-contents' on an empty file.

This no longer sets the buffer's "modified" flag.

* Value of `file-attributes' changed.

The function file-attributes returns a list containing many kinds of
information about a file.  Now the list has a tenth element, which is
`t' if deleting the file and creating another file of the same name
would result in a change in the file's group; `nil' if there would
be no change.  You can also think of this as comparing the file's group
with the default group for files created in the same directory by you.

* VMS-only function `file-name-all-versions'.

This function returns a list of all the completions, including version
number, of a specified version-number-less file name.  This is like
`file-name-all-completions', except that the latter returns values
that do not include version numbers.

* New functions `user-uid' and `user-real-uid'.

These functions take no arguments and return, respectively,
the effective uid and the real uid of the Emacs process.
The value in each case is an integer.

News regarding installation:

* Emacs no longer uses floating point arithmetic.  This may make it easier
to port to some machines.

* Macros `XPNTR' and `XSETPNTR'; flag `DATA_SEG_BITS'.

These macros exclusively are used to unpack a pointer from a Lisp_Object
and to insert a pointer into a Lisp_Object.  Redefining them may help
port Emacs to machines in which all pointers to data objects have
certain high bits set.

If `DATA_SEG_BITS' is defined, it should be a number which contains
the high bits to be inclusive or'ed with pointers that are unpacked.

* Compilation switch `A_TEXT_OFFSET'.

Define this switch on machines which use the BSD binary format
but count the header of the file as part of the size of the text segment.

* Macros `ARRAY_MARK_FLAG' and `DONT_COPY_FLAG'.

* `HAVE_ALLOCA' prevents assembly of `alloca.s'.

* `PNTR_COMPARISON_TYPE'

Pointers that need to be compared for ordering are converted to this type
first.  Normally this is `unsigned int'.

* `HAVE_VFORK', `HAVE_DUP2' and `HAVE_GETTIMEOFDAY'.

These flags just say whether certain system calls are available.

For older news, see the file ONEWS.

----------------------------------------------------------------------
Copyright information:

Copyright (C) 1986 Richard M. Stallman

   Permission is granted to anyone to make or distribute verbatim copies
   of this document as received, in any medium, provided that the
   copyright notice and this permission notice are preserved,
   thus giving the recipient permission to redistribute in turn.

   Permission is granted to distribute modified versions
   of this document, or of portions of it,
   under the above conditions, provided also that they
   carry prominent notices stating who last changed them.

Local variables:
mode: text
end:
