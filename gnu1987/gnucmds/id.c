/* for xc:
%  cc -O -o id id.c
%d cc -g -o id id.c
*/

/*
 * id - a trivial utility
 *
 *	Copyright (C) 1984 by David M. Ihnat
 *
 * Somewhere along the line, Unix(Tm) picked up this utility to
 * report the user and group ID's of the running shell.
 * This is a public domain version of that program.
 *
 * This program may be used and distributed by anyone, provided
 * no one sells it for profit and this notice remain intact.
 * I attempted to recreate the behavior of the original command
 * as faithfully as possible; I might suggest changes maintain
 * this congruence.
 * The user assumes liability for all losses,
 * direct or indirect, resulting from use of this program.
 *
 * Caveat Utilitor!
 */

#include <stdio.h>
#include <pwd.h>
#include <grp.h>

#define	REAL	0
#define	CURRENT	1

main()
{
	int getuid(),geteuid(),getgid(),getegid();
	int uid,euid,gid,egid;

	/*
	 * First, get all the appropriate numbers.
	 * It *can't fail--ya gotta have a pid and gid!
	 */
	uid = getuid();
	gid = getgid();
	euid = geteuid();
	egid = getegid();

	uidprint(uid,REAL);

	if(uid != euid)
		uidprint(euid,CURRENT);

	gidprint(gid,REAL);

	if(gid != egid)
		gidprint(egid,CURRENT);

	fprintf(stdout,"\n");
}

uidprint(id,type)
int id;
int type;
{
	struct passwd *getpwuid();
	struct passwd *pwdptr;

	if((pwdptr = getpwuid(id)) == (struct passwd *) NULL)
	{
		fprintf(stderr,"Can't find entry for %s %d in passwd file!\n",
			(type==REAL?"uid":"euid"),id);
		exit(1);
	}

	fprintf(stdout,"%s=%d(%s) ",(type==REAL?"uid":"euid"),
		id,pwdptr->pw_name);
}
gidprint(id,type)
int id;
int type;
{
	struct group *getgrgid();
	struct group *grpptr;

	if((grpptr = getgrgid(id)) == (struct group *) NULL)
	{
		fprintf(stderr,"Can't find entry for %s %d in group file!\n",
			(type==REAL?"gid":"egid"),id);
		exit(1);
	}

	fprintf(stdout,"%s=%d(%s) ",(type==REAL?"gid":"egid"),
		id,grpptr->gr_name);
}
