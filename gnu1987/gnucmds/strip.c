/* strip certain symbols from a rel file.
   Copyright (C) 1986 Richard M. Stallman

		       NO WARRANTY

  BECAUSE THIS PROGRAM IS LICENSED FREE OF CHARGE, WE PROVIDE ABSOLUTELY
NO WARRANTY, TO THE EXTENT PERMITTED BY APPLICABLE STATE LAW.  EXCEPT
WHEN OTHERWISE STATED IN WRITING, FREE SOFTWARE FOUNDATION, INC,
RICHARD M. STALLMAN AND/OR OTHER PARTIES PROVIDE THIS PROGRAM "AS IS"
WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY
AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE PROGRAM PROVE
DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR
CORRECTION.

 IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW WILL RICHARD M.
STALLMAN, THE FREE SOFTWARE FOUNDATION, INC., AND/OR ANY OTHER PARTY
WHO MAY MODIFY AND REDISTRIBUTE THIS PROGRAM AS PERMITTED BELOW, BE
LIABLE TO YOU FOR DAMAGES, INCLUDING ANY LOST PROFITS, LOST MONIES, OR
OTHER SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE
USE OR INABILITY TO USE (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR
DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY THIRD PARTIES OR
A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS) THIS
PROGRAM, EVEN IF YOU HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH
DAMAGES, OR FOR ANY CLAIM BY ANY OTHER PARTY.

		GENERAL PUBLIC LICENSE TO COPY

  1. You may copy and distribute verbatim copies of this source file
as you receive it, in any medium, provided that you conspicuously
and appropriately publish on each copy a valid copyright notice
"Copyright (C) 1986 Richard M. Stallman"; and include following the
copyright notice a verbatim copy of the above disclaimer of warranty
and of this License.

  2. You may modify your copy or copies of this source file or
any portion of it, and copy and distribute such modifications under
the terms of Paragraph 1 above, provided that you also do the following:

    a) cause the modified files to carry prominent notices stating
    that you changed the files and the date of any change; and

    b) cause the whole of any work that you distribute or publish,
    that in whole or in part contains or is a derivative of this
    program or any part thereof, to be freely distributed
    and licensed to all third parties on terms identical to those
    contained in this License Agreement (except that you may choose
    to grant more extensive warranty protection to third parties,
    at your option).

  3. You may copy and distribute this program or any portion of it in
compiled, executable or object code form under the terms of Paragraphs
1 and 2 above provided that you do the following:

    a) cause each such copy to be accompanied by the
    corresponding machine-readable source code, which must
    be distributed under the terms of Paragraphs 1 and 2 above; or,

    b) cause each such copy to be accompanied by a
    written offer, with no time limit, to give any third party
    free (except for a nominal shipping charge) a machine readable
    copy of the corresponding source code, to be distributed
    under the terms of Paragraphs 1 and 2 above; or,

    c) in the case of a recipient of this program in compiled, executable
    or object code form (without the corresponding source code) you
    shall cause copies you distribute to be accompanied by a copy
    of the written offer of source code which you received along
    with the copy you received.

  4. You may not copy, sublicense, distribute or transfer this program
except as expressly provided under this License Agreement.  Any attempt
otherwise to copy, sublicense, distribute or transfer this program is void and
your rights to use the program under this License agreement shall be
automatically terminated.  However, parties who have received computer
software programs from you with this License Agreement will not have
their licenses terminated so long as such parties remain in full compliance.

 In other words, you are welcome to use, share and improve this program.
 You are forbidden to forbid anyone else to use, share and improve
 what you give them.   Help stamp out software-hoarding!  */

/* define this for a linker for the TI NU machine.
 Slightly different defaults.  */

/* #define numachine 1 */

#ifdef numachine
#define O_RDONLY 0
#define O_RDWR 2
#endif

#include <a.out.h>
#include <stdio.h>
/* #include <sys/types.h> */
#include <strings.h>
/* #include <sys/stat.h> */

#ifndef numachine
#include <sys/file.h>
#endif

 /* count the number of nlist entries that are for local symbols. */
int local_sym_count;

 /* count number of nlist entries that are for local symbols whose names don't start with L. */
int non_L_local_sym_count;

 /* count the number of nlist entries for debugger info.  */
int debugger_sym_count;

 /* count the number of global symbols referenced or defined.  */
int global_sym_count;

 /* Total number of symbols to be preserved in the current file.  */
int nsyms;

/* Number of files specified in the command line. */

int number_of_files;

/* Each specified file has a file_entry structure for it.
These are contained in the vector which file_table points to.
*/

struct file_entry {
  char *filename;
  struct exec header;		/* the file's header */
  int ss_size;			/* size, in bytes, of symbols_and_strings data */
  struct nlist *symbols_and_strings;
};

struct file_entry *file_table;

/* descriptor on which current file is open.  */

int input_desc;

/* stream for writing that file using stdio.  */

FILE *outstream;

 /* 1 => strip all symbols; 2 => strip all but locals and globals */
int strip_symbols;

 /* 1 => discard locals starting with L; 2 => discard all locals */
int discard_locals;

void decode_command (), strip_file ();
void each_file ();
int file_open ();
void read_file_symbols (), rewrite_file_symbols(), file_close();
void read_header(), read_file_symbols (), read_entry_symbols();
void count_file_symbols ();
char *concat ();

main(argc, argv)
     char **argv;
     int argc;
{
  strip_symbols = 1;   /* default is to strip nothing.  *//* JF default is to strip everything! */
  discard_locals = 0;	/* JF new default was 0 */

  decode_command (argc, argv);

  each_file (strip_file);
}

/* process one input file */

void
strip_file (entry)
     struct file_entry *entry;
{
  local_sym_count = 0;
  non_L_local_sym_count = 0;
  debugger_sym_count = 0;
  global_sym_count = 0;

  file_open (entry);
  if (strip_symbols != 1)
    /* read in the existing symbols unless we are discarding everything.  */
    read_file_symbols (entry);
  rewrite_file_symbols (entry);
  if (strip_symbols != 1)
    free (entry->symbols_and_strings);
  file_close ();
}

void decode_switch ();

/* analyze a command line argument.
 Returns 0 if the argument is a filename.
 Returns 1 if the argument is a switch complete in itself.
 Returns 2 if the argument is a switch which gobbles the following argument. */

int
decode_arg(arg)
     char *arg;
{
  if (*arg != '-') return 0;
  return 1;
}

/* Process the command arguments, setting up file_table with an entry for
 each file, and setting variables according to the switches.  */

void
decode_command(argc, argv)
     char **argv;
     int argc;
{
  int i;
  struct file_entry *p;

  number_of_files = 0;

  /* First compute number_of_files so we know how long to make file_table */
  /* Also process most switches completely */

  for (i = 1; i < argc; i++)
    {
      int code = decode_arg (argv[i]);
      if (code)
	{
	  i += code - 1;
	}
      else
	number_of_files++;
    }

  if (!number_of_files)
    fatal ("no files specified", 0);

  p = file_table
    = (struct file_entry *) xmalloc (number_of_files * sizeof (struct file_entry));

  /* Now scan again and fill in file_table */

  for (i = 1; i < argc; i++)
    {
      int code = decode_arg (argv[i]);

      if (code)
	{
	  decode_switch (argv[i]);
	  i += code - 1;
	}
      else
	{
	  p->filename = argv[i];
	  p->symbols_and_strings = 0;
	  p++;
	}
    }
}

void
decode_switch (swt)
     char *swt;
{
  if (!strcmp (swt, "-S"))
    strip_symbols = 2;
  else if (!strcmp (swt, "-s"))
    strip_symbols = 1;
  else if (!strcmp (swt, "-X"))
    discard_locals = 1;
  else if (!strcmp (swt, "-x"))
    discard_locals = 2;
  else
    fatal ("invalid command switch %s", swt);
}

/** Convenient functions for operating on one or all files being processed.  */

/* Call 'function' on each file entry.

 'function' receives two arguments: the entry, and 'arg'.  */

void
each_file (function, arg)
     void (*function)();
     int arg;
{
  int i;

  for (i = 0; i < number_of_files; i++)
    {
      struct file_entry *entry = &file_table[i];
      (*function) (entry, arg);
    }
}

/* Close the file that is now open.  */

void
file_close ()
{
  close (input_desc);
  input_desc = 0;
}

/* open the file specified by 'entry', and return a descriptor.
 The descriptor is also saved in input_desc.  */
/* JF this also makes sure the file is in rel format */

int
file_open (entry)
     struct file_entry *entry;
{
  int desc;
  int len, magicnum;

  desc = open (entry->filename, O_RDWR, 0);

  if (desc > 0)
    {
      input_desc = desc;
      len = read (input_desc, (char *)&magicnum, sizeof magicnum);
      if (len != sizeof magicnum)
	fatal_with_file ("failure reading header of ", entry);
      if (N_BADMAG(*((struct exec *)&magicnum)))
	fatal_with_file ("malformed input file (not rel) ", entry);
      read_header (desc, &entry->header, entry);
      return desc;
    }

  perror_file (entry);
}

/* Print the filename of 'entry' on 'outfile' (a stdio stream), and then a newline.  */

prline_file_name (entry, outfile)
     struct file_entry *entry;
     FILE *outfile;
{
  print_file_name (entry, outfile);
  fprintf (outfile, "\n");
}

/* Print the filename of 'entry' on 'outfile' (a stdio stream).  */

print_file_name (entry, outfile)
     struct file_entry *entry;
     FILE *outfile;
{
  fprintf (outfile, "%s", entry->filename);
}

/* Validate file 'entry' and read its symbol and string sections into core. */

void
read_file_symbols (entry)
     struct file_entry *entry;
{
  read_entry_symbols (input_desc, entry);
  count_file_symbols (entry);
}

/* read a file's header into the proper place in the file_entry.  */

void
read_header (desc, loc, entry)
     int desc;
     struct exec *loc;
     struct file_entry *entry;
{
  int len;
  lseek (desc,0, 0);
  len = read (desc, loc, sizeof (struct exec));
  if (len != sizeof (struct exec))
    fatal_with_file ("failure reading header of ", entry);
  if (N_BADMAG(*loc))
    fatal_with_file ("bad magic number in ", entry);
}

/* Read the symbols and strings of file 'entry' into core.
 Assume it is already open, on descriptor 'desc'.  */

void
read_entry_symbols (desc, entry)
     struct file_entry *entry;
     int desc;
{
  int string_size;

  lseek (desc, N_STROFF(entry->header), 0);
  if (sizeof string_size != read (desc, &string_size, sizeof string_size))
    fatal_with_file ("bad string table in ", entry);

  entry->ss_size = string_size + entry->header.a_syms;
  entry->symbols_and_strings = (struct nlist *) xmalloc (entry->ss_size);

  lseek (desc, N_SYMOFF(entry->header), 0);
  if (entry->ss_size != read (desc, entry->symbols_and_strings, entry->ss_size))
    fatal_with_file ("premature end of file in symbols/strings of ", entry);
}


/* count the number of symbols of various categories in the file of 'entry' */

void
count_file_symbols (entry)
     struct file_entry *entry;
{
  struct nlist *p, *end = entry->symbols_and_strings + entry->header.a_syms / sizeof (struct nlist);
  char *name_base = entry->header.a_syms + (char *) entry->symbols_and_strings;

  for (p = entry->symbols_and_strings; p < end; p++)
    if (p->n_type & N_EXT)
      global_sym_count++;
    else if (p->n_un.n_strx && !(p->n_type & (N_STAB | N_EXT)))
      {
	if ((p->n_un.n_strx + name_base)[0] != 'L')
	  non_L_local_sym_count++;
	local_sym_count++;
      }
    else debugger_sym_count++;
}

void write_file_syms(), modify_relocation ();

/* total size of string table strings allocated so far */
int strtab_size;

/* Vector whose elements are the strings to go in the string table */
char **strtab_vector;

/* Index in strtab_vector at which the next string will be stored */
int strtab_index;

int sym_written_count;

int
assign_string_table_index (name)
     char *name;
{
  int index = strtab_size;

  strtab_size += strlen (name) + 1;
  strtab_vector[strtab_index++] = name;

  return index;
}

void
rewrite_file_symbols (entry)
     struct file_entry *entry;
{
  int i;
  struct nlist *newsyms;

  /* Calculate number of symbols to be preserved.  */

  if (strip_symbols == 1)
    nsyms = 0;
  else
    {
      nsyms = global_sym_count;
      if (discard_locals == 1)
	nsyms += non_L_local_sym_count;
      else if (discard_locals == 0)
	nsyms += local_sym_count;
    }

  if (strip_symbols == 0)
    nsyms += debugger_sym_count;

  strtab_vector = (char **) xmalloc (nsyms * sizeof (char *));
  strtab_index = 0;

  strtab_size = 4;

  /* Accumulate in 'newsyms' the symbol table to be written.  */

  newsyms = (struct nlist *) xmalloc (nsyms * sizeof (struct nlist));

  sym_written_count = 0;

  if (strip_symbols != 1)
    /* Write into newsyms the symbols we want to keep.  */
    write_file_syms (entry, newsyms);

  if (sym_written_count != nsyms)
    {
      fprintf (stderr, "written = %d, expected = %d\n", sym_written_count, nsyms);
      fatal_with_file ("internal error: wrong number of symbols written in ", entry);
    }

  /* modify the symbol-numbers in the relocation in the file, to preserve its meaning */
  modify_relocation (input_desc, entry);

  /* now write contents of 'newsyms' into the file. */

  lseek (input_desc, N_SYMOFF(entry->header), 0);
  write (input_desc, newsyms, nsyms * sizeof (struct nlist));
  free (newsyms);

  /* now write the string table.  */

  {
    char *strvec = (char *) xmalloc (strtab_size);
    char *p;

    *((long *) strvec) = strtab_size;

    p = strvec + sizeof (long);

    for (i = 0; i < strtab_index; i++)
      {
	int len = strlen (strtab_vector[i]);
	strcpy (p, strtab_vector[i]);
	*(p+len) = 0;
	p += len + 1;
      }

    write (input_desc, strvec, strtab_size);
    free (strvec);
  }

  /* adjust file to be smaller */

#ifndef numachine
/*  ftruncate (input_desc, ftell (outstream)); */
  if(ftruncate (input_desc, tell(input_desc)) < 0)		/* JF make it work right */
    perror_file(entry);
#endif

  /* write new symbol table size into file header.  */

  entry->header.a_syms = nsyms * sizeof (struct nlist);

  lseek (input_desc, 0, 0);
  write (input_desc, &entry->header, sizeof (struct exec));

  free (strtab_vector);
}

/* Copy into 'newsyms' the symbol entries to be preserved.
 Count them in sym_written_count.  */

/* We record, for each symbol written, its symbol number in the resulting file.
  This is so that the relocation can be updated later.
  Since the symbol names will not be needed again,
  this index goes in the n_strx field.
  If a symbol is not written, -1 is stored there.  */

void
write_file_syms (entry, newsyms)
     struct file_entry *entry;
     struct nlist *newsyms;
{
  struct nlist *p = entry->symbols_and_strings;
  struct nlist *end = p + entry->header.a_syms / sizeof (struct nlist);
  char *string_base = (char *) end;   /* address of start of file's string table */
  struct nlist *outp = newsyms;

  for (; p < end; p++)
    {
      int type = p->n_type;
      int write;
  
      if (p->n_type & N_EXT)
	write = 1;
      else if (p->n_un.n_strx && !(p->n_type & (N_STAB | N_EXT)))
	/* ordinary local symbol */
	write = (discard_locals != 2)
		&& !(discard_locals == 1 &&
		     (p->n_un.n_strx + string_base)[0] == 'L');
      else
	/* debugger symbol */
	write = (strip_symbols == 0);

      if (write)
	{
	  if (p->n_un.n_strx)
	    p->n_un.n_strx = assign_string_table_index (p->n_un.n_strx + string_base);

	  *outp++ = *p;

	  p->n_un.n_strx = sym_written_count++;
	}
      else p->n_un.n_strx = -1;
    }
}

/* read in 'entry's relocation, alter the symbolnum's in it, and write it out again.  */

void
modify_relocation (desc, entry)
     int desc;
     struct file_entry *entry;
{
  struct relocation_info *reloc, *p, *end;
  int size = entry->header.a_trsize + entry->header.a_drsize;
  struct nlist *sym_base = (struct nlist *) entry->symbols_and_strings;

  reloc = (struct relocation_info *) xmalloc (size);
  lseek (desc, N_TXTOFF(entry->header) + entry->header.a_text + entry->header.a_data, 0);
  read (desc, reloc, size);

  p = reloc;
  end = (struct relocation_info *) (size + (char *) reloc);
  while (p < end)
    {
      if (p->r_extern)
	{
	  int newnum = (sym_base + p->r_symbolnum) -> n_un.n_strx;
	  if (newnum < 0)
	    fatal_with_file ("stripped symbol needed for relocation in ", entry);
	  p->r_symbolnum = newnum;
	}
      p++;
    }

  lseek (desc, N_TXTOFF(entry->header) + entry->header.a_text + entry->header.a_data, 0);
  write (desc, reloc, size);
}

/* Report a fatal error.  'string' is a printf format string and 'arg' is one arg for it.  */

fatal (string, arg)
     char *string, *arg;
{
  fprintf (stderr, "strip: ");
  fprintf (stderr, string, arg);
  fprintf (stderr, "\n");
  exit (1);
}

/* Report a fatal error.  'string' is printed, followed by the filename of 'entry'.  */

fatal_with_file (string, entry)
     char *string;
     struct file_entry *entry;
{
  fprintf (stderr, "strip: ");
  fprintf (stderr, string);
  print_file_name (entry, stderr);
  fprintf (stderr, "\n");
  exit (1);
}

/* Report a fatal error using the message for the last failed system call,
 followed by the string 'name'.  */

perror_name (name)
     char *name;
{
  extern int errno, sys_nerr;
  extern char *sys_errlist[];
  char *s;

  if (errno < sys_nerr)
    s = concat ("", sys_errlist[errno], " for %s");
  else
    s = "cannot open %s";
  fatal (s, name);
}

/* Report a fatal error using the message for the last failed system call,
 followed by the name of file 'entry'.  */

perror_file (entry)
     struct file_entry *entry;
{
  extern int errno, sys_nerr;
  extern char *sys_errlist[];
  char *s;

  if (errno < sys_nerr)
    s = concat ("", sys_errlist[errno], " for ");
  else
    s = "cannot open ";
  fatal_with_file (s, entry);
}

/* Report a nonfatal error.
 'string' is a format for printf, and 'arg1' ... 'arg3' are args for it.  */

error (string, arg1, arg2, arg3)
     char *string, *arg1, *arg2, *arg3;
{
  fprintf (stderr, string, arg1, arg2, arg3);
  fprintf (stderr, "\n");
}

/* Return a newly-allocated string whose contents concatenate those of s1, s2, s3.  */

char *
concat (s1, s2, s3)
     char *s1, *s2, *s3;
{
  int len1 = strlen (s1), len2 = strlen (s2), len3 = strlen (s3);
  char *result = (char *) xmalloc (len1 + len2 + len3 + 1);

  strcpy (result, s1);
  strcpy (result + len1, s2);
  strcpy (result + len1 + len2, s3);
  *(result + len1 + len2 + len3) = 0;

  return result;
}

/* Like malloc but get fatal error if memory is exhausted.  */

int
xmalloc (size)
     int size;
{
  int result = malloc (size);
  if (!result)
    fatal ("virtual memory exhausted", 0);
  return result;
}
