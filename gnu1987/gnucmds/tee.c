/* tee: Read from standard input,
   and broadcast input to standard output and one or more files.
   Copyright (C) 1985 Mike Parker and Richard M. Stallman

This program is distributed in the hope that it will be useful,
but without any warranty.  No author or distributor
accepts responsibility to anyone for the consequences of using it
or for whether it serves any particular purpose or works at all,
unless he says so in writing.

   Permission is granted to anyone to distribute verbatim copies
   of this program's source code as received, in any medium, provided that
   the copyright notice, the nonwarraty notice above
   and this permission notice are preserved,
   and that the distributor grants the recipient all rights
   for further redistribution as permitted by this notice,
   and informs him of these rights.

   Permission is granted to distribute modified versions of this
   program's source code, or of portions of it, under the above
   conditions, plus the conditions that all changed files carry
   prominent notices stating who last changed them and that the
   derived material, including anything packaged together with it and
   conceptually functioning as a modification of it rather than an
   application of it, is in its entirety subject to a permission
   notice identical to this one.

   Permission is granted to distribute this program (verbatim or
   as modified) in compiled or executable form, provided verbatim
   redistribution is permitted as stated above for source code, and
    A.  it is accompanied by the corresponding machine-readable
      source code, under the above conditions, or
    B.  it is accompanied by a written offer, with no time limit,
      to distribute the corresponding machine-readable source code,
      under the above conditions, to any one, in return for reimbursement
      of the cost of distribution.   Verbatim redistribution of the
      written offer must be permitted.  Or,
    C.  it is distributed by someone who received only the
      compiled or executable form, and is accompanied by a copy of the
      written offer of source code which he received along with it.

   Permission is granted to distribute this program (verbatim or as modified)
   in executable form as part of a larger system provided that the source
   code for this program, including any modifications used,
   is also distributed or offered as stated in the preceding paragraph.

In other words, you are welcome to use, share and improve this program.
You are forbidden to forbid anyone else to use, share and improve
what you give them.   Help stamp out software-hoarding!  */

#include <stdio.h>
#include <signal.h>
	
char **files;
int nfiles;
int append;
FILE **fs;
	
char *pgm;
	
nomem ()
{
  fprintf (stderr, "%s: out of memory\n", pgm);
  exit(1);
}
	
char *
Malloc (nb)
     int nb;
{
  char *cp;
	
  cp = malloc(nb);
  if (cp == 0)
    {
      nomem ();
    }
  return cp;
}
	
char *
Realloc (ptr, nb)
     char *ptr;
     int nb;
{
  char *cp;
	
  cp = realloc (ptr,nb);
  if (cp == 0)
    {
      nomem ();
    }
  return cp;
}
	
main (ac, av)
     int ac;
     char **av;
{
  int i;
	
  pgm = av[0];
  append = 0;
  nfiles = 0;
  files = (char **) Malloc (0);
  for (ac--, av++; ac; ac--, av++)
    {
      if (**av == '-')
	{
	  for (++*av; **av; ++*av)
	    {
	      switch (**av)
		{
		case 'a':
		  append = 1;
		  break;

		case 'i':
		  signal (SIGINT, SIG_IGN);
		  break;

		default:
		  fprintf (stderr, "%s: unknown flag -%c\n", pgm, **av);
		  break;
		}
	    }
	}
      else
	{
	  files = (char **) Realloc ((char *)files,
				     (nfiles + 1) * sizeof (char *));
	  files[nfiles] = *av;
	  nfiles++;
	}
    }
  fs = (FILE **) Malloc (nfiles * sizeof (FILE *));
  for (i = 0; i < nfiles; i++)
    {
      fs[i] = fopen (files[i], append ? "a" : "w");
      if (fs[i] == NULL)
	{
	  fprintf (stderr, "%s: cannot open %s: ", pgm, files[i]);
	  perror (0);
	}
    }
  while (1)
    {
      register char c = getchar ();
      if (feof (stdin))
	{
	  exit (1);
	}
      putchar (c);
      for (i = 0; i < nfiles; i++)
	{
	  if (fs[i] != NULL)
	    {
	      putc (c, fs[i]);
	    }
	}
    }
}
