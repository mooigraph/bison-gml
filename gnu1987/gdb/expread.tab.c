
# line 31 "expread.y"
#include "defs.h"
#include "param.h"
#include "symtab.h"
#include "frame.h"
#include "expression.h"

#include <stdio.h>

static struct expression *expout;
static int expout_size;
static int expout_ptr;

static int yylex ();
static yyerror ();
static void write_exp_elt ();
static void write_exp_string ();
static void start_arglist ();
static int end_arglist ();
static void free_funcalls ();
static char *copy_name ();

/* If this is nonzero, this block is used as the lexical context
   for symbol names.  */

static struct block *expression_context_block;

/* Number of arguments seen so far in innermost function call.  */
static int arglist_len;

/* Data structure for saving values of arglist_len
   for function calls whose arguments contain other function calls.  */

struct funcall
  {
    struct funcall *next;
    int arglist_len;
  };

struct funcall *funcall_chain;

/* This kind of datum is used to represent the name
   of a symbol token.  */

struct stoken
  {
    char *ptr;
    int length;
  };

# line 85 "expread.y"
typedef union 
  {
    long lval;
    double dval;
    struct symbol *sym;
    struct type *tval;
    struct stoken sval;
    int voidval;
    struct block *bval;
    enum exp_opcode opcode;
    struct internalvar *ivar;
  } YYSTYPE;
# define INT 257
# define CHAR 258
# define FLOAT 259
# define NAME 260
# define TYPENAME 261
# define STRING 262
# define STRUCT 263
# define UNION 264
# define ENUM 265
# define SIZEOF 266
# define UNSIGNED 267
# define COLONCOLON 268
# define LAST 269
# define REGNAME 270
# define VARIABLE 271
# define ASSIGN_MODIFY 272
# define ABOVE_COMMA 273
# define OR 274
# define AND 275
# define EQUAL 276
# define NOTEQUAL 277
# define LEQ 278
# define GEQ 279
# define LSH 280
# define RSH 281
# define UNARY 282
# define INCREMENT 283
# define DECREMENT 284
# define ARROW 285
#define yyclearin yychar = -1
#define yyerrok yyerrflag = 0
extern int yychar;
extern short yyerrflag;
#ifndef YYMAXDEPTH
#define YYMAXDEPTH 150
#endif
YYSTYPE yylval, yyval;
# define YYERRCODE 256

# line 496 "expread.y"


/* Begin counting arguments for a function call,
   saving the data about any containing call.  */

static void
start_arglist ()
{
  register struct funcall *new = (struct funcall *) xmalloc (sizeof (struct funcall));

  new->next = funcall_chain;
  new->arglist_len = arglist_len;
  arglist_len = 0;
  funcall_chain = new;
}

/* Return the number of arguments in a function call just terminated,
   and restore the data for the containing function call.  */

static int
end_arglist ()
{
  register int val = arglist_len;
  register struct funcall *call = funcall_chain;
  funcall_chain = call->next;
  arglist_len = call->arglist_len;
  free (call);
  return val;
}

/* Free everything in the funcall chain.
   Used when there is an error inside parsing.  */

static void
free_funcalls ()
{
  register struct funcall *call, *next;

  for (call = funcall_chain; call; call = next)
    {
      next = call->next;
      free (call);
    }
}

/* This page contains the functions for adding data to the  struct expression
   being constructed.  */

/* Add one element to the end of the expression.  */

static void
write_exp_elt (expelt)
     union exp_element expelt;
{
  if (expout_ptr >= expout_size)
    {
      expout_size *= 2;
      expout = (struct expression *) xrealloc (expout,
					       sizeof (struct expression)
					       + expout_size * sizeof (union exp_element));
    }
  expout->elts[expout_ptr++] = expelt;
}

/* Add a string constant to the end of the expression.
   Follow it by its length in bytes, as a separate exp_element.  */

static void
write_exp_string (str)
     struct stoken str;
{
  register int len = str.length;
  register int lenelt
    = (len + sizeof (union exp_element)) / sizeof (union exp_element);

  expout_ptr += lenelt;

  if (expout_ptr >= expout_size)
    {
      expout_size = max (expout_size * 2, expout_ptr + 10);
      expout = (struct expression *) xrealloc (expout,
					       sizeof (struct expression)
					       + expout_size * sizeof (union exp_element));
    }
  bcopy (str.ptr, (char *) &expout->elts[expout_ptr - lenelt], len);
  ((char *) &expout->elts[expout_ptr - lenelt])[len] = 0;
  write_exp_elt (len);
}

/* During parsing of a C expression, the pointer to the next character
   is in this variable.  */

static char *lexptr;

/* Tokens that refer to names do so with explicit pointer and length,
   so they can share the storage that lexptr is parsing.

   When it is necessary to pass a name to a function that expects
   a null-terminated string, the substring is copied out
   into a block of storage that namecopy points to.

   namecopy is allocated once, guaranteed big enough, for each parsing.  */

static char *namecopy;

/* Take care of parsing a number (anything that starts with a digit).
   Set yylval and return the token type; update lexptr.
   LEN is the number of characters in it.  */

/*** Needs some error checking for the float case ***/

static int
parse_number (olen)
     int olen;
{
  register char *p = lexptr;
  register long n = 0;
  register int c;
  register int base = 10;
  register len = olen;
  char *err_copy;

  extern double atof ();

  for (c = 0; c < len; c++)
    if (p[c] == '.')
      {
	/* It's a float since it contains a point.  */
	yylval.dval = atof (p);
	lexptr += len;
	return FLOAT;
      }

  if (len >= 3 && (!strncmp (p, "0x", 2) || !strncmp (p, "0X", 2)))
    {
      p += 2;
      base = 16;
      len -= 2;
    }
  else if (*p == '0')
    base = 8;

  while (len-- > 0)
    {
      c = *p++;
      n *= base;
      if (c >= '0' && c <= '9')
	n += c - '0';
      else
	{
	  if (c >= 'A' && c <= 'Z') c += 'a' - 'A';
	  if (base == 16 && c >= 'a' && c <= 'f')
	    n += c - 'a' + 10;
	  else if (len == 0 && c == 'l')
	    ;
	  else
	    {
	      err_copy = (char *) alloca (olen + 1);
	      bcopy (lexptr, err_copy, olen);
	      err_copy[olen] = 0;
	      error ("Invalid number \"%s\".", err_copy);
	    }
	}
    }

  lexptr = p;
  yylval.lval = n;
  return INT;
}

struct token
{
  char *operator;
  int token;
  enum exp_opcode opcode;
};

static struct token tokentab3[] =
  {
    {">>=", ASSIGN_MODIFY, BINOP_RSH},
    {"<<=", ASSIGN_MODIFY, BINOP_LSH}
  };

static struct token tokentab2[] =
  {
    {"+=", ASSIGN_MODIFY, BINOP_ADD},
    {"-=", ASSIGN_MODIFY, BINOP_SUB},
    {"*=", ASSIGN_MODIFY, BINOP_MUL},
    {"/=", ASSIGN_MODIFY, BINOP_DIV},
    {"%=", ASSIGN_MODIFY, BINOP_REM},
    {"|=", ASSIGN_MODIFY, BINOP_LOGIOR},
    {"&=", ASSIGN_MODIFY, BINOP_LOGAND},
    {"^=", ASSIGN_MODIFY, BINOP_LOGXOR},
    {"++", INCREMENT, BINOP_END},
    {"--", DECREMENT, BINOP_END},
    {"->", ARROW, BINOP_END},
    {"&&", AND, BINOP_END},
    {"||", OR, BINOP_END},
    {"::", COLONCOLON, BINOP_END},
    {"<<", LSH, BINOP_END},
    {">>", RSH, BINOP_END},
    {"==", EQUAL, BINOP_END},
    {"!=", NOTEQUAL, BINOP_END},
    {"<=", LEQ, BINOP_END},
    {">=", GEQ, BINOP_END}
  };

/* Read one token, getting characters through lexptr.  */

static int
yylex ()
{
  register int c;
  register int namelen;
  register int i;
  register char *tokstart;

 retry:

  tokstart = lexptr;
  /* See if it is a special token of length 3.  */
  for (i = 0; i < sizeof tokentab3 / sizeof tokentab3[0]; i++)
    if (!strncmp (tokstart, tokentab3[i].operator, 3))
      {
	lexptr += 3;
	yylval.opcode = tokentab3[i].opcode;
	return tokentab3[i].token;
      }

  /* See if it is a special token of length 2.  */
  for (i = 0; i < sizeof tokentab2 / sizeof tokentab2[0]; i++)
    if (!strncmp (tokstart, tokentab2[i].operator, 2))
      {
	lexptr += 2;
	yylval.opcode = tokentab2[i].opcode;
	return tokentab2[i].token;
      }

  switch (c = *tokstart)
    {
    case 0:
      return 0;

    case ' ':
    case '\t':
    case '\n':
      lexptr++;
      goto retry;

    case '\'':
      lexptr++;
      c = *lexptr++;
      if (c == '\\')
	c = parse_escape (&lexptr);
      yylval.lval = c;
      c = *lexptr++;
      if (c != '\'')
	error ("Invalid character constant.");
      return CHAR;

    case '+':
    case '-':
    case '*':
    case '/':
    case '%':
    case '|':
    case '&':
    case '^':
    case '~':
    case '!':
    case '@':
    case '<':
    case '>':
    case '(':
    case ')':
    case '[':
    case ']':
    case '.':
    case '?':
    case ':':
    case '=':
    case '{':
    case '}':
    case ',':
      lexptr++;
      return c;

    case '"':
      for (namelen = 1; (c = tokstart[namelen]) != '"'; namelen++)
	if (c == '\\')
	  {
	    c = tokstart[++namelen];
	    if (c >= '0' && c <= '9')
	      {
		c = tokstart[++namelen];
		if (c >= '0' && c <= '9')
		  c = tokstart[++namelen];
	      }
	  }
      yylval.sval.ptr = tokstart + 1;
      yylval.sval.length = namelen - 1;
      lexptr += namelen + 1;
      return STRING;
    }
  if (c >= '0' && c <= '9')
    {
      /* It's a number */
      for (namelen = 0;
	   c = tokstart[namelen],
	   (c == '_' || c == '$' || c == '.' || (c >= '0' && c <= '9')
	    || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'));
	   namelen++)
	;
      return parse_number (namelen);
    }

  if (!(c == '_' || c == '$'
	|| (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')))
    error ("Invalid token in expression.");

  /* It is a name.  See how long it is.  */

  for (namelen = 0;
       c = tokstart[namelen],
       (c == '_' || c == '$' || (c >= '0' && c <= '9')
	|| (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'));
       namelen++)
    ;

  /* The token "if" terminates the expression and is NOT 
     removed from the input stream.  */
  if (namelen == 2 && tokstart[0] == 'i' && tokstart[1] == 'f')
    {
      return 0;
    }

  lexptr += namelen;

  /* Handle the tokens $digits; also $ (short for $0) and $$ (short for $$1)
     and $$digits (equivalent to $<-digits> if you could type that).
     Make token type LAST, and put the number (the digits) in yylval.  */

  if (*tokstart == '$')
    {
      register int negate = 0;
      c = 1;
      /* Double dollar means negate the number and add -1 as well.
	 Thus $$ alone means -1.  */
      if (namelen >= 2 && tokstart[1] == '$')
	{
	  negate = 1;
	  c = 2;
	}
      if (c == namelen)
	{
	  /* Just dollars (one or two) */
	  yylval.lval = - negate;
	  return LAST;
	}
      /* Is the rest of the token digits?  */
      for (; c < namelen; c++)
	if (!(tokstart[c] >= '0' && tokstart[c] <= '9'))
	  break;
      if (c == namelen)
	{
	  yylval.lval = atoi (tokstart + 1 + negate);
	  if (negate)
	    yylval.lval = - yylval.lval;
	  return LAST;
	}
    }

  /* Handle tokens that refer to machine registers:
     $ followed by a register name.  */

  if (*tokstart == '$')
    for (c = 0; c < NUM_REGS; c++)
      if (namelen - 1 == strlen (reg_names[c])
	  && !strncmp (tokstart + 1, reg_names[c], namelen - 1))
	{
	  yylval.lval = c;
	  return REGNAME;
	}

  if (namelen == 6 && !strncmp (tokstart, "struct", 6))
    {
      return STRUCT;
    }
  if (namelen == 5 && !strncmp (tokstart, "union", 5))
    {
      return UNION;
    }
  if (namelen == 4 && !strncmp (tokstart, "enum", 4))
    {
      return ENUM;
    }
  if (namelen == 6 && !strncmp (tokstart, "sizeof", 6))
    {
      return SIZEOF;
    }
  if (namelen == 8 && !strncmp (tokstart, "unsigned", 6))
    {
      return UNSIGNED;
    }
  yylval.sval.ptr = tokstart;
  yylval.sval.length = namelen;

  /* Any other names starting in $ are debugger internal variables.  */

  if (*tokstart == '$')
    {
      yylval.ivar = (struct internalvar *) lookup_internalvar (copy_name (yylval.sval) + 1);
      return VARIABLE;
    }

  /* Use token-type TYPENAME for symbols that happen to be defined
     currently as names of types; NAME for other symbols.
     The caller is not constrained to care about the distinction.  */
  if (lookup_typename (copy_name (yylval.sval), expression_context_block, 1))
    return TYPENAME;
  return NAME;
}

static
yyerror ()
{
  error ("Invalid syntax in expression.");
}

/* Return a null-terminated temporary copy of the name
   of a string token.  */

static char *
copy_name (token)
     struct stoken token;
{
  bcopy (token.ptr, namecopy, token.length);
  namecopy[token.length] = 0;
  return namecopy;
}

/* Reverse an expression from suffix form (in which it is constructed)
   to prefix form (in which we can conveniently print or execute it).  */

static void prefixify_subexp ();

static void
prefixify_expression (expr)
     register struct expression *expr;
{
  register int len = sizeof (struct expression) +
				    expr->nelts * sizeof (union exp_element);
  register struct expression *temp
    = (struct expression *) alloca (len);
  register int inpos = expr->nelts, outpos = 0;

  /* Copy the original expression into temp.  */
  bcopy (expr, temp, len);

  prefixify_subexp (temp, expr, inpos, outpos);
}

/* Return the number of exp_elements in the subexpression of EXPR
   whose last exp_element is at index ENDPOS - 1 in EXPR.  */

static int
length_of_subexp (expr, endpos)
     register struct expression *expr;
     register int endpos;
{
  register int oplen = 1;
  register int args = 0;
  register int i;

  i = (int) expr->elts[endpos - 1].opcode;

  switch (i)
    {
    case OP_LONG:
    case OP_DOUBLE:
      oplen = 4;
      break;

    case OP_VAR_VALUE:
    case OP_LAST:
    case OP_REGISTER:
    case OP_INTERNALVAR:
      oplen = 3;
      break;

    case OP_FUNCALL:
      oplen = 3;
      args = 1 + expr->elts[endpos - 2].longconst;
      break;

    case UNOP_CAST:
    case UNOP_MEMVAL:
      oplen = 3;
      args = 1;
      break;

    case STRUCTOP_STRUCT:
    case STRUCTOP_PTR:
      args = 1;
    case OP_STRING:
      oplen = 3 + ((expr->elts[endpos - 2].longconst
		    + sizeof (union exp_element))
		   / sizeof (union exp_element));
		   
      break;

    case TERNOP_COND:
      args = 3;
      break;

    case BINOP_ASSIGN_MODIFY:
      oplen = 3;
      args = 2;
      break;

    default:
      args = 1 + (i < (int) BINOP_END);
    }

  while (args > 0)
    {
      oplen += length_of_subexp (expr, endpos - oplen);
      args--;
    }

  return oplen;
}

/* Copy the subexpression ending just before index INEND in INEXPR
   into OUTEXPR, starting at index OUTBEG.
   In the process, convert it from suffix to prefix form.  */

static void
prefixify_subexp (inexpr, outexpr, inend, outbeg)
     register struct expression *inexpr;
     struct expression *outexpr;
     register int inend;
     int outbeg;
{
  register int oplen = 1;
  register int args = 0;
  register int i;
  int *arglens;
  enum exp_opcode opcode;

  /* Compute how long the last operation is (in OPLEN),
     and also how many preceding subexpressions serve as
     arguments for it (in ARGS).  */

  opcode = inexpr->elts[inend - 1].opcode;
  switch (opcode)
    {
    case OP_LONG:
    case OP_DOUBLE:
      oplen = 4;
      break;

    case OP_VAR_VALUE:
    case OP_LAST:
    case OP_REGISTER:
    case OP_INTERNALVAR:
      oplen = 3;
      break;

    case OP_FUNCALL:
      oplen = 3;
      args = 1 + inexpr->elts[inend - 2].longconst;
      break;

    case UNOP_CAST:
    case UNOP_MEMVAL:
      oplen = 3;
      args = 1;
      break;

    case STRUCTOP_STRUCT:
    case STRUCTOP_PTR:
      args = 1;
    case OP_STRING:
      oplen = 3 + ((inexpr->elts[inend - 2].longconst
		    + sizeof (union exp_element))
		   / sizeof (union exp_element));
		   
      break;

    case TERNOP_COND:
      args = 3;
      break;

    case BINOP_ASSIGN_MODIFY:
      oplen = 3;
      args = 2;
      break;

    default:
      args = 1 + ((int) opcode < (int) BINOP_END);
    }

  /* Copy the final operator itself, from the end of the input
     to the beginning of the output.  */
  inend -= oplen;
  bcopy (&inexpr->elts[inend], &outexpr->elts[outbeg],
	 oplen * sizeof (union exp_element));
  outbeg += oplen;

  /* Find the lengths of the arg subexpressions.  */
  arglens = (int *) alloca (args * sizeof (int));
  for (i = args - 1; i >= 0; i--)
    {
      oplen = length_of_subexp (inexpr, inend);
      arglens[i] = oplen;
      inend -= oplen;
    }

  /* Now copy each subexpression, preserving the order of
     the subexpressions, but prefixifying each one.
     In this loop, inend starts at the beginning of
     the expression this level is working on
     and marches forward over the arguments.
     outbeg does similarly in the output.  */
  for (i = 0; i < args; i++)
    {
      oplen = arglens[i];
      inend += oplen;
      prefixify_subexp (inexpr, outexpr, inend, outbeg);
      outbeg += oplen;
    }
}

/* This page contains the two entry points to this file.  */

/* Read a C expression from the string *STRINGPTR points to,
   parse it, and return a pointer to a  struct expression  that we malloc.
   Use block BLOCK as the lexical context for variable names;
   if BLOCK is zero, use the block of the selected stack frame.
   Meanwhile, advance *STRINGPTR to point after the expression,
   at the first nonwhite character that is not part of the expression
   (possibly a null character).  */

struct expression *
parse_c_1 (stringptr, block)
     char **stringptr;
     struct block *block;
{
  struct cleanup *old_chain;

  lexptr = *stringptr;

  if (lexptr == 0 || *lexptr == 0)
    error_no_arg ("expression to compute");

  old_chain = make_cleanup (free_funcalls, 0);
  funcall_chain = 0;

  expression_context_block = block ? block : get_selected_block ();

  namecopy = (char *) alloca (strlen (lexptr) + 1);
  expout_size = 10;
  expout_ptr = 0;
  expout = (struct expression *) xmalloc (sizeof (struct expression)
					  + expout_size * sizeof (union exp_element));
  make_cleanup (free_current_contents, &expout);
  if (yyparse ())
    yyerror ();
  discard_cleanups (old_chain);
  expout->nelts = expout_ptr;
  expout = (struct expression *)
    xrealloc (expout,
	      sizeof (struct expression)
	      + expout_ptr * sizeof (union exp_element));
  prefixify_expression (expout);
  *stringptr = lexptr;
  return expout;
}

/* Parse STRING as an expression, and complain if this fails
   to use up all of the contents of STRING.  */

struct expression *
parse_c_expression (string)
     char *string;
{
  register struct expression *exp;
  exp = parse_c_1 (&string, 0);
  if (*string)
    error ("Junk after end of expression.");
  return exp;
}
short yyexca[] ={
-1, 1,
	0, -1,
	-2, 0,
-1, 23,
	268, 67,
	-2, 59,
-1, 73,
	268, 68,
	-2, 62,
-1, 112,
	268, 57,
	-2, 58,
	};
# define YYNPROD 69
# define YYLAST 803
short yyact[]={

  36,  47,  74,  32,  26,  34,  37, 105,  38,  30,
  35,  66,  26,  67,  68,  69,  32,  70,  77,  25,
  64, 116,  30,  45,  53,  46,  52,  33, 120, 111,
 114, 121,  26,  80,  71,  36,  47,  22,  32,  52,
  34,  37,  65,  38,  30,  35, 117, 105, 110, 105,
  72,   2,  17, 113,  31,   1,   0,  48,  45,  53,
  46,  52,  33,   0,   0,   0,   0,  31,   0,   0,
  36,  47,   0,  32,   0,  34,  37,   0,  38,  30,
  35,   0,  79,   0, 103,   0,   0,  49,   0,  31,
 104,   0,  48,  45,   0,  46,  52,  33,   0,   0,
   0,   0,   0,   0,   0,  36,  47,   0,  32,   0,
  34,  37,   0,  38,  30,  35,   0,   0,   0,   0,
   0,   0,  49,   0,  31,   0,   0,  48,  45,   0,
  46,  52,  33,   0,   0,   0,   0,   0,   0,   0,
  36,  47,   0,  32,   0,  34,  37,   0,  38,  30,
  35,   0,   0,   0,  24,   0,   0,  49,   0,  31,
   0,   0,  48,  45,  32,  46,  52,  33,   0,   0,
  30,   0,   0,  36,  47,   0,  32,   0,  34,  37,
   0,  38,  30,  35,  76,  78,   0,  52,  33,   0,
   0,   0,  49,   0,  31,   0,  45,  48,  46,  52,
  33,   7,   0,   0,   0,   0,   5,   0,  13,   0,
   4,   0,   0,   6,   0,  31,   0,   0,   0,   0,
   0,   0, 106, 107, 108, 109,   0,  31,   0, 112,
   0,   0,   0,   0,   0,  54,   0,  51,  50,  41,
  42,  43,  44,  39,  40,   7,  27,  28,  29,   0,
   5,   0,  13,   0,   4,   0,   0,   6,   0,  27,
  28,  29,   0,   0,   0,   0,   0,   0,   0,   0,
  54,   0,  51,  50,  41,  42,  43,  44,  39,  40,
   7,  27,  28,  29,   0,   5,   0,  63,   0,   4,
   0,  12,   6,   0,   8,   0,  36,   0,   0,  32,
   0,  34,  37,   0,  38,  30,  35,   0,  50,  41,
  42,  43,  44,  39,  40,   0,  27,  28,  29,  45,
   0,  46,  52,  33,   0,   0,   0,   0,   0,   0,
   0,  36,   0,   0,  32,  12,  34,  37,   8,  38,
  30,  35,   0,   0,  41,  42,  43,  44,  39,  40,
  31,  27,  28,  29,  45,   0,  46,  52,  33,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
  12,   0,   0,   8,   0,   0,   0,   0,   0,  41,
  42,  43,  44,  39,  40,  31,  27,  28,  29,   0,
   0,   0,   0,   0,   0,   0,  36,   0,   0,  32,
   0,  34,  37,   0,  38,  30,  35,  27,  28,  29,
   0,   0,  41,  42,  43,  44,  39,  40,   0,  27,
  28,  29,  52,  33,   0,  14,  15,  16,  23,  73,
  21,  67,  68,  69,  11,  70,   0,  18,  19,  20,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
  31,   9,  10,   0,   0,   0,   0,   0,  36,   0,
   0,  32,   0,  34,  37,   0,  38,  30,  35,  14,
  15,  16,  23,  25,  21,   0,   0,   0,  11,   0,
   0,  18,  19,  20,  52,  33,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   9,  10,   0,   0,   0,
   0,   0,   0,   0,  14,  15,  16,  23,  25,  21,
   0,   0,  31,  11,   0,   0,  18,  19,  20,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
   9,  10,   0,   0,   0,  41,  42,  43,  44,  39,
  40,   0,  27,  28,  29,   0,   0,   0,   0,   0,
   0,   0,   0,   0,  36,   0,   0,  32,   0,  34,
   0,   0,   0,  30,  35,   0,   0,   0,   0,   0,
   0,   0,  43,  44,  39,  40,   0,  27,  28,  29,
  52,  33,   0,   0,   0,   0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,  31,   0,
   0,   3,   0,   0,   0,   0,  55,  56,  57,  58,
  59,  60,  61,  62,   0,   0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,  75,  39,
  40,   0,  27,  28,  29,  81,  82,  83,  84,  85,
  86,  87,  88,  89,  90,  91,  92,  93,  94,  95,
  96,  97,  98,  99, 100, 101, 102,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
   0,   0, 115,   0,   0,   0,   0,   0,   0,   0,
   0,   0,   0,   0,  27,  28,  29,   0,   0,   0,
   0,   0,   0,   0,   0,   0, 118,   0,   0,   0,
   0,   0, 119,   0,   0,   0,   0,   0, 122, 119,
   0,   0,   0, 123,   0,   0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
  27,  28,  29 };
short yypact[]={

 212,-1000, -32,  -2, 212, 212, 212, 212, 212, 212,
 212, 247,-250, 168,-1000,-1000,-1000,-1000,-1000,-1000,
-1000,-1000,-266,-1000,-1000,-1000, 212,-1000,-1000,-242,
-242, 212,-1000, 212, 212, 212, 212, 212, 212, 212,
 212, 212, 212, 212, 212, 212, 212, 212, 212, 212,
 212, 212, 212, 212, 212, -24, -24, -24, -24, -24,
 -24, -24, -24, 168, -35,-1000,-1000,-242,-242,-242,
-242,   7, -12,-1000,-242,  -2,-1000,-1000,-1000, -40,
 212, -24, 124, 124, 124, 517, 517, 421, 421, 294,
 294, 359, 359, 359, 359, 259, 136, 103,  68,  33,
 -37,  -2,  -2,   5, 212,-1000,-1000,-1000,-1000,-1000,
 212,-1000,-1000,-1000, -13,  -2, 212, 212, -24, -24,
-1000, 212,  -2,  -2 };
short yypgo[]={

   0, 611,  50,  55,  52,  20,  42,  37, 154,  33,
  30 };
short yyr1[]={

   0,   3,   2,   2,   1,   1,   1,   1,   1,   1,
   1,   1,   1,   1,   1,   1,   1,   9,   1,  10,
  10,  10,   1,   1,   1,   1,   1,   1,   1,   1,
   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
   1,   1,   1,   1,   1,   1,   7,   7,   4,   4,
   5,   5,   6,   6,   6,   6,   6,   8,   8 };
short yyr2[]={

   0,   1,   1,   3,   2,   2,   2,   2,   2,   2,
   2,   2,   2,   2,   3,   3,   4,   0,   5,   0,
   1,   3,   4,   4,   3,   3,   3,   3,   3,   3,
   3,   3,   3,   3,   3,   3,   3,   3,   3,   3,
   3,   3,   3,   3,   5,   3,   3,   1,   1,   1,
   1,   1,   1,   1,   4,   1,   1,   3,   3,   1,
   1,   2,   1,   2,   2,   2,   2,   1,   1 };
short yychk[]={

-1000,  -3,  -2,  -1,  42,  38,  45,  33, 126, 283,
 284, 266, 123,  40, 257, 258, 259,  -4, 269, 270,
 271, 262,  -7, 260,  -8, 261,  44, 283, 284, 285,
  46,  91,  40,  64,  42,  47,  37,  43,  45, 280,
 281, 276, 277, 278, 279,  60,  62,  38,  94, 124,
 275, 274,  63,  61, 272,  -1,  -1,  -1,  -1,  -1,
  -1,  -1,  -1,  40,  -5,  -6, 261, 263, 264, 265,
 267,  -5,  -2, 261, 268,  -1,  -8, 260,  -8,  -2,
  -9,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
  -1,  -1,  -1,  -5, 125,  42,  -8,  -8,  -8,  -8,
  41,  41,  -8,  93, -10,  -1,  58,  41,  -1,  -1,
  41,  44,  -1,  -1 };
short yydef[]={

   0,  -2,   1,   2,   0,   0,   0,   0,   0,   0,
   0,   0,   0,   0,  47,  48,  49,  50,  51,  52,
  53,  55,   0,  -2,  56,  68,   0,  11,  12,   0,
   0,   0,  17,   0,   0,   0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   4,   5,   6,   7,   8,
   9,  10,  13,   0,   0,  60,  62,   0,   0,   0,
   0,   0,   0,  -2,   0,   3,  14,  67,  15,   0,
  19,  25,  26,  27,  28,  29,  30,  31,  32,  33,
  34,  35,  36,  37,  38,  39,  40,  41,  42,  43,
   0,  45,  46,   0,   0,  61,  63,  64,  65,  66,
   0,  24,  -2,  16,   0,  20,   0,  54,  22,  23,
  18,   0,  44,  21 };
#ifndef lint
static char yaccpar_sccsid[] = "@(#)yaccpar	4.1	(Berkeley)	2/11/83";
#endif not lint

#
# define YYFLAG -1000
# define YYERROR goto yyerrlab
# define YYACCEPT return(0)
# define YYABORT return(1)

/*	parser for yacc output	*/

#ifdef YYDEBUG
int yydebug = 0; /* 1 for debugging */
#endif
YYSTYPE yyv[YYMAXDEPTH]; /* where the values are stored */
int yychar = -1; /* current input token number */
int yynerrs = 0;  /* number of errors */
short yyerrflag = 0;  /* error recovery flag */

yyparse() {

	short yys[YYMAXDEPTH];
	short yyj, yym;
	register YYSTYPE *yypvt;
	register short yystate, *yyps, yyn;
	register YYSTYPE *yypv;
	register short *yyxi;

	yystate = 0;
	yychar = -1;
	yynerrs = 0;
	yyerrflag = 0;
	yyps= &yys[-1];
	yypv= &yyv[-1];

 yystack:    /* put a state and value onto the stack */

#ifdef YYDEBUG
	if( yydebug  ) printf( "state %d, char 0%o\n", yystate, yychar );
#endif
		if( ++yyps> &yys[YYMAXDEPTH] ) { yyerror( "yacc stack overflow" ); return(1); }
		*yyps = yystate;
		++yypv;
		*yypv = yyval;

 yynewstate:

	yyn = yypact[yystate];

	if( yyn<= YYFLAG ) goto yydefault; /* simple state */

	if( yychar<0 ) if( (yychar=yylex())<0 ) yychar=0;
	if( (yyn += yychar)<0 || yyn >= YYLAST ) goto yydefault;

	if( yychk[ yyn=yyact[ yyn ] ] == yychar ){ /* valid shift */
		yychar = -1;
		yyval = yylval;
		yystate = yyn;
		if( yyerrflag > 0 ) --yyerrflag;
		goto yystack;
		}

 yydefault:
	/* default state action */

	if( (yyn=yydef[yystate]) == -2 ) {
		if( yychar<0 ) if( (yychar=yylex())<0 ) yychar = 0;
		/* look through exception table */

		for( yyxi=yyexca; (*yyxi!= (-1)) || (yyxi[1]!=yystate) ; yyxi += 2 ) ; /* VOID */

		while( *(yyxi+=2) >= 0 ){
			if( *yyxi == yychar ) break;
			}
		if( (yyn = yyxi[1]) < 0 ) return(0);   /* accept */
		}

	if( yyn == 0 ){ /* error */
		/* error ... attempt to resume parsing */

		switch( yyerrflag ){

		case 0:   /* brand new error */

			yyerror( "syntax error" );
		yyerrlab:
			++yynerrs;

		case 1:
		case 2: /* incompletely recovered error ... try again */

			yyerrflag = 3;

			/* find a state where "error" is a legal shift action */

			while ( yyps >= yys ) {
			   yyn = yypact[*yyps] + YYERRCODE;
			   if( yyn>= 0 && yyn < YYLAST && yychk[yyact[yyn]] == YYERRCODE ){
			      yystate = yyact[yyn];  /* simulate a shift of "error" */
			      goto yystack;
			      }
			   yyn = yypact[*yyps];

			   /* the current yyps has no shift onn "error", pop stack */

#ifdef YYDEBUG
			   if( yydebug ) printf( "error recovery pops state %d, uncovers %d\n", *yyps, yyps[-1] );
#endif
			   --yyps;
			   --yypv;
			   }

			/* there is no state on the stack with an error shift ... abort */

	yyabort:
			return(1);


		case 3:  /* no shift yet; clobber input char */

#ifdef YYDEBUG
			if( yydebug ) printf( "error recovery discards char %d\n", yychar );
#endif

			if( yychar == 0 ) goto yyabort; /* don't discard EOF, quit */
			yychar = -1;
			goto yynewstate;   /* try again in the same state */

			}

		}

	/* reduction by production yyn */

#ifdef YYDEBUG
		if( yydebug ) printf("reduce %d\n",yyn);
#endif
		yyps -= yyr2[yyn];
		yypvt = yypv;
		yypv -= yyr2[yyn];
		yyval = yypv[1];
		yym=yyn;
			/* consult goto table to find next state */
		yyn = yyr1[yyn];
		yyj = yypgo[yyn] + *yyps + 1;
		if( yyj>=YYLAST || yychk[ yystate = yyact[yyj] ] != -yyn ) yystate = yyact[yypgo[yyn]];
		switch(yym){
			
case 3:
# line 151 "expread.y"
{ write_exp_elt (BINOP_COMMA); } break;
case 4:
# line 156 "expread.y"
{ write_exp_elt (UNOP_IND); } break;
case 5:
# line 159 "expread.y"
{ write_exp_elt (UNOP_ADDR); } break;
case 6:
# line 162 "expread.y"
{ write_exp_elt (UNOP_NEG); } break;
case 7:
# line 166 "expread.y"
{ write_exp_elt (UNOP_ZEROP); } break;
case 8:
# line 170 "expread.y"
{ write_exp_elt (UNOP_LOGNOT); } break;
case 9:
# line 174 "expread.y"
{ write_exp_elt (UNOP_PREINCREMENT); } break;
case 10:
# line 178 "expread.y"
{ write_exp_elt (UNOP_PREDECREMENT); } break;
case 11:
# line 182 "expread.y"
{ write_exp_elt (UNOP_POSTINCREMENT); } break;
case 12:
# line 186 "expread.y"
{ write_exp_elt (UNOP_POSTDECREMENT); } break;
case 13:
# line 190 "expread.y"
{ write_exp_elt (UNOP_SIZEOF); } break;
case 14:
# line 194 "expread.y"
{ write_exp_elt (STRUCTOP_PTR);
			  write_exp_string (yypvt[-0].sval);
			  write_exp_elt (STRUCTOP_PTR); } break;
case 15:
# line 200 "expread.y"
{ write_exp_elt (STRUCTOP_STRUCT);
			  write_exp_string (yypvt[-0].sval);
			  write_exp_elt (STRUCTOP_STRUCT); } break;
case 16:
# line 206 "expread.y"
{ write_exp_elt (BINOP_SUBSCRIPT); } break;
case 17:
# line 212 "expread.y"
{ start_arglist (); } break;
case 18:
# line 214 "expread.y"
{ write_exp_elt (OP_FUNCALL);
			  write_exp_elt (end_arglist ());
			  write_exp_elt (OP_FUNCALL); } break;
case 20:
# line 223 "expread.y"
{ arglist_len = 1; } break;
case 21:
# line 227 "expread.y"
{ arglist_len++; } break;
case 22:
# line 231 "expread.y"
{ write_exp_elt (UNOP_MEMVAL);
			  write_exp_elt (yypvt[-2].tval);
			  write_exp_elt (UNOP_MEMVAL); } break;
case 23:
# line 237 "expread.y"
{ write_exp_elt (UNOP_CAST);
			  write_exp_elt (yypvt[-2].tval);
			  write_exp_elt (UNOP_CAST); } break;
case 24:
# line 243 "expread.y"
{ } break;
case 25:
# line 249 "expread.y"
{ write_exp_elt (BINOP_REPEAT); } break;
case 26:
# line 253 "expread.y"
{ write_exp_elt (BINOP_MUL); } break;
case 27:
# line 257 "expread.y"
{ write_exp_elt (BINOP_DIV); } break;
case 28:
# line 261 "expread.y"
{ write_exp_elt (BINOP_REM); } break;
case 29:
# line 265 "expread.y"
{ write_exp_elt (BINOP_ADD); } break;
case 30:
# line 269 "expread.y"
{ write_exp_elt (BINOP_SUB); } break;
case 31:
# line 273 "expread.y"
{ write_exp_elt (BINOP_LSH); } break;
case 32:
# line 277 "expread.y"
{ write_exp_elt (BINOP_RSH); } break;
case 33:
# line 281 "expread.y"
{ write_exp_elt (BINOP_EQUAL); } break;
case 34:
# line 285 "expread.y"
{ write_exp_elt (BINOP_NOTEQUAL); } break;
case 35:
# line 289 "expread.y"
{ write_exp_elt (BINOP_LEQ); } break;
case 36:
# line 293 "expread.y"
{ write_exp_elt (BINOP_GEQ); } break;
case 37:
# line 297 "expread.y"
{ write_exp_elt (BINOP_LESS); } break;
case 38:
# line 301 "expread.y"
{ write_exp_elt (BINOP_GTR); } break;
case 39:
# line 305 "expread.y"
{ write_exp_elt (BINOP_LOGAND); } break;
case 40:
# line 309 "expread.y"
{ write_exp_elt (BINOP_LOGXOR); } break;
case 41:
# line 313 "expread.y"
{ write_exp_elt (BINOP_LOGIOR); } break;
case 42:
# line 317 "expread.y"
{ write_exp_elt (BINOP_AND); } break;
case 43:
# line 321 "expread.y"
{ write_exp_elt (BINOP_OR); } break;
case 44:
# line 325 "expread.y"
{ write_exp_elt (TERNOP_COND); } break;
case 45:
# line 329 "expread.y"
{ write_exp_elt (BINOP_ASSIGN); } break;
case 46:
# line 333 "expread.y"
{ write_exp_elt (BINOP_ASSIGN_MODIFY);
			  write_exp_elt (yypvt[-1].opcode);
			  write_exp_elt (BINOP_ASSIGN_MODIFY); } break;
case 47:
# line 339 "expread.y"
{ write_exp_elt (OP_LONG);
			  write_exp_elt (builtin_type_long);
			  write_exp_elt (yypvt[-0].lval);
			  write_exp_elt (OP_LONG); } break;
case 48:
# line 346 "expread.y"
{ write_exp_elt (OP_LONG);
			  write_exp_elt (builtin_type_char);
			  write_exp_elt (yypvt[-0].lval);
			  write_exp_elt (OP_LONG); } break;
case 49:
# line 353 "expread.y"
{ write_exp_elt (OP_DOUBLE);
			  write_exp_elt (builtin_type_double);
			  write_exp_elt (yypvt[-0].dval);
			  write_exp_elt (OP_DOUBLE); } break;
case 51:
# line 363 "expread.y"
{ write_exp_elt (OP_LAST);
			  write_exp_elt (yypvt[-0].lval);
			  write_exp_elt (OP_LAST); } break;
case 52:
# line 369 "expread.y"
{ write_exp_elt (OP_REGISTER);
			  write_exp_elt (yypvt[-0].lval);
			  write_exp_elt (OP_REGISTER); } break;
case 53:
# line 375 "expread.y"
{ write_exp_elt (OP_INTERNALVAR);
			  write_exp_elt (yypvt[-0].ivar);
			  write_exp_elt (OP_INTERNALVAR); } break;
case 54:
# line 381 "expread.y"
{ write_exp_elt (OP_LONG);
			  write_exp_elt (builtin_type_int);
			  write_exp_elt ((long) TYPE_LENGTH (yypvt[-1].tval));
			  write_exp_elt (OP_LONG); } break;
case 55:
# line 388 "expread.y"
{ write_exp_elt (OP_STRING);
			  write_exp_string (yypvt[-0].sval);
			  write_exp_elt (OP_STRING); } break;
case 56:
# line 394 "expread.y"
{ struct symtab *tem = lookup_symtab (copy_name (yypvt[-0].sval));
			  struct symbol *sym;
			  
			  if (tem)
			    yyval.bval = BLOCKVECTOR_BLOCK (BLOCKVECTOR (tem), 1);
			  else
			    {
			      sym = lookup_symbol (copy_name (yypvt[-0].sval),
						   expression_context_block,
						   VAR_NAMESPACE);
			      if (sym && SYMBOL_CLASS (sym) == LOC_BLOCK)
				yyval.bval = SYMBOL_BLOCK_VALUE (sym);
			      else
				error ("No file or function \"%s\".",
				       copy_name (yypvt[-0].sval));
			    }} break;
case 57:
# line 413 "expread.y"
{ struct symbol *tem
			    = lookup_symbol (yypvt[-0].sval, copy_name (yypvt[-2].bval), VAR_NAMESPACE);
			  if (!tem || SYMBOL_CLASS (tem) != LOC_BLOCK)
			    error ("No function \"%s\" in specified context.",
				   copy_name (yypvt[-2].bval));
			  yyval.bval = SYMBOL_BLOCK_VALUE (tem); } break;
case 58:
# line 422 "expread.y"
{ struct symbol *sym;
			  sym = lookup_symbol (yypvt[-0].sval, copy_name (yypvt[-2].bval), VAR_NAMESPACE);
			  if (sym == 0)
			    error ("No symbol \"%s\" in specified context.",
				   copy_name (yypvt[-0].sval));
			  write_exp_elt (OP_VAR_VALUE);
			  write_exp_elt (sym);
			  write_exp_elt (OP_VAR_VALUE); } break;
case 59:
# line 433 "expread.y"
{ struct symbol *sym;
			  sym = lookup_symbol (copy_name (yypvt[-0].sval),
					      expression_context_block,
					      VAR_NAMESPACE);
			  if (sym)
			    {
			      write_exp_elt (OP_VAR_VALUE);
			      write_exp_elt (sym);
			      write_exp_elt (OP_VAR_VALUE);
			    }
			  else
			    {
			      register char *arg = copy_name (yypvt[-0].sval);
			      register int i;
			      for (i = 0; i < misc_function_count; i++)
				if (!strcmp (misc_function_vector[i].name, arg))
				  break;

			      if (i < misc_function_count)
				{
				  write_exp_elt (OP_LONG);
				  write_exp_elt (builtin_type_int);
				  write_exp_elt (misc_function_vector[i].address);
				  write_exp_elt (OP_LONG);
				  write_exp_elt (UNOP_MEMVAL);
				  write_exp_elt (builtin_type_char);
				  write_exp_elt (UNOP_MEMVAL);
				}
			      else
				if (symtab_list == 0)
				  error ("No symbol table is loaded.  Use the \"symbol-file\" command.");
				else
				  error ("No symbol \"%s\" in current context.",
					 copy_name (yypvt[-0].sval));
			    }
			} break;
case 61:
# line 473 "expread.y"
{ yyval.tval = lookup_pointer_type (yypvt[-1].tval); } break;
case 62:
# line 478 "expread.y"
{ yyval.tval = lookup_typename (copy_name (yypvt[-0].sval),
						expression_context_block, 0); } break;
case 63:
# line 481 "expread.y"
{ yyval.tval = lookup_struct (copy_name (yypvt[-0].sval),
					      expression_context_block); } break;
case 64:
# line 484 "expread.y"
{ yyval.tval = lookup_union (copy_name (yypvt[-0].sval),
					     expression_context_block); } break;
case 65:
# line 487 "expread.y"
{ yyval.tval = lookup_enum (copy_name (yypvt[-0].sval),
					    expression_context_block); } break;
case 66:
# line 490 "expread.y"
{ yyval.tval = lookup_unsigned_typename (copy_name (yypvt[-0].sval)); } break; 
		}
		goto yystack;  /* stack new state and value */

	}
