This is a GNU Assembler version from 1989 and gdb see the ChangeLog file  
  
~~~  
This is the beta-test version of the GNU assembler.  (Probably  
around Version 1.34, but check version.c which gets updated more  
often than this readme.)  
~~~  
  
This has support for the vax cpu  
This has support for the sparc cpu  
This has support for the Intel 80386 cpu  
This has support for the Motorola 68020 cpu  
This has support for the National Semiconductor 32k series cpu  
  
In a.out.h is this header  
~~~  
/* This file describes the a.out file format  
   Copyright (C) 1987 Free Software Foundation, Inc.  
  
This file is part of GAS, the GNU Assembler.  
...  
*/  
~~~  
The oldest file has this header generated in 1985  
~~~  
/*  
 *  
 *	$OBJRECDEF  
 *	Generated automatically by "vms_struct Version 1.00"  
 *	Created from VMS definition file "objrecdef.mar"  
 *	Mon Oct 14 14:01:29 1985  
 *  
 */  
~~~  
  
version.c has this text  
~~~   
Thu Jan  1 00:00:00 1970  Dennis Ritchie  (dmr at alice)  
  
	* universe.c (temporal_reality): Began Time.  
   
~~~  
  
See also oldlinux.org  
  
