#include <vga.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/kd.h>
#include <fcntl.h>
#include <termcap.h>

main(int argc, char* argv[])
{
    int  tty0_fd;   
    int  i, p;
    char *cl;
    char *buffer=0; 
    char *termtype=getenv("TERM");

    vga_setmode(G640x480x16); 

    if (fork() == 0) {
	execlp("startx", "startx", NULL);
	_exit(1);
    } 
    wait(NULL);

    vga_setmode(TEXT); 

    tty0_fd = open("/dev/tty0", O_RDWR);
    ioctl(tty0_fd, KDSETMODE, KD_GRAPHICS); 
    ioctl(tty0_fd, KDSETMODE, KD_TEXT);

    tgetent(buffer, termtype);
    cl=tgetstr("cl",0);
    printf("%s\n",cl);

    exit(1);
}
