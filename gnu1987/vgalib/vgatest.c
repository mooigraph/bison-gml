#include <vga.h>

static char line[1024];

static void testmode(int mode)
{
   int xmax, ymax, colors, i, x, y;

   vga_setmode(mode);

   vga_screenoff();

   xmax   = vga_getxdim()-1;
   ymax   = vga_getydim()-1;

   vga_drawline(   0,    0, xmax,    0);
   vga_drawline(xmax,    0, xmax, ymax);
   vga_drawline(xmax, ymax,    0, ymax);
   vga_drawline(   0, ymax,    0,    0);
   
   for(i = 0; i <= 15; i++) {
       vga_setcolor(i);
       vga_drawline(10+i*5, 10, 90+i*5, 90);
   }
   for(i = 0; i <= 15; i++) {
       vga_setcolor(i);
       vga_drawline(90+i*5, 10, 10+i*5, 90);
   }

   if (vga_getcolors() == 256) {
       for(i = 0; i < 64; i++)
           vga_setpalette(i+128, i, i, i);
       for(i = 0; i <= xmax; i++)
       	   line[i] = 128 + i%64;
   } 
   if (vga_getcolors() == 16)       
       for(i = 0; i <= xmax; i++)
       	   line[i] = i%16;
   if (vga_getcolors() == 2)       
       for(i = 0; i <= xmax; i++)
       	   line[i] = 0x11;
   
   for(i = 100; i < 190; i++)
       vga_drawscanline(i, line);

   vga_screenon(); 

   vga_getch();

   vga_setmode(TEXT);
}


main()
{
    int mode;

    printf("Chose from one of the following video modes: \n");
    printf("     1:  320x200 pixels, 16 colors \n");
    printf("     2:  640x200 pixels, 16 colors \n");
    printf("     3:  640x350 pixels, 16 colors \n");
    printf("     4:  640x480 pixels, 16 colors \n");
    printf("     5:  320x200 pixels, 256 colors \n");
    printf("     6:  320x240 pixels, 256 colors \n");
    printf("     7:  320x400 pixels, 256 colors \n");
    printf("     8:  360x480 pixels, 256 colors \n");
    printf("     9:  640x480 pixels, Monochrome \n");
    printf("    10:  640x480 pixels, 256 colors (only ET4000) \n");
    printf("    11:  800x600 pixels, 256 colors (only ET4000) \n");
    printf("    12: 1024x768 pixels, 256 colors (only ET4000) \n");
    printf("Enter mode number (1-12): ");
    scanf("%d", &mode);
    printf("\n");

    if (mode < 1 || mode > 12) {
	printf("Error: Mode number out of range \n");
	exit(-1);
    }

    if (vga_hasmode(mode))
	testmode(mode);
    else {
	printf("Error: Video mode not supported by graphics card\n");
	exit(-1);
    }
}
