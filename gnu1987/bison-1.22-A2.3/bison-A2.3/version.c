/* The version number for this version of Bison
   Copyright (C) 1984, 1989, 1992 Free Software Foundation, Inc.
   Modified (1993, 1994) from bison-1.22 by 
		Wilfred J. Hansen (wjh+@cmu.edu) 
		Andrew Consortium, Carnegie Mellon University
 */

char *version_string = "Bison version A2.3 (Andrew Consortium)\n";
