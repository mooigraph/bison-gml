# gnu1987

This are the oldest known GNU GPL Free sources from 1987 with the whole toolchain including gcc-0.9, gdb, emacs, chess and utils
  
The work on GNU GPL Free Software started at Date: Tue, 27-Sep-83 12:35:59 EST  
  
The oldest files are from July 1984 in GNU Bison and emacs and in gas from 1985 : Mon Oct 14 14:01:29 1985  
  
~~~  
/*  
 *  
 *	$OBJRECDEF  
 *	Generated automatically by "vms_struct Version 1.00"  
 *	Created from VMS definition file "objrecdef.mar"  
 *	Mon Oct 14 14:01:29 1985  
 *  
 */  
~~~  
  
Most of these Free Software Foundation C source is not on the servers from fsf.org itself  

This has earliest versions of:  
GNU GCC Compiler version 0.9  
GNU G++ Compiler version 1.21  
GNU Bison parser generator 1987  
GNU Bison parser generator 1987 re-compiled in 2021 and working  
GNU Bison parser generator 1988  
GNU Bison parser generator versions not on fsf.org  
GNU Chess program  
GNU Emacs text editor from 1985  
GNU GDB debugger  
GNU utilities with awk, sed, ld, ls, vi and many more  
GNU assembler and gdb   
GNU binutils with ar, ld, ranlib and many more  
  
This can be recompiled and works with some work  
  
The oldest file is in emacs of the year 1982  
~~~  
/*  
 * @(#)nmalloc.c 1 (Caltech) 2/21/82  
 *  
 *	U of M Modified: 20 Jun 1983 ACT: strange hacks for Emacs  
 *  
 *	Nov 1983, Mike@BRL, Added support for 4.1C/4.2 BSD.  
 *  
...  
 */   
~~~  
  
The directory gnu1988 has GNU C++ version 1.21, GNU C version 0.9,  GNU C version 1.21,  
GNU Bison 1.0, GNU GDB 2.5.1 and GNU lib-g++ version 1.20  
  
In directory gnu-bulletin are the GNU Bulletins including the first one  
  
version.c in gas assembler has this special cooment  
~~~   
Thu Jan  1 00:00:00 1970  Dennis Ritchie  (dmr at alice)  
  
	* universe.c (temporal_reality): Began Time.  
   
~~~  
  
See also the archive of Linux kernel sources and tools at oldinux.org  
  
The vgalib directory are the only vga routines for doing 16 color display drawing used at boot screens  
  
