/* Generated automatically by the program `genextract'
from the machine description file `md'.  */

#include "rtl.h"

extern rtx recog_operand[];
extern rtx *recog_operand_loc[];
extern rtx *recog_dup_loc[];
extern char recog_dup_num[];

int
extract_0 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 1));
}

int
extract_1 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 1));
}

int
extract_2 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 1));
}

int
extract_3 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 1));
}

int
extract_4 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 1));
}

int
extract_5 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_6 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_7 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_8 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_9 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_10 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_11 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_12 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_13 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (insn, 1));
}

int
extract_14 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (insn, 1));
}

int
extract_15 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (insn, 1));
}

int
extract_16 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (insn, 1));
}

int
extract_17 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (insn, 1));
}

int
extract_18 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (insn, 1));
}

int
extract_19 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (insn, 1));
}

int
extract_20 (insn)
     rtx insn;
{
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XVECEXP (insn, 0, 1), 0));
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XVECEXP (insn, 0, 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XVECEXP (insn, 0, 0), 1));
}

int
extract_21 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (insn, 1));
}

int
extract_22 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (insn, 1));
}

int
extract_23 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (insn, 1));
}

int
extract_24 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (insn, 1));
}

int
extract_25 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (insn, 1));
}

int
extract_26 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_27 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_28 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_29 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_30 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_31 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_32 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_33 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_34 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_35 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_36 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_37 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_38 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_39 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_40 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_41 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_42 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_43 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_44 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_45 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_46 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_47 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_48 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_49 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_50 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_51 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_52 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_53 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_54 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_55 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_56 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_57 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_58 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_59 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_60 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_61 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_62 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_63 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_64 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_65 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_66 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_67 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_68 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_69 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_70 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_71 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_72 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_73 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_74 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_75 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_76 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_77 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_78 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_79 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_80 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_81 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_82 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_83 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_84 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_85 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_86 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_87 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_88 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_89 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_90 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_91 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_92 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_93 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (insn, 1), 0), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (XEXP (insn, 1), 0), 1));
  recog_operand[3] = *(recog_operand_loc[3]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_94 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (insn, 1), 0), 1));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (XEXP (insn, 1), 0), 2));
  recog_operand[3] = *(recog_operand_loc[3]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_95 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (insn, 1), 0), 1));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (XEXP (insn, 1), 0), 2));
  recog_operand[3] = *(recog_operand_loc[3]
    = &XEXP (XEXP (insn, 1), 1));
}

int
extract_96 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
  recog_operand[3] = *(recog_operand_loc[3]
    = &XEXP (XEXP (insn, 1), 2));
}

int
extract_97 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
  recog_operand[3] = *(recog_operand_loc[3]
    = &XEXP (XEXP (insn, 1), 2));
}

int
extract_98 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
  recog_operand[3] = *(recog_operand_loc[3]
    = &XEXP (XEXP (insn, 1), 2));
}

int
extract_99 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 1), 0));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 1), 1));
  recog_operand[3] = *(recog_operand_loc[3]
    = &XEXP (XEXP (insn, 1), 2));
}

int
extract_100 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (insn, 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 0), 1));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 0), 2));
  recog_operand[3] = *(recog_operand_loc[3]
    = &XEXP (insn, 1));
}

int
extract_101 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (insn, 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (insn, 0), 1));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (insn, 0), 2));
  recog_operand[3] = *(recog_operand_loc[3]
    = &XEXP (insn, 1));
}

int
extract_102 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (insn, 1), 0));
}

int
extract_103 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_104 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_105 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_106 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_107 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_108 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_109 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_110 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_111 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_112 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_113 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 2), 0));
}

int
extract_114 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 2), 0));
}

int
extract_115 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 2), 0));
}

int
extract_116 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 2), 0));
}

int
extract_117 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 2), 0));
}

int
extract_118 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 2), 0));
}

int
extract_119 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 2), 0));
}

int
extract_120 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 2), 0));
}

int
extract_121 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 2), 0));
}

int
extract_122 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 2), 0));
}

int
extract_123 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 2));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_124 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 2));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_125 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 2));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (XEXP (insn, 1), 2), 0));
}

int
extract_126 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 2));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (XEXP (insn, 1), 2), 0));
}

int
extract_127 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 2));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_128 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 2));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_129 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 2));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (XEXP (insn, 1), 2), 0));
}

int
extract_130 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 2));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (XEXP (insn, 1), 2), 0));
}

int
extract_131 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_132 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_133 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (insn, 1), 2), 0));
}

int
extract_134 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (insn, 1), 2), 0));
}

int
extract_135 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_136 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (insn, 1), 1), 0));
}

int
extract_137 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (insn, 1), 2), 0));
}

int
extract_138 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (insn, 1), 2), 0));
}

int
extract_139 (insn)
     rtx insn;
{
  recog_dup_loc[0] = &XEXP (XVECEXP (insn, 0, 1), 0);
  recog_dup_num[0] = 0;
  recog_dup_loc[1] = &XEXP (XEXP (XVECEXP (insn, 0, 1), 1), 0);
  recog_dup_num[1] = 0;
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 1), 0));
}

int
extract_140 (insn)
     rtx insn;
{
  recog_dup_loc[0] = &XEXP (XVECEXP (insn, 0, 1), 0);
  recog_dup_num[0] = 0;
  recog_dup_loc[1] = &XEXP (XEXP (XVECEXP (insn, 0, 1), 1), 0);
  recog_dup_num[1] = 0;
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 1), 0));
}

int
extract_141 (insn)
     rtx insn;
{
  recog_dup_loc[0] = &XEXP (XVECEXP (insn, 0, 1), 0);
  recog_dup_num[0] = 0;
  recog_dup_loc[1] = &XEXP (XEXP (XVECEXP (insn, 0, 1), 1), 0);
  recog_dup_num[1] = 0;
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 2), 0));
}

int
extract_142 (insn)
     rtx insn;
{
  recog_dup_loc[0] = &XEXP (XVECEXP (insn, 0, 1), 0);
  recog_dup_num[0] = 0;
  recog_dup_loc[1] = &XEXP (XEXP (XVECEXP (insn, 0, 1), 1), 0);
  recog_dup_num[1] = 0;
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 2), 0));
}

int
extract_143 (insn)
     rtx insn;
{
  recog_dup_loc[0] = &XEXP (XVECEXP (insn, 0, 1), 0);
  recog_dup_num[0] = 0;
  recog_dup_loc[1] = &XEXP (XEXP (XVECEXP (insn, 0, 1), 1), 0);
  recog_dup_num[1] = 0;
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 0), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 0), 0), 1));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 1), 0));
}

int
extract_144 (insn)
     rtx insn;
{
  recog_dup_loc[0] = &XEXP (XVECEXP (insn, 0, 1), 0);
  recog_dup_num[0] = 0;
  recog_dup_loc[1] = &XEXP (XEXP (XVECEXP (insn, 0, 1), 1), 0);
  recog_dup_num[1] = 0;
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 0), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 0), 0), 1));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 1), 0));
}

int
extract_145 (insn)
     rtx insn;
{
  recog_dup_loc[0] = &XEXP (XVECEXP (insn, 0, 1), 0);
  recog_dup_num[0] = 0;
  recog_dup_loc[1] = &XEXP (XEXP (XVECEXP (insn, 0, 1), 1), 0);
  recog_dup_num[1] = 0;
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 0), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 0), 0), 1));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 2), 0));
}

int
extract_146 (insn)
     rtx insn;
{
  recog_dup_loc[0] = &XEXP (XVECEXP (insn, 0, 1), 0);
  recog_dup_num[0] = 0;
  recog_dup_loc[1] = &XEXP (XEXP (XVECEXP (insn, 0, 1), 1), 0);
  recog_dup_num[1] = 0;
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 0), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 0), 0), 1));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (XEXP (XVECEXP (insn, 0, 0), 1), 2), 0));
}

int
extract_147 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (insn, 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (insn, 1));
}

int
extract_148 (insn)
     rtx insn;
{
}

int
extract_149 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 0), 0), 1));
  recog_operand[2] = *(recog_operand_loc[2]
    = &XEXP (XEXP (XEXP (insn, 1), 0), 1));
  recog_operand[3] = *(recog_operand_loc[3]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 1), 0), 0));
  recog_dup_loc[0] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (insn, 1), 1), 1), 0), 0), 1), 0);
  recog_dup_num[0] = 0;
  recog_dup_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (insn, 1), 1), 1), 0), 0), 1), 1);
  recog_dup_num[1] = 1;
}

int
extract_150 (insn)
     rtx insn;
{
  recog_operand[0] = *(recog_operand_loc[0]
    = &XEXP (XEXP (XEXP (insn, 1), 0), 0));
  recog_operand[1] = *(recog_operand_loc[1]
    = &XEXP (XEXP (XEXP (insn, 1), 0), 1));
  recog_operand[3] = *(recog_operand_loc[3]
    = &XEXP (XEXP (XEXP (XEXP (insn, 1), 1), 0), 0));
  recog_dup_loc[0] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (insn, 1), 1), 1), 0), 0), 1);
  recog_dup_num[0] = 0;
}

int (*insn_extract_fn[]) () =
{ extract_0, extract_1, extract_2, extract_3,
  extract_4, extract_5, extract_6, extract_7,
  extract_8, extract_9, extract_10, extract_11,
  extract_12, extract_13, extract_14, extract_15,
  extract_16, extract_17, extract_18, extract_19,
  extract_20, extract_21, extract_22, extract_23,
  extract_24, extract_25, extract_26, extract_27,
  extract_28, extract_29, extract_30, extract_31,
  extract_32, extract_33, extract_34, extract_35,
  extract_36, extract_37, extract_38, extract_39,
  extract_40, extract_41, extract_42, extract_43,
  extract_44, extract_45, extract_46, extract_47,
  extract_48, extract_49, extract_50, extract_51,
  extract_52, extract_53, extract_54, extract_55,
  extract_56, extract_57, extract_58, extract_59,
  extract_60, extract_61, extract_62, extract_63,
  extract_64, extract_65, extract_66, extract_67,
  extract_68, extract_69, extract_70, extract_71,
  extract_72, extract_73, extract_74, extract_75,
  extract_76, extract_77, extract_78, extract_79,
  extract_80, extract_81, extract_82, extract_83,
  extract_84, extract_85, extract_86, extract_87,
  extract_88, extract_89, extract_90, extract_91,
  extract_92, extract_93, extract_94, extract_95,
  extract_96, extract_97, extract_98, extract_99,
  extract_100, extract_101, extract_102, extract_103,
  extract_104, extract_105, extract_106, extract_107,
  extract_108, extract_109, extract_110, extract_111,
  extract_112, extract_113, extract_114, extract_115,
  extract_116, extract_117, extract_118, extract_119,
  extract_120, extract_121, extract_122, extract_123,
  extract_124, extract_125, extract_126, extract_127,
  extract_128, extract_129, extract_130, extract_131,
  extract_132, extract_133, extract_134, extract_135,
  extract_136, extract_137, extract_138, extract_139,
  extract_140, extract_141, extract_142, extract_143,
  extract_144, extract_145, extract_146, extract_147,
  extract_148, extract_149, extract_150
};

void
insn_extract (insn)
     rtx insn;
{
  if (INSN_CODE (insn) == -1) abort ();
  (*insn_extract_fn[INSN_CODE (insn)]) (PATTERN (insn));
}
