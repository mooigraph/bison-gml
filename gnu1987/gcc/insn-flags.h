/* Generated automatically by the program `genflags'
from the machine description file `md'.  */

#define HAVE_tstdf 1
extern rtx gen_tstdf ();
#define HAVE_tstsf 1
extern rtx gen_tstsf ();
#define HAVE_tstsi 1
extern rtx gen_tstsi ();
#define HAVE_tsthi 1
extern rtx gen_tsthi ();
#define HAVE_tstqi 1
extern rtx gen_tstqi ();
#define HAVE_cmpdf 1
extern rtx gen_cmpdf ();
#define HAVE_cmpsf 1
extern rtx gen_cmpsf ();
#define HAVE_cmpsi 1
extern rtx gen_cmpsi ();
#define HAVE_cmphi 1
extern rtx gen_cmphi ();
#define HAVE_cmpqi 1
extern rtx gen_cmpqi ();
#define HAVE_movdf 1
extern rtx gen_movdf ();
#define HAVE_movsf 1
extern rtx gen_movsf ();
#define HAVE_movti 1
extern rtx gen_movti ();
#define HAVE_movdi 1
extern rtx gen_movdi ();
#define HAVE_movsi 1
extern rtx gen_movsi ();
#define HAVE_movhi 1
extern rtx gen_movhi ();
#define HAVE_movqi 1
extern rtx gen_movqi ();
#define HAVE_movstrhi 1
extern rtx gen_movstrhi ();
#define HAVE_extendqihi2 1
extern rtx gen_extendqihi2 ();
#define HAVE_extendqisi2 1
extern rtx gen_extendqisi2 ();
#define HAVE_floatqisf2 1
extern rtx gen_floatqisf2 ();
#define HAVE_floatqidf2 1
extern rtx gen_floatqidf2 ();
#define HAVE_trunchiqi2 1
extern rtx gen_trunchiqi2 ();
#define HAVE_extendhisi2 1
extern rtx gen_extendhisi2 ();
#define HAVE_floathisf2 1
extern rtx gen_floathisf2 ();
#define HAVE_floathidf2 1
extern rtx gen_floathidf2 ();
#define HAVE_truncsiqi2 1
extern rtx gen_truncsiqi2 ();
#define HAVE_truncsihi2 1
extern rtx gen_truncsihi2 ();
#define HAVE_floatsisf2 1
extern rtx gen_floatsisf2 ();
#define HAVE_floatsidf2 1
extern rtx gen_floatsidf2 ();
#define HAVE_fixsfqi2 1
extern rtx gen_fixsfqi2 ();
#define HAVE_fixsfhi2 1
extern rtx gen_fixsfhi2 ();
#define HAVE_fixsfsi2 1
extern rtx gen_fixsfsi2 ();
#define HAVE_extendsfdf2 1
extern rtx gen_extendsfdf2 ();
#define HAVE_fixdfqi2 1
extern rtx gen_fixdfqi2 ();
#define HAVE_fixdfhi2 1
extern rtx gen_fixdfhi2 ();
#define HAVE_fixdfsi2 1
extern rtx gen_fixdfsi2 ();
#define HAVE_truncdfsf2 1
extern rtx gen_truncdfsf2 ();
#define HAVE_zero_extendqihi2 1
extern rtx gen_zero_extendqihi2 ();
#define HAVE_zero_extendqisi2 1
extern rtx gen_zero_extendqisi2 ();
#define HAVE_zero_extendhisi2 1
extern rtx gen_zero_extendhisi2 ();
#define HAVE_adddf3 1
extern rtx gen_adddf3 ();
#define HAVE_addsf3 1
extern rtx gen_addsf3 ();
#define HAVE_addsi3 1
extern rtx gen_addsi3 ();
#define HAVE_addhi3 1
extern rtx gen_addhi3 ();
#define HAVE_addqi3 1
extern rtx gen_addqi3 ();
#define HAVE_subdf3 1
extern rtx gen_subdf3 ();
#define HAVE_subsf3 1
extern rtx gen_subsf3 ();
#define HAVE_subsi3 1
extern rtx gen_subsi3 ();
#define HAVE_subhi3 1
extern rtx gen_subhi3 ();
#define HAVE_subqi3 1
extern rtx gen_subqi3 ();
#define HAVE_muldf3 1
extern rtx gen_muldf3 ();
#define HAVE_mulsf3 1
extern rtx gen_mulsf3 ();
#define HAVE_mulsi3 1
extern rtx gen_mulsi3 ();
#define HAVE_mulhi3 1
extern rtx gen_mulhi3 ();
#define HAVE_mulqi3 1
extern rtx gen_mulqi3 ();
#define HAVE_divdf3 1
extern rtx gen_divdf3 ();
#define HAVE_divsf3 1
extern rtx gen_divsf3 ();
#define HAVE_divsi3 1
extern rtx gen_divsi3 ();
#define HAVE_divhi3 1
extern rtx gen_divhi3 ();
#define HAVE_divqi3 1
extern rtx gen_divqi3 ();
#define HAVE_andcbsi3 1
extern rtx gen_andcbsi3 ();
#define HAVE_andcbhi3 1
extern rtx gen_andcbhi3 ();
#define HAVE_andcbqi3 1
extern rtx gen_andcbqi3 ();
#define HAVE_iorsi3 1
extern rtx gen_iorsi3 ();
#define HAVE_iorhi3 1
extern rtx gen_iorhi3 ();
#define HAVE_iorqi3 1
extern rtx gen_iorqi3 ();
#define HAVE_xorsi3 1
extern rtx gen_xorsi3 ();
#define HAVE_xorhi3 1
extern rtx gen_xorhi3 ();
#define HAVE_xorqi3 1
extern rtx gen_xorqi3 ();
#define HAVE_negdf2 1
extern rtx gen_negdf2 ();
#define HAVE_negsf2 1
extern rtx gen_negsf2 ();
#define HAVE_negsi2 1
extern rtx gen_negsi2 ();
#define HAVE_neghi2 1
extern rtx gen_neghi2 ();
#define HAVE_negqi2 1
extern rtx gen_negqi2 ();
#define HAVE_one_cmplsi2 1
extern rtx gen_one_cmplsi2 ();
#define HAVE_one_cmplhi2 1
extern rtx gen_one_cmplhi2 ();
#define HAVE_one_cmplqi2 1
extern rtx gen_one_cmplqi2 ();
#define HAVE_ashlsi3 1
extern rtx gen_ashlsi3 ();
#define HAVE_ashldi3 1
extern rtx gen_ashldi3 ();
#define HAVE_rotlsi3 1
extern rtx gen_rotlsi3 ();
#define HAVE_rotldi3 1
extern rtx gen_rotldi3 ();
#define HAVE_extv 1
extern rtx gen_extv ();
#define HAVE_extzv 1
extern rtx gen_extzv ();
#define HAVE_insv 1
extern rtx gen_insv ();
#define HAVE_jump 1
extern rtx gen_jump ();
#define HAVE_beq 1
extern rtx gen_beq ();
#define HAVE_bne 1
extern rtx gen_bne ();
#define HAVE_bgt 1
extern rtx gen_bgt ();
#define HAVE_bgtu 1
extern rtx gen_bgtu ();
#define HAVE_blt 1
extern rtx gen_blt ();
#define HAVE_bltu 1
extern rtx gen_bltu ();
#define HAVE_bge 1
extern rtx gen_bge ();
#define HAVE_bgeu 1
extern rtx gen_bgeu ();
#define HAVE_ble 1
extern rtx gen_ble ();
#define HAVE_bleu 1
extern rtx gen_bleu ();
#define HAVE_call 1
extern rtx gen_call ();
#define HAVE_return 1
extern rtx gen_return ();
#define HAVE_casesi 1
extern rtx gen_casesi ();
