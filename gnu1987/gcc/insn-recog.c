/* Generated automatically by the program `genrecog'
from the machine description file `md'.  */

#include "config.h"
#include "rtl.h"
#include "insn-config.h"
#include "recog.h"

/* `recog' contains a decision tree
   that recognizes whether the rtx X0 is a valid instruction.

   recog returns -1 if the rtx is not valid.
   If the rtx is valid, recog returns a nonnegative number
   which is the insn code number for the pattern that matched.
   This is the same as the order in the machine description of the
   entry that matched.  This number can be used as an index into
   insn_templates and insn_n_operands (found in insn-output.c)
   or as an argument to output_insn_hairy (also in insn-output.c).  */

rtx recog_operand[MAX_RECOG_OPERANDS];

rtx *recog_operand_loc[MAX_RECOG_OPERANDS];

rtx *recog_dup_loc[MAX_DUP_OPERANDS];

char recog_dup_num[MAX_DUP_OPERANDS];

extern rtx recog_addr_dummy;

#define operands recog_operand

int
recog (x0, insn)
     register rtx x0;
     rtx insn;{
  register rtx x1, x2, x3, x4, x5;
  rtx x6, x7, x8, x9, x10, x11;
 L0:
  if (GET_CODE (x0) == SET && 1)
    goto L1;
  if (GET_CODE (x0) == PARALLEL && XVECLEN (x0, 0) == 9 && 1)
    goto L77;
 L849:
  if (GET_CODE (x0) == PARALLEL && XVECLEN (x0, 0) == 2 && 1)
    goto L850;
  if (GET_CODE (x0) == CALL && 1)
    goto L994;
  if (GET_CODE (x0) == RETURN && 1)
    return 148;
  goto ret0;
 L1:
  x1 = XEXP (x0, 0);
  if (x1 == cc0_rtx && 1)
    goto L17;
  if (x1 == pc_rtx && 1)
    goto L479;
  if (GET_CODE (x1) == ZERO_EXTRACT && GET_MODE (x1) == SImode && 1)
    goto L467;
 L56:
  if (general_operand (x1, DFmode))
    { recog_operand[0] = x1; goto L125; }
 L59:
  if (general_operand (x1, SFmode))
    { recog_operand[0] = x1; goto L121; }
 L62:
  if (general_operand (x1, TImode))
    { recog_operand[0] = x1; goto L63; }
 L65:
  if (general_operand (x1, DImode))
    { recog_operand[0] = x1; goto L405; }
 L68:
  if (general_operand (x1, SImode))
    { recog_operand[0] = x1; goto L117; }
 L71:
  if (general_operand (x1, HImode))
    { recog_operand[0] = x1; goto L113; }
 L74:
  if (general_operand (x1, QImode))
    { recog_operand[0] = x1; goto L129; }
  goto ret0;
 L17:
  x1 = XEXP (x0, 1);
  if (GET_CODE (x1) == MINUS && 1)
    goto L428;
  if (GET_CODE (x1) == AND && GET_MODE (x1) == SImode && 1)
    goto L43;
  if (GET_CODE (x1) == AND && GET_MODE (x1) == HImode && 1)
    goto L48;
  if (GET_CODE (x1) == AND && GET_MODE (x1) == QImode && 1)
    goto L53;
 L2:
  if (general_operand (x1, DFmode))
    { recog_operand[0] = x1; return 0; }
 L5:
  if (general_operand (x1, SFmode))
    { recog_operand[0] = x1; return 1; }
 L8:
  if (general_operand (x1, SImode))
    { recog_operand[0] = x1; return 2; }
 L11:
  if (general_operand (x1, HImode))
    { recog_operand[0] = x1; return 3; }
 L14:
  if (general_operand (x1, QImode))
    { recog_operand[0] = x1; return 4; }
  x1 = XEXP (x0, 0);
  goto L56;
 L428:
  x2 = XEXP (x1, 0);
  if (GET_CODE (x2) == SIGN_EXTRACT && GET_MODE (x2) == SImode && 1)
    goto L429;
  if (GET_CODE (x2) == ZERO_EXTRACT && GET_MODE (x2) == SImode && 1)
    goto L437;
 L18:
  if (general_operand (x2, DFmode))
    { recog_operand[0] = x2; goto L19; }
 L23:
  if (general_operand (x2, SFmode))
    { recog_operand[0] = x2; goto L24; }
 L28:
  if (general_operand (x2, SImode))
    { recog_operand[0] = x2; goto L29; }
 L33:
  if (general_operand (x2, HImode))
    { recog_operand[0] = x2; goto L34; }
 L38:
  if (general_operand (x2, QImode))
    { recog_operand[0] = x2; goto L39; }
  goto L2;
 L429:
  x3 = XEXP (x2, 0);
  if (general_operand (x3, QImode))
    { recog_operand[0] = x3; goto L430; }
  goto L18;
 L430:
  x3 = XEXP (x2, 1);
  if (general_operand (x3, SImode))
    { recog_operand[1] = x3; goto L431; }
  goto L18;
 L431:
  x3 = XEXP (x2, 2);
  if (general_operand (x3, SImode))
    { recog_operand[2] = x3; goto L432; }
  goto L18;
 L432:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SImode))
    { recog_operand[3] = x2; return 94; }
  x2 = XEXP (x1, 0);
  goto L18;
 L437:
  x3 = XEXP (x2, 0);
  if (general_operand (x3, QImode))
    { recog_operand[0] = x3; goto L438; }
  goto L18;
 L438:
  x3 = XEXP (x2, 1);
  if (general_operand (x3, SImode))
    { recog_operand[1] = x3; goto L439; }
  goto L18;
 L439:
  x3 = XEXP (x2, 2);
  if (general_operand (x3, SImode))
    { recog_operand[2] = x3; goto L440; }
  goto L18;
 L440:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SImode))
    { recog_operand[3] = x2; return 95; }
  x2 = XEXP (x1, 0);
  goto L18;
 L19:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, DFmode))
    { recog_operand[1] = x2; return 5; }
  x2 = XEXP (x1, 0);
  goto L23;
 L24:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SFmode))
    { recog_operand[1] = x2; return 6; }
  x2 = XEXP (x1, 0);
  goto L28;
 L29:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; return 7; }
  x2 = XEXP (x1, 0);
  goto L33;
 L34:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, HImode))
    { recog_operand[1] = x2; return 8; }
  x2 = XEXP (x1, 0);
  goto L38;
 L39:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, QImode))
    { recog_operand[1] = x2; return 9; }
  goto L2;
 L43:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SImode))
    { recog_operand[0] = x2; goto L44; }
  goto L2;
 L44:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; return 10; }
  goto L2;
 L48:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, HImode))
    { recog_operand[0] = x2; goto L49; }
  goto L2;
 L49:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, HImode))
    { recog_operand[1] = x2; return 11; }
  goto L2;
 L53:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, QImode))
    { recog_operand[0] = x2; goto L54; }
  goto L2;
 L54:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, QImode))
    { recog_operand[1] = x2; return 12; }
  goto L2;
 L479:
  x1 = XEXP (x0, 1);
  if (GET_CODE (x1) == LABEL_REF && 1)
    goto L480;
  if (GET_CODE (x1) == IF_THEN_ELSE && 1)
    goto L484;
  x1 = XEXP (x0, 0);
  goto L56;
 L480:
  x2 = XEXP (x1, 0);
  if (1)
    { recog_operand[0] = x2; return 102; }
  x1 = XEXP (x0, 0);
  goto L56;
 L484:
  x2 = XEXP (x1, 0);
 switch (GET_CODE (x2))
  {
  case EQ:
  if (1)
    goto L485;
  break;
  case NE:
  if (1)
    goto L494;
  break;
  case GT:
  if (1)
    goto L503;
  break;
  case GTU:
  if (1)
    goto L512;
  break;
  case LT:
  if (1)
    goto L521;
  break;
  case LTU:
  if (1)
    goto L530;
  break;
  case GE:
  if (1)
    goto L539;
  break;
  case GEU:
  if (1)
    goto L548;
  break;
  case LE:
  if (1)
    goto L557;
  break;
  case LEU:
  if (1)
    goto L566;
  break;
  }
  x1 = XEXP (x0, 0);
  goto L56;
 L485:
  x3 = XEXP (x2, 0);
  if (x3 == cc0_rtx && 1)
    goto L486;
  if (GET_MODE (x3) != SImode)
    {
      x1 = XEXP (x0, 0);
      goto L56;
    }
  if (GET_CODE (x3) == SIGN_EXTRACT && 1)
    goto L678;
  if (GET_CODE (x3) == AND && 1)
    goto L773;
  x1 = XEXP (x0, 0);
  goto L56;
 L486:
  x3 = XEXP (x2, 1);
  if (x3 == const0_rtx && 1)
    goto L487;
  x1 = XEXP (x0, 0);
  goto L56;
 L487:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L488;
  if (x2 == pc_rtx && 1)
    goto L578;
  x1 = XEXP (x0, 0);
  goto L56;
 L488:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; goto L489; }
  x1 = XEXP (x0, 0);
  goto L56;
 L489:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 103;
  x1 = XEXP (x0, 0);
  goto L56;
 L578:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L579;
  x1 = XEXP (x0, 0);
  goto L56;
 L579:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; return 113; }
  x1 = XEXP (x0, 0);
  goto L56;
 L678:
  x4 = XEXP (x3, 0);
  if (general_operand (x4, QImode))
    { recog_operand[0] = x4; goto L679; }
 L726:
  if (general_operand (x4, SImode))
    { recog_operand[0] = x4; goto L727; }
  x1 = XEXP (x0, 0);
  goto L56;
 L679:
  x4 = XEXP (x3, 1);
  if (x4 == const1_rtx && 1)
    goto L680;
  x4 = XEXP (x3, 0);
  goto L726;
 L680:
  x4 = XEXP (x3, 2);
  if (general_operand (x4, SImode))
    { recog_operand[1] = x4; goto L681; }
  x4 = XEXP (x3, 0);
  goto L726;
 L681:
  x3 = XEXP (x2, 1);
  if (x3 == const0_rtx && 1)
    goto L682;
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L726;
 L682:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L683;
  if (x2 == pc_rtx && 1)
    goto L707;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L726;
 L683:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[2] = x3; goto L684; }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L726;
 L684:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 124;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L726;
 L707:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L708;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L726;
 L708:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[2] = x3; return 126; }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L726;
 L727:
  x4 = XEXP (x3, 1);
  if (x4 == const1_rtx && 1)
    goto L728;
  x1 = XEXP (x0, 0);
  goto L56;
 L728:
  x4 = XEXP (x3, 2);
  if (general_operand (x4, SImode))
    { recog_operand[1] = x4; goto L729; }
  x1 = XEXP (x0, 0);
  goto L56;
 L729:
  x3 = XEXP (x2, 1);
  if (x3 == const0_rtx && 1)
    goto L730;
  x1 = XEXP (x0, 0);
  goto L56;
 L730:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L731;
  if (x2 == pc_rtx && 1)
    goto L755;
  x1 = XEXP (x0, 0);
  goto L56;
 L731:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[2] = x3; goto L732; }
  x1 = XEXP (x0, 0);
  goto L56;
 L732:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 128;
  x1 = XEXP (x0, 0);
  goto L56;
 L755:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L756;
  x1 = XEXP (x0, 0);
  goto L56;
 L756:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[2] = x3; return 130; }
  x1 = XEXP (x0, 0);
  goto L56;
 L773:
  x4 = XEXP (x3, 0);
  if (general_operand (x4, SImode))
    { recog_operand[0] = x4; goto L774; }
  x1 = XEXP (x0, 0);
  goto L56;
 L774:
  x4 = XEXP (x3, 1);
  if (x4 == const1_rtx && 1)
    goto L775;
  if (GET_CODE (x4) == NOT && GET_MODE (x4) == SImode && 1)
    goto L820;
  x1 = XEXP (x0, 0);
  goto L56;
 L775:
  x3 = XEXP (x2, 1);
  if (x3 == const0_rtx && 1)
    goto L776;
  x1 = XEXP (x0, 0);
  goto L56;
 L776:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L777;
  if (x2 == pc_rtx && 1)
    goto L799;
  x1 = XEXP (x0, 0);
  goto L56;
 L777:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[1] = x3; goto L778; }
  x1 = XEXP (x0, 0);
  goto L56;
 L778:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 132;
  x1 = XEXP (x0, 0);
  goto L56;
 L799:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L800;
  x1 = XEXP (x0, 0);
  goto L56;
 L800:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[1] = x3; return 134; }
  x1 = XEXP (x0, 0);
  goto L56;
 L820:
  x5 = XEXP (x4, 0);
  if (GET_CODE (x5) == CONST_INT && XINT (x5, 0) == -2 && 1)
    goto L821;
  x1 = XEXP (x0, 0);
  goto L56;
 L821:
  x3 = XEXP (x2, 1);
  if (x3 == const0_rtx && 1)
    goto L822;
  x1 = XEXP (x0, 0);
  goto L56;
 L822:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L823;
  if (x2 == pc_rtx && 1)
    goto L847;
  x1 = XEXP (x0, 0);
  goto L56;
 L823:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[1] = x3; goto L824; }
  x1 = XEXP (x0, 0);
  goto L56;
 L824:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 136;
  x1 = XEXP (x0, 0);
  goto L56;
 L847:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L848;
  x1 = XEXP (x0, 0);
  goto L56;
 L848:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[1] = x3; return 138; }
  x1 = XEXP (x0, 0);
  goto L56;
 L494:
  x3 = XEXP (x2, 0);
  if (x3 == cc0_rtx && 1)
    goto L495;
  if (GET_MODE (x3) != SImode)
    {
      x1 = XEXP (x0, 0);
      goto L56;
    }
  if (GET_CODE (x3) == SIGN_EXTRACT && 1)
    goto L666;
  if (GET_CODE (x3) == AND && 1)
    goto L762;
  x1 = XEXP (x0, 0);
  goto L56;
 L495:
  x3 = XEXP (x2, 1);
  if (x3 == const0_rtx && 1)
    goto L496;
  x1 = XEXP (x0, 0);
  goto L56;
 L496:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L497;
  if (x2 == pc_rtx && 1)
    goto L587;
  x1 = XEXP (x0, 0);
  goto L56;
 L497:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; goto L498; }
  x1 = XEXP (x0, 0);
  goto L56;
 L498:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 104;
  x1 = XEXP (x0, 0);
  goto L56;
 L587:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L588;
  x1 = XEXP (x0, 0);
  goto L56;
 L588:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; return 114; }
  x1 = XEXP (x0, 0);
  goto L56;
 L666:
  x4 = XEXP (x3, 0);
  if (general_operand (x4, QImode))
    { recog_operand[0] = x4; goto L667; }
 L714:
  if (general_operand (x4, SImode))
    { recog_operand[0] = x4; goto L715; }
  x1 = XEXP (x0, 0);
  goto L56;
 L667:
  x4 = XEXP (x3, 1);
  if (x4 == const1_rtx && 1)
    goto L668;
  x4 = XEXP (x3, 0);
  goto L714;
 L668:
  x4 = XEXP (x3, 2);
  if (general_operand (x4, SImode))
    { recog_operand[1] = x4; goto L669; }
  x4 = XEXP (x3, 0);
  goto L714;
 L669:
  x3 = XEXP (x2, 1);
  if (x3 == const0_rtx && 1)
    goto L670;
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L714;
 L670:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L671;
  if (x2 == pc_rtx && 1)
    goto L695;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L714;
 L671:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[2] = x3; goto L672; }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L714;
 L672:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 123;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L714;
 L695:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L696;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L714;
 L696:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[2] = x3; return 125; }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L714;
 L715:
  x4 = XEXP (x3, 1);
  if (x4 == const1_rtx && 1)
    goto L716;
  x1 = XEXP (x0, 0);
  goto L56;
 L716:
  x4 = XEXP (x3, 2);
  if (general_operand (x4, SImode))
    { recog_operand[1] = x4; goto L717; }
  x1 = XEXP (x0, 0);
  goto L56;
 L717:
  x3 = XEXP (x2, 1);
  if (x3 == const0_rtx && 1)
    goto L718;
  x1 = XEXP (x0, 0);
  goto L56;
 L718:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L719;
  if (x2 == pc_rtx && 1)
    goto L743;
  x1 = XEXP (x0, 0);
  goto L56;
 L719:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[2] = x3; goto L720; }
  x1 = XEXP (x0, 0);
  goto L56;
 L720:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 127;
  x1 = XEXP (x0, 0);
  goto L56;
 L743:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L744;
  x1 = XEXP (x0, 0);
  goto L56;
 L744:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[2] = x3; return 129; }
  x1 = XEXP (x0, 0);
  goto L56;
 L762:
  x4 = XEXP (x3, 0);
  if (general_operand (x4, SImode))
    { recog_operand[0] = x4; goto L763; }
  x1 = XEXP (x0, 0);
  goto L56;
 L763:
  x4 = XEXP (x3, 1);
  if (x4 == const1_rtx && 1)
    goto L764;
  if (GET_CODE (x4) == NOT && GET_MODE (x4) == SImode && 1)
    goto L808;
  x1 = XEXP (x0, 0);
  goto L56;
 L764:
  x3 = XEXP (x2, 1);
  if (x3 == const0_rtx && 1)
    goto L765;
  x1 = XEXP (x0, 0);
  goto L56;
 L765:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L766;
  if (x2 == pc_rtx && 1)
    goto L788;
  x1 = XEXP (x0, 0);
  goto L56;
 L766:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[1] = x3; goto L767; }
  x1 = XEXP (x0, 0);
  goto L56;
 L767:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 131;
  x1 = XEXP (x0, 0);
  goto L56;
 L788:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L789;
  x1 = XEXP (x0, 0);
  goto L56;
 L789:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[1] = x3; return 133; }
  x1 = XEXP (x0, 0);
  goto L56;
 L808:
  x5 = XEXP (x4, 0);
  if (GET_CODE (x5) == CONST_INT && XINT (x5, 0) == -2 && 1)
    goto L809;
  x1 = XEXP (x0, 0);
  goto L56;
 L809:
  x3 = XEXP (x2, 1);
  if (x3 == const0_rtx && 1)
    goto L810;
  x1 = XEXP (x0, 0);
  goto L56;
 L810:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L811;
  if (x2 == pc_rtx && 1)
    goto L835;
  x1 = XEXP (x0, 0);
  goto L56;
 L811:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[1] = x3; goto L812; }
  x1 = XEXP (x0, 0);
  goto L56;
 L812:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 135;
  x1 = XEXP (x0, 0);
  goto L56;
 L835:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L836;
  x1 = XEXP (x0, 0);
  goto L56;
 L836:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[1] = x3; return 137; }
  x1 = XEXP (x0, 0);
  goto L56;
 L503:
  x3 = XEXP (x2, 0);
  if (x3 == cc0_rtx && 1)
    goto L504;
  x1 = XEXP (x0, 0);
  goto L56;
 L504:
  x3 = XEXP (x2, 1);
  if (x3 == const0_rtx && 1)
    goto L505;
  x1 = XEXP (x0, 0);
  goto L56;
 L505:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L506;
  if (x2 == pc_rtx && 1)
    goto L596;
  x1 = XEXP (x0, 0);
  goto L56;
 L506:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; goto L507; }
  x1 = XEXP (x0, 0);
  goto L56;
 L507:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 105;
  x1 = XEXP (x0, 0);
  goto L56;
 L596:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L597;
  x1 = XEXP (x0, 0);
  goto L56;
 L597:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; return 115; }
  x1 = XEXP (x0, 0);
  goto L56;
 L512:
  x3 = XEXP (x2, 0);
  if (x3 == cc0_rtx && 1)
    goto L513;
  x1 = XEXP (x0, 0);
  goto L56;
 L513:
  x3 = XEXP (x2, 1);
  if (x3 == const0_rtx && 1)
    goto L514;
  x1 = XEXP (x0, 0);
  goto L56;
 L514:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L515;
  if (x2 == pc_rtx && 1)
    goto L605;
  x1 = XEXP (x0, 0);
  goto L56;
 L515:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; goto L516; }
  x1 = XEXP (x0, 0);
  goto L56;
 L516:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 106;
  x1 = XEXP (x0, 0);
  goto L56;
 L605:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L606;
  x1 = XEXP (x0, 0);
  goto L56;
 L606:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; return 116; }
  x1 = XEXP (x0, 0);
  goto L56;
 L521:
  x3 = XEXP (x2, 0);
  if (x3 == cc0_rtx && 1)
    goto L522;
  x1 = XEXP (x0, 0);
  goto L56;
 L522:
  x3 = XEXP (x2, 1);
  if (x3 == const0_rtx && 1)
    goto L523;
  x1 = XEXP (x0, 0);
  goto L56;
 L523:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L524;
  if (x2 == pc_rtx && 1)
    goto L614;
  x1 = XEXP (x0, 0);
  goto L56;
 L524:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; goto L525; }
  x1 = XEXP (x0, 0);
  goto L56;
 L525:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 107;
  x1 = XEXP (x0, 0);
  goto L56;
 L614:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L615;
  x1 = XEXP (x0, 0);
  goto L56;
 L615:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; return 117; }
  x1 = XEXP (x0, 0);
  goto L56;
 L530:
  x3 = XEXP (x2, 0);
  if (x3 == cc0_rtx && 1)
    goto L531;
  x1 = XEXP (x0, 0);
  goto L56;
 L531:
  x3 = XEXP (x2, 1);
  if (x3 == const0_rtx && 1)
    goto L532;
  x1 = XEXP (x0, 0);
  goto L56;
 L532:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L533;
  if (x2 == pc_rtx && 1)
    goto L623;
  x1 = XEXP (x0, 0);
  goto L56;
 L533:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; goto L534; }
  x1 = XEXP (x0, 0);
  goto L56;
 L534:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 108;
  x1 = XEXP (x0, 0);
  goto L56;
 L623:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L624;
  x1 = XEXP (x0, 0);
  goto L56;
 L624:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; return 118; }
  x1 = XEXP (x0, 0);
  goto L56;
 L539:
  x3 = XEXP (x2, 0);
  if (x3 == cc0_rtx && 1)
    goto L540;
  x1 = XEXP (x0, 0);
  goto L56;
 L540:
  x3 = XEXP (x2, 1);
  if (x3 == const0_rtx && 1)
    goto L541;
  x1 = XEXP (x0, 0);
  goto L56;
 L541:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L542;
  if (x2 == pc_rtx && 1)
    goto L632;
  x1 = XEXP (x0, 0);
  goto L56;
 L542:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; goto L543; }
  x1 = XEXP (x0, 0);
  goto L56;
 L543:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 109;
  x1 = XEXP (x0, 0);
  goto L56;
 L632:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L633;
  x1 = XEXP (x0, 0);
  goto L56;
 L633:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; return 119; }
  x1 = XEXP (x0, 0);
  goto L56;
 L548:
  x3 = XEXP (x2, 0);
  if (x3 == cc0_rtx && 1)
    goto L549;
  x1 = XEXP (x0, 0);
  goto L56;
 L549:
  x3 = XEXP (x2, 1);
  if (x3 == const0_rtx && 1)
    goto L550;
  x1 = XEXP (x0, 0);
  goto L56;
 L550:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L551;
  if (x2 == pc_rtx && 1)
    goto L641;
  x1 = XEXP (x0, 0);
  goto L56;
 L551:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; goto L552; }
  x1 = XEXP (x0, 0);
  goto L56;
 L552:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 110;
  x1 = XEXP (x0, 0);
  goto L56;
 L641:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L642;
  x1 = XEXP (x0, 0);
  goto L56;
 L642:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; return 120; }
  x1 = XEXP (x0, 0);
  goto L56;
 L557:
  x3 = XEXP (x2, 0);
  if (x3 == cc0_rtx && 1)
    goto L558;
  if (GET_CODE (x3) == MINUS && GET_MODE (x3) == SImode && 1)
    goto L1002;
 L1020:
  if (general_operand (x3, SImode))
    { recog_operand[0] = x3; goto L1021; }
  x1 = XEXP (x0, 0);
  goto L56;
 L558:
  x3 = XEXP (x2, 1);
  if (x3 == const0_rtx && 1)
    goto L559;
  x3 = XEXP (x2, 0);
  goto L1020;
 L559:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L560;
  if (x2 == pc_rtx && 1)
    goto L650;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1020;
 L560:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; goto L561; }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1020;
 L561:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 111;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1020;
 L650:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L651;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1020;
 L651:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; return 121; }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1020;
 L1002:
  x4 = XEXP (x3, 0);
  if (general_operand (x4, SImode))
    { recog_operand[0] = x4; goto L1003; }
  goto L1020;
 L1003:
  x4 = XEXP (x3, 1);
  if (general_operand (x4, SImode))
    { recog_operand[1] = x4; goto L1004; }
  goto L1020;
 L1004:
  x3 = XEXP (x2, 1);
  if (general_operand (x3, SImode))
    { recog_operand[2] = x3; goto L1005; }
  x3 = XEXP (x2, 0);
  goto L1020;
 L1005:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == PLUS && GET_MODE (x2) == SImode && 1)
    goto L1006;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1020;
 L1006:
  x3 = XEXP (x2, 0);
  if (GET_CODE (x3) == LABEL_REF && GET_MODE (x3) == SImode && 1)
    goto L1007;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1020;
 L1007:
  x4 = XEXP (x3, 0);
  if (1)
    { recog_operand[3] = x4; goto L1008; }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1020;
 L1008:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == SIGN_EXTEND && GET_MODE (x3) == SImode && 1)
    goto L1009;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1020;
 L1009:
  x4 = XEXP (x3, 0);
  if (GET_CODE (x4) == MEM && GET_MODE (x4) == HImode && 1)
    goto L1010;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1020;
 L1010:
  x5 = XEXP (x4, 0);
  if (GET_CODE (x5) == PLUS && GET_MODE (x5) == SImode && 1)
    goto L1011;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1020;
 L1011:
  x6 = XEXP (x5, 0);
  if (x6 == pc_rtx && 1)
    goto L1012;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1020;
 L1012:
  x6 = XEXP (x5, 1);
  if (GET_CODE (x6) == MINUS && GET_MODE (x6) == SImode && 1)
    goto L1013;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1020;
 L1013:
  x7 = XEXP (x6, 0);
  if (rtx_equal_p (x7, recog_operand[0]) && 1)
    goto L1014;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1020;
 L1014:
  x7 = XEXP (x6, 1);
  if (rtx_equal_p (x7, recog_operand[1]) && 1)
    goto L1015;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1020;
 L1015:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 149;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1020;
 L1021:
  x3 = XEXP (x2, 1);
  if (general_operand (x3, SImode))
    { recog_operand[1] = x3; goto L1022; }
  x1 = XEXP (x0, 0);
  goto L56;
 L1022:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == PLUS && GET_MODE (x2) == SImode && 1)
    goto L1023;
  x1 = XEXP (x0, 0);
  goto L56;
 L1023:
  x3 = XEXP (x2, 0);
  if (GET_CODE (x3) == LABEL_REF && GET_MODE (x3) == SImode && 1)
    goto L1024;
  x1 = XEXP (x0, 0);
  goto L56;
 L1024:
  x4 = XEXP (x3, 0);
  if (1)
    { recog_operand[3] = x4; goto L1025; }
  x1 = XEXP (x0, 0);
  goto L56;
 L1025:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == SIGN_EXTEND && GET_MODE (x3) == SImode && 1)
    goto L1026;
  x1 = XEXP (x0, 0);
  goto L56;
 L1026:
  x4 = XEXP (x3, 0);
  if (GET_CODE (x4) == MEM && GET_MODE (x4) == HImode && 1)
    goto L1027;
  x1 = XEXP (x0, 0);
  goto L56;
 L1027:
  x5 = XEXP (x4, 0);
  if (GET_CODE (x5) == PLUS && GET_MODE (x5) == SImode && 1)
    goto L1028;
  x1 = XEXP (x0, 0);
  goto L56;
 L1028:
  x6 = XEXP (x5, 0);
  if (x6 == pc_rtx && 1)
    goto L1029;
  x1 = XEXP (x0, 0);
  goto L56;
 L1029:
  x6 = XEXP (x5, 1);
  if (rtx_equal_p (x6, recog_operand[0]) && 1)
    goto L1030;
  x1 = XEXP (x0, 0);
  goto L56;
 L1030:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 150;
  x1 = XEXP (x0, 0);
  goto L56;
 L566:
  x3 = XEXP (x2, 0);
  if (x3 == cc0_rtx && 1)
    goto L567;
  x1 = XEXP (x0, 0);
  goto L56;
 L567:
  x3 = XEXP (x2, 1);
  if (x3 == const0_rtx && 1)
    goto L568;
  x1 = XEXP (x0, 0);
  goto L56;
 L568:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L569;
  if (x2 == pc_rtx && 1)
    goto L659;
  x1 = XEXP (x0, 0);
  goto L56;
 L569:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; goto L570; }
  x1 = XEXP (x0, 0);
  goto L56;
 L570:
  x2 = XEXP (x1, 2);
  if (x2 == pc_rtx && 1)
    return 112;
  x1 = XEXP (x0, 0);
  goto L56;
 L659:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L660;
  x1 = XEXP (x0, 0);
  goto L56;
 L660:
  x3 = XEXP (x2, 0);
  if (1)
    { recog_operand[0] = x3; return 122; }
  x1 = XEXP (x0, 0);
  goto L56;
 L467:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, QImode))
    { recog_operand[0] = x2; goto L468; }
 L473:
  if (general_operand (x2, SImode))
    { recog_operand[0] = x2; goto L474; }
  goto L56;
 L468:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; goto L469; }
  x2 = XEXP (x1, 0);
  goto L473;
 L469:
  x2 = XEXP (x1, 2);
  if (general_operand (x2, SImode))
    { recog_operand[2] = x2; goto L470; }
  x2 = XEXP (x1, 0);
  goto L473;
 L470:
  x1 = XEXP (x0, 1);
  if (general_operand (x1, SImode))
    { recog_operand[3] = x1; return 100; }
  x1 = XEXP (x0, 0);
  x2 = XEXP (x1, 0);
  goto L473;
 L474:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; goto L475; }
  goto L56;
 L475:
  x2 = XEXP (x1, 2);
  if (general_operand (x2, SImode))
    { recog_operand[2] = x2; goto L476; }
  goto L56;
 L476:
  x1 = XEXP (x0, 1);
  if (general_operand (x1, SImode))
    { recog_operand[3] = x1; return 101; }
  x1 = XEXP (x0, 0);
  goto L56;
 L125:
  x1 = XEXP (x0, 1);
 switch (GET_CODE (x1))
  {
  case FLOAT:
  if (GET_MODE (x1) == DFmode && 1)
    goto L126;
  break;
  case FLOAT_EXTEND:
  if (GET_MODE (x1) == DFmode && 1)
    goto L174;
  break;
  case PLUS:
  if (GET_MODE (x1) == DFmode && 1)
    goto L206;
  break;
  case MINUS:
  if (GET_MODE (x1) == DFmode && 1)
    goto L231;
  break;
  case MULT:
  if (GET_MODE (x1) == DFmode && 1)
    goto L256;
  break;
  case DIV:
  if (GET_MODE (x1) == DFmode && 1)
    goto L281;
  break;
  case NEG:
  if (GET_MODE (x1) == DFmode && 1)
    goto L369;
  break;
  }
 L57:
  if (general_operand (x1, DFmode))
    { recog_operand[1] = x1; return 13; }
  x1 = XEXP (x0, 0);
  goto L59;
 L126:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, QImode))
    { recog_operand[1] = x2; return 29; }
 L142:
  if (general_operand (x2, HImode))
    { recog_operand[1] = x2; return 33; }
 L158:
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; return 37; }
  goto L57;
 L174:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SFmode))
    { recog_operand[1] = x2; return 41; }
  goto L57;
 L206:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, DFmode))
    { recog_operand[1] = x2; goto L207; }
  goto L57;
 L207:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, DFmode))
    { recog_operand[2] = x2; return 49; }
  goto L57;
 L231:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, DFmode))
    { recog_operand[1] = x2; goto L232; }
  goto L57;
 L232:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, DFmode))
    { recog_operand[2] = x2; return 54; }
  goto L57;
 L256:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, DFmode))
    { recog_operand[1] = x2; goto L257; }
  goto L57;
 L257:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, DFmode))
    { recog_operand[2] = x2; return 59; }
  goto L57;
 L281:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, DFmode))
    { recog_operand[1] = x2; goto L282; }
  goto L57;
 L282:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, DFmode))
    { recog_operand[2] = x2; return 64; }
  goto L57;
 L369:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, DFmode))
    { recog_operand[1] = x2; return 81; }
  goto L57;
 L121:
  x1 = XEXP (x0, 1);
 switch (GET_CODE (x1))
  {
  case FLOAT:
  if (GET_MODE (x1) == SFmode && 1)
    goto L122;
  break;
  case FLOAT_TRUNCATE:
  if (GET_MODE (x1) == SFmode && 1)
    goto L190;
  break;
  case PLUS:
  if (GET_MODE (x1) == SFmode && 1)
    goto L211;
  break;
  case MINUS:
  if (GET_MODE (x1) == SFmode && 1)
    goto L236;
  break;
  case MULT:
  if (GET_MODE (x1) == SFmode && 1)
    goto L261;
  break;
  case DIV:
  if (GET_MODE (x1) == SFmode && 1)
    goto L286;
  break;
  case NEG:
  if (GET_MODE (x1) == SFmode && 1)
    goto L373;
  break;
  }
 L60:
  if (general_operand (x1, SFmode))
    { recog_operand[1] = x1; return 14; }
  x1 = XEXP (x0, 0);
  goto L62;
 L122:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, QImode))
    { recog_operand[1] = x2; return 28; }
 L138:
  if (general_operand (x2, HImode))
    { recog_operand[1] = x2; return 32; }
 L154:
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; return 36; }
  goto L60;
 L190:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, DFmode))
    { recog_operand[1] = x2; return 45; }
  goto L60;
 L211:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SFmode))
    { recog_operand[1] = x2; goto L212; }
  goto L60;
 L212:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SFmode))
    { recog_operand[2] = x2; return 50; }
  goto L60;
 L236:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SFmode))
    { recog_operand[1] = x2; goto L237; }
  goto L60;
 L237:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SFmode))
    { recog_operand[2] = x2; return 55; }
  goto L60;
 L261:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SFmode))
    { recog_operand[1] = x2; goto L262; }
  goto L60;
 L262:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SFmode))
    { recog_operand[2] = x2; return 60; }
  goto L60;
 L286:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SFmode))
    { recog_operand[1] = x2; goto L287; }
  goto L60;
 L287:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SFmode))
    { recog_operand[2] = x2; return 65; }
  goto L60;
 L373:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SFmode))
    { recog_operand[1] = x2; return 82; }
  goto L60;
 L63:
  x1 = XEXP (x0, 1);
  if (general_operand (x1, TImode))
    { recog_operand[1] = x1; return 15; }
  x1 = XEXP (x0, 0);
  goto L65;
 L405:
  x1 = XEXP (x0, 1);
  if (GET_CODE (x1) == ASHIFT && GET_MODE (x1) == DImode && 1)
    goto L406;
  if (GET_CODE (x1) == ROTATE && GET_MODE (x1) == DImode && 1)
    goto L416;
 L66:
  if (general_operand (x1, DImode))
    { recog_operand[1] = x1; return 16; }
  x1 = XEXP (x0, 0);
  goto L68;
 L406:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, DImode))
    { recog_operand[1] = x2; goto L407; }
  goto L66;
 L407:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, QImode))
    { recog_operand[2] = x2; return 90; }
  goto L66;
 L416:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, DImode))
    { recog_operand[1] = x2; goto L417; }
  goto L66;
 L417:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, QImode))
    { recog_operand[2] = x2; return 92; }
  goto L66;
 L117:
  x1 = XEXP (x0, 1);
 switch (GET_CODE (x1))
  {
  case SIGN_EXTEND:
  if (GET_MODE (x1) == SImode && 1)
    goto L118;
  break;
  case FIX:
  if (GET_MODE (x1) == SImode && 1)
    goto L170;
  break;
  case ZERO_EXTEND:
  if (GET_MODE (x1) == SImode && 1)
    goto L198;
  break;
  case PLUS:
  if (GET_MODE (x1) == SImode && 1)
    goto L216;
  break;
  case MINUS:
  if (GET_MODE (x1) == SImode && 1)
    goto L241;
  break;
  case MULT:
  if (GET_MODE (x1) == SImode && 1)
    goto L421;
  break;
  case DIV:
  if (GET_MODE (x1) == SImode && 1)
    goto L291;
  break;
  case AND:
  if (GET_MODE (x1) == SImode && 1)
    goto L306;
  break;
  case IOR:
  if (GET_MODE (x1) == SImode && 1)
    goto L339;
  break;
  case XOR:
  if (GET_MODE (x1) == SImode && 1)
    goto L354;
  break;
  case NEG:
  if (GET_MODE (x1) == SImode && 1)
    goto L377;
  break;
  case NOT:
  if (GET_MODE (x1) == SImode && 1)
    goto L389;
  break;
  case ASHIFT:
  if (GET_MODE (x1) == SImode && 1)
    goto L401;
  break;
  case ROTATE:
  if (GET_MODE (x1) == SImode && 1)
    goto L411;
  break;
  case SIGN_EXTRACT:
  if (GET_MODE (x1) == SImode && 1)
    goto L444;
  break;
  case ZERO_EXTRACT:
  if (GET_MODE (x1) == SImode && 1)
    goto L450;
  break;
  }
 L69:
  if (general_operand (x1, SImode))
    { recog_operand[1] = x1; return 17; }
 L98:
  if (address_operand (x1, QImode))
    { recog_operand[1] = x1; return 21; }
 L101:
  if (address_operand (x1, HImode))
    { recog_operand[1] = x1; return 22; }
 L104:
  if (address_operand (x1, SImode))
    { recog_operand[1] = x1; return 23; }
 L107:
  if (address_operand (x1, SFmode))
    { recog_operand[1] = x1; return 24; }
 L110:
  if (address_operand (x1, DFmode))
    { recog_operand[1] = x1; return 25; }
  x1 = XEXP (x0, 0);
  goto L71;
 L118:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, QImode))
    { recog_operand[1] = x2; return 27; }
 L134:
  if (general_operand (x2, HImode))
    { recog_operand[1] = x2; return 31; }
  goto L69;
 L170:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SFmode))
    { recog_operand[1] = x2; return 40; }
 L186:
  if (general_operand (x2, DFmode))
    { recog_operand[1] = x2; return 44; }
  goto L69;
 L198:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, QImode))
    { recog_operand[1] = x2; return 47; }
 L202:
  if (general_operand (x2, HImode))
    { recog_operand[1] = x2; return 48; }
  goto L69;
 L216:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; goto L217; }
  goto L69;
 L217:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SImode))
    { recog_operand[2] = x2; return 51; }
  goto L69;
 L241:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; goto L242; }
  goto L69;
 L242:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SImode))
    { recog_operand[2] = x2; return 56; }
  goto L69;
 L421:
  x2 = XEXP (x1, 0);
  if (GET_CODE (x2) == PLUS && GET_MODE (x2) == SImode && 1)
    goto L422;
 L266:
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; goto L267; }
  goto L69;
 L422:
  x3 = XEXP (x2, 0);
  if (general_operand (x3, SImode))
    { recog_operand[1] = x3; goto L423; }
  goto L266;
 L423:
  x3 = XEXP (x2, 1);
  if (general_operand (x3, SImode))
    { recog_operand[2] = x3; goto L424; }
  goto L266;
 L424:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SImode))
    { recog_operand[3] = x2; return 93; }
  x2 = XEXP (x1, 0);
  goto L266;
 L267:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SImode))
    { recog_operand[2] = x2; return 61; }
  goto L69;
 L291:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; goto L292; }
  goto L69;
 L292:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SImode))
    { recog_operand[2] = x2; return 66; }
  goto L69;
 L306:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; goto L307; }
  goto L69;
 L307:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == NOT && GET_MODE (x2) == SImode && 1)
    goto L308;
 L325:
  if (general_operand (x2, SImode))
    { recog_operand[2] = x2; if (GET_CODE (operands[2]) == CONST_INT) return 72; }
  goto L69;
 L308:
  x3 = XEXP (x2, 0);
  if (general_operand (x3, SImode))
    { recog_operand[2] = x3; return 69; }
  goto L325;
 L339:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; goto L340; }
  goto L69;
 L340:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SImode))
    { recog_operand[2] = x2; return 75; }
  goto L69;
 L354:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; goto L355; }
  goto L69;
 L355:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SImode))
    { recog_operand[2] = x2; return 78; }
  goto L69;
 L377:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; return 83; }
  goto L69;
 L389:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; return 86; }
  goto L69;
 L401:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; goto L402; }
  goto L69;
 L402:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, QImode))
    { recog_operand[2] = x2; return 89; }
  goto L69;
 L411:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; goto L412; }
  goto L69;
 L412:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, QImode))
    { recog_operand[2] = x2; return 91; }
  goto L69;
 L444:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, QImode))
    { recog_operand[1] = x2; goto L445; }
 L456:
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; goto L457; }
  goto L69;
 L445:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SImode))
    { recog_operand[2] = x2; goto L446; }
  x2 = XEXP (x1, 0);
  goto L456;
 L446:
  x2 = XEXP (x1, 2);
  if (general_operand (x2, SImode))
    { recog_operand[3] = x2; return 96; }
  x2 = XEXP (x1, 0);
  goto L456;
 L457:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SImode))
    { recog_operand[2] = x2; goto L458; }
  goto L69;
 L458:
  x2 = XEXP (x1, 2);
  if (general_operand (x2, SImode))
    { recog_operand[3] = x2; return 98; }
  goto L69;
 L450:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, QImode))
    { recog_operand[1] = x2; goto L451; }
 L462:
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; goto L463; }
  goto L69;
 L451:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SImode))
    { recog_operand[2] = x2; goto L452; }
  x2 = XEXP (x1, 0);
  goto L462;
 L452:
  x2 = XEXP (x1, 2);
  if (general_operand (x2, SImode))
    { recog_operand[3] = x2; return 97; }
  x2 = XEXP (x1, 0);
  goto L462;
 L463:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, SImode))
    { recog_operand[2] = x2; goto L464; }
  goto L69;
 L464:
  x2 = XEXP (x1, 2);
  if (general_operand (x2, SImode))
    { recog_operand[3] = x2; return 99; }
  goto L69;
 L113:
  x1 = XEXP (x0, 1);
 switch (GET_CODE (x1))
  {
  case SIGN_EXTEND:
  if (GET_MODE (x1) == HImode && 1)
    goto L114;
  break;
  case TRUNCATE:
  if (GET_MODE (x1) == HImode && 1)
    goto L150;
  break;
  case FIX:
  if (GET_MODE (x1) == HImode && 1)
    goto L166;
  break;
  case ZERO_EXTEND:
  if (GET_MODE (x1) == HImode && 1)
    goto L194;
  break;
  case PLUS:
  if (GET_MODE (x1) == HImode && 1)
    goto L221;
  break;
  case MINUS:
  if (GET_MODE (x1) == HImode && 1)
    goto L246;
  break;
  case MULT:
  if (GET_MODE (x1) == HImode && 1)
    goto L271;
  break;
  case DIV:
  if (GET_MODE (x1) == HImode && 1)
    goto L296;
  break;
  case AND:
  if (GET_MODE (x1) == HImode && 1)
    goto L312;
  break;
  case IOR:
  if (GET_MODE (x1) == HImode && 1)
    goto L344;
  break;
  case XOR:
  if (GET_MODE (x1) == HImode && 1)
    goto L359;
  break;
  case NEG:
  if (GET_MODE (x1) == HImode && 1)
    goto L381;
  break;
  case NOT:
  if (GET_MODE (x1) == HImode && 1)
    goto L393;
  break;
  }
 L72:
  if (general_operand (x1, HImode))
    { recog_operand[1] = x1; return 18; }
  x1 = XEXP (x0, 0);
  goto L74;
 L114:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, QImode))
    { recog_operand[1] = x2; return 26; }
  goto L72;
 L150:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; return 35; }
  goto L72;
 L166:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SFmode))
    { recog_operand[1] = x2; return 39; }
 L182:
  if (general_operand (x2, DFmode))
    { recog_operand[1] = x2; return 43; }
  goto L72;
 L194:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, QImode))
    { recog_operand[1] = x2; return 46; }
  goto L72;
 L221:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, HImode))
    { recog_operand[1] = x2; goto L222; }
  goto L72;
 L222:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, HImode))
    { recog_operand[2] = x2; return 52; }
  goto L72;
 L246:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, HImode))
    { recog_operand[1] = x2; goto L247; }
  goto L72;
 L247:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, HImode))
    { recog_operand[2] = x2; return 57; }
  goto L72;
 L271:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, HImode))
    { recog_operand[1] = x2; goto L272; }
  goto L72;
 L272:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, HImode))
    { recog_operand[2] = x2; return 62; }
  goto L72;
 L296:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, HImode))
    { recog_operand[1] = x2; goto L297; }
  goto L72;
 L297:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, HImode))
    { recog_operand[2] = x2; return 67; }
  goto L72;
 L312:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, HImode))
    { recog_operand[1] = x2; goto L313; }
  goto L72;
 L313:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == NOT && GET_MODE (x2) == HImode && 1)
    goto L314;
 L330:
  if (general_operand (x2, HImode))
    { recog_operand[2] = x2; if (GET_CODE (operands[2]) == CONST_INT) return 73; }
  goto L72;
 L314:
  x3 = XEXP (x2, 0);
  if (general_operand (x3, HImode))
    { recog_operand[2] = x3; return 70; }
  goto L330;
 L344:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, HImode))
    { recog_operand[1] = x2; goto L345; }
  goto L72;
 L345:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, HImode))
    { recog_operand[2] = x2; return 76; }
  goto L72;
 L359:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, HImode))
    { recog_operand[1] = x2; goto L360; }
  goto L72;
 L360:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, HImode))
    { recog_operand[2] = x2; return 79; }
  goto L72;
 L381:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, HImode))
    { recog_operand[1] = x2; return 84; }
  goto L72;
 L393:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, HImode))
    { recog_operand[1] = x2; return 87; }
  goto L72;
 L129:
  x1 = XEXP (x0, 1);
 switch (GET_CODE (x1))
  {
  case TRUNCATE:
  if (GET_MODE (x1) == QImode && 1)
    goto L130;
  break;
  case FIX:
  if (GET_MODE (x1) == QImode && 1)
    goto L162;
  break;
  case PLUS:
  if (GET_MODE (x1) == QImode && 1)
    goto L226;
  break;
  case MINUS:
  if (GET_MODE (x1) == QImode && 1)
    goto L251;
  break;
  case MULT:
  if (GET_MODE (x1) == QImode && 1)
    goto L276;
  break;
  case DIV:
  if (GET_MODE (x1) == QImode && 1)
    goto L301;
  break;
  case AND:
  if (GET_MODE (x1) == QImode && 1)
    goto L318;
  break;
  case IOR:
  if (GET_MODE (x1) == QImode && 1)
    goto L349;
  break;
  case XOR:
  if (GET_MODE (x1) == QImode && 1)
    goto L364;
  break;
  case NEG:
  if (GET_MODE (x1) == QImode && 1)
    goto L385;
  break;
  case NOT:
  if (GET_MODE (x1) == QImode && 1)
    goto L397;
  break;
  }
 L75:
  if (general_operand (x1, QImode))
    { recog_operand[1] = x1; return 19; }
  goto ret0;
 L130:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, HImode))
    { recog_operand[1] = x2; return 30; }
 L146:
  if (general_operand (x2, SImode))
    { recog_operand[1] = x2; return 34; }
  goto L75;
 L162:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, SFmode))
    { recog_operand[1] = x2; return 38; }
 L178:
  if (general_operand (x2, DFmode))
    { recog_operand[1] = x2; return 42; }
  goto L75;
 L226:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, QImode))
    { recog_operand[1] = x2; goto L227; }
  goto L75;
 L227:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, QImode))
    { recog_operand[2] = x2; return 53; }
  goto L75;
 L251:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, QImode))
    { recog_operand[1] = x2; goto L252; }
  goto L75;
 L252:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, QImode))
    { recog_operand[2] = x2; return 58; }
  goto L75;
 L276:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, QImode))
    { recog_operand[1] = x2; goto L277; }
  goto L75;
 L277:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, QImode))
    { recog_operand[2] = x2; return 63; }
  goto L75;
 L301:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, QImode))
    { recog_operand[1] = x2; goto L302; }
  goto L75;
 L302:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, QImode))
    { recog_operand[2] = x2; return 68; }
  goto L75;
 L318:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, QImode))
    { recog_operand[1] = x2; goto L319; }
  goto L75;
 L319:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == NOT && GET_MODE (x2) == QImode && 1)
    goto L320;
 L335:
  if (general_operand (x2, QImode))
    { recog_operand[2] = x2; if (GET_CODE (operands[2]) == CONST_INT) return 74; }
  goto L75;
 L320:
  x3 = XEXP (x2, 0);
  if (general_operand (x3, QImode))
    { recog_operand[2] = x3; return 71; }
  goto L335;
 L349:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, QImode))
    { recog_operand[1] = x2; goto L350; }
  goto L75;
 L350:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, QImode))
    { recog_operand[2] = x2; return 77; }
  goto L75;
 L364:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, QImode))
    { recog_operand[1] = x2; goto L365; }
  goto L75;
 L365:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, QImode))
    { recog_operand[2] = x2; return 80; }
  goto L75;
 L385:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, QImode))
    { recog_operand[1] = x2; return 85; }
  goto L75;
 L397:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, QImode))
    { recog_operand[1] = x2; return 88; }
  goto L75;
 L77:
  x1 = XVECEXP (x0, 0, 0);
  if (GET_CODE (x1) == SET && 1)
    goto L78;
  goto L849;
 L78:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, BLKmode))
    { recog_operand[0] = x2; goto L79; }
  goto L849;
 L79:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, BLKmode))
    { recog_operand[1] = x2; goto L80; }
  goto L849;
 L80:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == USE && 1)
    goto L81;
  goto L849;
 L81:
  x2 = XEXP (x1, 0);
  if (general_operand (x2, HImode))
    { recog_operand[2] = x2; goto L82; }
  goto L849;
 L82:
  x1 = XVECEXP (x0, 0, 2);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L83;
  goto L849;
 L83:
  x2 = XEXP (x1, 0);
  if (1)
    goto L84;
  goto L849;
 L84:
  x1 = XVECEXP (x0, 0, 3);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L85;
  goto L849;
 L85:
  x2 = XEXP (x1, 0);
  if (GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    goto L86;
  goto L849;
 L86:
  x1 = XVECEXP (x0, 0, 4);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L87;
  goto L849;
 L87:
  x2 = XEXP (x1, 0);
  if (GET_CODE (x2) == REG && XINT (x2, 0) == 1 && 1)
    goto L88;
  goto L849;
 L88:
  x1 = XVECEXP (x0, 0, 5);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L89;
  goto L849;
 L89:
  x2 = XEXP (x1, 0);
  if (GET_CODE (x2) == REG && XINT (x2, 0) == 2 && 1)
    goto L90;
  goto L849;
 L90:
  x1 = XVECEXP (x0, 0, 6);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L91;
  goto L849;
 L91:
  x2 = XEXP (x1, 0);
  if (GET_CODE (x2) == REG && XINT (x2, 0) == 3 && 1)
    goto L92;
  goto L849;
 L92:
  x1 = XVECEXP (x0, 0, 7);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L93;
  goto L849;
 L93:
  x2 = XEXP (x1, 0);
  if (GET_CODE (x2) == REG && XINT (x2, 0) == 4 && 1)
    goto L94;
  goto L849;
 L94:
  x1 = XVECEXP (x0, 0, 8);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L95;
  goto L849;
 L95:
  x2 = XEXP (x1, 0);
  if (GET_CODE (x2) == REG && XINT (x2, 0) == 5 && 1)
    return 20;
  goto L849;
 L850:
  x1 = XVECEXP (x0, 0, 0);
  if (GET_CODE (x1) == SET && 1)
    goto L851;
  goto ret0;
 L851:
  x2 = XEXP (x1, 0);
  if (x2 == pc_rtx && 1)
    goto L852;
  goto ret0;
 L852:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == IF_THEN_ELSE && 1)
    goto L853;
  goto ret0;
 L853:
  x3 = XEXP (x2, 0);
 switch (GET_CODE (x3))
  {
  case GT:
  if (1)
    goto L854;
  break;
  case GE:
  if (1)
    goto L871;
  break;
  case LE:
  if (1)
    goto L888;
  break;
  case LT:
  if (1)
    goto L905;
  break;
  }
  goto ret0;
 L854:
  x4 = XEXP (x3, 0);
  if (GET_CODE (x4) != MINUS)
    goto ret0;
  if (GET_MODE (x4) == SImode && 1)
    goto L855;
 L979:
  if (1)
    goto L980;
  goto ret0;
 L855:
  x5 = XEXP (x4, 0);
  if (general_operand (x5, SImode))
    { recog_operand[0] = x5; goto L856; }
  goto L979;
 L856:
  x5 = XEXP (x4, 1);
  if (x5 == const1_rtx && 1)
    goto L857;
  goto L979;
 L857:
  x4 = XEXP (x3, 1);
  if (x4 == const0_rtx && 1)
    goto L858;
  x4 = XEXP (x3, 0);
  goto L979;
 L858:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == LABEL_REF && 1)
    goto L859;
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L979;
 L859:
  x4 = XEXP (x3, 0);
  if (1)
    { recog_operand[1] = x4; goto L860; }
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L979;
 L860:
  x3 = XEXP (x2, 2);
  if (x3 == pc_rtx && 1)
    goto L861;
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L979;
 L861:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L862;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L979;
 L862:
  x2 = XEXP (x1, 0);
  if (rtx_equal_p (x2, recog_operand[0]) && 1)
    goto L863;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L979;
 L863:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == MINUS && GET_MODE (x2) == SImode && 1)
    goto L864;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L979;
 L864:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, recog_operand[0]) && 1)
    goto L865;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L979;
 L865:
  x3 = XEXP (x2, 1);
  if (x3 == const1_rtx && 1)
    if (!TARGET_UNIX_ASM) return 139;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L979;
 L980:
  x5 = XEXP (x4, 0);
  if (GET_CODE (x5) == PLUS && GET_MODE (x5) == SImode && 1)
    goto L981;
  goto ret0;
 L981:
  x6 = XEXP (x5, 0);
  if (general_operand (x6, SImode))
    { recog_operand[0] = x6; goto L982; }
  goto ret0;
 L982:
  x6 = XEXP (x5, 1);
  if (x6 == const1_rtx && 1)
    goto L983;
  goto ret0;
 L983:
  x5 = XEXP (x4, 1);
  if (general_operand (x5, SImode))
    { recog_operand[1] = x5; goto L984; }
  goto ret0;
 L984:
  x4 = XEXP (x3, 1);
  if (x4 == const0_rtx && 1)
    goto L985;
  goto ret0;
 L985:
  x3 = XEXP (x2, 1);
  if (x3 == pc_rtx && 1)
    goto L986;
  goto ret0;
 L986:
  x3 = XEXP (x2, 2);
  if (GET_CODE (x3) == LABEL_REF && 1)
    goto L987;
  goto ret0;
 L987:
  x4 = XEXP (x3, 0);
  if (1)
    { recog_operand[2] = x4; goto L988; }
  goto ret0;
 L988:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L989;
  goto ret0;
 L989:
  x2 = XEXP (x1, 0);
  if (rtx_equal_p (x2, recog_operand[0]) && 1)
    goto L990;
  goto ret0;
 L990:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == PLUS && GET_MODE (x2) == SImode && 1)
    goto L991;
  goto ret0;
 L991:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, recog_operand[0]) && 1)
    goto L992;
  goto ret0;
 L992:
  x3 = XEXP (x2, 1);
  if (x3 == const1_rtx && 1)
    if (!TARGET_UNIX_ASM) return 146;
  goto ret0;
 L871:
  x4 = XEXP (x3, 0);
  if (GET_CODE (x4) != MINUS)
    goto ret0;
  if (GET_MODE (x4) == SImode && 1)
    goto L872;
 L960:
  if (1)
    goto L961;
  goto ret0;
 L872:
  x5 = XEXP (x4, 0);
  if (general_operand (x5, SImode))
    { recog_operand[0] = x5; goto L873; }
  goto L960;
 L873:
  x5 = XEXP (x4, 1);
  if (x5 == const1_rtx && 1)
    goto L874;
  goto L960;
 L874:
  x4 = XEXP (x3, 1);
  if (x4 == const0_rtx && 1)
    goto L875;
  x4 = XEXP (x3, 0);
  goto L960;
 L875:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == LABEL_REF && 1)
    goto L876;
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L960;
 L876:
  x4 = XEXP (x3, 0);
  if (1)
    { recog_operand[1] = x4; goto L877; }
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L960;
 L877:
  x3 = XEXP (x2, 2);
  if (x3 == pc_rtx && 1)
    goto L878;
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L960;
 L878:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L879;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L960;
 L879:
  x2 = XEXP (x1, 0);
  if (rtx_equal_p (x2, recog_operand[0]) && 1)
    goto L880;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L960;
 L880:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == MINUS && GET_MODE (x2) == SImode && 1)
    goto L881;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L960;
 L881:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, recog_operand[0]) && 1)
    goto L882;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L960;
 L882:
  x3 = XEXP (x2, 1);
  if (x3 == const1_rtx && 1)
    if (!TARGET_UNIX_ASM) return 140;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L960;
 L961:
  x5 = XEXP (x4, 0);
  if (GET_CODE (x5) == PLUS && GET_MODE (x5) == SImode && 1)
    goto L962;
  goto ret0;
 L962:
  x6 = XEXP (x5, 0);
  if (general_operand (x6, SImode))
    { recog_operand[0] = x6; goto L963; }
  goto ret0;
 L963:
  x6 = XEXP (x5, 1);
  if (x6 == const1_rtx && 1)
    goto L964;
  goto ret0;
 L964:
  x5 = XEXP (x4, 1);
  if (general_operand (x5, SImode))
    { recog_operand[1] = x5; goto L965; }
  goto ret0;
 L965:
  x4 = XEXP (x3, 1);
  if (x4 == const0_rtx && 1)
    goto L966;
  goto ret0;
 L966:
  x3 = XEXP (x2, 1);
  if (x3 == pc_rtx && 1)
    goto L967;
  goto ret0;
 L967:
  x3 = XEXP (x2, 2);
  if (GET_CODE (x3) == LABEL_REF && 1)
    goto L968;
  goto ret0;
 L968:
  x4 = XEXP (x3, 0);
  if (1)
    { recog_operand[2] = x4; goto L969; }
  goto ret0;
 L969:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L970;
  goto ret0;
 L970:
  x2 = XEXP (x1, 0);
  if (rtx_equal_p (x2, recog_operand[0]) && 1)
    goto L971;
  goto ret0;
 L971:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == PLUS && GET_MODE (x2) == SImode && 1)
    goto L972;
  goto ret0;
 L972:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, recog_operand[0]) && 1)
    goto L973;
  goto ret0;
 L973:
  x3 = XEXP (x2, 1);
  if (x3 == const1_rtx && 1)
    if (!TARGET_UNIX_ASM) return 145;
  goto ret0;
 L888:
  x4 = XEXP (x3, 0);
  if (GET_CODE (x4) != MINUS)
    goto ret0;
  if (GET_MODE (x4) == SImode && 1)
    goto L889;
 L941:
  if (1)
    goto L942;
  goto ret0;
 L889:
  x5 = XEXP (x4, 0);
  if (general_operand (x5, SImode))
    { recog_operand[0] = x5; goto L890; }
  goto L941;
 L890:
  x5 = XEXP (x4, 1);
  if (x5 == const1_rtx && 1)
    goto L891;
  goto L941;
 L891:
  x4 = XEXP (x3, 1);
  if (x4 == const0_rtx && 1)
    goto L892;
  x4 = XEXP (x3, 0);
  goto L941;
 L892:
  x3 = XEXP (x2, 1);
  if (x3 == pc_rtx && 1)
    goto L893;
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L941;
 L893:
  x3 = XEXP (x2, 2);
  if (GET_CODE (x3) == LABEL_REF && 1)
    goto L894;
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L941;
 L894:
  x4 = XEXP (x3, 0);
  if (1)
    { recog_operand[1] = x4; goto L895; }
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L941;
 L895:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L896;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L941;
 L896:
  x2 = XEXP (x1, 0);
  if (rtx_equal_p (x2, recog_operand[0]) && 1)
    goto L897;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L941;
 L897:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == MINUS && GET_MODE (x2) == SImode && 1)
    goto L898;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L941;
 L898:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, recog_operand[0]) && 1)
    goto L899;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L941;
 L899:
  x3 = XEXP (x2, 1);
  if (x3 == const1_rtx && 1)
    if (!TARGET_UNIX_ASM) return 141;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L941;
 L942:
  x5 = XEXP (x4, 0);
  if (GET_CODE (x5) == PLUS && GET_MODE (x5) == SImode && 1)
    goto L943;
  goto ret0;
 L943:
  x6 = XEXP (x5, 0);
  if (general_operand (x6, SImode))
    { recog_operand[0] = x6; goto L944; }
  goto ret0;
 L944:
  x6 = XEXP (x5, 1);
  if (x6 == const1_rtx && 1)
    goto L945;
  goto ret0;
 L945:
  x5 = XEXP (x4, 1);
  if (general_operand (x5, SImode))
    { recog_operand[1] = x5; goto L946; }
  goto ret0;
 L946:
  x4 = XEXP (x3, 1);
  if (x4 == const0_rtx && 1)
    goto L947;
  goto ret0;
 L947:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == LABEL_REF && 1)
    goto L948;
  goto ret0;
 L948:
  x4 = XEXP (x3, 0);
  if (1)
    { recog_operand[2] = x4; goto L949; }
  goto ret0;
 L949:
  x3 = XEXP (x2, 2);
  if (x3 == pc_rtx && 1)
    goto L950;
  goto ret0;
 L950:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L951;
  goto ret0;
 L951:
  x2 = XEXP (x1, 0);
  if (rtx_equal_p (x2, recog_operand[0]) && 1)
    goto L952;
  goto ret0;
 L952:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == PLUS && GET_MODE (x2) == SImode && 1)
    goto L953;
  goto ret0;
 L953:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, recog_operand[0]) && 1)
    goto L954;
  goto ret0;
 L954:
  x3 = XEXP (x2, 1);
  if (x3 == const1_rtx && 1)
    if (!TARGET_UNIX_ASM) return 144;
  goto ret0;
 L905:
  x4 = XEXP (x3, 0);
  if (GET_CODE (x4) != MINUS)
    goto ret0;
  if (GET_MODE (x4) == SImode && 1)
    goto L906;
 L922:
  if (1)
    goto L923;
  goto ret0;
 L906:
  x5 = XEXP (x4, 0);
  if (general_operand (x5, SImode))
    { recog_operand[0] = x5; goto L907; }
  goto L922;
 L907:
  x5 = XEXP (x4, 1);
  if (x5 == const1_rtx && 1)
    goto L908;
  goto L922;
 L908:
  x4 = XEXP (x3, 1);
  if (x4 == const0_rtx && 1)
    goto L909;
  x4 = XEXP (x3, 0);
  goto L922;
 L909:
  x3 = XEXP (x2, 1);
  if (x3 == pc_rtx && 1)
    goto L910;
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L922;
 L910:
  x3 = XEXP (x2, 2);
  if (GET_CODE (x3) == LABEL_REF && 1)
    goto L911;
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L922;
 L911:
  x4 = XEXP (x3, 0);
  if (1)
    { recog_operand[1] = x4; goto L912; }
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L922;
 L912:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L913;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L922;
 L913:
  x2 = XEXP (x1, 0);
  if (rtx_equal_p (x2, recog_operand[0]) && 1)
    goto L914;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L922;
 L914:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == MINUS && GET_MODE (x2) == SImode && 1)
    goto L915;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L922;
 L915:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, recog_operand[0]) && 1)
    goto L916;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L922;
 L916:
  x3 = XEXP (x2, 1);
  if (x3 == const1_rtx && 1)
    if (!TARGET_UNIX_ASM) return 142;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L922;
 L923:
  x5 = XEXP (x4, 0);
  if (GET_CODE (x5) == PLUS && GET_MODE (x5) == SImode && 1)
    goto L924;
  goto ret0;
 L924:
  x6 = XEXP (x5, 0);
  if (general_operand (x6, SImode))
    { recog_operand[0] = x6; goto L925; }
  goto ret0;
 L925:
  x6 = XEXP (x5, 1);
  if (x6 == const1_rtx && 1)
    goto L926;
  goto ret0;
 L926:
  x5 = XEXP (x4, 1);
  if (general_operand (x5, SImode))
    { recog_operand[1] = x5; goto L927; }
  goto ret0;
 L927:
  x4 = XEXP (x3, 1);
  if (x4 == const0_rtx && 1)
    goto L928;
  goto ret0;
 L928:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == LABEL_REF && 1)
    goto L929;
  goto ret0;
 L929:
  x4 = XEXP (x3, 0);
  if (1)
    { recog_operand[2] = x4; goto L930; }
  goto ret0;
 L930:
  x3 = XEXP (x2, 2);
  if (x3 == pc_rtx && 1)
    goto L931;
  goto ret0;
 L931:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L932;
  goto ret0;
 L932:
  x2 = XEXP (x1, 0);
  if (rtx_equal_p (x2, recog_operand[0]) && 1)
    goto L933;
  goto ret0;
 L933:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == PLUS && GET_MODE (x2) == SImode && 1)
    goto L934;
  goto ret0;
 L934:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, recog_operand[0]) && 1)
    goto L935;
  goto ret0;
 L935:
  x3 = XEXP (x2, 1);
  if (x3 == const1_rtx && 1)
    if (!TARGET_UNIX_ASM) return 143;
  goto ret0;
 L994:
  x1 = XEXP (x0, 0);
  if (general_operand (x1, QImode))
    { recog_operand[0] = x1; goto L995; }
  goto ret0;
 L995:
  x1 = XEXP (x0, 1);
  if (general_operand (x1, QImode))
    { recog_operand[1] = x1; return 147; }
  goto ret0;
 ret0: return -1;
}
