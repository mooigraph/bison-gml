/* Generated automatically by the program `genoutput'
from the machine description file `md'.  */

#include "config.h"
#include "rtl.h"
#include "regs.h"
#include "conditions.h"
#include "insn-flags.h"
#include "insn-config.h"

extern rtx adj_offsetable_operand();
extern void output_asm_insn();

#include "aux-output.c"

char *
output_insn_hairy (code_number, operands, insn)
     int code_number;
     rtx *operands;
     rtx insn;
{
  switch (code_number)
    {
    case 13:

{
  if (operands[1] == dconst0_rtx)
    return "clrd %0";
  return "movd %1,%0";
}
    case 14:

{
  if (operands[1] == fconst0_rtx)
    return "clrf %0";
  return "movf %1,%0";
}
    case 17:

{ if (operands[1] == const1_rtx
      && GET_MODE (REG_NOTES (insn)) == (enum machine_mode) REG_WAS_0)
    return "incl %0";
  if (GET_CODE (operands[1]) == SYMBOL_REF || GET_CODE (operands[1]) == CONST)
    {
      if (push_operand (operands[0], SImode))
	return "pushab %a1";
      return "movab %a1,%0";
    }
  if (operands[1] == const0_rtx)
    return "clrl %0";
  if (GET_CODE (operands[1]) == CONST_INT
      && (unsigned) INTVAL (operands[1]) >= 64)
    {
      int i = INTVAL (operands[1]);
      if ((unsigned)(-i) < 64)
	{
	  operands[1] = gen_rtx (CONST_INT, VOIDmode, -i);
	  return "mnegl %1,%0";
	}
      if ((unsigned)i < 0x100)
	return "movzbl %1,%0";
      if (i >= -0x80 && i < 0)
	return "cvtbl %1,%0";
      if ((unsigned)i < 0x10000)
	return "movzwl %1,%0";
      if (i >= -0x8000 && i < 0)
	return "cvtwl %1,%0";
    }
  if (push_operand (operands[0], SImode))
    return "pushl %1";
  return "movl %1,%0";
}
    case 18:

{
 if (operands[1] == const1_rtx
      && GET_MODE (REG_NOTES (insn)) == (enum machine_mode) REG_WAS_0)
    return "incw %0";
  if (operands[1] == const0_rtx)
    return "clrw %0";
  if (GET_CODE (operands[1]) == CONST_INT
      && (unsigned) INTVAL (operands[1]) >= 64)
    {
      int i = INTVAL (operands[1]);
      if ((unsigned)(-i) < 64)
	{
	  operands[1] = gen_rtx (CONST_INT, VOIDmode, -i);
	  return "mnegw %1,%0";
	}
      if ((unsigned)i < 0x100)
	return "movzbw %1,%0";
      if (i >= -0x80 && i < 0)
	return "cvtbw %1,%0";
    }
  return "movw %1,%0";
}
    case 19:

{
  if (operands[1] == const0_rtx)
    return "clrb %0";
  return "movb %1,%0";
}
    case 21:

{
  if (push_operand (operands[0], SImode))
    return "pushab %a1";
  return "movab %a1,%0";
}
    case 22:

{
  if (push_operand (operands[0], SImode))
    return "pushaw %a1";
  return "movaw %a1,%0";
}
    case 23:

{
  if (push_operand (operands[0], SImode))
    return "pushal %a1";
  return "moval %a1,%0";
}
    case 24:

{
  if (push_operand (operands[0], SImode))
    return "pushaf %a1";
  return "movaf %a1,%0";
}
    case 25:

{
  if (push_operand (operands[0], SImode))
    return "pushad %a1";
  return "movad %a1,%0";
}
    case 49:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "addd2 %2,%0";
  if (rtx_equal_p (operands[0], operands[2]))
    return "addd2 %1,%0";
  return "addd3 %1,%2,%0";
}
    case 50:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "addf2 %2,%0";
  if (rtx_equal_p (operands[0], operands[2]))
    return "addf2 %1,%0";
  return "addf3 %1,%2,%0";
}
    case 51:

{
  if (rtx_equal_p (operands[0], operands[1]))
    {
      if (operands[2] == const1_rtx)
	return "incl %0";
      if (GET_CODE (operands[1]) == CONST_INT
	  && INTVAL (operands[1]) == -1)
	return "decl %0";
      if (GET_CODE (operands[2]) == CONST_INT
	  && (unsigned) (- INTVAL (operands[2])) < 64)
	return "subl2 $%n2,%0";
      return "addl2 %2,%0";
    }
  if (rtx_equal_p (operands[0], operands[2]))
    return "addl2 %1,%0";
  if (GET_CODE (operands[2]) == CONST_INT
      && GET_CODE (operands[1]) == REG)
    {
      if (push_operand (operands[0], SImode))
        return "pushab %c2(%1)";
      return "movab %c2(%1),%0";
    }
  if (GET_CODE (operands[2]) == CONST_INT
      && (unsigned) (- INTVAL (operands[2])) < 64)
    return "subl3 $%n2,%1,%0";
  return "addl3 %1,%2,%0";
}
    case 52:

{
  if (rtx_equal_p (operands[0], operands[1]))
    {
      if (operands[2] == const1_rtx)
	return "incw %0";
      if (GET_CODE (operands[1]) == CONST_INT
	  && INTVAL (operands[1]) == -1)
	return "decw %0";
      if (GET_CODE (operands[2]) == CONST_INT
	  && (unsigned) (- INTVAL (operands[2])) < 64)
	return "subw2 $%n2,%0";
      return "addw2 %2,%0";
    }
  if (rtx_equal_p (operands[0], operands[2]))
    return "addw2 %1,%0";
  if (GET_CODE (operands[2]) == CONST_INT
      && (unsigned) (- INTVAL (operands[2])) < 64)
    return "subw3 $%n2,%1,%0";
  return "addw3 %1,%2,%0";
}
    case 53:

{
  if (rtx_equal_p (operands[0], operands[1]))
    {
      if (operands[2] == const1_rtx)
	return "incb %0";
      if (GET_CODE (operands[1]) == CONST_INT
	  && INTVAL (operands[1]) == -1)
	return "decb %0";
      if (GET_CODE (operands[2]) == CONST_INT
	  && (unsigned) (- INTVAL (operands[2])) < 64)
	return "subb2 $%n2,%0";
      return "addb2 %2,%0";
    }
  if (rtx_equal_p (operands[0], operands[2]))
    return "addb2 %1,%0";
  if (GET_CODE (operands[2]) == CONST_INT
      && (unsigned) (- INTVAL (operands[2])) < 64)
    return "subb3 $%n2,%1,%0";
  return "addb3 %1,%2,%0";
}
    case 54:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "subd2 %2,%0";
  return "subd3 %2,%1,%0";
}
    case 55:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "subf2 %2,%0";
  return "subf3 %2,%1,%0";
}
    case 56:

{
  if (rtx_equal_p (operands[0], operands[1]))
    {
      if (operands[2] == const1_rtx)
	return "decl %0";
      return "subl2 %2,%0";
    }
  return "subl3 %2,%1,%0";
}
    case 57:

{
  if (rtx_equal_p (operands[0], operands[1]))
    {
      if (operands[2] == const1_rtx)
	return "decw %0";
      return "subw2 %2,%0";
    }
  return "subw3 %2,%1,%0";
}
    case 58:

{
  if (rtx_equal_p (operands[0], operands[1]))
    {
      if (operands[2] == const1_rtx)
	return "decb %0";
      return "subb2 %2,%0";
    }
  return "subb3 %2,%1,%0";
}
    case 59:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "muld2 %2,%0";
  if (rtx_equal_p (operands[0], operands[2]))
    return "muld2 %1,%0";
  return "muld3 %1,%2,%0";
}
    case 60:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "mulf2 %2,%0";
  if (rtx_equal_p (operands[0], operands[2]))
    return "mulf2 %1,%0";
  return "mulf3 %1,%2,%0";
}
    case 61:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "mull2 %2,%0";
  if (rtx_equal_p (operands[0], operands[2]))
    return "mull2 %1,%0";
  return "mull3 %1,%2,%0";
}
    case 62:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "mulw2 %2,%0";
  if (rtx_equal_p (operands[0], operands[2]))
    return "mulw2 %1,%0";
  return "mulw3 %1,%2,%0";
}
    case 63:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "mulb2 %2,%0";
  if (rtx_equal_p (operands[0], operands[2]))
    return "mulb2 %1,%0";
  return "mulb3 %1,%2,%0";
}
    case 64:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "divd2 %2,%0";
  return "divd3 %2,%1,%0";
}
    case 65:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "divf2 %2,%0";
  return "divf3 %2,%1,%0";
}
    case 66:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "divl2 %2,%0";
  return "divl3 %2,%1,%0";
}
    case 67:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "divw2 %2,%0";
  return "divw3 %2,%1,%0";
}
    case 68:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "divb2 %2,%0";
  return "divb3 %2,%1,%0";
}
    case 69:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "bicl2 %2,%0";
  return "bicl3 %2,%1,%0";
}
    case 70:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "bicw2 %2,%0";
  return "bicw3 %2,%1,%0";
}
    case 71:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "bicb2 %2,%0";
  return "bicb3 %2,%1,%0";
}
    case 72:

{ operands[2] = gen_rtx (CONST_INT, VOIDmode, ~INTVAL (operands[2]));
  if (rtx_equal_p (operands[1], operands[0]))
    return "bicl2 %2,%0";
  return "bicl3 %2,%1,%0";
}
    case 73:

{ operands[2] = gen_rtx (CONST_INT, VOIDmode, 0xffff & ~INTVAL (operands[2]));
  if (rtx_equal_p (operands[1], operands[0]))
    return "bicw2 %2,%0";
  return "bicw3 %2,%1,%0";
}
    case 74:

{ operands[2] = gen_rtx (CONST_INT, VOIDmode, 0xff & ~INTVAL (operands[2]));
  if (rtx_equal_p (operands[1], operands[0]))
    return "bicb2 %2,%0";
  return "bicb3 %2,%1,%0";
}
    case 75:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "bisl2 %2,%0";
  if (rtx_equal_p (operands[0], operands[2]))
    return "bisl2 %1,%0";
  return "bisl3 %2,%1,%0";
}
    case 76:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "bisw2 %2,%0";
  if (rtx_equal_p (operands[0], operands[2]))
    return "bisw2 %1,%0";
  return "bisw3 %2,%1,%0";
}
    case 77:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "bisb2 %2,%0";
  if (rtx_equal_p (operands[0], operands[2]))
    return "bisb2 %1,%0";
  return "bisb3 %2,%1,%0";
}
    case 78:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "xorl2 %2,%0";
  if (rtx_equal_p (operands[0], operands[2]))
    return "xorl2 %1,%0";
  return "xorl3 %2,%1,%0";
}
    case 79:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "xorw2 %2,%0";
  if (rtx_equal_p (operands[0], operands[2]))
    return "xorw2 %1,%0";
  return "xorw3 %2,%1,%0";
}
    case 80:

{
  if (rtx_equal_p (operands[0], operands[1]))
    return "xorb2 %2,%0";
  if (rtx_equal_p (operands[0], operands[2]))
    return "xorb2 %1,%0";
  return "xorb3 %2,%1,%0";
}
    default: abort ();
    }
}

char *insn_template[] =
  {
    "tstd %0",
    "tstf %0",
    "tstl %0",
    "tstw %0",
    "tstb %0",
    "cmpd %0,%1",
    "cmpf %0,%1",
    "cmpl %0,%1",
    "cmpw %0,%1",
    "cmpb %0,%1",
    "bitl %0,%1",
    "bitw %0,%1",
    "bitb %0,%1",
    0,
    0,
    "movh %1,%0",
    "movd %1,%0",
    0,
    0,
    0,
    "movc3 %2,%1,%0",
    0,
    0,
    0,
    0,
    0,
    "cvtbw %1,%0",
    "cvtbl %1,%0",
    "cvtbf %1,%0",
    "cvtbd %1,%0",
    "cvtwb %1,%0",
    "cvtwl %1,%0",
    "cvtwf %1,%0",
    "cvtwd %1,%0",
    "cvtlb %1,%0",
    "cvtlw %1,%0",
    "cvtlf %1,%0",
    "cvtld %1,%0",
    "cvtfb %1,%0",
    "cvtfw %1,%0",
    "cvtfl %1,%0",
    "cvtfd %1,%0",
    "cvtdb %1,%0",
    "cvtdw %1,%0",
    "cvtdl %1,%0",
    "cvtdf %1,%0",
    "movzbw %1,%0",
    "movzbl %1,%0",
    "movzwl %1,%0",
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    "mnegd %1,%0",
    "mnegf %1,%0",
    "mnegl %1,%0",
    "mnegw %1,%0",
    "mnegb %1,%0",
    "mcoml %1,%0",
    "mcomw %1,%0",
    "mcomb %1,%0",
    "ashl %2,%1,%0",
    "ashq %2,%1,%0",
    "rotl %2,%1,%0",
    "rotq %2,%1,%0",
    "index %1,$??,$??,%3,%2,%0",
    "cmpv %2,%1,%0,%3",
    "cmpzv %2,%1,%0,%3",
    "extv %3,%2,%1,%0",
    "extzv %3,%2,%1,%0",
    "extv %3,%2,%1,%0",
    "extzv %3,%2,%1,%0",
    "insv %3,%2,%1,%0",
    "insv %3,%2,%1,%0",
    "jbr %l0",
    "jeql %l0",
    "jneq %l0",
    "jgtr %l0",
    "jgtru %l0",
    "jlss %l0",
    "jlssu %l0",
    "jgeq %l0",
    "jgequ %l0",
    "jleq %l0",
    "jlequ %l0",
    "jneq %l0",
    "jeql %l0",
    "jleq %l0",
    "jlequ %l0",
    "jgeq %l0",
    "jgequ %l0",
    "jlss %l0",
    "jlssu %l0",
    "jgtr %l0",
    "jgtru %l0",
    "jbs %1,%0,%l2",
    "jbc %1,%0,%l2",
    "jbc %1,%0,%l2",
    "jbs %1,%0,%l2",
    "jbs %1,%0,%l2",
    "jbc %1,%0,%l2",
    "jbc %1,%0,%l2",
    "jbs %1,%0,%l2",
    "jlbs %0,%l1",
    "jlbc %0,%l1",
    "jlbc %0,%l1",
    "jlbs %0,%l1",
    "jlbs %0,%l1",
    "jlbc %0,%l1",
    "jlbc %0,%l1",
    "jlbs %0,%l1",
    "jsobgtr %0,%l1",
    "jsobgeq %0,%l1",
    "jsobgtr %0,%l1",
    "jsobgeq %0,%l1",
    "jaoblss %1,%0,%l2",
    "jaobleq %1,%0,%l2",
    "jaoblss %1,%0,%l2",
    "jaobleq %1,%0,%l2",
    "calls %1,%0",
    "ret",
    "casel %0,%1,%2",
    "casel %0,$0,%1",
  };

rtx (*insn_gen_function[]) () =
  {
    gen_tstdf,
    gen_tstsf,
    gen_tstsi,
    gen_tsthi,
    gen_tstqi,
    gen_cmpdf,
    gen_cmpsf,
    gen_cmpsi,
    gen_cmphi,
    gen_cmpqi,
    0,
    0,
    0,
    gen_movdf,
    gen_movsf,
    gen_movti,
    gen_movdi,
    gen_movsi,
    gen_movhi,
    gen_movqi,
    gen_movstrhi,
    0,
    0,
    0,
    0,
    0,
    gen_extendqihi2,
    gen_extendqisi2,
    gen_floatqisf2,
    gen_floatqidf2,
    gen_trunchiqi2,
    gen_extendhisi2,
    gen_floathisf2,
    gen_floathidf2,
    gen_truncsiqi2,
    gen_truncsihi2,
    gen_floatsisf2,
    gen_floatsidf2,
    gen_fixsfqi2,
    gen_fixsfhi2,
    gen_fixsfsi2,
    gen_extendsfdf2,
    gen_fixdfqi2,
    gen_fixdfhi2,
    gen_fixdfsi2,
    gen_truncdfsf2,
    gen_zero_extendqihi2,
    gen_zero_extendqisi2,
    gen_zero_extendhisi2,
    gen_adddf3,
    gen_addsf3,
    gen_addsi3,
    gen_addhi3,
    gen_addqi3,
    gen_subdf3,
    gen_subsf3,
    gen_subsi3,
    gen_subhi3,
    gen_subqi3,
    gen_muldf3,
    gen_mulsf3,
    gen_mulsi3,
    gen_mulhi3,
    gen_mulqi3,
    gen_divdf3,
    gen_divsf3,
    gen_divsi3,
    gen_divhi3,
    gen_divqi3,
    gen_andcbsi3,
    gen_andcbhi3,
    gen_andcbqi3,
    0,
    0,
    0,
    gen_iorsi3,
    gen_iorhi3,
    gen_iorqi3,
    gen_xorsi3,
    gen_xorhi3,
    gen_xorqi3,
    gen_negdf2,
    gen_negsf2,
    gen_negsi2,
    gen_neghi2,
    gen_negqi2,
    gen_one_cmplsi2,
    gen_one_cmplhi2,
    gen_one_cmplqi2,
    gen_ashlsi3,
    gen_ashldi3,
    gen_rotlsi3,
    gen_rotldi3,
    0,
    0,
    0,
    gen_extv,
    gen_extzv,
    0,
    0,
    gen_insv,
    0,
    gen_jump,
    gen_beq,
    gen_bne,
    gen_bgt,
    gen_bgtu,
    gen_blt,
    gen_bltu,
    gen_bge,
    gen_bgeu,
    gen_ble,
    gen_bleu,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    gen_call,
    gen_return,
    gen_casesi,
    0,
  };

int insn_n_operands[] =
  {
    1,
    1,
    1,
    1,
    1,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    3,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    3,
    3,
    3,
    3,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    2,
    2,
    2,
    2,
    2,
    0,
    3,
    2,
  };

int insn_n_dups[] =
  {
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    0,
    0,
    2,
    1,
  };

char *insn_operand_constraint[][MAX_RECOG_OPERANDS] =
  {
    { "gF", },
    { "gF", },
    { "g", },
    { "g", },
    { "g", },
    { "gF", "gF", },
    { "gF", "gF", },
    { "g", "g", },
    { "g", "g", },
    { "g", "g", },
    { "g", "g", },
    { "g", "g", },
    { "g", "g", },
    { "=g", "gF", },
    { "=g", "gF", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", "g", },
    { "=g", "p", },
    { "=g", "p", },
    { "=g", "p", },
    { "=g", "p", },
    { "=g", "p", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "gF", },
    { "=g", "gF", },
    { "=g", "gF", },
    { "=g", "gF", },
    { "=g", "gF", },
    { "=g", "gF", },
    { "=g", "gF", },
    { "=g", "gF", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "gF", "gF", },
    { "=g", "gF", "gF", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "gF", "gF", },
    { "=g", "gF", "gF", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "gF", "gF", },
    { "=g", "gF", "gF", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "gF", "gF", },
    { "=g", "gF", "gF", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "gF", },
    { "=g", "gF", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", },
    { "=g", "g", "g", "g", },
    { "g", "g", "g", "g", },
    { "g", "g", "g", "g", },
    { "=g", "g", "g", "g", },
    { "=g", "g", "g", "g", },
    { "=g", "r", "g", "g", },
    { "=g", "r", "g", "g", },
    { "=g", "g", "g", "g", },
    { "=r", "g", "g", "g", },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { "g", "g", },
    { "g", "g", },
    { "g", "g", },
    { "g", "g", },
    { "r", "g", },
    { "r", "g", },
    { "r", "g", },
    { "r", "g", },
    { "g", },
    { "g", },
    { "g", },
    { "g", },
    { "g", },
    { "g", },
    { "g", },
    { "g", },
    { "+g", },
    { "+g", },
    { "+g", },
    { "+g", },
    { "+g", "g", },
    { "+g", "g", },
    { "+g", "g", },
    { "+g", "g", },
    { "g", "g", },
    { 0 },
    { "g", "g", "g", },
    { "g", "g", },
  };

enum machine_mode insn_operand_mode[][MAX_RECOG_OPERANDS] =
  {
    { DFmode, },
    { SFmode, },
    { SImode, },
    { HImode, },
    { QImode, },
    { DFmode, DFmode, },
    { SFmode, SFmode, },
    { SImode, SImode, },
    { HImode, HImode, },
    { QImode, QImode, },
    { SImode, SImode, },
    { HImode, HImode, },
    { QImode, QImode, },
    { DFmode, DFmode, },
    { SFmode, SFmode, },
    { TImode, TImode, },
    { DImode, DImode, },
    { SImode, SImode, },
    { HImode, HImode, },
    { QImode, QImode, },
    { BLKmode, BLKmode, HImode, },
    { SImode, QImode, },
    { SImode, HImode, },
    { SImode, SImode, },
    { SImode, SFmode, },
    { SImode, DFmode, },
    { HImode, QImode, },
    { SImode, QImode, },
    { SFmode, QImode, },
    { DFmode, QImode, },
    { QImode, HImode, },
    { SImode, HImode, },
    { SFmode, HImode, },
    { DFmode, HImode, },
    { QImode, SImode, },
    { HImode, SImode, },
    { SFmode, SImode, },
    { DFmode, SImode, },
    { QImode, SFmode, },
    { HImode, SFmode, },
    { SImode, SFmode, },
    { DFmode, SFmode, },
    { QImode, DFmode, },
    { HImode, DFmode, },
    { SImode, DFmode, },
    { SFmode, DFmode, },
    { HImode, QImode, },
    { SImode, QImode, },
    { SImode, HImode, },
    { DFmode, DFmode, DFmode, },
    { SFmode, SFmode, SFmode, },
    { SImode, SImode, SImode, },
    { HImode, HImode, HImode, },
    { QImode, QImode, QImode, },
    { DFmode, DFmode, DFmode, },
    { SFmode, SFmode, SFmode, },
    { SImode, SImode, SImode, },
    { HImode, HImode, HImode, },
    { QImode, QImode, QImode, },
    { DFmode, DFmode, DFmode, },
    { SFmode, SFmode, SFmode, },
    { SImode, SImode, SImode, },
    { HImode, HImode, HImode, },
    { QImode, QImode, QImode, },
    { DFmode, DFmode, DFmode, },
    { SFmode, SFmode, SFmode, },
    { SImode, SImode, SImode, },
    { HImode, HImode, HImode, },
    { QImode, QImode, QImode, },
    { SImode, SImode, SImode, },
    { HImode, HImode, HImode, },
    { QImode, QImode, QImode, },
    { SImode, SImode, SImode, },
    { HImode, HImode, HImode, },
    { QImode, QImode, QImode, },
    { SImode, SImode, SImode, },
    { HImode, HImode, HImode, },
    { QImode, QImode, QImode, },
    { SImode, SImode, SImode, },
    { HImode, HImode, HImode, },
    { QImode, QImode, QImode, },
    { DFmode, DFmode, },
    { SFmode, SFmode, },
    { SImode, SImode, },
    { HImode, HImode, },
    { QImode, QImode, },
    { SImode, SImode, },
    { HImode, HImode, },
    { QImode, QImode, },
    { SImode, SImode, QImode, },
    { DImode, DImode, QImode, },
    { SImode, SImode, QImode, },
    { DImode, DImode, QImode, },
    { SImode, SImode, SImode, SImode, },
    { QImode, SImode, SImode, SImode, },
    { QImode, SImode, SImode, SImode, },
    { SImode, QImode, SImode, SImode, },
    { SImode, QImode, SImode, SImode, },
    { SImode, SImode, SImode, SImode, },
    { SImode, SImode, SImode, SImode, },
    { QImode, SImode, SImode, SImode, },
    { SImode, SImode, SImode, SImode, },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { QImode, SImode, },
    { QImode, SImode, },
    { QImode, SImode, },
    { QImode, SImode, },
    { SImode, SImode, },
    { SImode, SImode, },
    { SImode, SImode, },
    { SImode, SImode, },
    { SImode, },
    { SImode, },
    { SImode, },
    { SImode, },
    { SImode, },
    { SImode, },
    { SImode, },
    { SImode, },
    { SImode, },
    { SImode, },
    { SImode, },
    { SImode, },
    { SImode, SImode, },
    { SImode, SImode, },
    { SImode, SImode, },
    { SImode, SImode, },
    { QImode, QImode, },
    { VOIDmode },
    { SImode, SImode, SImode, },
    { SImode, SImode, },
  };
