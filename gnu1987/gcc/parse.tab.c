
/*  A Bison parser, made from parse.y  */

#define	IDENTIFIER	258
#define	TYPENAME	259
#define	SCSPEC	260
#define	TYPESPEC	261
#define	TYPEMOD	262
#define	CONSTANT	263
#define	STRING	264
#define	ELLIPSIS	265
#define	SIZEOF	266
#define	ENUM	267
#define	STRUCT	268
#define	UNION	269
#define	IF	270
#define	ELSE	271
#define	WHILE	272
#define	DO	273
#define	FOR	274
#define	SWITCH	275
#define	CASE	276
#define	DEFAULT	277
#define	BREAK	278
#define	CONTINUE	279
#define	RETURN	280
#define	GOTO	281
#define	ASM	282
#define	ASSIGN	283
#define	OROR	284
#define	ANDAND	285
#define	EQCOMPARE	286
#define	ARITHCOMPARE	287
#define	LSHIFT	288
#define	RSHIFT	289
#define	UNARY	290
#define	PLUSPLUS	291
#define	MINUSMINUS	292
#define	HYPERUNARY	293
#define	POINTSAT	294

#line 25 "parse.y"

#include "config.h"
#include "tree.h"
#include "parse.h"
#include "c-tree.h"

#line 34 "parse.y"
typedef union {long itype; tree ttype; enum tree_code code} YYSTYPE;
#line 108 "parse.y"

/* the declaration found for the last IDENTIFIER token read in.
   yylex must look this up to detect typedefs, which get token type TYPENAME,
   so it is left around in case the identifier is not a typedef but is
   used in a context which makes it a reference to a variable.  */
static tree lastiddecl;

tree current_function_decl, current_switch_stmt, current_block;
tree current_continue_label, current_break_label;
tree continue_label_stack, break_label_stack;

static void pushbreak(), popbreak();
static tree finish_compound_stmt();
static tree make_pointer_declarator ();
static tree combine_strings ();

/* list of types and structure classes of the current declaration */
tree current_declspecs;

char *input_filename;		/* file being read */

#ifndef YYLTYPE
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;

#define YYLTYPE yyltype
#endif

#define	YYACCEPT	return(0)
#define	YYABORT	return(1)
#define	YYERROR	return(1)
#include <stdio.h>



#define	YYFINAL		390
#define	YYFLAG		-32768
#define	YYNTBASE	62

#define YYTRANSLATE(x) (yytranslate[x])

static char yytranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,    57,     2,     2,     2,    45,    36,     2,    52,
    53,    43,    41,    58,    42,    51,    44,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,    31,    54,     2,
    29,     2,    30,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
    59,     2,    60,    35,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,    61,    34,    55,    56,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     2,     3,     4,     5,
     6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
    16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
    26,    27,    28,    32,    33,    37,    38,    39,    40,    46,
    47,    48,    49,    50
};

static short yyrline[] = {     0,
   131,   139,   141,   141,   142,   144,   146,   147,   151,   153,
   155,   157,   159,   161,   162,   163,   166,   171,   173,   174,
   176,   180,   182,   183,   185,   189,   191,   192,   195,   197,
   200,   202,   204,   206,   208,   210,   212,   216,   220,   223,
   226,   229,   233,   235,   237,   239,   241,   243,   245,   247,
   249,   251,   253,   255,   257,   259,   261,   263,   265,   267,
   269,   271,   273,   275,   277,   283,   302,   303,   305,   307,
   309,   311,   313,   315,   317,   319,   324,   326,   330,   332,
   335,   337,   338,   339,   344,   348,   351,   353,   355,   361,
   364,   366,   368,   370,   372,   378,   380,   386,   389,   391,
   393,   397,   398,   399,   400,   403,   405,   408,   410,   413,
   418,   419,   424,   429,   430,   435,   437,   439,   445,   448,
   455,   457,   462,   465,   467,   469,   471,   473,   479,   482,
   484,   486,   488,   490,   492,   494,   497,   500,   502,   504,
   506,   508,   510,   512,   514,   516,   518,   522,   524,   525,
   527,   530,   533,   537,   540,   541,   545,   548,   550,   559,
   561,   563,   567,   570,   574,   579,   582,   585,   588,   592,
   595,   597,   599,   601,   603,   605,   607,   609,   620,   622,
   624,   627,   634,   642,   644,   647,   650,   654,   659,   660,
   662,   664,   666,   668,   675,   677,   684,   686,   697,   704,
   710,   722,   728,   737,   746,   748,   750,   752,   754,   756,
   760,   763,   768,   770,   773,   777,   780,   784,   787,   792,
   795
};

static char * yytname[] = {     0,
"error","$illegal.","IDENTIFIER","TYPENAME","SCSPEC","TYPESPEC","TYPEMOD","CONSTANT","STRING","ELLIPSIS",
"SIZEOF","ENUM","STRUCT","UNION","IF","ELSE","WHILE","DO","FOR","SWITCH",
"CASE","DEFAULT","BREAK","CONTINUE","RETURN","GOTO","ASM","ASSIGN","'='","'?'",
"':'","OROR","ANDAND","'|'","'^'","'&'","EQCOMPARE","ARITHCOMPARE","LSHIFT","RSHIFT",
"'+'","'-'","'*'","'/'","'%'","UNARY","PLUSPLUS","MINUSMINUS","HYPERUNARY","POINTSAT",
"'.'","'('","')'","';'","'}'","'~'","'!'","','","'['","']'",
"'{'","program"
};

static short yyr1[] = {     0,
    62,    64,    63,    65,    63,    66,    66,    66,    67,    67,
    67,    67,    67,    67,    67,    67,    69,    70,    68,    68,
    71,    72,    68,    68,    73,    74,    68,    68,    75,    75,
    76,    76,    76,    76,    76,    76,    76,    77,    78,    78,
    79,    79,    80,    80,    80,    80,    80,    80,    80,    80,
    80,    80,    80,    80,    80,    80,    80,    80,    80,    80,
    80,    80,    80,    80,    80,    81,    81,    81,    81,    81,
    81,    81,    81,    81,    81,    81,    82,    82,    83,    83,
    84,    84,    84,    84,    85,    86,    86,    86,    86,    87,
    87,    87,    87,    87,    87,    88,    88,    89,    89,    89,
    89,    90,    90,    90,    90,    91,    91,    92,    92,    94,
    93,    93,    96,    95,    95,    97,    97,    97,    98,    98,
    99,    99,   100,   100,   100,   100,   100,   100,   101,   101,
   101,   101,   101,   101,   101,   101,   102,   102,   102,   102,
   102,   102,   103,   102,   104,   102,   102,   105,   105,   105,
   105,   106,   106,   107,   107,   107,   108,   108,   108,   109,
   109,   109,   110,   110,   111,   112,   112,   113,   113,   114,
   114,   114,   114,   114,   114,   114,   114,   114,   115,   115,
   115,   116,   117,   118,   118,   118,   118,   118,   119,   119,
   119,   119,   120,   119,   121,   119,   122,   119,   123,   119,
   119,   119,   119,   119,   119,   119,   119,   119,   119,   119,
   124,   124,   125,   125,   125,   126,   126,   127,   127,   128,
   128
};

static short yyr2[] = {     0,
     1,     0,     2,     0,     3,     1,     1,     5,     3,     4,
     4,     2,     2,     2,     2,     1,     0,     0,     7,     4,
     0,     0,     7,     4,     0,     0,     6,     3,     1,     1,
     1,     1,     1,     1,     1,     1,     1,     1,     0,     1,
     1,     3,     1,     2,     2,     4,     2,     4,     3,     3,
     3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
     3,     3,     5,     3,     3,     1,     1,     1,     3,     3,
     4,     4,     3,     3,     2,     2,     1,     2,     0,     1,
     1,     1,     2,     2,     0,     4,     4,     2,     2,     1,
     2,     2,     2,     2,     2,     1,     2,     1,     2,     2,
     2,     1,     1,     1,     1,     1,     3,     1,     3,     0,
     4,     1,     0,     4,     1,     1,     3,     4,     1,     3,
     1,     1,     4,     4,     4,     4,     3,     1,     4,     4,
     4,     3,     3,     4,     3,     1,     5,     4,     2,     5,
     4,     2,     0,     6,     0,     5,     2,     0,     1,     3,
     2,     3,     1,     0,     1,     3,     1,     3,     2,     1,
     3,     2,     1,     3,     2,     0,     1,     0,     2,     3,
     3,     2,     4,     4,     3,     3,     3,     2,     1,     2,
     2,     2,     0,     2,     5,     4,     4,     4,     1,     2,
     5,     7,     0,     6,     0,     8,     0,    10,     0,     6,
     4,     3,     2,     2,     2,     3,     3,     5,     3,     1,
     0,     1,     0,     1,     3,     1,     3,     2,     2,     1,
     3
};

static short yydefact[] = {     2,
     4,     0,     0,     0,   105,    96,   102,   103,     0,     0,
     0,     0,    16,     3,     7,     6,     0,    85,    85,    90,
   104,     5,    14,    15,    29,    30,   145,   147,     0,   139,
     0,   142,     0,   136,   168,     0,     0,   108,     0,    95,
    92,    93,    13,     0,    94,    97,    12,     0,    91,     0,
   143,   153,    85,    98,     0,   149,     0,     0,     0,     0,
     0,     0,     9,     0,    28,   113,     0,     0,     0,   128,
     0,   106,     0,   121,   122,     0,     0,   163,     0,   160,
     0,    99,   100,   154,   101,     0,   138,     0,   141,     0,
     0,   169,   133,   132,   109,   112,     0,     0,   220,   166,
     0,   214,   216,     0,    66,    67,    77,     0,    31,    33,
    32,     0,    34,    35,     0,    36,    37,   135,     0,     0,
    38,    41,    43,    68,     0,    26,    80,     0,    85,    85,
    82,    11,     0,    20,   110,     0,     0,     0,    10,    24,
     0,     0,   146,   162,     0,     0,   157,   152,   155,   150,
   137,   140,     8,     0,   116,   114,   131,   168,   213,     0,
   218,   219,   167,   129,     0,   130,     0,     0,    47,    44,
     0,     0,   166,     0,    45,   134,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,    75,    76,     0,     0,    39,     0,
    78,   182,     0,    83,    84,    88,     0,    89,     0,   107,
     0,    18,     0,     0,     0,   127,     0,    22,   164,   161,
   144,   159,     0,     0,   119,     0,   172,     0,     0,   178,
     0,   213,     0,   215,   217,   221,     0,    70,    69,   168,
   213,   165,     0,    42,    65,    64,     0,    62,    61,    59,
    60,    58,    57,    56,    54,    55,    49,    50,    51,    52,
    53,    74,    73,     0,    40,     0,   183,    27,     0,     0,
   115,   111,     0,   125,   123,   124,   126,     0,   158,   156,
   117,     0,   171,   170,   176,   177,     0,   175,     0,    48,
   172,    46,     0,    71,    72,   184,     0,    86,    87,    19,
    23,   118,   120,   173,   174,    63,     0,    66,   105,     0,
   193,   195,   197,     0,     0,     0,     0,     0,     0,     0,
     0,   210,     0,     0,     0,     0,   189,   179,   187,     0,
     0,     0,     0,     0,     0,     0,   203,   204,   205,     0,
     0,     0,     0,   190,   186,     0,   188,   181,   180,     0,
     0,     0,   211,     0,     0,   202,   206,   207,     0,   209,
   185,     0,     0,     0,   212,     0,   199,   201,     0,   191,
     0,     0,   211,     0,   208,     0,   194,     0,     0,   200,
   192,     0,   211,   196,     0,     0,   198,     0,     0,     0
};

static short yydefgoto[] = {   388,
     1,     2,     3,    14,    15,    16,   136,   273,   141,   278,
    69,   203,   323,   119,   324,   264,   121,   122,   123,   124,
   126,   127,    17,   128,   129,   130,   100,    54,    71,    37,
    72,   211,    38,    97,   156,   226,    96,    74,    75,    21,
    81,    50,    55,    56,   148,   149,    79,    80,   174,   162,
    61,   163,   326,   131,   297,   327,   328,   331,   332,   333,
   374,   366,   229,   102,   103,   104
};

static short yypact[] = {-32768,
    16,  1023,  1023,   156,-32768,-32768,-32768,-32768,    21,    30,
    33,   -30,-32768,-32768,-32768,-32768,    28,   653,   599,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,   -33,   297,   -21,
   297,    -5,    50,-32768,-32768,    28,    51,-32768,   174,-32768,
-32768,-32768,-32768,    38,-32768,-32768,-32768,    28,-32768,   162,
-32768,-32768,   347,-32768,   167,-32768,   297,   175,   297,    43,
    58,    74,-32768,    38,-32768,-32768,  1048,   753,   211,-32768,
   118,-32768,   556,    -8,     5,   137,   233,    57,    14,-32768,
   162,-32768,-32768,    23,-32768,   991,-32768,   187,-32768,   189,
    63,-32768,     5,-32768,-32768,    95,   731,    83,-32768,   254,
    85,   141,-32768,    84,-32768,-32768,-32768,   937,-32768,-32768,
-32768,   966,-32768,-32768,   895,-32768,-32768,-32768,   966,   153,
   161,  1054,    71,   218,   177,-32768,   464,   321,  1051,  1027,
-32768,-32768,    38,-32768,-32768,   211,  1066,   782,-32768,-32768,
   211,   966,-32768,   162,   149,   966,   205,   191,-32768,-32768,
-32768,-32768,-32768,   731,  1054,-32768,-32768,-32768,   523,   808,
-32768,-32768,    54,-32768,   580,-32768,   250,   895,-32768,-32768,
   202,   236,   763,   237,-32768,-32768,   966,   966,   966,   966,
   966,   966,   966,   966,   966,   966,   966,   966,   966,   966,
   966,   966,   966,   966,-32768,-32768,   162,   162,   966,   966,
-32768,-32768,   232,-32768,-32768,-32768,    38,-32768,    28,-32768,
   731,-32768,   243,   246,   109,-32768,   240,-32768,  1054,-32768,
-32768,  1054,   966,    23,-32768,   150,    40,   111,   249,-32768,
   252,   863,   837,-32768,-32768,-32768,   262,-32768,-32768,-32768,
   906,-32768,   966,  1054,  1054,  1054,   274,  1101,   239,  1071,
  1111,  1120,  1128,  1135,   554,   554,   139,   139,-32768,-32768,
-32768,-32768,-32768,   264,   161,   259,   265,-32768,   143,   144,
   -14,-32768,   232,-32768,-32768,-32768,-32768,   232,  1054,-32768,
-32768,   702,    54,-32768,-32768,-32768,   278,-32768,   277,   363,
    55,-32768,   966,-32768,-32768,-32768,   380,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,  1088,   197,   283,   290,   298,
-32768,-32768,-32768,   303,   966,   318,   302,   304,   914,   162,
   313,-32768,   336,   316,   621,   439,-32768,-32768,-32768,   966,
   327,   676,   328,   966,   359,   676,-32768,-32768,-32768,   342,
   354,   400,   676,-32768,-32768,   498,-32768,-32768,-32768,   360,
   966,   395,   966,   361,   676,-32768,-32768,-32768,   364,-32768,
-32768,   676,   365,   372,-32768,   371,-32768,-32768,   375,   410,
   676,   966,   966,   676,-32768,   676,-32768,   377,   379,-32768,
-32768,   381,   966,-32768,   378,   676,-32768,   438,   444,-32768
};

static short yypgoto[] = {-32768,
-32768,-32768,-32768,   436,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,    -4,-32768,   -65,-32768,   247,   -34,-32768,-32768,
    49,   148,     2,  -119,   261,   268,     1,    -2,   242,   -38,
   -45,-32768,-32768,-32768,  -141,-32768,   -32,-32768,   -13,     0,
-32768,-32768,    45,   366,-32768,   227,   374,   309,   299,   300,
  -147,  -130,   147,  -126,-32768,  -189,   245,-32768,-32768,-32768,
-32768,  -334,   -58,-32768,   314,   337
};


#define	YYLAST		1180


static short yytable[] = {    20,
    20,   205,   120,    39,    28,    30,    32,   204,   101,    76,
   227,    73,   225,   268,    66,    -1,    49,    45,    95,    44,
    48,    33,    62,    25,    26,    34,    70,    51,   228,    53,
    34,    53,    25,    26,    77,    25,    26,    67,   379,    57,
    34,    70,    34,   137,    68,    78,    92,    93,   385,   172,
   138,   147,    85,   146,    84,    59,    67,    53,    60,    53,
    34,    92,   155,    68,    92,    35,    20,   161,   143,   272,
    35,   144,   217,   169,    36,    58,    78,   170,   214,    36,
    35,    27,   158,   300,   175,   142,    53,   210,   301,    36,
    29,   159,   291,    31,   231,    91,   283,   240,   160,    85,
    35,    88,   172,    90,    63,   232,   241,   219,    64,    36,
   228,   222,   233,   160,   247,   173,   153,   195,   196,   155,
   197,   198,   199,   135,    20,    67,    94,    49,    45,   200,
   207,   209,    68,    20,   266,   157,   166,   164,    20,    78,
   303,   167,   244,   245,   246,    62,   248,   249,   250,   251,
   252,   253,   254,   255,   256,   257,   258,   259,   260,   261,
   283,   276,   232,   284,    25,    26,   167,   289,   173,   233,
   270,   132,    85,   287,    65,   133,   155,   -25,   -25,   -25,
   -25,   192,   193,   194,   212,   -25,   -25,   -25,   279,   218,
   139,   147,   262,   263,    64,   271,   298,   299,   165,   348,
   133,    64,    66,   221,   281,   204,   144,   282,   292,    23,
    24,   125,   176,    93,     5,     6,     7,     8,   177,   348,
    86,    87,     9,    10,    11,    67,   201,  -115,    86,    89,
   202,  -115,    68,   140,   -25,   223,   -21,   -21,   -21,   -21,
    86,   151,    86,   152,   -21,   -21,   -21,   155,   224,   335,
   202,   329,   236,   340,   238,   292,    34,    70,   306,    82,
    83,    66,    18,    18,   350,     9,    10,    11,   354,    19,
    19,   -79,   183,   184,   185,   186,   187,   188,   189,   190,
   191,   192,   193,   194,    67,   363,  -115,   365,   239,   243,
  -115,    68,   267,   -21,    20,   274,   158,    52,   275,   277,
     5,   285,     7,     8,   293,   159,   378,   365,     9,    10,
    11,   286,   160,   -29,   290,   341,   294,   365,   295,   296,
   -30,   125,    20,   -81,   -81,   -81,   -81,   -81,   -81,   -81,
   304,   -81,   -81,   -81,   -81,   -81,   305,   -81,   -81,   -81,
   -81,   -81,   -81,   -81,   -81,   -81,   -81,   -81,   336,   330,
  -148,  -148,    82,    83,   334,   337,   -81,   338,     9,    10,
    11,   -81,   -81,   -81,   342,   105,   343,   -81,   -81,   344,
   106,   107,   -81,   108,   -81,   -81,   -81,   -81,   351,   353,
   307,   -81,   308,   309,     6,     7,     8,   106,   107,   355,
   108,     9,    10,    11,   310,   357,   311,   312,   313,   314,
   315,   316,   317,   318,   319,   320,   321,   358,   359,   113,
   114,   364,   362,   367,   115,   109,   369,   371,   116,   117,
   110,   111,   112,   372,   373,   376,   113,   114,   375,   382,
   386,   115,   383,   322,   384,   116,   117,   389,    22,   125,
   267,   308,    26,   390,   325,   265,   106,   107,   269,   108,
   280,   150,   220,   310,   145,   311,   312,   313,   314,   315,
   316,   317,   318,   319,   320,   321,   237,     5,     6,     7,
     8,   346,   242,   215,   109,     9,    10,    11,   235,   110,
   111,   112,     0,     0,     0,   113,   114,     0,     0,     0,
   115,     0,   322,   347,   116,   117,     0,     0,   125,   267,
   308,    26,     0,     0,     0,   106,   107,     0,   108,     0,
     0,     0,   310,     0,   311,   312,   313,   314,   315,   316,
   317,   318,   319,   320,   321,    34,     5,     0,     7,     8,
     0,     0,     0,   109,     9,    10,    11,     0,   110,   111,
   112,     0,     0,     0,   113,   114,     0,     0,     0,   115,
     0,   322,   361,   116,   117,     0,   134,     0,   267,   -17,
   -17,   -17,   -17,     0,     0,   158,     0,   -17,   -17,   -17,
   349,     0,     0,     0,   159,     0,   352,     0,     0,     0,
   356,   160,     0,     5,   135,     7,     8,   360,     0,   234,
   349,     9,    10,    11,   190,   191,   192,   193,   194,   368,
     0,     0,     5,    46,     7,     8,   370,     0,     0,  -112,
     9,    10,    11,  -112,     0,   377,   -17,     0,   380,     0,
   381,     0,     0,   308,   309,     6,     7,     8,   106,   107,
   387,   108,     9,    10,    11,   310,     0,   311,   312,   313,
   314,   315,   316,   317,   318,   319,   320,   321,     0,     0,
     0,     0,    47,     0,     0,     0,   109,    40,    41,    42,
     0,   110,   111,   112,     9,    10,    11,   113,   114,     0,
     0,     0,   115,     0,   322,   345,   116,   117,   308,    26,
     0,   267,     0,   106,   107,     0,   108,     0,     0,     0,
   310,     0,   311,   312,   313,   314,   315,   316,   317,   318,
   319,   320,   321,     0,   105,     0,    43,     0,     0,   106,
   107,   109,   108,     0,     0,     0,   110,   111,   112,     0,
     0,     0,   113,   114,     0,     0,     0,   115,     0,   322,
     0,   116,   117,   105,     0,     0,   267,   109,   106,   107,
     0,   108,   110,   111,   112,     0,     0,     0,   113,   114,
     0,     0,     0,   115,     0,   105,   302,   116,   117,     0,
   106,   107,   154,   108,     0,     0,   109,     0,    82,    83,
     0,   110,   111,   112,     9,    10,    11,   113,   114,     0,
     0,     0,   115,     0,   105,     0,   116,   117,   109,   106,
   107,   154,   108,   110,   111,   112,     0,     0,     0,   113,
   114,     0,     0,     0,   115,   240,     0,     0,   116,   117,
   105,     0,   118,     0,   241,   106,   107,   109,   108,     0,
     0,   160,   110,   111,   112,     0,     0,     0,   113,   114,
     0,     0,     0,   115,     0,     0,     0,   116,   117,   105,
     0,   216,     0,   109,   106,   107,     0,   108,   110,   111,
   112,     0,     0,     0,   113,   114,     0,     0,     0,   115,
     0,     0,     0,   116,   117,     0,     5,   230,     7,     8,
     0,     0,   109,     0,     9,    10,    11,   110,   111,   112,
     0,     0,     0,   113,   114,     0,     0,     0,   115,     0,
     0,     0,   116,   117,     0,   171,   288,   105,     5,     0,
     7,     8,   106,   107,     0,   108,     9,    10,    11,     5,
     0,     7,     8,     0,     0,     0,   105,     9,    10,    11,
     0,   106,   107,     0,   108,     0,     0,     0,     0,     0,
   109,     0,     0,     0,     0,   110,   111,   112,     0,   105,
     0,   113,   114,     0,   106,   107,   115,   108,   240,   109,
   116,   117,     0,     0,   110,   111,   112,   241,     0,     0,
   113,   114,     0,     0,   160,   115,     0,   339,   105,   116,
   117,     0,   109,   106,   107,     0,   108,   110,   111,   112,
     0,     0,     0,   113,   114,     0,     0,     0,   168,     0,
     0,    52,   116,   117,     5,     0,     7,     8,     0,     0,
     0,   109,     9,    10,    11,     0,   110,   111,   112,     0,
     0,     0,   113,   114,     0,     0,     0,   115,     0,     0,
     0,   116,   117,     4,     0,   -85,     5,     6,     7,     8,
     5,    46,     7,     8,     9,    10,    11,     0,     9,    10,
    11,     0,     0,     0,  -151,  -151,     0,     0,    98,    12,
    99,     5,     0,     7,     8,    40,    41,    42,     0,     9,
    10,    11,     9,    10,    11,   -85,   213,     0,    99,     5,
     0,     7,     8,     0,   -85,     0,    13,     9,    10,    11,
   208,   178,   179,   180,     0,   181,   182,   183,   184,   185,
   186,   187,   188,   189,   190,   191,   192,   193,   194,     0,
  -213,     0,     0,     0,   206,   184,   185,   186,   187,   188,
   189,   190,   191,   192,   193,   194,     0,   180,  -213,   181,
   182,   183,   184,   185,   186,   187,   188,   189,   190,   191,
   192,   193,   194,   182,   183,   184,   185,   186,   187,   188,
   189,   190,   191,   192,   193,   194,   185,   186,   187,   188,
   189,   190,   191,   192,   193,   194,   186,   187,   188,   189,
   190,   191,   192,   193,   194,   187,   188,   189,   190,   191,
   192,   193,   194,   188,   189,   190,   191,   192,   193,   194
};

static short yycheck[] = {     2,
     3,   128,    68,    17,     9,    10,    11,   127,    67,    48,
   158,    44,   154,   203,    29,     0,    19,    18,    64,    18,
    19,    52,    36,     3,     4,     3,     4,    61,   159,    29,
     3,    31,     3,     4,    48,     3,     4,    52,   373,    61,
     3,     4,     3,    52,    59,    50,     7,    61,   383,   115,
    59,    84,    53,    31,    53,    61,    52,    57,     9,    59,
     3,     7,    97,    59,     7,    43,    69,   100,    55,   211,
    43,    58,   138,   108,    52,    31,    81,   112,   137,    52,
    43,    61,    43,   273,   119,    29,    86,   133,   278,    52,
    61,    52,   240,    61,   160,    53,   227,    43,    59,   100,
    43,    57,   168,    59,    54,    52,    52,   142,    58,    52,
   241,   146,    59,    59,   180,   115,    54,    47,    48,   154,
    50,    51,    52,    29,   127,    52,    53,   130,   129,    59,
   129,   130,    59,   136,   200,    53,    53,    53,   141,   144,
   282,    58,   177,   178,   179,   159,   181,   182,   183,   184,
   185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
   291,    53,    52,    53,     3,     4,    58,   233,   168,    59,
   209,    54,   173,   232,     1,    58,   211,     4,     5,     6,
     7,    43,    44,    45,   136,    12,    13,    14,   223,   141,
    54,   224,   197,   198,    58,   209,    54,    54,    58,   326,
    58,    58,    29,    55,    55,   325,    58,    58,   243,    54,
    55,     1,    60,   227,     4,     5,     6,     7,    58,   346,
    54,    55,    12,    13,    14,    52,     9,    54,    54,    55,
    54,    58,    59,     1,    61,    31,     4,     5,     6,     7,
    54,    55,    54,    55,    12,    13,    14,   282,    58,   315,
    54,    55,     3,   319,    53,   290,     3,     4,   293,     6,
     7,    29,     2,     3,   330,    12,    13,    14,   334,     2,
     3,    61,    34,    35,    36,    37,    38,    39,    40,    41,
    42,    43,    44,    45,    52,   351,    54,   353,    53,    53,
    58,    59,    61,    61,   297,    53,    43,     1,    53,    60,
     4,    53,     6,     7,    31,    52,   372,   373,    12,    13,
    14,    60,    59,    31,    53,   320,    53,   383,    60,    55,
    31,     1,   325,     3,     4,     5,     6,     7,     8,     9,
    53,    11,    12,    13,    14,    15,    60,    17,    18,    19,
    20,    21,    22,    23,    24,    25,    26,    27,    31,    52,
    54,    55,     6,     7,    52,    54,    36,    54,    12,    13,
    14,    41,    42,    43,    52,     3,    31,    47,    48,    54,
     8,     9,    52,    11,    54,    55,    56,    57,    52,    52,
     1,    61,     3,     4,     5,     6,     7,     8,     9,    31,
    11,    12,    13,    14,    15,    54,    17,    18,    19,    20,
    21,    22,    23,    24,    25,    26,    27,    54,     9,    47,
    48,    17,    53,    53,    52,    36,    53,    53,    56,    57,
    41,    42,    43,    52,    54,    16,    47,    48,    54,    53,
    53,    52,    54,    54,    54,    56,    57,     0,     3,     1,
    61,     3,     4,     0,   297,   199,     8,     9,   207,    11,
   224,    86,   144,    15,    81,    17,    18,    19,    20,    21,
    22,    23,    24,    25,    26,    27,   168,     4,     5,     6,
     7,   325,   173,   137,    36,    12,    13,    14,   165,    41,
    42,    43,    -1,    -1,    -1,    47,    48,    -1,    -1,    -1,
    52,    -1,    54,    55,    56,    57,    -1,    -1,     1,    61,
     3,     4,    -1,    -1,    -1,     8,     9,    -1,    11,    -1,
    -1,    -1,    15,    -1,    17,    18,    19,    20,    21,    22,
    23,    24,    25,    26,    27,     3,     4,    -1,     6,     7,
    -1,    -1,    -1,    36,    12,    13,    14,    -1,    41,    42,
    43,    -1,    -1,    -1,    47,    48,    -1,    -1,    -1,    52,
    -1,    54,    55,    56,    57,    -1,     1,    -1,    61,     4,
     5,     6,     7,    -1,    -1,    43,    -1,    12,    13,    14,
   326,    -1,    -1,    -1,    52,    -1,   332,    -1,    -1,    -1,
   336,    59,    -1,     4,    29,     6,     7,   343,    -1,    10,
   346,    12,    13,    14,    41,    42,    43,    44,    45,   355,
    -1,    -1,     4,     5,     6,     7,   362,    -1,    -1,    54,
    12,    13,    14,    58,    -1,   371,    61,    -1,   374,    -1,
   376,    -1,    -1,     3,     4,     5,     6,     7,     8,     9,
   386,    11,    12,    13,    14,    15,    -1,    17,    18,    19,
    20,    21,    22,    23,    24,    25,    26,    27,    -1,    -1,
    -1,    -1,    54,    -1,    -1,    -1,    36,     5,     6,     7,
    -1,    41,    42,    43,    12,    13,    14,    47,    48,    -1,
    -1,    -1,    52,    -1,    54,    55,    56,    57,     3,     4,
    -1,    61,    -1,     8,     9,    -1,    11,    -1,    -1,    -1,
    15,    -1,    17,    18,    19,    20,    21,    22,    23,    24,
    25,    26,    27,    -1,     3,    -1,    54,    -1,    -1,     8,
     9,    36,    11,    -1,    -1,    -1,    41,    42,    43,    -1,
    -1,    -1,    47,    48,    -1,    -1,    -1,    52,    -1,    54,
    -1,    56,    57,     3,    -1,    -1,    61,    36,     8,     9,
    -1,    11,    41,    42,    43,    -1,    -1,    -1,    47,    48,
    -1,    -1,    -1,    52,    -1,     3,    55,    56,    57,    -1,
     8,     9,    61,    11,    -1,    -1,    36,    -1,     6,     7,
    -1,    41,    42,    43,    12,    13,    14,    47,    48,    -1,
    -1,    -1,    52,    -1,     3,    -1,    56,    57,    36,     8,
     9,    61,    11,    41,    42,    43,    -1,    -1,    -1,    47,
    48,    -1,    -1,    -1,    52,    43,    -1,    -1,    56,    57,
     3,    -1,    60,    -1,    52,     8,     9,    36,    11,    -1,
    -1,    59,    41,    42,    43,    -1,    -1,    -1,    47,    48,
    -1,    -1,    -1,    52,    -1,    -1,    -1,    56,    57,     3,
    -1,    60,    -1,    36,     8,     9,    -1,    11,    41,    42,
    43,    -1,    -1,    -1,    47,    48,    -1,    -1,    -1,    52,
    -1,    -1,    -1,    56,    57,    -1,     4,    60,     6,     7,
    -1,    -1,    36,    -1,    12,    13,    14,    41,    42,    43,
    -1,    -1,    -1,    47,    48,    -1,    -1,    -1,    52,    -1,
    -1,    -1,    56,    57,    -1,     1,    60,     3,     4,    -1,
     6,     7,     8,     9,    -1,    11,    12,    13,    14,     4,
    -1,     6,     7,    -1,    -1,    -1,     3,    12,    13,    14,
    -1,     8,     9,    -1,    11,    -1,    -1,    -1,    -1,    -1,
    36,    -1,    -1,    -1,    -1,    41,    42,    43,    -1,     3,
    -1,    47,    48,    -1,     8,     9,    52,    11,    43,    36,
    56,    57,    -1,    -1,    41,    42,    43,    52,    -1,    -1,
    47,    48,    -1,    -1,    59,    52,    -1,    54,     3,    56,
    57,    -1,    36,     8,     9,    -1,    11,    41,    42,    43,
    -1,    -1,    -1,    47,    48,    -1,    -1,    -1,    52,    -1,
    -1,     1,    56,    57,     4,    -1,     6,     7,    -1,    -1,
    -1,    36,    12,    13,    14,    -1,    41,    42,    43,    -1,
    -1,    -1,    47,    48,    -1,    -1,    -1,    52,    -1,    -1,
    -1,    56,    57,     1,    -1,     3,     4,     5,     6,     7,
     4,     5,     6,     7,    12,    13,    14,    -1,    12,    13,
    14,    -1,    -1,    -1,    54,    55,    -1,    -1,     1,    27,
     3,     4,    -1,     6,     7,     5,     6,     7,    -1,    12,
    13,    14,    12,    13,    14,    43,     1,    -1,     3,     4,
    -1,     6,     7,    -1,    52,    -1,    54,    12,    13,    14,
    54,    28,    29,    30,    -1,    32,    33,    34,    35,    36,
    37,    38,    39,    40,    41,    42,    43,    44,    45,    -1,
    53,    -1,    -1,    -1,    54,    35,    36,    37,    38,    39,
    40,    41,    42,    43,    44,    45,    -1,    30,    53,    32,
    33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
    43,    44,    45,    33,    34,    35,    36,    37,    38,    39,
    40,    41,    42,    43,    44,    45,    36,    37,    38,    39,
    40,    41,    42,    43,    44,    45,    37,    38,    39,    40,
    41,    42,    43,    44,    45,    38,    39,    40,    41,    42,
    43,    44,    45,    39,    40,    41,    42,    43,    44,    45
};
#define YYPURE 1

#line 2 "bison.simple"

/* Skeleton output parser for bison,
   copyright (C) 1984 Bob Corbett and Richard Stallman

   Permission is granted to anyone to make or distribute verbatim copies of this program
   provided that the copyright notice and this permission notice are preserved;
   and provided that the recipient is not asked to waive or limit his right to
   redistribute copies as permitted by this permission notice;
   and provided that anyone possessing an executable copy
   is granted access to copy the source code, in machine-readable form,
   in some reasonable manner.

   Permission is granted to distribute derived works or enhanced versions of
   this program under the above conditions with the additional condition
   that the entire derivative or enhanced work
   must be covered by a permission notice identical to this one.

   Anything distributed as part of a package containing portions derived
   from this program, which cannot in current practice perform its function usefully
   in the absense of what was derived directly from this program,
   is to be considered as forming, together with the latter,
   a single work derived from this program,
   which must be entirely covered by a permission notice identical to this one
   in order for distribution of the package to be permitted.

 In other words, you are welcome to use, share and improve this program.
 You are forbidden to forbid anyone else to use, share and improve
 what you give them.   Help stamp out software-hoarding!  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYFAIL		goto yyerrlab;

#define YYTERROR	1

#ifndef YYIMPURE
#define YYLEX		yylex()
#endif

#ifndef YYPURE
#define YYLEX		yylex(&yylval, &yylloc)
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYIMPURE

int	yychar;			/*  the lookahead symbol		*/
YYSTYPE	yylval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

YYLTYPE yylloc;			/*  location data for the lookahead	*/
				/*  symbol				*/

int yydebug = 0;		/*  nonzero means print parse trace	*/

#endif  /* YYIMPURE */


/*  YYMAXDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYMAXDEPTH
#define YYMAXDEPTH 200
#endif

/*  YYMAXLIMIT is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#ifndef YYMAXLIMIT
#define YYMAXLIMIT 10000
#endif


#line 87 "bison.simple"
int
yyparse()
{
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YYSTYPE *yyvsp;
  YYLTYPE *yylsp;
  int yyerrstatus;	/*  number of tokens to shift before error messages enabled */
  int yychar1;		/*  lookahead token as an internal (translated) token number */

  short	yyssa[YYMAXDEPTH];	/*  the state stack			*/
  YYSTYPE yyvsa[YYMAXDEPTH];	/*  the semantic value stack		*/
  YYLTYPE yylsa[YYMAXDEPTH];	/*  the location stack			*/

  short *yyss = yyssa;		/*  refer to the stacks thru separate pointers */
  YYSTYPE *yyvs = yyvsa;	/*  to allow yyoverflow to reallocate them elsewhere */
  YYLTYPE *yyls = yylsa;

  int yymaxdepth = YYMAXDEPTH;

#ifndef YYPURE

  int yychar;
  YYSTYPE yylval;
  YYLTYPE yylloc;

  extern int yydebug;

#endif


  YYSTYPE yyval;		/*  the variable used to return		*/
				/*  semantic values from the action	*/
				/*  routines				*/

  int yylen;

  if (yydebug)
    fprintf(stderr, "Starting parse\n");

  yystate = 0;
  yyerrstatus = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.  */

  yyssp = yyss - 1;
  yyvsp = yyvs;
  yylsp = yyls;

/* Push a new state, which is found in  yystate  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
yynewstate:

  *++yyssp = yystate;

  if (yyssp >= yyss + yymaxdepth - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YYSTYPE *yyvs1 = yyvs;
      YYLTYPE *yyls1 = yyls;
      short *yyss1 = yyss;

      /* Get the current used size of the three stacks, in elements.  */
      int size = yyssp - yyss + 1;

#ifdef yyoverflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yyls1, size * sizeof (*yylsp),
		 &yymaxdepth);

      yyss = yyss1; yyvs = yyvs1; yyls = yyls1;
#else /* no yyoverflow */
      /* Extend the stack our own way.  */
      if (yymaxdepth >= YYMAXLIMIT)
	yyerror("parser stack overflow");
      yymaxdepth *= 2;
      if (yymaxdepth > YYMAXLIMIT)
	yymaxdepth = YYMAXLIMIT;
      yyss = (short *) alloca (yymaxdepth * sizeof (*yyssp));
      bcopy ((char *)yyss1, (char *)yyss, size * sizeof (*yyssp));
      yyls = (YYLTYPE *) alloca (yymaxdepth * sizeof (*yylsp));
      bcopy ((char *)yyls1, (char *)yyls, size * sizeof (*yylsp));
      yyvs = (YYSTYPE *) alloca (yymaxdepth * sizeof (*yyvsp));
      bcopy ((char *)yyvs1, (char *)yyvs, size * sizeof (*yyvsp));
#endif /* no yyoverflow */

      yyssp = yyss + size - 1;
      yylsp = yyls + size - 1;
      yyvsp = yyvs + size - 1;

      if (yydebug)
	fprintf(stderr, "Stack size increased to %d\n", yymaxdepth);

      if (yyssp >= yyss + yymaxdepth - 1)
	YYERROR;
    }

  if (yydebug)
    fprintf(stderr, "Entering state %d\n", yystate);

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
yyresume:

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

      if (yydebug)
	fprintf(stderr, "Now at end of input.\n");
    }
  else
    {
      yychar1 = YYTRANSLATE(yychar);

      if (yydebug)
	fprintf(stderr, "Parsing next token; it is %d (%s)\n", yychar, yytname[yychar1]);
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */

  if (yydebug)
    fprintf(stderr, "Shifting token %d (%s), ", yychar, yytname[yychar1]);

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
  *++yylsp = yylloc;

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yyerrstatus) yyerrstatus--;

  yystate = yyn;
  goto yynewstate;

/* Do the default action for the current state.  */
yydefault:

  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;

/* Do a reduction.  yyn is the number of a rule to reduce with.  */
yyreduce:
  yylen = yyr2[yyn];
  yyval = yyvsp[1-yylen]; /* implement default value of the action */

  if (yydebug)
    {
      if (yylen == 1)
	fprintf (stderr, "Reducing 1 value via line %d, ",
		 yyrline[yyn]);
      else
	fprintf (stderr, "Reducing %d values via line %d, ",
		 yylen, yyrline[yyn]);
    }


  switch (yyn) {

case 2:
#line 140 "parse.y"
{yyval.ttype = NULL_TREE;
    break;}
case 4:
#line 141 "parse.y"
{yyval.ttype = NULL_TREE;
    break;}
case 8:
#line 148 "parse.y"
{ assemble_asm (yyvsp[-2].ttype); ;
    break;}
case 10:
#line 154 "parse.y"
{;
    break;}
case 11:
#line 156 "parse.y"
{;
    break;}
case 12:
#line 158 "parse.y"
{ yyerror ("empty declaration"); ;
    break;}
case 13:
#line 160 "parse.y"
{ shadow_tag (yyvsp[-1].ttype); ;
    break;}
case 17:
#line 168 "parse.y"
{ if (! start_function (yyvsp[-2].ttype, yyvsp[0].ttype))
		    YYFAIL; ;
    break;}
case 18:
#line 171 "parse.y"
{ store_parm_decls (); ;
    break;}
case 19:
#line 173 "parse.y"
{ finish_function (input_filename, yylsp[0].first_line, yyvsp[0].ttype); ;
    break;}
case 20:
#line 175 "parse.y"
{ ;
    break;}
case 21:
#line 177 "parse.y"
{ if (! start_function (yyvsp[-2].ttype, yyvsp[0].ttype))
		    YYFAIL; ;
    break;}
case 22:
#line 180 "parse.y"
{ store_parm_decls (); ;
    break;}
case 23:
#line 182 "parse.y"
{ finish_function (input_filename, yylsp[0].first_line, yyvsp[0].ttype); ;
    break;}
case 24:
#line 184 "parse.y"
{ ;
    break;}
case 25:
#line 186 "parse.y"
{ if (! start_function (0, yyvsp[0].ttype))
		    YYFAIL; ;
    break;}
case 26:
#line 189 "parse.y"
{ store_parm_decls (); ;
    break;}
case 27:
#line 191 "parse.y"
{ finish_function (input_filename, yylsp[0].first_line, yyvsp[0].ttype); ;
    break;}
case 31:
#line 201 "parse.y"
{ yyval.code = ADDR_EXPR; ;
    break;}
case 32:
#line 203 "parse.y"
{ yyval.code = NEGATE_EXPR; ;
    break;}
case 33:
#line 205 "parse.y"
{ yyval.code = CONVERT_EXPR; ;
    break;}
case 34:
#line 207 "parse.y"
{ yyval.code = PREINCREMENT_EXPR; ;
    break;}
case 35:
#line 209 "parse.y"
{ yyval.code = PREDECREMENT_EXPR; ;
    break;}
case 36:
#line 211 "parse.y"
{ yyval.code = BIT_NOT_EXPR; ;
    break;}
case 37:
#line 213 "parse.y"
{ yyval.code = TRUTH_NOT_EXPR; ;
    break;}
case 38:
#line 217 "parse.y"
{ yyval.ttype = build_compound_expr(yyvsp[0].ttype); ;
    break;}
case 39:
#line 222 "parse.y"
{ yyval.ttype = NULL_TREE; ;
    break;}
case 41:
#line 228 "parse.y"
{ yyval.ttype = build_tree_list (NULL_TREE, yyvsp[0].ttype); ;
    break;}
case 42:
#line 230 "parse.y"
{ chainon (yyvsp[-2].ttype, build_tree_list (NULL_TREE, yyvsp[0].ttype)); ;
    break;}
case 44:
#line 236 "parse.y"
{ yyval.ttype = build_indirect_ref (yyvsp[0].ttype); ;
    break;}
case 45:
#line 238 "parse.y"
{ yyval.ttype = build_unary_op (yyvsp[-1].code, yyvsp[0].ttype, 0); ;
    break;}
case 46:
#line 240 "parse.y"
{ yyval.ttype = build_c_cast (groktypename(yyvsp[-2].ttype), yyvsp[0].ttype); ;
    break;}
case 47:
#line 242 "parse.y"
{ yyval.ttype = c_sizeof (TREE_TYPE (yyvsp[0].ttype)); ;
    break;}
case 48:
#line 244 "parse.y"
{ yyval.ttype = c_sizeof (groktypename(yyvsp[-1].ttype)); ;
    break;}
case 49:
#line 246 "parse.y"
{ yyval.ttype = build_binary_op (yyvsp[-1].code, yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 50:
#line 248 "parse.y"
{ yyval.ttype = build_binary_op (yyvsp[-1].code, yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 51:
#line 250 "parse.y"
{ yyval.ttype = build_binary_op (yyvsp[-1].code, yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 52:
#line 252 "parse.y"
{ yyval.ttype = build_binary_op (yyvsp[-1].code, yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 53:
#line 254 "parse.y"
{ yyval.ttype = build_binary_op (yyvsp[-1].code, yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 54:
#line 256 "parse.y"
{ yyval.ttype = build_binary_op (yyvsp[-1].code, yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 55:
#line 258 "parse.y"
{ yyval.ttype = build_binary_op (yyvsp[-1].code, yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 56:
#line 260 "parse.y"
{ yyval.ttype = build_binary_op (yyvsp[-1].code, yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 57:
#line 262 "parse.y"
{ yyval.ttype = build_binary_op (yyvsp[-1].code, yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 58:
#line 264 "parse.y"
{ yyval.ttype = build_binary_op (yyvsp[-1].code, yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 59:
#line 266 "parse.y"
{ yyval.ttype = build_binary_op (yyvsp[-1].code, yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 60:
#line 268 "parse.y"
{ yyval.ttype = build_binary_op (yyvsp[-1].code, yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 61:
#line 270 "parse.y"
{ yyval.ttype = build_binary_op (TRUTH_ANDIF_EXPR, yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 62:
#line 272 "parse.y"
{ yyval.ttype = build_binary_op (TRUTH_ORIF_EXPR, yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 63:
#line 274 "parse.y"
{ yyval.ttype = build_conditional_expr(yyvsp[-4].ttype, yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 64:
#line 276 "parse.y"
{ yyval.ttype = build_modify_expr(yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 65:
#line 278 "parse.y"
{ register tree tem 
		    = duplicate_reference (yyvsp[-2].ttype);
		  yyval.ttype = build_modify_expr(tem, build_binary_op (yyvsp[-1].code, tem, yyvsp[0].ttype)); ;
    break;}
case 66:
#line 285 "parse.y"
{ yyval.ttype = lastiddecl;
		  if (!yyval.ttype)
		    {
		      if (yychar == YYEMPTY)
			yychar = YYLEX;
		      if (yychar == '(')
			yyval.ttype = implicitly_declare(yyvsp[0].ttype);
		      else
			{
			  yyerror("variable %s used but not declared",
				  IDENTIFIER_POINTER (yyvsp[0].ttype));
			  yyval.ttype = error_mark_node;
			}
		    }
		  if (TREE_CODE (yyval.ttype) == CONST_DECL)
		    yyval.ttype = DECL_INITIAL (yyval.ttype);
		;
    break;}
case 68:
#line 304 "parse.y"
{ yyval.ttype = combine_strings (yyvsp[0].ttype); ;
    break;}
case 69:
#line 306 "parse.y"
{ yyval.ttype = yyvsp[-1].ttype; ;
    break;}
case 70:
#line 308 "parse.y"
{ yyval.ttype = error_mark_node; ;
    break;}
case 71:
#line 310 "parse.y"
{ yyval.ttype = build_function_call (yyvsp[-3].ttype, yyvsp[-1].ttype); ;
    break;}
case 72:
#line 312 "parse.y"
{ yyval.ttype = build_array_ref (yyvsp[-3].ttype, yyvsp[-1].ttype); ;
    break;}
case 73:
#line 314 "parse.y"
{ yyval.ttype = build_component_ref(yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 74:
#line 316 "parse.y"
{ yyval.ttype = build_component_ref(build_indirect_ref (yyvsp[-2].ttype), yyvsp[0].ttype); ;
    break;}
case 75:
#line 318 "parse.y"
{ yyval.ttype = build_unary_op (POSTINCREMENT_EXPR, yyvsp[-1].ttype, 0); ;
    break;}
case 76:
#line 320 "parse.y"
{ yyval.ttype = build_unary_op (POSTDECREMENT_EXPR, yyvsp[-1].ttype, 0); ;
    break;}
case 78:
#line 327 "parse.y"
{ yyval.ttype = chainon (yyvsp[-1].ttype, yyvsp[0].ttype); ;
    break;}
case 85:
#line 345 "parse.y"
{ current_declspecs = yyvsp[0].ttype; ;
    break;}
case 86:
#line 350 "parse.y"
{;
    break;}
case 87:
#line 352 "parse.y"
{;
    break;}
case 88:
#line 354 "parse.y"
{ shadow_tag (yyvsp[-1].ttype); ;
    break;}
case 89:
#line 356 "parse.y"
{ warning ("empty declaration"); ;
    break;}
case 90:
#line 363 "parse.y"
{ yyval.ttype = build_tree_list (NULL_TREE, yyvsp[0].ttype); ;
    break;}
case 91:
#line 365 "parse.y"
{ yyval.ttype = tree_cons (NULL_TREE, yyvsp[0].ttype, yyvsp[-1].ttype); ;
    break;}
case 92:
#line 367 "parse.y"
{ yyval.ttype = tree_cons (NULL_TREE, yyvsp[0].ttype, yyvsp[-1].ttype); ;
    break;}
case 93:
#line 369 "parse.y"
{ yyval.ttype = tree_cons (NULL_TREE, yyvsp[0].ttype, yyvsp[-1].ttype); ;
    break;}
case 94:
#line 371 "parse.y"
{ yyval.ttype = tree_cons (NULL_TREE, yyvsp[0].ttype, yyvsp[-1].ttype); ;
    break;}
case 95:
#line 373 "parse.y"
{ yyval.ttype = tree_cons (NULL_TREE, yyvsp[0].ttype, yyvsp[-1].ttype); ;
    break;}
case 96:
#line 379 "parse.y"
{ yyval.ttype = build_tree_list (NULL_TREE, yyvsp[0].ttype); ;
    break;}
case 97:
#line 381 "parse.y"
{ yyval.ttype = tree_cons (NULL_TREE, yyvsp[0].ttype, yyvsp[-1].ttype); ;
    break;}
case 98:
#line 388 "parse.y"
{ yyval.ttype = build_tree_list (NULL_TREE, yyvsp[0].ttype); ;
    break;}
case 99:
#line 390 "parse.y"
{ yyval.ttype = tree_cons (NULL_TREE, yyvsp[0].ttype, yyvsp[-1].ttype); ;
    break;}
case 100:
#line 392 "parse.y"
{ yyval.ttype = tree_cons (NULL_TREE, yyvsp[0].ttype, yyvsp[-1].ttype); ;
    break;}
case 101:
#line 394 "parse.y"
{ yyval.ttype = tree_cons (NULL_TREE, yyvsp[0].ttype, yyvsp[-1].ttype); ;
    break;}
case 110:
#line 415 "parse.y"
{ yyval.ttype = start_decl (yyvsp[-1].ttype, current_declspecs, 1); ;
    break;}
case 111:
#line 418 "parse.y"
{ finish_decl (input_filename, yylsp[-3].first_line, yyvsp[-1].ttype, yyvsp[0].ttype); ;
    break;}
case 112:
#line 420 "parse.y"
{ tree d = start_decl (yyvsp[0].ttype, current_declspecs, 0);
		  finish_decl (input_filename, yylsp[0].first_line, d, NULL_TREE); ;
    break;}
case 113:
#line 426 "parse.y"
{ yyval.ttype = start_decl (yyvsp[-1].ttype, current_declspecs, 1); ;
    break;}
case 114:
#line 429 "parse.y"
{ finish_decl (input_filename, yylsp[-3].first_line, yyvsp[-1].ttype, yyvsp[0].ttype); ;
    break;}
case 115:
#line 431 "parse.y"
{ tree d = start_decl (yyvsp[0].ttype, current_declspecs, 0);
		  finish_decl (input_filename, yylsp[0].first_line, d, NULL_TREE); ;
    break;}
case 117:
#line 438 "parse.y"
{ yyval.ttype = build1 (CONSTRUCTOR, nreverse (yyvsp[-1].ttype)); ;
    break;}
case 118:
#line 440 "parse.y"
{ yyval.ttype = build1 (CONSTRUCTOR, nreverse (yyvsp[-2].ttype)); ;
    break;}
case 119:
#line 447 "parse.y"
{ yyval.ttype = build_tree_list (NULL_TREE, yyvsp[0].ttype); ;
    break;}
case 120:
#line 449 "parse.y"
{ yyval.ttype = tree_cons (NULL, yyvsp[0].ttype, yyvsp[-2].ttype); ;
    break;}
case 123:
#line 464 "parse.y"
{ yyval.ttype = build2 (CALL_EXPR, yyvsp[-3].ttype, yyvsp[-1].ttype); ;
    break;}
case 124:
#line 466 "parse.y"
{ yyval.ttype = build2 (CALL_EXPR, yyvsp[-3].ttype, yyvsp[-1].ttype); ;
    break;}
case 125:
#line 468 "parse.y"
{ yyval.ttype = build2 (CALL_EXPR, yyvsp[-3].ttype, NULL_TREE); ;
    break;}
case 126:
#line 470 "parse.y"
{ yyval.ttype = build2 (ARRAY_REF, yyvsp[-3].ttype, yyvsp[-1].ttype); ;
    break;}
case 127:
#line 472 "parse.y"
{ yyval.ttype = build2 (ARRAY_REF, yyvsp[-2].ttype, NULL_TREE); ;
    break;}
case 129:
#line 481 "parse.y"
{ yyval.ttype = build2 (CALL_EXPR, yyvsp[-3].ttype, yyvsp[-1].ttype); ;
    break;}
case 130:
#line 483 "parse.y"
{ yyval.ttype = build2 (CALL_EXPR, yyvsp[-3].ttype, yyvsp[-1].ttype); ;
    break;}
case 131:
#line 485 "parse.y"
{ yyval.ttype = build2 (CALL_EXPR, yyvsp[-3].ttype, NULL_TREE); ;
    break;}
case 132:
#line 487 "parse.y"
{ yyval.ttype = yyvsp[-1].ttype; ;
    break;}
case 133:
#line 489 "parse.y"
{ yyval.ttype = make_pointer_declarator (yyvsp[-1].ttype, yyvsp[0].ttype); ;
    break;}
case 134:
#line 491 "parse.y"
{ yyval.ttype = build2 (ARRAY_REF, yyvsp[-3].ttype, yyvsp[-1].ttype); ;
    break;}
case 135:
#line 493 "parse.y"
{ yyval.ttype = build2 (ARRAY_REF, yyvsp[-2].ttype, NULL_TREE); ;
    break;}
case 137:
#line 499 "parse.y"
{ yyval.ttype = build_struct (RECORD_TYPE, input_filename, yylsp[-4].first_line, yyvsp[-3].ttype, yyvsp[-1].ttype, 0); ;
    break;}
case 138:
#line 501 "parse.y"
{ yyval.ttype = build_struct (RECORD_TYPE, input_filename, yylsp[-3].first_line, NULL_TREE, yyvsp[-1].ttype, 0); ;
    break;}
case 139:
#line 503 "parse.y"
{ yyval.ttype = build_struct (RECORD_TYPE, input_filename, yylsp[-1].first_line, yyvsp[0].ttype, NULL_TREE, 1); ;
    break;}
case 140:
#line 505 "parse.y"
{ yyval.ttype = build_struct (UNION_TYPE, input_filename, yylsp[-4].first_line, yyvsp[-3].ttype, yyvsp[-1].ttype, 0); ;
    break;}
case 141:
#line 507 "parse.y"
{ yyval.ttype = build_struct (UNION_TYPE, input_filename, yylsp[-3].first_line, NULL_TREE, yyvsp[-1].ttype, 0); ;
    break;}
case 142:
#line 509 "parse.y"
{ yyval.ttype = build_struct (UNION_TYPE, input_filename, yylsp[-1].first_line, yyvsp[0].ttype, NULL_TREE, 1); ;
    break;}
case 143:
#line 511 "parse.y"
{ yyval.ttype = start_enum (yyvsp[-1].ttype); ;
    break;}
case 144:
#line 513 "parse.y"
{ yyval.ttype = finish_enum (yyvsp[-2].ttype, nreverse (yyvsp[-1].ttype)); ;
    break;}
case 145:
#line 515 "parse.y"
{ yyval.ttype = start_enum (NULL_TREE); ;
    break;}
case 146:
#line 517 "parse.y"
{ yyval.ttype = finish_enum (yyvsp[-2].ttype, nreverse (yyvsp[-1].ttype)); ;
    break;}
case 147:
#line 519 "parse.y"
{ yyval.ttype = xref_enum (yyvsp[0].ttype); ;
    break;}
case 148:
#line 523 "parse.y"
{ yyval.ttype = NULL_TREE; ;
    break;}
case 150:
#line 526 "parse.y"
{ yyval.ttype = chainon (yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 152:
#line 532 "parse.y"
{ yyval.ttype = yyvsp[0].ttype; ;
    break;}
case 153:
#line 534 "parse.y"
{ yyval.ttype == NULL_TREE; ;
    break;}
case 154:
#line 539 "parse.y"
{ yyval.ttype = NULL_TREE; ;
    break;}
case 156:
#line 542 "parse.y"
{ yyval.ttype = chainon (yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 157:
#line 547 "parse.y"
{ yyval.ttype = grokfield (input_filename, yylsp[0].first_line, yyvsp[0].ttype, current_declspecs, NULL_TREE); ;
    break;}
case 158:
#line 549 "parse.y"
{ yyval.ttype = grokfield (input_filename, yylsp[-2].first_line, yyvsp[-2].ttype, current_declspecs, yyvsp[0].ttype); ;
    break;}
case 159:
#line 551 "parse.y"
{ yyval.ttype = grokfield (input_filename, yylsp[-1].first_line, NULL_TREE, current_declspecs, yyvsp[0].ttype); ;
    break;}
case 161:
#line 562 "parse.y"
{ yyval.ttype = chainon (yyvsp[0].ttype, yyvsp[-2].ttype); ;
    break;}
case 163:
#line 569 "parse.y"
{ yyval.ttype = build_enumerator (yyvsp[0].ttype, NULL_TREE); ;
    break;}
case 164:
#line 571 "parse.y"
{ yyval.ttype = build_enumerator (yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 165:
#line 576 "parse.y"
{ yyval.ttype = build_tree_list (yyvsp[-1].ttype, yyvsp[0].ttype); ;
    break;}
case 166:
#line 581 "parse.y"
{ yyval.ttype = NULL_TREE; ;
    break;}
case 168:
#line 587 "parse.y"
{ yyval.ttype = NULL_TREE; ;
    break;}
case 169:
#line 589 "parse.y"
{ yyval.ttype = tree_cons (NULL_TREE, yyvsp[0].ttype, yyvsp[-1].ttype); ;
    break;}
case 170:
#line 594 "parse.y"
{ yyval.ttype = yyvsp[-1].ttype; ;
    break;}
case 171:
#line 596 "parse.y"
{ yyval.ttype = make_pointer_declarator (yyvsp[-1].ttype, yyvsp[0].ttype); ;
    break;}
case 172:
#line 598 "parse.y"
{ yyval.ttype = make_pointer_declarator (yyvsp[0].ttype, NULL_TREE); ;
    break;}
case 173:
#line 600 "parse.y"
{ yyval.ttype = build2 (CALL_EXPR, yyvsp[-3].ttype, yyvsp[-1].ttype); ;
    break;}
case 174:
#line 602 "parse.y"
{ yyval.ttype = build2 (ARRAY_REF, yyvsp[-3].ttype, yyvsp[-1].ttype); ;
    break;}
case 175:
#line 604 "parse.y"
{ yyval.ttype = build2 (ARRAY_REF, yyvsp[-2].ttype, NULL_TREE); ;
    break;}
case 176:
#line 606 "parse.y"
{ yyval.ttype = build2 (CALL_EXPR, NULL_TREE, yyvsp[-1].ttype); ;
    break;}
case 177:
#line 608 "parse.y"
{ yyval.ttype = build2 (ARRAY_REF, NULL_TREE, yyvsp[-1].ttype); ;
    break;}
case 178:
#line 610 "parse.y"
{ yyval.ttype = build2 (ARRAY_REF, NULL_TREE, NULL_TREE); ;
    break;}
case 180:
#line 623 "parse.y"
{ yyval.ttype = chainon (yyvsp[0].ttype, yyvsp[-1].ttype); ;
    break;}
case 183:
#line 635 "parse.y"
{ pushlevel();
		  yyval.ttype = current_block;
		  current_block
		    = build_let (input_filename, 0, 0, 0, yyval.ttype, 0);
		;
    break;}
case 184:
#line 643 "parse.y"
{ yyval.ttype = build_compound (input_filename, yylsp[-1].first_line, 0); ;
    break;}
case 185:
#line 645 "parse.y"
{ yyval.ttype = finish_compound_stmt (current_block, nreverse (yyvsp[-1].ttype),
					     yyvsp[-3].ttype, yylsp[-4].first_line); ;
    break;}
case 186:
#line 648 "parse.y"
{ yyval.ttype = finish_compound_stmt (current_block, NULL_TREE,
					     yyvsp[-2].ttype, yylsp[-3].first_line); ;
    break;}
case 187:
#line 651 "parse.y"
{ yyval.ttype = error_mark_node;
		  current_block = yyvsp[-2].ttype;
		  poplevel(); ;
    break;}
case 188:
#line 655 "parse.y"
{ yyval.ttype = finish_compound_stmt (current_block, nreverse (yyvsp[-1].ttype), yyvsp[-2].ttype,
					     yylsp[-3].first_line); ;
    break;}
case 190:
#line 661 "parse.y"
{ yyval.ttype = build_expr_stmt (input_filename, yylsp[-1].first_line, yyvsp[-1].ttype); ;
    break;}
case 191:
#line 663 "parse.y"
{ yyval.ttype = build_if (input_filename, yylsp[-4].first_line, default_conversion (yyvsp[-2].ttype), yyvsp[0].ttype, 0); ;
    break;}
case 192:
#line 665 "parse.y"
{ yyval.ttype = build_if (input_filename, yylsp[-6].first_line, default_conversion (yyvsp[-4].ttype), yyvsp[-2].ttype, yyvsp[0].ttype); ;
    break;}
case 193:
#line 667 "parse.y"
{ pushbreak(1); ;
    break;}
case 194:
#line 669 "parse.y"
{ yyval.ttype = build_loop (input_filename, yylsp[-5].first_line,
			 chainon (build_exit (input_filename, yylsp[-2].first_line,
					      default_conversion (yyvsp[-2].ttype)),
				  chainon (yyvsp[0].ttype, current_continue_label)));
		  yyval.ttype = build_compound (input_filename, yylsp[-5].first_line, chainon (yyval.ttype, current_break_label));
		  popbreak(1); ;
    break;}
case 195:
#line 676 "parse.y"
{ pushbreak(1); ;
    break;}
case 196:
#line 678 "parse.y"
{ yyval.ttype = build_loop (input_filename, yylsp[-7].first_line,
			 chainon (yyvsp[-5].ttype, chainon(current_continue_label,
					      build_exit (input_filename, yylsp[-2].first_line,
							  default_conversion (yyvsp[-2].ttype)))));
		  yyval.ttype = build_compound (input_filename, yylsp[-7].first_line, chainon (yyval.ttype, current_break_label));
		  popbreak(1); ;
    break;}
case 197:
#line 685 "parse.y"
{ pushbreak(1); ;
    break;}
case 198:
#line 687 "parse.y"
{ yyval.ttype = build_compound (input_filename, yylsp[-9].first_line,
			 chainon (yyvsp[-6].ttype ? build_expr_stmt (input_filename, yylsp[-6].first_line, yyvsp[-6].ttype) : NULL_TREE,
			   build_loop (input_filename, yylsp[-9].first_line,
			     chainon (yyvsp[-4].ttype ? build_exit (input_filename, yylsp[-4].first_line,
						       default_conversion (yyvsp[-4].ttype))
					 : NULL_TREE,
			       chainon (chainon (yyvsp[0].ttype, current_continue_label),
				        yyvsp[-2].ttype ? build_expr_stmt (input_filename, yylsp[-2].first_line, yyvsp[-2].ttype) : NULL_TREE)))));
		  yyval.ttype = build_compound (input_filename, yylsp[-9].first_line, chainon (yyval.ttype, current_break_label));
		  popbreak(1); ;
    break;}
case 199:
#line 698 "parse.y"
{ yyval.ttype = current_switch_stmt;
		  pushbreak(0);
		  current_switch_stmt
		    = build_switch_stmt (input_filename, yylsp[-3].first_line,
					 default_conversion (yyvsp[-1].ttype)); ;
    break;}
case 200:
#line 704 "parse.y"
{ yyval.ttype = build_compound (input_filename, yylsp[-5].first_line, 
				chainon(current_switch_stmt,
					chainon(yyvsp[0].ttype, current_break_label)));
		  finish_switch_stmt (current_switch_stmt, current_break_label);
		  popbreak (0);
		  current_switch_stmt = yyvsp[-1].ttype; ;
    break;}
case 201:
#line 711 "parse.y"
{ register tree value = fold(yyvsp[-2].ttype);
		  tree l = build_label (input_filename, yylsp[-3].first_line, NULL_TREE, current_block);

		  if (TREE_CODE (value) != INTEGER_CST)
		    {
		      yyerror("case label does not reduce to an integer constant");
		      value = error_mark_node;
		    }
		  pushcase(value, l);
		  yyval.ttype = build_compound (input_filename, yylsp[-3].first_line, chainon(l, yyvsp[0].ttype));
		;
    break;}
case 202:
#line 723 "parse.y"
{
		  tree l = build_label (input_filename, yylsp[-2].first_line, 0, current_block);
		  pushcase(NULL_TREE, l);
		  yyval.ttype = build_compound (input_filename, yylsp[-2].first_line, chainon(l, yyvsp[0].ttype));
		;
    break;}
case 203:
#line 729 "parse.y"
{ if (current_break_label)
		    yyval.ttype = build_goto (input_filename, yylsp[-1].first_line, STMT_BODY (current_break_label));
		  else
		    {
		      yyerror("break statement not within a do, for, while or switch statement");
		      yyval.ttype = error_mark_node;
		    }
		;
    break;}
case 204:
#line 738 "parse.y"
{ if (current_continue_label)
		    yyval.ttype = build_goto (input_filename, yylsp[-1].first_line, STMT_BODY (current_continue_label));
		  else
		    {
		      yyerror("continue statement not within a do, for or while statement");
		      yyval.ttype = error_mark_node;
		    }
		;
    break;}
case 205:
#line 747 "parse.y"
{ yyval.ttype = build_return (input_filename, yylsp[-1].first_line, NULL_TREE); ;
    break;}
case 206:
#line 749 "parse.y"
{ yyval.ttype = build_return_stmt (input_filename, yylsp[-2].first_line, yyvsp[-1].ttype); ;
    break;}
case 207:
#line 751 "parse.y"
{ pushgoto(yyval.ttype = build_goto (input_filename, yylsp[-2].first_line, yyvsp[-1].ttype)); ;
    break;}
case 208:
#line 753 "parse.y"
{ yyval.ttype = build_asm_stmt (input_filename, yylsp[-4].first_line, yyvsp[-2].ttype); ;
    break;}
case 209:
#line 755 "parse.y"
{ yyval.ttype = build_compound (input_filename, yylsp[-2].first_line, chainon (build_label (input_filename, yylsp[-2].first_line, yyvsp[-2].ttype, current_block), yyvsp[0].ttype)); ;
    break;}
case 210:
#line 757 "parse.y"
{ yyval.ttype = build_compound (input_filename, yylsp[0].first_line, 0); ;
    break;}
case 211:
#line 762 "parse.y"
{ yyval.ttype = NULL_TREE; ;
    break;}
case 213:
#line 769 "parse.y"
{ yyval.ttype = NULL_TREE; ;
    break;}
case 214:
#line 771 "parse.y"
{ yyval.ttype = chainon (yyvsp[0].ttype, build_tree_list (NULL_TREE,
						     void_type_node)); ;
    break;}
case 216:
#line 779 "parse.y"
{ yyval.ttype = build_tree_list (NULL_TREE, yyvsp[0].ttype); ;
    break;}
case 217:
#line 781 "parse.y"
{ yyval.ttype = chainon (yyvsp[-2].ttype, build_tree_list (NULL_TREE, yyvsp[0].ttype)); ;
    break;}
case 218:
#line 786 "parse.y"
{ yyval.ttype = build_tree_list (yyvsp[-1].ttype, yyvsp[0].ttype)	; ;
    break;}
case 219:
#line 788 "parse.y"
{ yyval.ttype = build_tree_list (yyvsp[-1].ttype, yyvsp[0].ttype); ;
    break;}
case 220:
#line 794 "parse.y"
{ yyval.ttype = build_tree_list (NULL_TREE, yyvsp[0].ttype); ;
    break;}
case 221:
#line 796 "parse.y"
{ yyval.ttype = chainon (yyvsp[-2].ttype, build_tree_list (NULL_TREE, yyvsp[0].ttype)); ;
    break;}
}
   /* the action file gets copied in in place of this dollarsign */
#line 303 "bison.simple"

  yyvsp -= yylen;
  yylsp -= yylen;
  yyssp -= yylen;

  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "state stack now", yyssp-yyss);
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }

  *++yyvsp = yyval;

  yylsp++;
  if (yylen == 0)
    {
      yylsp->first_line = yylloc.first_line;
      yylsp->first_column = yylloc.first_column;
      yylsp->last_line = (yylsp-1)->last_line;
      yylsp->last_column = (yylsp-1)->last_column;
      yylsp->text = 0;
    }
  else
    {
      yylsp->last_line = (yylsp+yylen-1)->last_line;
      yylsp->last_column = (yylsp+yylen-1)->last_column;
    }

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;

yyerrlab:   /* here on detecting error */

  if (! yyerrstatus)
    /* If not already recovering from an error, report this error.  */
    {
      yyerror("parse error");
    }

  if (yyerrstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYERROR;

      if (yydebug)
	fprintf(stderr, "Discarding token %d (%s).\n", yychar, yytname[yychar1]);

      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;

yyerrdefault:  /* current state does not do anything special for the error token. */

  yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yyn) goto yydefault;

yyerrpop:   /* pop the current state because it cannot handle the error token */

  if (yyssp == yyss) YYERROR;
  yyvsp--;
  yylsp--;
  yystate = *--yyssp;

  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "Error: state stack now", yyssp-yyss);
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }

yyerrhandle:

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

  if (yydebug)
    fprintf(stderr, "Shifting error token, ");

  *++yyvsp = yylval;
  *++yylsp = yylloc;

  yystate = yyn;
  goto yynewstate;
}
#line 798 "parse.y"


static tree
finish_compound_stmt(block, stmts, outer_block, line)
     tree block, stmts, outer_block;
     int line;
{
  register tree decls = getdecls();
  register tree tags = gettags ();

  if (decls || tags)
    {
      finish_block (block, decls, tags, stmts);
      poplevel();
      current_block = outer_block;
      STMT_SOURCE_LINE (block) = line;
      return block;
    }
  else
    {
      current_block = outer_block;
      poplevel();
      return build_compound (input_filename, line, stmts);  
    }
}



static void
pushbreak(a)
int a;
{
  if (current_break_label)
    TREE_CHAIN (current_break_label) = break_label_stack;
  break_label_stack = current_break_label;
  current_break_label = build_label (0, 0, NULL_TREE, current_block);

  if (a)
    {
      if (current_continue_label)
	TREE_CHAIN (current_continue_label) = continue_label_stack;
      continue_label_stack = current_continue_label;
      current_continue_label = build_label (0, 0, NULL_TREE, current_block);
    }
}

static void
popbreak(a)
int a;
{
  current_break_label = break_label_stack;
  if (current_break_label)
    break_label_stack = TREE_CHAIN (break_label_stack);

  if (a)
    {
      current_continue_label = continue_label_stack;
      if (current_continue_label)
	continue_label_stack = TREE_CHAIN (continue_label_stack);
    }

  if (current_break_label)
    TREE_CHAIN (current_break_label) = NULL;
  if (current_continue_label)
    TREE_CHAIN (current_continue_label) = NULL;
}

/* Return something to represent absolute declarators containing a *.
   TARGET is the absolute declarator that the * contains.
   TYPEMODS is a list of modifiers such as const or volatile
   to apply to the pointer type, represented as identifiers.

   We return an INDIRECT_REF whose "contents" are TARGET
   and whose type is the modifier list.  */
   
static tree
make_pointer_declarator (typemods, target)
     tree typemods, target;
{
  register tree t = build1 (INDIRECT_REF, target);
  TREE_TYPE (t) = typemods;
  return t;
}

/* Given a chain of STRING_CST nodes,
   concatenate them into one STRING_CST
   and then return an ADDR_EXPR for it.  */

static tree
combine_strings (strings)
     tree strings;
{
  register tree value, t;

  if (TREE_CHAIN (strings))
    {
      /* More than one in the chain, so concatenate.  */
      register char *p, *q;
      register int length = 1;

      /* Don't include the \0 at the end of each substring,
	 except for the last one.  */
      for (t = strings; t; t = TREE_CHAIN (t))
	length += TREE_STRING_LENGTH (t) - 1;

      p = (char *) oballoc (length);

      q = p;
      for (t = strings; t; t = TREE_CHAIN (t))
	{
	  bcopy (TREE_STRING_POINTER (t), q, TREE_STRING_LENGTH (t) - 1);
	  q += TREE_STRING_LENGTH (t) - 1;
	}
      *q = 0;

      value = make_node (STRING_CST);
      TREE_TYPE (value) = TREE_TYPE (strings);
      TREE_STRING_POINTER (value) = p;
      TREE_STRING_LENGTH (value) = length;
      TREE_LITERAL (value) = 1;
    }
  else
    value = strings;	  

  value = build1 (ADDR_EXPR, value);
  TREE_TYPE (value) = string_type_node;
  TREE_LITERAL (value) = 1;
  return value;
}

int lineno;			/* current line number in file being read */

FILE *finput;			/* input file.
				   Normally a pipe from the preprocessor.  */

/* lexical analyzer */

static int maxtoken;		/* Current length of token buffer */
static char *token_buffer;	/* Pointer to token buffer */

/* frw[i] is index in rw of the first word whose length is i. */

#define MAXRESERVED 9

static char frw[10] =
  { 0, 0, 0, 2, 5, 13, 19, 27, 29, 33 };

static char *rw[] =
  { "if", "do", "int", "for", "asm",
    "case", "char", "auto", "goto", "else", "long", "void", "enum",
    "float", "short", "union", "break", "while", "const",
    "double", "static", "extern", "struct", "return", "sizeof", "switch", "signed",
    "typedef", "default",
    "unsigned", "continue", "register", "volatile" };

static short rtoken[] =
  { IF, DO, TYPESPEC, FOR, ASM,
    CASE, TYPESPEC, SCSPEC, GOTO, ELSE, TYPESPEC, TYPESPEC, ENUM,
    TYPESPEC, TYPESPEC, UNION, BREAK, WHILE, TYPEMOD,
    TYPESPEC, SCSPEC, SCSPEC, STRUCT, RETURN, SIZEOF, SWITCH, TYPESPEC,
    SCSPEC, DEFAULT,
    TYPESPEC, CONTINUE, SCSPEC, TYPEMOD };

/* This table corresponds to rw and rtoken.
   Its element is an index in ridpointers  */

#define NORID (enum rid) 0

static enum rid rid[] =
  { NORID, NORID, RID_INT, NORID, NORID,
    NORID, RID_CHAR, RID_AUTO, NORID, NORID, RID_LONG, RID_VOID, NORID,
    RID_FLOAT, RID_SHORT, NORID, NORID, NORID, RID_CONST,
    RID_DOUBLE, RID_STATIC, RID_EXTERN, NORID, NORID, NORID, NORID, RID_SIGNED,
    RID_TYPEDEF, NORID,
    RID_UNSIGNED, NORID, RID_REGISTER, RID_VOLATILE };

/* The elements of `ridpointers' are identifier nodes
   for the reserved type names and storage classes.  */

tree ridpointers[(int) RID_MAX];

static tree line_identifier;   /* The identifier node named "line" */

void check_newline();

void
init_lex()
{
  extern char *malloc();

  /* Start it at 0, because check_newline is called atthe very beginning
     and will increment it to 1.  */
  lineno = 0;
  current_function_decl = NULL;
  current_switch_stmt = NULL;
  current_block = NULL;
  current_break_label = NULL;
  current_continue_label = NULL;
  break_label_stack = NULL;
  continue_label_stack = NULL;
  line_identifier = get_identifier("line");

  maxtoken = 40;
  token_buffer = malloc((unsigned)(maxtoken+1));
  ridpointers[(int) RID_INT] = get_identifier("int");
  ridpointers[(int) RID_CHAR] = get_identifier("char");
  ridpointers[(int) RID_VOID] = get_identifier("void");
  ridpointers[(int) RID_FLOAT] = get_identifier("float");
  ridpointers[(int) RID_DOUBLE] = get_identifier("double");
  ridpointers[(int) RID_SHORT] = get_identifier("short");
  ridpointers[(int) RID_LONG] = get_identifier("long");
  ridpointers[(int) RID_UNSIGNED] = get_identifier("unsigned");
  ridpointers[(int) RID_SIGNED] = get_identifier("signed");
  ridpointers[(int) RID_CONST] = get_identifier("const");
  ridpointers[(int) RID_VOLATILE] = get_identifier("volatile");
  ridpointers[(int) RID_AUTO] = get_identifier("auto");
  ridpointers[(int) RID_STATIC] = get_identifier("static");
  ridpointers[(int) RID_EXTERN] = get_identifier("extern");
  ridpointers[(int) RID_TYPEDEF] = get_identifier("typedef");
  ridpointers[(int) RID_REGISTER] = get_identifier("register");
}

static int
skip_white_space()
{
  register int c;
  register int inside;

  c = getc(finput);

  for (;;)
    {
      switch (c)
	{
	case '/':
	  c = getc(finput);
	  if (c != '*')
	    {
	      ungetc(c, finput);
	      return '/';
	    }

	  c = getc(finput);

	  inside = 1;
	  while (inside)
	    {
	      if (c == '*')
		{
		  while (c == '*')
		    c = getc(finput);

		  if (c == '/')
		    {
		      inside = 0;
		      c = getc(finput);
		    }
		}
	      else if (c == '\n')
		{
		  lineno++;
		  c = getc(finput);
		}
	      else if (c == EOF)
		yyerror("unterminated comment");
	      else
		c = getc(finput);
	    }

	  break;

	case '\n':
	  check_newline();

	case ' ':
	case '\t':
	case '\f':
	case '\r':
	case '\b':
	  c = getc(finput);
	  break;

	case '\\':
	  c = getc(finput);
	  if (c == '\n')
	    lineno++;
	  else
	    yyerror("stray '\\' in program");
	  c = getc(finput);
	  break;

	default:
	  return (c);
	}
    }
}



/* make the token buffer longer, preserving the data in it.
p should point to just beyond the last valid character in the old buffer
and the value points to the corresponding place in the new one.  */

static char *
extend_token_buffer(p)
char *p;
{
  register char *newbuf;
  register char *value;
  int newlength = maxtoken * 2 + 10;
  register char *p2, *p1;
  extern char *malloc();

  newbuf = malloc((unsigned)(newlength+1));

  p2 = newbuf;
  p1 = newbuf + newlength + 1;
  while (p1 != p2) *p2++ = 0;

  value = newbuf;
  p2 = token_buffer;
  while (p2 != p)
   *value++ = *p2++;

  token_buffer = newbuf;

  maxtoken = newlength;

  return (value);
}



/* At the beginning of a line,
   increment the line number
   and handle a #line directive immediately following  */

void
check_newline ()
{
  register int c;
  register int token;

  while (1)
    {
      c = getc (finput);
      lineno++;

      if (c != '#')
	{
	  /* If no #, unread the character,
	     except don't bother if it is whitespace.  */
	  if (c == ' ' || c == '\t')
	    return;
	  ungetc (c, finput);
	  return;
	}

      /* Skip whitespace after the #.  */

      while (1)
	{
	  c = getc (finput);
	  if (! (c == ' ' || c == '\t'))
	    break;
	}

      /* If the # is the only nonwhite char on the line,
	 just ignore it.  Check the new newline.  */
      if (c == '\n')
	continue;

      /* Something follows the #; read a token.  */

      ungetc (c, finput);
      token = yylex ();

      if (token == CONSTANT
	  && TREE_CODE (yylval.ttype) == INTEGER_CST)
	{
	  /* subtract one, because it is the following line that
	     gets the specified number */

	  int l = TREE_INT_CST_LOW (yylval.ttype) - 1;

	  token = yylex ();
	  if (token != STRING || TREE_CODE (yylval.ttype) != STRING_CST)
	    yyerror ("invalid #line");

	  input_filename
	    = (char *) permalloc (TREE_STRING_LENGTH (yylval.ttype) + 1);
	  strcpy (input_filename, TREE_STRING_POINTER (yylval.ttype));
	  lineno = l;
	}
      else
	yyerror ("undefined or invalid # directive");

      /* skip the rest of this line.  */
      while ((c = getc (finput)) != '\n');
    }
}



#define isalnum(char) ((char >= 'a' && char <= 'z') || (char >= 'A' && char <= 'Z') || (char >= '0' && char <= '9'))
#define isdigit(char) (char >= '0' && char <= '9')
#define ENDFILE -1  /* token that represents end-of-file */


static int
readescape ()
{
  register int c = getc (finput);
  register int count, code;

  switch (c)
    {
    case 'x':
      code = 0;
      count = 0;
      while (1)
	{
	  c = getc (finput);
	  if (!(c >= 'a' && c <= 'f')
	      && !(c >= 'A' && c <= 'F')
	      && !(c >= '0' && c <= '9'))
	    {
	      ungetc (c, finput);
	      break;
	    }
	  if (c >= 'a' && c <= 'z')
	    c -= 'a' - 'A';
	  code *= 16;
	  if (c >= 'a' && c <= 'f')
	    code += c - 'a' + 10;
	  if (c >= 'A' && c <= 'F')
	    code += c - 'A' + 10;
	  if (c >= '0' && c <= '9')
	    code += c - '0';
	  count++;
	  if (count == 3)
	    break;
	}
      if (count == 0)
	yyerror ("\\x used with no following hex digits");
      return code;

    case '0':  case '1':  case '2':  case '3':  case '4':
    case '5':  case '6':  case '7':
      code = 0;
      count = 0;
      while ((c <= '7') && (c >= '0') && (count++ < 3))
	{
	  code = (code * 8) + (c - '0');
	  c = getc (finput);
	}
      ungetc (c, finput);
      return code;

    case '\\': case '\'': case '"':
      return c;

    case '\n':
      lineno++;
      return -1;

    case 'n':
      return TARGET_NEWLINE;

    case 't':
      return TARGET_TAB;

    case 'r':
      return TARGET_CR;

    case 'f':
      return TARGET_FF;

    case 'b':
      return TARGET_BS;

    case 'a':
      return TARGET_BELL;

    case 'v':
      return TARGET_VT;
    }
  return c;
}


static int
yylex()
{
  register int c;
  register char *p;
  register int value;

  c = skip_white_space();

  yylloc.first_line = lineno;

  switch (c)
    {
    case EOF:
      value = ENDFILE; break;

    case 'A':  case 'B':  case 'C':  case 'D':  case 'E':
    case 'F':  case 'G':  case 'H':  case 'I':  case 'J':
    case 'K':  case 'L':  case 'M':  case 'N':  case 'O':
    case 'P':  case 'Q':  case 'R':  case 'S':  case 'T':
    case 'U':  case 'V':  case 'W':  case 'X':  case 'Y':
    case 'Z':
    case 'a':  case 'b':  case 'c':  case 'd':  case 'e':
    case 'f':  case 'g':  case 'h':  case 'i':  case 'j':
    case 'k':  case 'l':  case 'm':  case 'n':  case 'o':
    case 'p':  case 'q':  case 'r':  case 's':  case 't':
    case 'u':  case 'v':  case 'w':  case 'x':  case 'y':
    case 'z':
    case '_':
      p = token_buffer;
      while (isalnum(c) || (c == '_'))
	{
	  if (p >= token_buffer + maxtoken)
	    p = extend_token_buffer(p);
	  *p++ = c;
	  c = getc(finput);
	}

      *p = 0;
      ungetc(c, finput);

      value = IDENTIFIER;
      yylval.itype = 0;

      if (p - token_buffer <= MAXRESERVED)
	{
	  register int lim = frw [p - token_buffer + 1];
	  register int i;

	  for (i = frw[p - token_buffer]; i < lim; i++)
	    if (rw[i][0] == token_buffer[0] && !strcmp(rw[i], token_buffer))
	      {
		if (rid[i])
		  yylval.ttype = ridpointers[(int) rid[i]];
		value = (int) rtoken[i];
		break;
	      }
	}

      if (value == IDENTIFIER)
	{
          yylval.ttype = get_identifier(token_buffer);
	  lastiddecl = lookup_name (yylval.ttype);

	  if (lastiddecl != 0 && TREE_CODE (lastiddecl) == TYPE_DECL)
	    value = TYPENAME;
	}

      break;

    case '0':  case '1':  case '2':  case '3':  case '4':
    case '5':  case '6':  case '7':  case '8':  case '9':
    case '.':
      {
	int base = 10;
	int count = 0;
	/* for multi-precision arithmetic, we store only 8 live bits in each short,
	   giving us 64 bits of reliable precision */
	short shorts[8];
	char *floatflag = NULL;  /* set nonzero if we learn this is a floating constant */
				 /* in fact, it points to the first fractional digit.  */

	for (count = 0; count < 8; count++)
	  shorts[count] = 0;

	p = token_buffer;
	*p++ = c;

	if (c == '0')
	  {
	    *p++ = (c = getc(finput));
	    if ((c == 'x') || (c == 'X'))
	      {
		base = 16;
		*p++ = (c = getc(finput));
	      }
	    else
	      {
		base = 8;
	      }
	  }

	while (c == '.'
	       || (isalnum (c) && (c != 'l') && (c != 'L')
		   && (c != 'u') && (c != 'U')))
	  {
	    if (c == '.')
	      {
		floatflag = p - 1;
		p[-1] = c = getc(finput); /* omit the decimal point from
				     the token buffer.  */
		/* Accept '.' as the start of a floating-point number
		   only when it is followed by a digit.
		   Otherwise, unread the following non-digit
		   and use the '.' as a structural token.  */
		if (floatflag == token_buffer && !isdigit (c))
		  {
		    if (c == '.')
		      {
			c = getc (finput);
			if (c == '.')
			  return ELLIPSIS;
			yyerror ("syntax error");
		      }
		    ungetc (c, finput);
		    return '.';
		  }
	      }
	    else
	      {
		if (isdigit(c))
		  {
		    c = c - '0';
		  }
		else if (base <= 10)
		  {
		    if ((c&~040) == 'E')
		      {
			if (floatflag == 0)
			  floatflag = p - 1;
			break;   /* start of exponent */
		      }
		    yyerror("nondigits in number and not hexadecimal");
		    c = 0;
		  }
		else if (c >= 'a')
		  {
		    c = c - 'a' + 10;
		  }
		else
		  {
		    c = c - 'A' + 10;
		  }
		if (c >= base)
		  yyerror("numeric constant contains digits beyond the radix");
	    
		for (count = 0; count < 8; count++)
		  {
		    (shorts[count] *= base);
		    if (count)
		      {
			shorts[count] += (shorts[count-1] >> 8);
			shorts[count-1] &= (1<<8)-1;
		      }
		    else shorts[0] += c;
		  }
    
		*p++ = (c = getc(finput));
	      }
	  }

	/* Remove terminating char from the token buffer and delimit the string */
	*--p = 0;

	if (floatflag)
	  {
	    register ex = -(p - floatflag);  /* exponent is minus # digits after decimal pt */

	    tree type = double_type_node;

	    /* read explicit exponent if any, and add into ex. */

	    if ((c == 'e') || (c == 'E'))
	      {
		register int exval = 0;
		register int exsign = 1;

		c = getc(finput);
		if ((c == '+') || (c == '-'))
		  {
		    if (c == '-') exsign = -1;
		    c = getc(finput);
		  }
	        while (isdigit(c))
		  {
		    exval *= 10;
		    exval += c - '0';
		    c = getc(finput);
		  }
		ex += exsign*exval;
	      }

	    while (1)
	      {
		if (c == 'f' || c == 'F')
		  type = float_type_node;
		else if (c == 'l' || c == 'L')
		  type = long_double_type_node;
		else break;
		c = getc (finput);
	      }

	    ungetc(c, finput);

	    yylval.ttype = build_real_from_string (token_buffer, ex);
	    TREE_TYPE (yylval.ttype) = type;
	  }
	else
	  {
	    tree type;
	    int spec_unsigned = 0;
	    int spec_long = 0;

	    while (1)
	      {
		if (c == 'u' || c == 'U')
		  {
		    spec_unsigned = 1;
		    c = getc (finput);
		  }
		else if (c == 'l' || c == 'L')
		  {
		    spec_long = 1;
		    c = getc (finput);
		  }
		else break;
	      }

	    ungetc (c, finput);

	    /* This is simplified by the fact that our constant
	       is always positive.  */
	    yylval.ttype
	      = build_int_2 ((shorts[3]<<24) + (shorts[2]<<16) + (shorts[1]<<8) + shorts[0],
			     (shorts[7]<<24) + (shorts[6]<<16) + (shorts[5]<<8) + shorts[4]);
    
	    if (!spec_long && !spec_unsigned
		&& int_fits_type_p (yylval.ttype, integer_type_node))
	      type = integer_type_node;

	    else if (!spec_long && base != 10
		&& int_fits_type_p (yylval.ttype, unsigned_type_node))
	      type = unsigned_type_node;

	    else if (!spec_unsigned
		&& int_fits_type_p (yylval.ttype, long_integer_type_node))
	      type = long_integer_type_node;

	    else
	      type = long_unsigned_type_node;

	    TREE_TYPE (yylval.ttype) = type;
	  }

	value = CONSTANT; break;
      }

    case '\'':
      c = getc(finput);
      {
	register int code = 0;

      tryagain:

	if (c == '\\')
	  {
	    c = readescape ();
	    if (c < 0)
	      goto tryagain;
	  }
	code = c;
	c = getc (finput);
	if (c != '\'')
	  yyerror("malformatted character constant");

	if (char_type_node == unsigned_char_type_node
	    || (c >> (BITS_PER_UNIT - 1)) == 0)
	  yylval.ttype = build_int_2 (code, 0);
	else
	  yylval.ttype = build_int_2 (code | (1 << BITS_PER_UNIT), -1);

	TREE_TYPE (yylval.ttype) = char_type_node;
	value = CONSTANT; break;
      }

    case '"':
      {
	c = getc(finput);
	p = token_buffer;

	while (c != '"')
	  {
	    if (c == '\\')
	      {
		c = readescape ();
		if (c < 0)
		  goto skipnewline;
	      }
	    else if (c == '\n')
	      {
		lineno++;
	      }

	    if (p == token_buffer + maxtoken)
	      p = extend_token_buffer(p);
	    *p++ = c;

	  skipnewline:
	    c = getc (finput);
	  }

	*p++ = 0;

	yylval.ttype = build_string (p - token_buffer, token_buffer);
	TREE_TYPE (yylval.ttype) = char_array_type_node;

	value = STRING; break;
      }
      
    case '+':
    case '-':
    case '&':
    case '|':
    case '<':
    case '>':
    case '*':
    case '/':
    case '%':
    case '^':
    case '!':
    case '=':
      {
	register int c1;

      combine:

	switch (c)
	  {
	  case '+':
	    yylval.code = PLUS_EXPR; break;
	  case '-':
	    yylval.code = MINUS_EXPR; break;
	  case '&':
	    yylval.code = BIT_AND_EXPR; break;
	  case '|':
	    yylval.code = BIT_IOR_EXPR; break;
	  case '*':
	    yylval.code = MULT_EXPR; break;
	  case '/':
	    yylval.code = TRUNC_DIV_EXPR; break;
	  case '%':
	    yylval.code = TRUNC_MOD_EXPR; break;
	  case '^':
	    yylval.code = BIT_XOR_EXPR; break;
	  case LSHIFT:
	    yylval.code = LSHIFT_EXPR; break;
	  case RSHIFT:
	    yylval.code = RSHIFT_EXPR; break;
	  case '<':
	    yylval.code = LT_EXPR; break;
	  case '>':
	    yylval.code = GT_EXPR; break;
	  }	

	c1 = getc(finput);

	if (c1 == '=')
	  {
	    switch (c)
	      {
	      case '<':
		value = ARITHCOMPARE; yylval.code = LE_EXPR; goto done;
	      case '>':
		value = ARITHCOMPARE; yylval.code = GE_EXPR; goto done;
	      case '!':
		value = EQCOMPARE; yylval.code = NE_EXPR; goto done;
	      case '=':
		value = EQCOMPARE; yylval.code = EQ_EXPR; goto done;
	      }	
	    value = ASSIGN; goto done;
	  }
	else if (c == c1)
	  switch (c)
	    {
	    case '+':
	      value = PLUSPLUS; goto done;
	    case '-':
	      value = MINUSMINUS; goto done;
	    case '&':
	      value = ANDAND; goto done;
	    case '|':
	      value = OROR; goto done;
	    case '<':
	      c = LSHIFT;
	      goto combine;
	    case '>':
	      c = RSHIFT;
	      goto combine;
	    }
	else if ((c == '-') && (c1 == '>'))
	  { value = POINTSAT; goto done; }
	ungetc (c1, finput);

	if ((c == '<') || (c == '>'))
	  value = ARITHCOMPARE;
	else value = c;
	goto done;
      }

    default:
      value = c;
    }

done:
  yylloc.last_line = lineno;

  return (value);
}
